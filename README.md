# glass-client

[![Build Status](https://img.shields.io/circleci/build/bb/metalgear121/glass-client?token=79b5c824ed5923a5d1ab2d63b473a9ca12552e00)](https://circleci.com/bb/metalgear121/glass-client)
[![Language](https://img.shields.io/badge/language-TypeScript-red)](https://www.typescriptlang.org)
[![Angular Version](https://img.shields.io/badge/Angular-v10.0.4-blue)](https://angular.io)
[![Ionic Version](https://img.shields.io/badge/Ionic-v5.2.3-blue)](https://ionicframework.com/)