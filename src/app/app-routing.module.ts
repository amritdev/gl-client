import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

import { AuthGuard, CastGuard, GuestGuard, RegisterGuard } from 'src/app/guards';
import { GuestComponent } from 'src/app/pages/regist/guest/guest.component';
import { CastComponent } from 'src/app/pages/regist/cast/cast.component';
import { LineComponent } from './pages/deeplink/line/line.component';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./pages/auth/auth.module').then(m => m.AuthModule),
    canActivate: [AuthGuard],
  },
  {
    path: 'main',
    loadChildren: () => import('./pages/main/main.module').then(m => m.MainModule),
    canLoad: [AuthGuard],
  },
  {
    path: 'guest_regist_refresh',
    component: GuestComponent,
    canActivate: [RegisterGuard, GuestGuard],
  },
  {
    path: 'cast_regist_refresh',
    component: CastComponent,
    canActivate: [RegisterGuard, CastGuard],
  },
  {
    path: 'line/tutorial',
    component: LineComponent
  },
  {
    path: 'line/mypage/link',
    component: LineComponent,
  },
  {
    path: 'line/home',
    component: LineComponent,
  },
  {
    path: '**',
    redirectTo: 'main',
  },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules, relativeLinkResolution: 'legacy' })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
