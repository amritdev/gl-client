import { Component, NgZone } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';
import { NavController, Platform } from '@ionic/angular';
import { GoogleTagManagerService } from 'angular-google-tag-manager';
import {
  Plugins,
  PushNotification,
  PushNotificationToken,
  PushNotificationActionPerformed,
  StatusBarStyle
} from '@capacitor/core';
import { AuthService } from './services';

const { App, PushNotifications, SplashScreen, StatusBar } = Plugins;

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  curUrl = '';

  constructor(
    private zone: NgZone,
    private platform: Platform,
    private router: Router,
    private gtmService: GoogleTagManagerService,
    private navController: NavController,
    private auth: AuthService
  ) {
    this.initializeApp();

    this.router.events.subscribe( e => {
      const routerEvent = e as RouterEvent;
      if (routerEvent.url && routerEvent.url !== this.curUrl){
        this.curUrl = routerEvent.url;
        // console.log(this.curUrl);
        this.gtmService.pushTag({
          event: 'page',
          pageName: routerEvent.url
        });
      }
    });
  }

  initializeApp(): void {
    this.platform.ready().then(() => {
      if (this.platform.is('hybrid')) {
        StatusBar.setStyle({
          style: StatusBarStyle.Light
        });
        StatusBar.setBackgroundColor({
          color: '#FFFFFF'
        });
        SplashScreen.hide();
        App.addListener('appUrlOpen', (data) => {
          this.zone.run(() => {
              const slug = data.url.split('glass.dating/s').pop();
              if (slug?.includes('/main')) {
                  this.navController.navigateForward(slug, {
                    queryParams: {
                      deeplink: Math.random() * 1000000,
                      telecom: Math.random() * 1000000
                    }
                  });
              } else if (slug?.includes('/line')) {
                let link = '';
                let deeplink = '';
                if (slug?.includes('/line/tutorial')) {
                  link = '/main/tutorial',
                  deeplink = slug.split('/line/tutorial/').pop() ?? '';
                } else if (slug?.includes('/line/mypage/link')) {
                  link = '/main/mypage/link',
                  deeplink = slug.split('/line/mypage/link/').pop() ?? '';
                } else if (slug?.includes('/line/home')) {
                  link = '/',
                  deeplink = slug.split('/line/home/').pop() ?? '';
                }
                this.navController.navigateForward(link, {
                  queryParams: {
                    deeplink: Math.random() * 1000000,
                    line: deeplink
                  }
                });
              }
          });
        });

        // Push Notification
        // Request permission to use push notifications
        // iOS will prompt user and return if they granted permission or not
        // Android will just grant without prompting
        PushNotifications.requestPermission().then( result => {
          if (result.granted) {
            // Register with Apple / Google to receive push via APNS/FCM
            PushNotifications.register();
          } else {
            // Show some error
            console.warn('Permission is not granted: Push Notification');
          }
        });

        // On success, we should be able to receive notifications
        PushNotifications.addListener('registration',
          (token: PushNotificationToken) => {
            this.auth.firebasePushToken.next(token.value);
          }
        );

        // Some issue with our setup and push will not work
        PushNotifications.addListener('registrationError',
          error => {
            console.log('Error on registration: ' + JSON.stringify(error));
          }
        );

        // Show us the notification payload if the app is open on our device
        PushNotifications.addListener('pushNotificationReceived',
          (notification: PushNotification) => {
            console.log('Push received: ' + JSON.stringify(notification));
          }
        );

        // Method called when tapping on a notification
        PushNotifications.addListener('pushNotificationActionPerformed',
          (notification: PushNotificationActionPerformed) => {
            console.log('Push action performed: ' + JSON.stringify(notification));
            const targetLink = notification.notification.data.link;
            if (typeof(targetLink) === 'string' && targetLink !== ''){
              this.router.navigate([targetLink]);
            }
          }
        );
      }

    });
  }

}
