import { NgModule } from '@angular/core';
import { BrowserModule, Title } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GuestComponent } from './pages/regist/guest/guest.component';
import { CastComponent } from './pages/regist/cast/cast.component';
import { environment } from 'src/environments/environment';
import { LineComponent } from './pages/deeplink/line/line.component';

@NgModule({
  declarations: [
    AppComponent,
    GuestComponent,
    CastComponent,
    LineComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    IonicModule.forRoot(),
    HttpClientModule,
    AppRoutingModule,
  ],
  providers: [
    Title,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: 'googleTagManagerId', useValue: environment.GTM_ID }
  ],
  bootstrap: [
    AppComponent,
  ],
})
export class AppModule { }
