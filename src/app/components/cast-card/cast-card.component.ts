import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { InViewportMetadata } from 'ng-in-viewport';

@Component({
  selector: 'app-cast-card',
  templateUrl: './cast-card.component.html',
  styleUrls: ['./cast-card.component.scss'],
})
export class CastCardComponent implements OnInit {

  @Input() nickname: string | null = '';
  @Input() birthday: string | null = '';
  @Input() birthdayHidden = false;
  @Input() point = 0;
  @Input() status = true;
  @Input() leftAt = '';
  @Input() word: string | null = null;
  @Input() isMatching = false;
  @Input() className: string | undefined;
  @Input() classColor: string | undefined;
  @Input() castStart: string | null = null;
  @Input() image: string | null = null;
  @Input() locationName = '';
  @Input() isDesireCard = false;
  @Input() isFavoriteCard = false;
  @Input() isFavorite = false;
  @Input() video = '';
  @Input() profileID = '';
  @Input() todayFree = false;

  @Output()
  cardClick = new EventEmitter();

  @Output()
  changeState = new EventEmitter();

  @Output()
  changeFavorite = new EventEmitter();

  constructor() {
  }

  ngOnInit(): void { }

  getStatusColor(): string {
    if (this.status) {
      return '#1be31b';
    } else {
      try {
        const timeDiff = Date.now() - new Date(this.leftAt).getTime();
        const days = Math.floor(timeDiff / 86400 / 1000);
        if (days < 3) {
          return '#f3b21d';
        } else if (days < 15) {
          return '#cdcdcd';
        } else {
          return '';
        }
      } catch {
        return '';
      }
    }
  }

  isNew(): boolean {
    if (this.castStart) {
      const startDate: string = this.castStart;
      const dateDiff = Math.ceil((Date.now() - new Date(startDate).getTime()) / 1000 / 3600 / 24);
      return dateDiff <= 7;
    } else {
      return false;
    }
  }

  isBirthdayThisMonth(): boolean {
    const thisMonth = new Date().getMonth();
    if (this.birthday === null || this.birthday === '') {
      return false;
    } else {
      return thisMonth === new Date(this.birthday).getMonth();
    }
  }

  matchCast(): void {
    this.changeState.emit();
  }

  viewCast(): void {
    this.cardClick.emit();
  }

  favoriteCast(): void {
    this.changeFavorite.emit();
  }

  getStatusClass(): string {
    if (this.status) {
      return 'green';
    } else {
      const curTime = Date.now();
      const leftAt = new Date(this.leftAt).getTime();
      const seconds = (curTime - leftAt) / 1000;
      if (seconds < 3600) {
        return 'green';
      } else if (seconds < 3600 * 24) {
        return 'yellow';
      } else if (seconds < 3600 * 24 * 3){
        return 'gray';
      } else {
        return '';
      }
    }
  }

  viewStatusChanged(event: {
    [InViewportMetadata]: {entry: IntersectionObserverEntry},
    target: HTMLElement,
    visible: boolean;
  }): void {
    const {[InViewportMetadata]: { entry }, target, visible} = event;
    const videoElement: HTMLVideoElement = document.getElementById(`video-normal-${this.profileID}`) as HTMLVideoElement;
    if (visible){
      if (videoElement){
        if (videoElement.paused){
          setTimeout(() => {
            videoElement.play();
          }, 500);
        }
      }
    }else{
      if (videoElement){
        if (!videoElement.paused){
          videoElement.pause();
        }
      }
    }
  }
}
