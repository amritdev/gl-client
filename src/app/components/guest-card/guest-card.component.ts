import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-guest-card',
  templateUrl: './guest-card.component.html',
  styleUrls: ['./guest-card.component.scss'],
})
export class GuestCardComponent implements OnInit {

  @Input() nickname: string | null = '';
  @Input() birthday: string | null = '';
  @Input() birthdayHidden = false;
  @Input() status = true;
  @Input() leftAt = '';
  @Input() word: string | null = null;
  @Input() levelNum = 1;
  @Input() levelName = '';
  @Input() locationName = '';
  @Input() guestStart: string | null = null;
  @Input() image: string | null = null;
  @Input() isFavorite = false;

  @Output()
  changeFavorite = new EventEmitter();

  @Output()
  cardClick = new EventEmitter();

  constructor() { }

  ngOnInit(): void { }

  getStatusColor(): string {
    if (this.status) {
      return '#1be31b';
    } else {
      try {
        const timeDiff = Date.now() - new Date(this.leftAt).getTime();
        const days = Math.floor(timeDiff / 86400 / 1000);
        if (days < 3) {
          return '#f3b21d';
        } else if (days < 15) {
          return '#cdcdcd';
        } else {
          return '';
        }
      } catch {
        return '';
      }
    }
  }

  isNew(): boolean {
    if (this.guestStart) {
      const startDate: string = this.guestStart;
      const dateDiff = Math.ceil((Date.now() - new Date(startDate).getTime()) / 1000 / 3600 / 24);
      return dateDiff <= 7;
    } else {
      return false;
    }
  }

  repeatArray(n: number): number[] {
    return Array(n);
  }

  getStatusClass(): string {
    if (this.status) {
      return 'green';
    } else {
      const curTime = Date.now();
      const leftAt = new Date(this.leftAt).getTime();
      const seconds = (curTime - leftAt) / 1000;
      if (seconds < 3600) {
        return 'green';
      } else if (seconds < 3600 * 24) {
        return 'yellow';
      } else if (seconds < 3600 * 24 * 3){
        return 'gray';
      } else {
        return '';
      }
    }
  }

  favoriteGuest(): void {
    this.changeFavorite.emit();
  }

  viewGuest(): void {
    this.cardClick.emit();
  }
}
