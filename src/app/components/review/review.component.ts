import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { User, Room, Review, Call } from 'src/app/interfaces';
import { AuthService } from 'src/app/services';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ModalController, AlertController } from '@ionic/angular';
import t from 'src/locales';
import * as moment from 'moment-timezone';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.scss'],
})
export class ReviewComponent implements OnInit, OnDestroy {

  @Input() roomID = 0;
  @Input() call: Call | null = null;
  @Input() role = 0;
  @Input() must = false;

  users: User[] = [];
  source !: User;
  familyIDs: number[] = [];
  reviews: Review[] = [];
  daysOfWeek = ['日', '月', '火', '水', '木', '金', '土'];
  badChoices: string[] = [
    '長く待たされた', '退屈だった', '不適切な言動', 'タイマーに誤りがある', '延長の確認がなかった', 'その他'
  ];
  userSubscription: Subscription | null = null;

  constructor(
    private auth: AuthService,
    private http: HttpClient,
    private alert: AlertController,
    private modal: ModalController
  ) {
    if (this.userSubscription === null) {
      this.userSubscription = this.auth.currentUserSubject.subscribe((user) => {
        if (user) {
          this.source = user;
          const idArray = this.source.FamilyIDs.split(',');
          idArray.pop();
          idArray.shift();
          this.familyIDs = idArray.map((item) => parseInt(item, 10));
          // this.familyIDs = this.guest.FamilyIDs;
        }
      });
    }
  }

  ngOnInit(): void {
    if (this.roomID > 0) {
      if (this.role === 1){
        this.getCasts();
      }else{
        if (this.call && this.call.User) {
          this.users = [this.call.User];
          this.setReviews();
        }
      }
    }
  }

  ngOnDestroy(): void {
    this.userSubscription?.unsubscribe();
  }

  getCasts(): void {
    this.http.get<Room>(`${environment.API_URL}/users/room/${this.roomID}`).subscribe(room => {
      // console.log(room);
      this.users = room.Users.filter((userItem) => {
        return userItem.ID !== this.source.ID && userItem.Role === 0;
      });

      // get reviews
      if (this.call){
        this.setReviews();
      }

    });
  }

  setReviews(): void {
    this.http.get<Review[]>(`${environment.API_URL}/calls/reviews/${this.call?.ID}`).subscribe(res => {
      for (const cast of this.users) {
        const reviewIndex = res.findIndex(item => item.UserID === cast.ID);
        if (reviewIndex > -1) {
          this.reviews.push(res[reviewIndex]);
        } else {
          this.reviews.push({
            ID: 0,
            IsGood: null,
            TypeStr: null,
            UserID: cast.ID,
            SourceID: this.source.ID,
            Source: this.source,
            CallID: this.call?.ID,
            HowGuest: '',
            HowUp: '',
            Comment: ''
          });
        }
      }
    });
  }

  onClose(): void {
    this.modal.dismiss();
  }

  onSave(): void {
    // this.source.LastRoomID = 0;
    // this.auth.currentUserSubject.next(this.source);

    if (this.role === 0) {
      for (const reviewItem of this.reviews) {
        if (reviewItem.IsGood === null){
          this.showAlert(t('Please give the guest review'));
          return;
        }else{
          if (reviewItem.HowGuest.trim() === '' || reviewItem.HowUp.trim() === '') {
            this.showAlert('内容をすべて入力してください');
            return;
          }
        }
      }
    }

    // save reviews
    let reviewCount = 0;
    for (const reviewItem of this.reviews) {
      if (reviewItem.IsGood !== null) {
        reviewItem.Comment = reviewItem.Comment.trim();
        reviewItem.HowGuest = reviewItem.HowGuest.trim();
        reviewItem.HowUp = reviewItem.HowUp.trim();

        if (reviewItem.ID === 0){
          this.http.post<{
            'success': boolean
          }>(`${environment.API_URL}/reviews`, reviewItem).subscribe((res) => {
            if (res.success) {
              reviewCount++;
              if (reviewCount === this.reviews.length) {
                // successfully saved
                if (this.source) {
                  if (this.source.Role === 0 && this.source.LastRoomID > 0 && this.source.LastRoomID === this.call?.ID){
                    this.http.post<{
                      'success': boolean
                    }>(`${environment.API_URL}/users/lastroom`, {
                      last_room: 0
                    }).subscribe(result => {
                      if (result) {
                        this.source.LastRoomID = 0;
                        this.auth.currentUserSubject.next(this.source);
                      }
                    });
                  }
                }
              }
            }
          });
        }else{
          this.http.put<{
            'success': boolean
          }>(`${environment.API_URL}/reviews`, reviewItem).subscribe((res) => {
            if (res.success) {
              reviewCount++;
              if (reviewCount === this.reviews.length) {
                // successfully saved
                this.source.LastRoomID = 0;
                this.auth.currentUserSubject.next(this.source);
              }
            }
          });
        }
      }
    }

    this.modal.dismiss();
  }

  getTime(): string {
    if (this.call) {
      const meetTime = moment(`${this.call.MeetTimeISO} +0900`, 'YYYY-MM-DD HH:mm:ss Z');
      return meetTime.format('MM月DD日') +
        `(${this.daysOfWeek[meetTime.day()]}) ${meetTime.format('HH:mm')}~`;
    } else {
      return '';
    }
  }

  onMinus(index: number): void {
    this.reviews[index].IsGood = false;
    this.reviews[index].Comment = '';
    this.reviews[index].TypeStr = '長く待たされた';
  }

  onPlus(index: number): void {
    this.reviews[index].IsGood = true;
    this.reviews[index].Comment = '';
    this.reviews[index].TypeStr = '';
  }

  isActive(review: Review, result: boolean): boolean {
    if (review.IsGood !== null) {
      return review.IsGood === result;
    } else {
      return false;
    }
  }

  async addToFamily(castID: number): Promise<void> {
    const confirmation = await this.warn(t('Are you sure to add to family?'));
    if (confirmation) {
      this.familyIDs.push(castID);
      // console.log(`,${this.familyIDs.join(',')},`);
      // update guest family cast
      this.http.post<{
        'success': boolean
      }>(`${environment.API_URL}/users/family`, {
        familyIDs: `,${this.familyIDs.join(',')},`
      }).subscribe(res => {
        if (res.success) {
          // successfully saved
          this.source.FamilyIDs = `,${this.familyIDs.join(',')},`;
          this.auth.currentUserSubject.next(this.source);
          this.showAlert(t('Successfully added to family casts'));
        } else {
          // console.log('error updating family ids');
        }
      });
    }
  }

  async showAlert(msgStr: string): Promise<void> {
    const alert = await this.alert.create({
      message: msgStr,
      buttons: ['OK']
    });
    await alert.present();
  }

  isNotFamilyCast(castID: number): boolean {
    return !this.familyIDs.includes(castID);
  }

  async warn(message: string, title: string = ''): Promise<boolean> {
    return new Promise(async (resolve) => {
      const confirm = await this.alert.create({
        header: title === '' ? t('Alert Notify') : t(title),
        message: t(message),
        buttons: [
          {
            text: t('Cancel'),
            role: 'cancel',
            handler: () => {
              return resolve(false);
            },
          },
          {
            text: t('OK'),
            handler: () => {
              return resolve(true);
            },
          },
        ],
      });

      await confirm.present();
    });
  }

  selectChoice(choice: string, index: number): void {
    this.reviews[index].TypeStr = choice;
  }

}
