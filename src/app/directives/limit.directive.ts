import { Directive, Input, HostListener } from '@angular/core';

@Directive({
  selector: '[appLimit]'
})
export class LimitDirective {

  @Input('appLimit') limit = 20;

  constructor() { }

  @HostListener('keypress', ['$event'])
  onkeypress(event: KeyboardEvent): void {
    if (event && event.target instanceof HTMLInputElement) {
      if (event.target.value.length > this.limit - 1) {
        event.preventDefault();
      }
    }
  }

}
