import { TestBed } from '@angular/core/testing';

import { AntiLikeGuard } from './anti-like.guard';

describe('AntiLikeGuard', () => {
  let guard: AntiLikeGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(AntiLikeGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
