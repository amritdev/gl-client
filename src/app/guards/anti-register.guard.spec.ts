import { TestBed } from '@angular/core/testing';

import { AntiRegisterGuard } from './anti-register.guard';

describe('AntiRegisterGuard', () => {
  let guard: AntiRegisterGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(AntiRegisterGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
