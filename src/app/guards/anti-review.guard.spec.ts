import { TestBed } from '@angular/core/testing';

import { AntiReviewGuard } from './anti-review.guard';

describe('AntiReviewGuard', () => {
  let guard: AntiReviewGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(AntiReviewGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
