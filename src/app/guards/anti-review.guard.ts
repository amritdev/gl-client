import { Injectable } from '@angular/core';
import { CanActivate, UrlTree } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/services';


@Injectable({
  providedIn: 'root'
})
export class AntiReviewGuard implements CanActivate {
  role = 0;
  lastCallID = 0;

  constructor(
    private nav: NavController,
    private auth: AuthService
  ) {
    this.auth.currentUserSubject.subscribe(user => {
      this.role = user?.Role ?? 1;
      this.lastCallID = user?.LastRoomID ?? 0;
    });
  }

  canActivate(): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.role === 0 || this.role === 10) {
      if (this.role === 0) {
        if (this.lastCallID > 0){
          return true;
        }
      }
      this.nav.navigateRoot('/main/apply');
      return false;
    } else if (this.role === 2) {
      this.nav.navigateRoot('/main/mypage');
      return false;
    } else {
      this.nav.navigateRoot('/main/call');
      return false;
    }
  }

}
