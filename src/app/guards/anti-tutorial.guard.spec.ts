import { TestBed } from '@angular/core/testing';

import { AntiTutorialGuard } from './anti-tutorial.guard';

describe('AntiTutorialGuard', () => {
  let guard: AntiTutorialGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(AntiTutorialGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
