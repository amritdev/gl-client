import { Injectable } from '@angular/core';
import { CanActivate, UrlTree } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import * as moment from 'moment-timezone';
import { Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AntiTutorialGuard implements CanActivate {

  isProfileAdded = false;
  isLineAdded = false;
  isCastLiked = false;
  isAboutSet = false;
  isWordTodaySet = false;
  nickname = '';
  role = 1;
  likedCasts = 0;
  periodNotAllowed = false;
  isCreditAdded = false;
  userSubscription: Subscription | null = null;

  constructor(
    private nav: NavController,
    private auth: AuthService
  ) {
    if (this.userSubscription === null) {
      this.userSubscription = this.auth.currentUserSubject.subscribe(user => {
        if (user){
          this.isProfileAdded = user.IsProfileAdded;
          this.isLineAdded = user.IsLineAdded;
          this.isCastLiked = user.IsCastLiked;
          this.isCreditAdded = user.IsCreditAdded;
          this.isAboutSet = user.About !== null && user.About !== '';
          this.isWordTodaySet = user.Word !== null && user.Word !== '';
          this.nickname = user.Nickname ?? '';
          this.role = user.Role;
          const favoriteCastsStr = JSON.parse(user.Favorites ?? '[]');
          if (user.TutorialViewedAt){
            const lastTutorialSeen = new Date(user.TutorialViewedAt);
            const timeNow = moment();
            const hours = moment.duration(timeNow.diff(lastTutorialSeen));
            // console.log(hours.asHours());
            this.periodNotAllowed = hours.asHours() < 24;
          }
          this.likedCasts = favoriteCastsStr.length;
          // console.log(this.likedCasts);
        }
      });
    }

  }

  canActivate(): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.isProfileAdded && this.isLineAdded && this.nickname !== ''){
      // console.log(this.role);
      if (this.role === 1){
        if (this.likedCasts >= 5 && this.isCreditAdded){
          this.nav.navigateRoot('/main');
          return false;
        }
      }else{
        if (this.isAboutSet && this.isWordTodaySet){
          this.nav.navigateRoot('/main');
          return false;
        }
      }
    }
    if (this.periodNotAllowed){
      this.nav.navigateRoot('/main');
      return false;
    }
    return true;
  }

}
