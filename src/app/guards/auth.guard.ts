import { Injectable } from '@angular/core';
import { CanLoad, UrlTree } from '@angular/router';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { NavController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { AuthService, WebsocketService } from 'src/app/services';
import { User } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanLoad {

  constructor(
    private authService: AuthService,
    private wsService: WebsocketService,
    private http: HttpClient,
    private navController: NavController,
  ) { }

  canLoad(): Observable<boolean> | Promise<boolean> | boolean {
    const token = localStorage.getItem('token');
    if (token) {
      return this.http.get<User>(`${environment.API_URL}/users/info`, {
        headers: new HttpHeaders({
          Authorization: token,
        })
      }).pipe(map(user => {
        if (user.IsBlocked || (user.Provider === 'email' && !user.IsVerified)) {
          localStorage.removeItem('token');
          if (user.Role === 0 || user.Role === 10) {
            this.navController.navigateRoot('/cast');
          } else if (user.Role === 2) {
            this.navController.navigateRoot('/agent');
          } else {
            this.navController.navigateRoot('/');
          }
          return false;
        } else {
          this.authService.currentUserSubject.next(user);
          this.wsService.init(user.ID);
          return true;
        }
      })).pipe(catchError(async (err: HttpErrorResponse) => {
        if (err.status === 401 || err.status === 403){
          localStorage.removeItem('token');
          this.authService.saveTokenStatus(token, 'guard', err.status, 'logout');
          this.navController.navigateRoot('/');
        }
        return false;
      }));
    } else {
      this.navController.navigateRoot('/');
      return false;
    }
  }

  canActivate(): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const token = localStorage.getItem('token');
    if (token) {
      this.navController.navigateRoot('/main');
      return false;
    }
    return true;
  }
}
