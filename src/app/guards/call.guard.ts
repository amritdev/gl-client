import { Injectable } from '@angular/core';
import { CanActivate, UrlTree, Router } from '@angular/router';

import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CallGuard implements CanActivate {

  constructor(
    private router: Router,
  ) { }

  canActivate(): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const nav = this.router.getCurrentNavigation();

    if (nav && nav.id < 2) {
      this.router.navigate(['/main/call']);
      return false;
    } else {
      return true;
    }
  }

}
