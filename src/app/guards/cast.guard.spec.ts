import { TestBed } from '@angular/core/testing';

import { CastGuard } from './cast.guard';

describe('CastGuard', () => {
  let guard: CastGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(CastGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
