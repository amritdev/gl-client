import { Injectable } from '@angular/core';
import { CanActivate, UrlTree } from '@angular/router';

import { NavController } from '@ionic/angular';

import { Observable } from 'rxjs';

import { AuthService } from 'src/app/services';

@Injectable({
  providedIn: 'root'
})
export class GuestGuard implements CanActivate {

  role = 1;
  sight = '';

  constructor(
    private nav: NavController,
    private auth: AuthService
  ) {
    this.auth.currentUserSubject.subscribe(user => {
      this.role = user?.Role ?? 1;
      this.sight = user?.Sight ?? '';
    });
  }

  canActivate(): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.role === 1) {
      if (this.sight !== 'blind'){
        return true;
      }else{
        this.nav.navigateRoot('/main/tweet');
        return false;
      }
    } else if (this.role === 2) {
      this.nav.navigateRoot('/main/mypage');
      return false;
    } else {
      this.nav.navigateRoot('/main/apply');
      return false;
    }
  }

}
