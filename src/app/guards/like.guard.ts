import { Injectable } from '@angular/core';
import { CanActivate, UrlTree } from '@angular/router';
import { NavController } from '@ionic/angular';
import * as moment from 'moment';
import { Observable, Subscription } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class LikeGuard implements CanActivate {

  likedGuests = 0;
  periodNotAllowed = false;
  userSubscription: Subscription | null = null;
  role = 1;

  constructor(
    private nav: NavController,
    private auth: AuthService
  ) {
    if (this.userSubscription === null) {
      this.userSubscription = this.auth.currentUserSubject.subscribe(user => {
        if (user){
          const favoriteGuestStr = JSON.parse(user.Favorites ?? '[]');
          this.role = user.Role;
          if (user.LikeViewedAt){
            const lastLikeSeen = new Date(user.LikeViewedAt);
            const timeNow = moment();
            const hours = moment.duration(timeNow.diff(lastLikeSeen));
            this.periodNotAllowed = hours.asHours() < 24;
          }
          this.likedGuests = favoriteGuestStr.length;
        }
      });
    }

  }

  canActivate(): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.role !== 0){
      return true;
    }else{
      if (this.likedGuests >= 20) {
        return true;
      }
      if (this.periodNotAllowed){
        return true;
      }
      this.nav.navigateForward('/main/like');
      return false;
    }
  }

}
