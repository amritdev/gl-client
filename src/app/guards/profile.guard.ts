import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CanActivate, UrlTree, ActivatedRouteSnapshot } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import t from 'src/locales';

@Injectable({
  providedIn: 'root'
})
export class ProfileGuard implements CanActivate {

  targetId = '';

  constructor(
    private http: HttpClient,
    private alert: AlertController
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    this.targetId = route.paramMap.get('id') ?? '';
    if (this.targetId === ''){
      return false;
    }else{
      return this.checkProfile.pipe(map(res => {
        if (!res){
          this.showAlert(t('Profile can not be displayed'));
        }
        return res;
      }));
    }
  }

  get checkProfile(): Observable<boolean> {
    const token = localStorage.getItem('token');
    return this.http.get<boolean>(`${environment.API_URL}/users/check_profile/${this.targetId}`, {
      headers: new HttpHeaders({
        Authorization: token ?? '',
      })
    });
  }

  async showAlert(msgStr: string): Promise<void> {
    const alert = await this.alert.create({
      message: msgStr,
      buttons: ['OK']
    });
    await alert.present();
  }

}
