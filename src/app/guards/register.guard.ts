import { Injectable } from '@angular/core';
import { CanActivate, UrlTree } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/services';
import { Image } from 'src/app/interfaces';

@Injectable({
  providedIn: 'root'
})
export class RegisterGuard implements CanActivate {

  locationIDs: string | null = null;
  nickname: string | null = '';
  birthday: string | null = null;
  images: Image[] | null = null;
  role = 0;
  realName: string | null = null;
  realNameFrigana: string | null = null;
  coporateName: string | null = null;
  coporateNameFrigana: string | null = null;

  constructor(
    private authService: AuthService,
    private navController: NavController,
  ) {
    this.authService.currentUserSubject.subscribe(user => {
      this.nickname = user?.Nickname ?? null;
      this.locationIDs = user?.LocationIDs ?? null;
      this.birthday = user?.Birthday ?? null;
      this.images = user?.Images ?? null;
      this.role = user?.Role ?? 0;
      this.realName = user?.RealName ?? null;
      this.realNameFrigana = user?.RealNameFrigana ?? null;
      this.coporateName = user?.CoporateName ?? null;
      this.coporateNameFrigana = user?.CoporateNameFrigana ?? null;
    });
  }

  canActivate(): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.role === 0 || this.role === 1 || this.role === 10) {
      if (this.nickname && this.locationIDs && this.birthday && this.images && this.images.length > 0) {
        return true;
      } else {
        this.navController.navigateRoot('/main/register');
        return false;
      }
    } else if (this.role === 2) {
      if (this.realName && this.realNameFrigana) {
        return true;
      } else {
        this.navController.navigateRoot('/main/register');
        return false;
      }
    } else {
      this.authService.logout();
      this.navController.navigateRoot('/');
      return false;
    }
  }
}
