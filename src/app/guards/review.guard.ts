import { Injectable } from '@angular/core';
import { CanActivate, UrlTree } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/services';

@Injectable({
  providedIn: 'root'
})
export class ReviewGuard implements CanActivate {
  role = 0;
  lastCallID = 0;

  constructor(
    private nav: NavController,
    private auth: AuthService
  ) {
    this.auth.currentUserSubject.subscribe(user => {
      this.role = user?.Role ?? 1;
      this.lastCallID = user?.LastRoomID ?? 0;
    });
  }

  canActivate(): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.role === 0) {
      if (this.lastCallID > 0){
        this.nav.navigateRoot('/main/review');
        return false;
      }
    }
    return true;
  }

}
