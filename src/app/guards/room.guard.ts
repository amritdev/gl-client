import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CanActivate, UrlTree, ActivatedRouteSnapshot } from '@angular/router';
import { AlertController, NavController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import t from 'src/locales';

@Injectable({
  providedIn: 'root'
})
export class RoomGuard implements CanActivate {
  roomID = '';

  constructor(
    private http: HttpClient,
    private alert: AlertController,
    private navi: NavController
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    this.roomID = route.paramMap.get('id') ?? '';
    if (this.roomID === ''){
      return false;
    }else{
      return this.checkRoomMember.pipe(map(res => {
        if (!res){
          this.showAlert(t('You can not get into this room because you are not a room member'));
          this.navi.navigateRoot('/main/chat');
        }
        return res;
      }));
    }
  }

  get checkRoomMember(): Observable<boolean> {
    const token = localStorage.getItem('token');
    return this.http.get<boolean>(`${environment.API_URL}/users/check_room/${this.roomID}`, {
      headers: new HttpHeaders({
        Authorization: token ?? '',
      })
    });
  }

  async showAlert(msgStr: string): Promise<void> {
    const alert = await this.alert.create({
      message: msgStr,
      buttons: ['OK']
    });
    await alert.present();
  }
}
