import { TestBed } from '@angular/core/testing';

import { SightGuard } from './sight.guard';

describe('SightGuard', () => {
  let guard: SightGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(SightGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
