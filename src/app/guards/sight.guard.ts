import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { AuthService } from '../services';

@Injectable({
  providedIn: 'root'
})
export class SightGuard implements CanActivate {

  sight = '';

  constructor(
    private nav: NavController,
    private auth: AuthService
  ){
    this.auth.currentUserSubject.subscribe(user => {
      if (user){
        this.sight = user.Sight;
      }
    });
  }

  canActivate(): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.sight === 'blind'){
      this.nav.navigateRoot('/main/tweet');
      return false;
    }else{
      return true;
    }
  }
}
