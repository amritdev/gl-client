import { TestBed } from '@angular/core/testing';

import { TelecomGuard } from './telecom.guard';

describe('TelecomGuard', () => {
  let guard: TelecomGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(TelecomGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
