import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class TelecomGuard implements CanActivate {
  teleFailed = true;

  constructor(
    private auth: AuthService,
    private navi: NavController
  ) {
    this.auth.currentUserSubject.subscribe((user) => {
      if (user) {
        this.teleFailed = user.TelecomFailed;
      }
    });
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this.teleFailed) {
      this.navi.navigateRoot('/main/chat');
      return false;
    } else {
      return true;
    }
  }

}
