import { Injectable } from '@angular/core';
import { CanActivate, UrlTree } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import * as moment from 'moment-timezone';
import { Subscription } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class TutorialGuard implements CanActivate {
  isProfileAdded = false;
  isLineAdded = false;
  isCastLiked = false;
  nickname = '';
  role = 1;
  likedCasts = 0;
  periodNotAllowed = false;
  isCreditAdded = false;
  isAboutSet = false;
  isWordTodaySet = false;
  userSubscription: Subscription | null = null;

  constructor(
    private nav: NavController,
    private auth: AuthService
  ) {
    if (this.userSubscription === null) {
      this.userSubscription = this.auth.currentUserSubject.subscribe(user => {
        if (user){
          this.isProfileAdded = user.IsProfileAdded;
          this.isLineAdded = user.IsLineAdded;
          this.isCastLiked = user.IsCastLiked;
          this.isCreditAdded = user.IsCreditAdded;
          this.isAboutSet = user.About !== null && user.About !== '';
          this.isWordTodaySet = user.Word !== null && user.Word !== '';
          this.nickname = user.Nickname ?? '';
          this.role = user.Role;
          const favoriteCastsStr = JSON.parse(user.Favorites ?? '[]');
          if (user.TutorialViewedAt){
            const lastTutorialSeen = new Date(user.TutorialViewedAt);
            const timeNow = moment();
            const hours = moment.duration(timeNow.diff(lastTutorialSeen));
            // console.log(hours.asHours());
            this.periodNotAllowed = hours.asHours() < 24;
          }
          this.likedCasts = favoriteCastsStr.length;
          // console.log(this.likedCasts);
        }
      });
    }

  }

  canActivate(): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.role === 2){
      return true;
    }else{
      if (this.isProfileAdded && this.isLineAdded && this.nickname !== ''){
        // console.log(this.role);
        if (this.role === 1){
          if (this.isCastLiked && this.isCreditAdded){
            // console.log("IMPASSED");
            return true;
          }
        }else{
          if (this.isAboutSet && this.isWordTodaySet){
            return true;
          }
        }
      }
      if (this.periodNotAllowed){
        return true;
      }
      // console.log('tutorial page');
      this.nav.navigateRoot('/main/tutorial');
      return false;
    }
  }
}
