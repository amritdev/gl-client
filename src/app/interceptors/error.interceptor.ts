import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpClient
} from '@angular/common/http';

import { NavController } from '@ionic/angular';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthService } from 'src/app/services';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

  constructor(
    private navController: NavController,
    private authService: AuthService
  ) { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(catchError(err => {
      if (err.status === 401 || err.status === 403) {
        const token = localStorage.getItem('token');
        if (token && token !== ''){
          this.authService.saveTokenStatus(token, 'api', err.status, 'logout');
        }else{
          if (this.authService.getToken() !== ''){
            this.authService.saveTokenStatus(this.authService.getToken(), 'token_gone', err.status, 'logout');
          }
        }
        this.authService.logout();
        this.navController.navigateRoot('/');
      }
      return throwError(err);
    }));
  }
}
