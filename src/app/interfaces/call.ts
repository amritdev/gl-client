import { User } from './user';
import { Class } from './class';
import { Location } from './location';
import { Room } from './room';
import { Invoice } from './invoice';

interface ClassItem {
  class: Class;
  number: number;
}
export interface Call{
  Joined: User[];
  Applied: User[];
  ID: number;
  User: User | null;
  UserID: number;
  Status: string;
  Place: string | null;
  Reservation: string | null;
  // JoinStatus: string | null;
  BigLocationID: number;
  BigLocation: Location | null;
  MeetTime: string;
  MeetTimeISO: string;
  TimeOther: boolean;
  LocationID: number;
  Location: Location | null;
  OtherLocation: string;
  Person: number;
  Period: number;
  ClassID: number;
  Class: Class | null;
  Classes: ClassItem[];
  SituationIDArray: number[];
  GiftPoint: number;
  GiftDetail?: string;
  DesiredArray: number[];
  MixStr: string | null;
  MixClassIDs: string | null;
  IsPrivate: boolean;
  SituationIDs: string | null;
  Desired: string | null;
  RoomID?: number;
  Room?: Room;
  Invoices?: Invoice[];
  SuggesterID: number;
  IsDeleted: boolean;
  NotifyAdmin: string | null;
  IsAnonymous: boolean;
  CastPoint: number;
  Extra?: string | null;
  KeepApplying: boolean;

  CreatedAt?: string;
  UpdatedAt?: string;
}

export interface CallCondition{
  Call: Call;
  Status: string;
}
