export interface NotifCategory{
  ID: number;
  Name: string;
  Shown: boolean;
  Order: number;
  CreatedAt?: string;
  UpdatedAt?: string;
}
