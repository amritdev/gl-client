export interface Class {
  ID: number;
  Name: string;
  EnName: string;
  Color: string;
  Point: number;
  CreatedAt: string;
  UpdatedAt: string;
}
