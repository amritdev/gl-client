export interface Config {
  ID: number;
  GuestPoint: number;
  CastPoint: number;
  DefaultCampaignID: number;
  IntroCampaignID: number;
  CallNightStart: string;
  CallNightEnd: string;
  CallGroupNight: boolean;
  CallPrivateNight: boolean;
  CallNightPoint: number;
  CallDesirePoint: number;
}
