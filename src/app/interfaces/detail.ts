export interface Detail {
  ID: number;
  Name: string;
  ParentID: number;
  Shown: boolean;
  Order: number;
  MaleShown: boolean;
  FemaleShown: boolean;
}
