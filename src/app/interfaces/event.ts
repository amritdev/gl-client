import { Call } from './call';
import { User } from './user';
import { Join } from './join';

export interface CallEvent{
  call: Call;
  event: string;
}

export interface CastEvent{
  cast: User;
  event: string;
}

export interface JoinEvent{
  join: Join;
  type: string;
  event: string;
}
