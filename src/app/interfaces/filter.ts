export interface CastSearchFilter {
  Location: number;
  Class: number[];
  Tag: number[];
  Schedule: string[];
  PointMin: number;
  PointMax: number;
  Recently: boolean;
  Birthday: boolean;
  TodayFree: boolean;
  SearchStr: string[];
}

export interface GuestSearchFilter {
  Location: number;
  Level: number[];
  Recently: boolean;
  LevelStr: string;
  SearchStr: string[];
}
