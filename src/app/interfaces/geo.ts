export interface GeoLoc {
  lng: number;
  lat: number;
}
