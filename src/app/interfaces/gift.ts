export interface Gift{
  ID: number;
  Name: string | null;
  ImageURL: string | null;
  IsCallShown: boolean;
  Point: number;
  ParentID: number;
  Order: number;
}

export interface UserGift{
  Gift: Gift;
  Count: number;
}

export interface GiftRes{
  success: boolean;
  data: Gift;
}
