import { User } from './user';


export interface GiftValue{
  ID: number;
  Point: number;
}

export interface GiftData{
  ID: number;
  CallID: number;
  Casts: User[];
  Gift: number;
  GiftStr: string;
  GiftInfo: GiftValue[];
}
