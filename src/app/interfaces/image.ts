export interface Image {
  ID: number;
  Path: string;
  UserID: number;
  Key: string | null;
  CreatedAt: string;
  UpdatedAt: string;
}

export interface Video {
  ID: number;
  Path: string;
  UserID: number;
  Key: string | null;
  CreatedAt: string;
  UpdatedAt: string;
}
