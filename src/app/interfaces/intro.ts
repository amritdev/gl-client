import { User } from './user';

export interface Intro {
  ID: number;
  VisitorID: number;
  Visitor: User;
  IntroducerID: number;
  Introducer: User;
  IsPointAdded: boolean;
  AddedPoint: number;
  CreatedAt: string;
  UpdatedAt: string;
}
