import { User } from './user';

export interface InvoiceDetail {
    ID: number;
    JoinTime: number;
    Period: number;
    JoinPoint: number;
    ExtendPoint: number;
    NightPoint: number;
    DesirePoint: number;
    TotalPoint: number;
    GiftPoint: number;
    UserID: number;
    User: User;
    InvoiceID: number;
    StartedAt: string;
    EndedAt: string;
    CreatedAt: string;
    UpdatedAt: string;
}
