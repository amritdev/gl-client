import { User } from './user';
import { InvoiceDetail } from './invoice-detail';
import { Call } from './call';

export interface Invoice {
  ID: number;
  Type: string;
  Amount: number;
  UserID: number;
  User: User;
  CallID: number;
  Call: Call;
  Detail: InvoiceDetail[] | null;
  CreatedAt: string;
  UpdatedAt: string;
}
