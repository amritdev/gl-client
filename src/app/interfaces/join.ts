
export interface Join{
  ID: number;
  UserID: number;
  RoomID: number;
  CallID: number;
  IsStarted: boolean;
  Commented: boolean;
  IsExtended: boolean;
  IsPassed: boolean;
  IsDone: boolean;
  StartedAt: string;
  EndedAt?: string;
}
