export interface Location {
  ID: number;
  Name: string;
  ParentID: number;
  Shown: boolean;
  Order: number;
  CreatedAt: string;
  UpdatedAt: string;
}
