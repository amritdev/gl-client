import { Image } from './image';
import { Room } from './room';
import { User } from './user';
import { Gift } from './gift';

export interface Message {
  ID?: number;
  Content?: string | null;
  ImageID?: number | null;
  Image?: Image | null;
  GiftID?: number | null;
  Gift?: Gift | null;
  IsRead?: boolean;
  RoomID: number;
  Room?: Room;
  SenderID?: number;
  Sender?: User;
  ReceiverID?: number;
  Receiver?: User;
  IsNotice?: boolean;
  Number?: number;
  CreatedAt?: string;
  UpdatedAt?: string;
  RoomTitle?: string;
}
