import { User } from './user';
import { NotifCategory } from './category';

export interface Notice {
  ID: number;
  Content: string;
  UserID: number;
  User: User;
  FromUserID: number;
  FromUser: User;
  Type: string;
  CreatedAt: string;
  UpdatedAt: string;
}

export interface Notification {
  ID: number;
  Title: string;
  Content: string;
  CategoryID: number;
  Category?: NotifCategory;
  Role: number;
  LocationIDArray?: number[];
  LocationIDs: string;
  CreatedAt?: string;
  UpdatedAt?: string;
}
