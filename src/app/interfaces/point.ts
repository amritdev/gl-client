export interface Point {
    ID: number;
    Value: number;
    CreatedAt: string;
    UpdatedAt: string;
}
