export interface Report {
  Overall: number;
  PCallPoint: number;
  GCallPoint: number;
  GiftPoint: number;
  Introduce: number;
  MAll: number;
  MSend: number;
  MReceive: number;
  CallNum: number;
  PCallNum: number;
  GCallNum: number;
  CallPerson: number;
  NCallPerson: number;
  RCallPerson: number;
  GiftNum: number;
  NLikes: number;
  NLiked: number;
}
