import { User } from './user';

export interface Review {
  ID: number;
  IsGood: boolean | null;
  TypeStr: string | null;
  UserID: number;
  User ?: User;
  SourceID: number;
  Source: User;
  Comment: string;
  HowGuest: string;
  HowUp: string;
  CallID?: number;
  UpdatedAt?: string;
}
