import { User } from './user';
import { Call } from './call';
import { Join } from './join';

export interface Room {
  ID: number;
  IsGroup: boolean;
  Users: User[];
  LastMessage: string;
  Type: string;
  Calls: Call[];
  Title: string | null;
  Joins: Join[] | null;
  IsDeleted: boolean;

  CreatedAt: string;
  UpdatedAt: string;
}

export interface ResRoom {
  Room: Room;
  Unread: number;
}

export interface RemoveRoom{
  Room: Room;
  UserID: number;
}
