import { User } from './user';
import { Call } from './call';

export interface SalesData {
  User: User;
  Call: Call;
  Location: string;
  IsFirst: boolean;
  First: number;
  Spent: number;
  Introducer: User;
  Intro: number;
}
