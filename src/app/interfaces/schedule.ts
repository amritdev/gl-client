export interface ScheduleDay {
  date: string;
  day: number;
}

export interface Schedule {
  ID?: number;
  Date: string;
  From: string | null;
  To: string | null;
  Midnight: boolean;
  UserID: number | null;
  Enabled: boolean;
  CreatedAt?: string;
  UpdatedAt?: string;
}
