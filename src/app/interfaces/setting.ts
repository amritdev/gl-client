export interface Setting {
  ID: number;
  AppFootprint: boolean;
  AppTweetLike: boolean;
  AppAutoDelayNotify: boolean;
  AppAutoCharge: boolean;
  AppAutoRemove: boolean;
  // AppMessage: boolean;
  // EmailFootprint: boolean;
  EmailLike: boolean;
  EmailMessage: boolean;
  // EmailAdminMessage: boolean;
  EmailJoinLeaveNotify: boolean;
  EmailAutoDelayNotify: boolean;
  EmailTweetLike: boolean;
  EmailAutoChargeNotify: boolean;
  EmailAutoRemoveNotify: boolean;
  EmailAutoCallNotify: boolean;
  EmailAdsNotify: boolean;
  EmailCallNotify: boolean;
  // EmailAdminNotify: boolean;
  // PrivacyAllRanking: boolean;
  // PrivacyFavoriteRanking: boolean;
  // FeverTag: boolean;
  // LineFootprint: boolean;
  LineLike: boolean;
  LineMessage: boolean;
  // LineAdminMessage: boolean;
  LineJoinLeaveNotify: boolean;
  LineAutoDelayNotify: boolean;
  LineTweetLike: boolean;
  LineAutoChargeNotify: boolean;
  LineAutoRemoveNotify: boolean;
  LineAutoCallNotify: boolean;
  LineNewGuestNotify: boolean;
  LineAdsNotify: boolean;
  LineCallNotify: boolean;

  // PushFootprint: boolean;
  PushLike: boolean;
  PushMessage: boolean;
  // PushAdminMessage: boolean;
  PushJoinLeaveNotify: boolean;
  PushAutoDelayNotify: boolean;
  PushTweetLike: boolean;
  PushAutoChargeNotify: boolean;
  PushAutoRemoveNotify: boolean;
  PushAutoCallNotify: boolean;
  PushAdsNotify: boolean;
  PushCallNotify: boolean;

  PhoneAdsNotify: boolean;
  RankingVisible: boolean;

  CreatedAt: string;
  UpdatedAt: string;
}

export type SettingEmailBooleanKeyType = 'EmailAutoChargeNotify' | 'EmailAutoDelayNotify' |
  'EmailAutoRemoveNotify' | 'EmailTweetLike' | 'EmailLike' | 'EmailMessage' |
  'EmailJoinLeaveNotify' | 'EmailAutoCallNotify' | 'EmailAdsNotify' | 'EmailCallNotify';

export type SettingLineBooleanKeyType = 'LineAutoChargeNotify' | 'LineAutoDelayNotify' |
  'LineAutoRemoveNotify' | 'LineTweetLike' |
  'LineLike' | 'LineMessage' | 'LineJoinLeaveNotify' | 'LineAutoCallNotify' | 'LineNewGuestNotify' |
  'LineAdsNotify' | 'LineCallNotify';

export type SettingAppPushBooleanKeyType = 'PushAutoChargeNotify' | 'PushAutoDelayNotify' |
  'PushAutoRemoveNotify' | 'PushTweetLike' |
  'PushLike' | 'PushMessage' | 'PushJoinLeaveNotify' | 'PushAutoCallNotify' |
  'PushAdsNotify' | 'PushCallNotify';

