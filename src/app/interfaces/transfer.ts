import { User } from './user';

export interface Transfer {
  ID: number;
  Type: string;
  UserID: number;
  User: User;
  Point: number;
  Status: string;
  LastTransferedAt: string;
  CreatedAt: string;
  UpdatedAt: string;
}
