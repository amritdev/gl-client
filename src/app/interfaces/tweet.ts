import { User } from './user';

export interface Tweet {
    ID: number;
    Content: string | null;
    ImageURL: string | null;
    FavoriteUserIDs: string | null;
    BlockedUserIDs: string | null;
    Bonus: number;
    UserID: number | null;
    User: User | null;
    IsRecruit: boolean;
    CreatedAt: string;
    UpdatedAt: string;
}
