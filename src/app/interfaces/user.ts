import { Class } from './class';
import { Image, Video } from './image';
import { Setting } from './setting';
import { Schedule } from './schedule';
import { Room } from './room';

export interface User {
  ID: number;
  ProfileID: string;
  Nickname: string | null;
  RealName: string | null;
  RealNameFrigana: string | null;
  CoporateName: string | null;
  CoporateNameFrigana: string | null;
  Email: string | null;
  Password: string | null;
  // FacebookID: string | null;
  LineID: string | null;
  PhoneNumber: string | null;
  VerificationCode: string | null;
  Gender: boolean;
  Birthday: string | null;
  Word: string | null;
  About: string | null;
  Detail: string | null;
  Tags: string | null;
  Point: number;
  PointHalf: number;
  PointUsed: number;
  PointRatio: number;
  RequestedPoint: number;
  Role: number;
  Status: boolean;
  BlockedTweetIDs: string | null;
  BlockedTweetUserIDs: string | null;
  BlockedRoomIDs: string | null;
  FavoriteRoomIDs: string | null;
  // FavoriteGuests: string | null;
  // FavoriteCasts: string | null;
  Favorites: string;
  TelecomCredit: boolean;
  TelecomFailed: boolean;
  BankName: string | null;
  BankBranchName: string | null;
  BankAccountType: string | null;
  BankAccount: string | null;
  BankAccountHolder: string | null;
  PaypalAccount: string | null;
  LocationIDs: string | null;
  ClassID: number | null;
  Class: Class | null;
  SettingID: number | null;
  Setting: Setting | null;
  BirthdayHidden: boolean;
  Images: Image[];
  Videos: Video[];
  Schedules: Schedule[] | null;
  Rooms: Room[] | null;
  CastStart: string | null;
  GuestStart: string | null;
  LeftAt: string | null;
  // IsPresent: boolean;
  IsBlocked: boolean;
  IsApplied: boolean;
  FamilyIDs: string;
  LastRoomID: number;
  BackPercent: number;
  IsVerified: boolean;
  GuestPoint: number;
  CastPoint: number;
  TutorialViewedAt: string | null;
  GiftReceived: string | null;
  IsProfileAdded: boolean;
  IsLineAdded: boolean;
  IsCastLiked: boolean;
  IsCreditAdded: boolean;
  FixedPhrase: string | null;
  PhraseStatus: boolean;
  Withdrawn: boolean;
  FirstJoinAt: string | null;
  Cards: Image[] | null;
  IsChecked: boolean;
  LikeViewedAt: string | null;
  Sight: string;
  Provider: string;
  CreatedAt: string;
  UpdatedAt: string;
  LastConnectedAt: string;
}

export interface UserLoginResponse {
  token: string;
  data: User;
  favorites: number[];
}

export interface UserCall {
  Cast: User;
  MetAt: string;
}

export interface UserRegisterRequest {
  Nickname: string;
  Birthday: string;
  LocationIDs: string;
}

export interface AdminData{
  Birthday: string;
  Image: string;
}
