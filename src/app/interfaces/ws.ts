import { Message } from './message';
import { Notice, Notification } from './notice';
import { Call } from './call';
import { User } from './user';
import { Room } from './room';
import { Join } from './join';

export interface WS {
    Type: string;
    SenderID?: number;
    ReceiverIDs?: number[];
    Data: Message | Notice | Notification | Call | User | Room | Join | string;
    Event?: string;
}
