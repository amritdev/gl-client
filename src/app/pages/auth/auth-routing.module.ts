import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent as EmailLoginComponent } from './email/login/login.component';
import { RegisterComponent as EmailRegisterComponent } from './email/register/register.component';
import { CastComponent } from './cast/cast.component';
import { environment } from 'src/environments/environment';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  // {
  //   path: 'cast',
  //   redirectTo: 'cast/email',
  //   pathMatch: 'full'
  // },
  // {
  //   path: 'login',
  //   component: LoginComponent,
  // },
  {
    path: 'sms',
    loadChildren: () => import('./sms/sms.module').then(m => m.SmsModule),
  },
  {
    path: `cast/${environment.EMAIL_ROUTE}`,
    component: CastComponent
  },
  // {
  //   path: 'cast/login',
  //   component: LoginComponent,
  // },
  // {
  //   path: 'cast/sms',
  //   loadChildren: () => import('./sms/sms.module').then(m => m.SmsModule),
  // },
  {
    path: 'login/sms',
    loadChildren: () => import('./sms/sms.module').then(m => m.SmsModule)
  },
  {
    path: 'agent',
    redirectTo: 'agent/login',
    pathMatch: 'full'
  },
  {
    path: environment.EMAIL_ROUTE,
    redirectTo: `${environment.EMAIL_ROUTE}/login`,
    pathMatch: 'full'
  },
  {
    path: `${environment.EMAIL_ROUTE}/login`,
    component: EmailLoginComponent
  },
  {
    path: `${environment.EMAIL_ROUTE}/register`,
    component: EmailRegisterComponent
  },
  {
    path: `cast/${environment.EMAIL_ROUTE}/login`,
    component: EmailLoginComponent
  },
  {
    path: `cast/${environment.EMAIL_ROUTE}/register`,
    component: EmailRegisterComponent
  },
  {
    path: 'agent/login',
    component: EmailLoginComponent
  },
  {
    path: 'agent/register',
    component: EmailRegisterComponent
  },
  // {
  //   path: 'new',
  //   component: NewComponent
  // },
  // {
  //   path: 'cast/new',
  //   component: NewComponent
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthRoutingModule { }
