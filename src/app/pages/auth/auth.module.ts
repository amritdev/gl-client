import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SocialLoginModule, SocialAuthServiceConfig, FacebookLoginProvider } from 'angularx-social-login';

import { PipesModule } from 'src/app/shared';
import { environment } from 'src/environments/environment';

import { AuthRoutingModule } from './auth-routing.module';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { LoginComponent as AgentLoginComponent } from './email/login/login.component';
import { RegisterComponent as AgentRegisterComponent } from './email/register/register.component';
import { CastComponent } from './cast/cast.component';
import { NewComponent } from './new/new.component';
import { NicknameComponent } from './new/nickname/nickname.component';
import { LocationComponent } from './new/location/location.component';

@NgModule({
  declarations: [
    HomeComponent,
    LoginComponent,
    AgentLoginComponent,
    AgentRegisterComponent,
    CastComponent,
    NewComponent,
    NicknameComponent,
    LocationComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    PipesModule,
    AuthRoutingModule,
    SocialLoginModule,
  ],
  providers: [
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: FacebookLoginProvider.PROVIDER_ID,
            provider: new FacebookLoginProvider(environment.FACEBOOK_APP_ID, {
              locale: 'ja_JP',
              fields: 'id',
              version: 'v4.0',
            }),
          },
        ],
      } as SocialAuthServiceConfig,
    },
  ],
})
export class AuthModule { }
