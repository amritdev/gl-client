import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-cast',
  templateUrl: './cast.component.html',
  styleUrls: ['./cast.component.scss'],
})
export class CastComponent implements OnInit {

  loginRoute = `/cast/${environment.EMAIL_ROUTE}/login`;
  registerRoute = `/cast/${environment.EMAIL_ROUTE}/register`;

  constructor() { }

  ngOnInit(): void { }

}
