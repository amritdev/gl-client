import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { AlertController, NavController } from '@ionic/angular';
import { first } from 'rxjs/operators';
import { AuthService, WebsocketService } from 'src/app/services';
import { environment } from 'src/environments/environment';

import t from 'src/locales';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  isLoading = false;
  email = '';
  password = '';
  role = 'guest';

  guestRegisterURL = `/${environment.EMAIL_ROUTE}/register`;
  castRegisterURL = `/cast/${environment.EMAIL_ROUTE}/register`;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private auth: AuthService,
    private ws: WebsocketService,
    private nav: NavController,
    private alert: AlertController
  ) { }

  ngOnInit(): void {
    this.role = this.router.url.includes('cast')
      ? 'cast'
      : this.router.url.includes('agent')
        ? 'agent' : 'guest';
    const result = this.route.snapshot.queryParamMap.get('result');
    if (result === 'valid_token') {

    }
  }

  async presentAlert(msg: string): Promise<void> {
    const alert = await this.alert.create({
      message: msg,
      buttons: [t('OK')],
    });
    await alert.present();
  }

  validateEmail(email: string): boolean {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  onLogin(): void {
    if (this.email.trim() === '') {
      this.presentAlert(t('Email is required'));
    } else if (this.validateEmail(this.email) === false) {
      this.presentAlert(t('Not a valid email address'));
    } else if (this.password === '') {
      this.presentAlert(t('Password is required'));
    } else if (this.password.length < 8 || this.password.length > 30) {
      this.presentAlert(t('Password length is 8 to 30 characters'));
    } else {
      this.isLoading = true;
      this.auth.emailLogin('email', this.email, this.password, this.role)
        .pipe(first())
        .subscribe((res) => {
          localStorage.removeItem('source');
          localStorage.removeItem('introducer_id');
          localStorage.removeItem('campaign_id');
          if (res.data.IsBlocked) {
            localStorage.removeItem('token');
            this.presentAlert(t('Account has been blocked'));
          } else if (!res.data.IsVerified) {
            localStorage.removeItem('token');
            this.presentAlert(t('Email is not verified'));
          } else if (res.data.Withdrawn) {
            this.presentAlert(t('You are already withdrawn'));
            localStorage.removeItem('token');
          } else {
            this.nav.navigateRoot('/main');
            this.ws.init(res.data.ID);
            // this.auth.startRefreshTokenTimer();

            const token = localStorage.getItem('token');
            this.auth.saveTokenStatus(token ?? '', 'login', 200, 'login');
          }
          this.isLoading = false;
        }, (err: HttpErrorResponse) => {
          if (err.status === 401) {
            if (err.error.msg && err.error.msg !== ''){
              if (err.error.msg === 'Authentication Error') {
                this.presentAlert(t('Email or password is not correct'));
              } else {
                this.presentAlert(err.error.msg.replace(/(?:\r\n|\r|\n)/g, '<br>'));
              }
            }else{
              this.presentAlert(t('Operation Failed'));
            }
          }else{
            this.presentAlert(t('Operation Failed'));
          }
          this.isLoading = false;
        });
    }
  }

  getDefaultHref(): string {
    if (this.role === 'guest') {
      return '/email';
    } else if (this.role === 'cast') {
      return '/cast';
    } else {
      return '/agent';
    }
  }

}
