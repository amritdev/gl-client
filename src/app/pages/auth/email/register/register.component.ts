import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AlertController, NavController } from '@ionic/angular';
import { first } from 'rxjs/operators';

import { AuthService } from 'src/app/services';
import { environment } from 'src/environments/environment';

import t from 'src/locales';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {

  isLoading = false;
  email = '';
  password = '';
  passwordConfirm = '';
  role = 'guest';

  guestLoginURL = `/${environment.EMAIL_ROUTE}/login`;
  castLoginURL = `/cast/${environment.EMAIL_ROUTE}/login`;

  constructor(
    private router: Router,
    private auth: AuthService,
    private nav: NavController,
    private alert: AlertController
  ) { }

  ngOnInit(): void {
    this.role = this.router.url.includes('cast')
      ? 'cast'
      : this.router.url.includes('agent')
        ? 'agent' : 'guest';
  }

  async presentAlert(msg: string): Promise<void> {
    const alert = await this.alert.create({
      message: msg,
      buttons: [t('OK')],
    });
    await alert.present();
  }

  validateEmail(email: string): boolean {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  onRegister(): void {
    if (this.email.trim() === '') {
      this.presentAlert(t('Email is required'));
    } else if (this.validateEmail(this.email) === false) {
      this.presentAlert(t('Not a valid email address'));
    } else if (this.password === '') {
      this.presentAlert(t('Password is required'));
    } else if (this.password.length < 8 || this.password.length > 30) {
      this.presentAlert(t('Password length is 8 to 30 characters'));
    } else if (this.password !== this.passwordConfirm) {
      this.presentAlert(t('Password does not match'));
    } else {
      this.auth.emailRegister(this.role, this.email, this.password)
        .pipe(first())
        .subscribe(() => {
          this.presentAlert(t('Email has been registered'));
          if (this.role === 'cast') {
            this.nav.navigateBack('/cast/email/login');
          } else if (this.role === 'agent') {
            this.nav.navigateBack('/agent/login');
          } else {
            this.nav.navigateBack('/email/login');
          }
        }, (err) => {
          if (err.status === 409) {
            this.presentAlert(t('Email already exists'));
          } else {
            this.presentAlert(t('Operation Failed'));
          }
        });
    }
  }

  getDefaultHref(): string {
    if (this.role === 'guest') {
      return '/email';
    } else if (this.role === 'cast') {
      return '/cast';
    } else {
      return '/agent';
    }
  }

}
