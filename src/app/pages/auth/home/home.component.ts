import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, NavController, Platform } from '@ionic/angular';
import { first } from 'rxjs/operators';
import { AuthService, WebsocketService } from 'src/app/services';
import { User } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';
import t from 'src/locales';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {

  role = '';
  colorScheme = '';
  isChecked = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private wsService: WebsocketService,
    private http: HttpClient,
    private alertController: AlertController,
    private navController: NavController,
    private changeDetectorRef: ChangeDetectorRef,
    private platform: Platform,
  ) { }

  ngOnInit(): void {
    // get params
    this.role = this.router.url.includes('cast') ? 'cast' : '';

    // get source, introducer, campaign_id
    const source = this.route.snapshot.queryParamMap.get('source');
    const introducerID = this.route.snapshot.queryParamMap.get('id');
    const campaignID = this.route.snapshot.queryParamMap.get('c_id');
    const pushToken = this.route.snapshot.queryParamMap.get('fcm_token');

    if (source && introducerID) {
      localStorage.setItem('source', source);
      localStorage.setItem('introducer_id', introducerID);
    }
    if (campaignID !== null){
      localStorage.setItem('campaign_id', campaignID);
    }

    // get push token
    if (pushToken) {
      localStorage.setItem('fcm_token', pushToken);
      localStorage.setItem('fcm_device', source === 'appstore' ? 'ios' : 'android');
    }

    const result = this.route.snapshot.queryParamMap.get('result');
    if (result === 'invalid_token') {
      this.showAlert(t('This token is not valid'));
    } else if (result === 'expired_token') {
      this.showAlert(t('Token expiration date has expired'));
    }

    // for pc
    const lineCode = this.route.snapshot.queryParamMap.get('code');

    if (lineCode) {
      if (this.role === 'cast') {
        this.http.get<User | null>(`${environment.API_URL}/users/line/${lineCode}`).subscribe(async user => {
          if (user && user.Role === 1) {
            const alert = await this.alertController.create({
              message: t('You are already a guest. Do you really want a cast?'),
              buttons: [t('Cancel'), {
                text: t('OK'),
                handler: () => {
                  this.authService.lineUpgrade('upgrade', user.LineID ?? '')
                    .pipe(first())
                    .subscribe(async res => {
                      localStorage.removeItem('source');
                      localStorage.removeItem('introducer_id');
                      localStorage.removeItem('campaign_id');
                      if (res.data.IsBlocked) {
                        localStorage.removeItem('token');
                        this.showAlert(t('Account has been blocked'));
                        // } else if (res.data.IsApplied) {
                        //   localStorage.removeItem('token');
                        //   this.showAlert(t('The application was successful. Pleaes wait for the contact from the concierge.'));
                      } else {
                        this.navController.navigateRoot('/main');
                        this.wsService.init(res.data.ID);
                      }
                    }, () => {
                      this.showAlert(t('Sign in failed with LINE'));
                    });
                },
              }],
            });
            await alert.present();
          } else {
            this.authService.lineConfirm('confirm', user?.LineID ?? '')
              .pipe(first())
              .subscribe((res) => {
                localStorage.removeItem('source');
                localStorage.removeItem('introducer_id');
                localStorage.removeItem('campaign_id');
                if (res.data.IsBlocked) {
                  localStorage.removeItem('token');
                  this.showAlert(t('Account has been blocked'));
                  // } else if (res.data.IsApplied) {
                  //   localStorage.removeItem('token');
                  //   this.showAlert(t('The application was successful. Pleaes wait for the contact from the concierge.'));
                } else {
                  this.navController.navigateRoot('/main');
                  this.wsService.init(res.data.ID);
                }
              }, () => {
                this.showAlert(t('Sign in failed with LINE'));
              });
          }
        });
      } else {
        this.getLineID(lineCode);
      }
    }

    // for mobile
    this.route.queryParamMap.subscribe((params) => {
      if (params.get('line')) {
        const code = params.get('line');
        if (code){
          this.getLineID(code, 'mobile');
        }
      }
    });

    const prefersDark = window.matchMedia('(prefers-color-scheme: dark)');
    // tslint:disable
    prefersDark.addEventListener("change", e => {
      // tslint:enable
      this.colorScheme = e.matches ? 'dark' : 'light';
      this.changeDetectorRef.detectChanges();
    });
    this.colorScheme = prefersDark.matches ? 'dark' : 'light';
  }

  goCastPage(): void {
    this.navController.navigateRoot('/cast');
  }

  goGuestPage(): void {
    this.navController.navigateRoot('/');
  }

  onSignInWithLINE(): void {
    let redirectURL = `${environment.SITE_URL}`;
    if (this.platform.is('hybrid')) {
      redirectURL = `${environment.SITE_URL}/line/home`;
    }
    if (this.role === 'cast') {
      location.href = 'https://access.line.me/oauth2/v2.1/authorize?'
        + 'response_type=code&'
        + `client_id=${environment.LINE_CHANNEL_ID}&`
        + `redirect_uri=${redirectURL}/cast&`
        + `state=${Math.random().toString(36).substring(2, 15)}&`
        + 'scope=profile%20openid%20email&'
        + 'bot_prompt=aggressive';
    } else {
      location.href = 'https://access.line.me/oauth2/v2.1/authorize?'
        + 'response_type=code&'
        + `client_id=${environment.LINE_CHANNEL_ID}&`
        + `redirect_uri=${redirectURL}&`
        + `state=${Math.random().toString(36).substring(2, 15)}&`
        + 'scope=profile%20openid%20email&'
        + 'bot_prompt=aggressive';
    }
  }

  onLineLoginWithCode(lineCode: string): void {
    this.authService.lineLogin('line', lineCode, this.role)
      .pipe(first())
      .subscribe(async res => {
        localStorage.removeItem('source');
        localStorage.removeItem('introducer_id');
        localStorage.removeItem('campaign_id');
        if (res.data.IsBlocked) {
          localStorage.removeItem('token');
          this.showAlert(t('Account has been blocked'));
        } else if (res.data.Withdrawn) {
          localStorage.removeItem('token');
          this.showAlert(t('You are already withdrawn'));
        } else {
          this.wsService.init(res.data.ID);

          const token = localStorage.getItem('token');
          this.authService.saveTokenStatus(token ?? '', 'login', 200, 'login');

          this.navController.navigateRoot('/main');
        }
      }, async () => {
        this.showAlert(t('Sign in failed with LINE'));
      });
  }

  onLineLoginWithID(lineID: string): void {
    this.authService.lineLoginWithID(lineID)
      .pipe(first())
      .subscribe(async res => {
        if (res.data.IsBlocked) {
          localStorage.removeItem('token');
          this.showAlert(t('Account has been blocked'));
        } else {
          this.navController.navigateRoot('/main');
          this.wsService.init(res.data.ID);
        }
      }, async () => {
        this.showAlert(t('Sign in failed with LINE'));
      });
  }

  getLineID(lineCode: string, mode= ''): void {
    let observe: Observable<{
      line_id: string;
      exist: boolean;
    }>;
    if (mode === ''){
      observe = this.http.get<{
        line_id: string;
        exist: boolean;
      }>(`${environment.API_URL}/users/lineid/${lineCode}`);
    }else{
      observe = this.http.get<{
        line_id: string;
        exist: boolean;
      }>(`${environment.API_URL}/users/lineid/${lineCode}?platform=mobile`);
    }

    observe.subscribe(res => {
      if (res.exist){
        this.onLineLoginWithID(res.line_id);
      }else{
        localStorage.setItem('line_id', res.line_id);
        this.navController.navigateForward('/sms');
      }
    }, async _ => {
      const alert = await this.alertController.create({
        message: t('Sign in failed with LINE'),
        buttons: [t('OK')],
      });
      await alert.present();
    });
  }

  async showAlert(msgStr: string): Promise<void> {
    const alert = await this.alertController.create({
      message: msgStr,
      buttons: ['OK']
    });
    await alert.present();
  }

}
