import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';

import { AlertController, NavController } from '@ionic/angular';

import { SocialAuthService, FacebookLoginProvider, SocialUser } from 'angularx-social-login';
import { first } from 'rxjs/operators';

import { AuthService, WebsocketService } from 'src/app/services';
import { User } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';
import t from 'src/locales';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  role = '';
  action = 'login';
  clicked = false;
  colorScheme = '';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private wsService: WebsocketService,
    private socialAuthService: SocialAuthService,
    private http: HttpClient,
    private navController: NavController,
    private alertController: AlertController,
    private changeDetectorRef: ChangeDetectorRef,
  ) { }

  ngOnInit(): void {
    this.clicked = false;
    this.role = this.router.url.includes('cast') ? 'cast' : '';
    this.action = this.route.snapshot.queryParamMap.get('action') ?? 'login';
    const source = this.route.snapshot.queryParamMap.get('source');
    const introducerID = this.route.snapshot.queryParamMap.get('id');
    const campaignID = this.route.snapshot.queryParamMap.get('c_id');
    if (source && introducerID) {
      localStorage.setItem('source', source);
      localStorage.setItem('introducer_id', introducerID);
    }

    if (campaignID !== null) {
      localStorage.setItem('campaign_id', campaignID);
    }
    this.socialAuthService.authState.subscribe((user: SocialUser) => {
      if (this.clicked && user.id && user.authToken) {
        this.authService.facebookLogin('facebook', user.id, user.authToken, this.role)
          .pipe(first())
          .subscribe(async res => {
            localStorage.removeItem('source');
            localStorage.removeItem('introducer_id');
            localStorage.removeItem('campaign_id');
            if (res.data.IsBlocked) {
              localStorage.removeItem('token');
              const alert = await this.alertController.create({
                message: t('Account has been blocked'),
                buttons: [t('OK')],
              });
              await alert.present();
              // } else if (res.data.IsApplied) {
              //   localStorage.removeItem('token');
              //   const alert = await this.alertController.create({
              //     message: t('The application was successful. Pleaes wait for the contact from the concierge.'),
              //     buttons: [t('OK')],
              //   });
              //   await alert.present();
            } else {
              this.navController.navigateRoot('/main');
              this.wsService.init(res.data.ID);
            }
          }, async () => {
            const alert = await this.alertController.create({
              message: t('Sign in failed with facebook'),
              buttons: [t('OK')],
            });
            await alert.present();
          });
      }
    });
    const lineCode = this.route.snapshot.queryParamMap.get('code');
    if (lineCode) {
      if (this.role === 'cast') {
        this.http.get<User | null>(`${environment.API_URL}/users/line/${lineCode}`).subscribe(async user => {
          if (user && user.Role === 1) {
            const alert = await this.alertController.create({
              message: t('You are already a guest. Do you really want a cast?'),
              buttons: [t('Cancel'), {
                text: t('OK'),
                handler: () => {
                  this.authService.lineUpgrade('upgrade', user.LineID ?? '')
                    .pipe(first())
                    .subscribe(async res => {
                      localStorage.removeItem('source');
                      localStorage.removeItem('introducer_id');
                      localStorage.removeItem('campaign_id');
                      if (res.data.IsBlocked) {
                        localStorage.removeItem('token');
                        const otherAlert = await this.alertController.create({
                          message: t('Account has been blocked'),
                          buttons: [t('OK')],
                        });
                        await otherAlert.present();
                        // } else if (res.data.IsApplied) {
                        //   localStorage.removeItem('token');
                        //   const otherAlert = await this.alertController.create({
                        //     message: t('The application was successful. Pleaes wait for the contact from the concierge.'),
                        //     buttons: [t('OK')],
                        //   });
                        //   await otherAlert.present();
                      } else {
                        this.navController.navigateRoot('/main');
                        this.wsService.init(res.data.ID);
                      }
                    }, async () => {
                      const otherAlert = await this.alertController.create({
                        message: t('Sign in failed with LINE'),
                        buttons: [t('OK')],
                      });
                      await otherAlert.present();
                    });
                },
              }],
            });
            await alert.present();
          } else {
            this.authService.lineConfirm('confirm', user?.LineID ?? '')
              .pipe(first())
              .subscribe(async res => {
                localStorage.removeItem('source');
                localStorage.removeItem('introducer_id');
                localStorage.removeItem('campaign_id');
                if (res.data.IsBlocked) {
                  localStorage.removeItem('token');
                  const otherAlert = await this.alertController.create({
                    message: t('Account has been blocked'),
                    buttons: [t('OK')],
                  });
                  await otherAlert.present();
                  // } else if (res.data.IsApplied) {
                  //   localStorage.removeItem('token');
                  //   const otherAlert = await this.alertController.create({
                  //     message: t('The application was successful. Pleaes wait for the contact from the concierge.'),
                  //     buttons: [t('OK')],
                  //   });
                  //   await otherAlert.present();
                } else {
                  this.navController.navigateRoot('/main');
                  this.wsService.init(res.data.ID);
                }
              }, async () => {
                const otherAlert = await this.alertController.create({
                  message: t('Sign in failed with LINE'),
                  buttons: [t('OK')],
                });
                await otherAlert.present();
              });
          }
        });
      } else {
        this.onLineLoginWithCode(lineCode);
      }
    }
    const prefersDark = window.matchMedia('(prefers-color-scheme: dark)');
    // tslint:disable
    prefersDark.addListener(e => {
      // tslint:enable
      this.colorScheme = e.matches ? 'dark' : 'light';
      this.changeDetectorRef.detectChanges();
    });
    this.colorScheme = prefersDark.matches ? 'dark' : 'light';
  }

  goCastLogin(): void {
    this.navController.navigateRoot('/cast/login');
  }

  goGuestLogin(): void {
    this.navController.navigateRoot('/login');
  }

  onSignInWithFacebook(): void {
    this.clicked = true;
    this.socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID);
  }

  onSignInWithLINE(): void {
    if (this.role === 'cast') {
      location.href = 'https://access.line.me/oauth2/v2.1/authorize?'
        + 'response_type=code&'
        + `client_id=${environment.LINE_CHANNEL_ID}&`
        + `redirect_uri=${environment.SITE_URL}/cast/login&`
        + `state=${Math.random().toString(36).substring(2, 15)}&`
        + 'scope=profile%20openid%20email&'
        + 'bot_prompt=aggressive';
    } else {
      location.href = 'https://access.line.me/oauth2/v2.1/authorize?'
        + 'response_type=code&'
        + `client_id=${environment.LINE_CHANNEL_ID}&`
        + `redirect_uri=${environment.SITE_URL}/login&`
        + `state=${Math.random().toString(36).substring(2, 15)}&`
        + 'scope=profile%20openid%20email&'
        + 'bot_prompt=aggressive';
    }
  }

  onLineLoginWithCode(lineCode: string): void {
    this.authService.lineLogin('line', lineCode, this.role)
      .pipe(first())
      .subscribe(async res => {
        localStorage.removeItem('source');
        localStorage.removeItem('introducer_id');
        localStorage.removeItem('campaign_id');
        if (res.data.IsBlocked) {
          localStorage.removeItem('token');
          const alert = await this.alertController.create({
            message: t('Account has been blocked'),
            buttons: [t('OK')],
          });
          await alert.present();
          // } else if (res.data.IsApplied) {
          //   localStorage.removeItem('token');
          //   const alert = await this.alertController.create({
          //     message: t('The application was successful. Pleaes wait for the contact from the concierge.'),
          //     buttons: [t('OK')],
          //   });
          //   await alert.present();
        } else {
          this.navController.navigateRoot('/main');
          this.wsService.init(res.data.ID);
        }
      }, async () => {
        const alert = await this.alertController.create({
          message: t('Sign in failed with LINE'),
          buttons: [t('OK')],
        });
        await alert.present();
      });
  }
}
