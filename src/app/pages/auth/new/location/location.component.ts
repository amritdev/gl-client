import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { AlertController, ModalController } from '@ionic/angular';

import { Location } from 'src/app/interfaces/location';
import { environment } from 'src/environments/environment';
import t from 'src/locales';

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss'],
})
export class LocationComponent implements OnInit {

  @Input() locationID: number | null = null;
  locations: Location[] = [];

  constructor(
    private http: HttpClient,
    private modalController: ModalController,
    private alertController: AlertController,
  ) { }

  ngOnInit(): void {
    this.getLocations();
  }

  getLocations(): void {
    this.http.get<Location[]>(`${environment.API_URL}/locations?pid=0`).subscribe(locations => {
      this.locations = locations;
    }, async () => {
      const alert = await this.alertController.create({
        message: t('Operation Failed'),
        buttons: [t('OK')],
      });
      await alert.present();
    });
  }

  onSave(): void {
    this.modalController.dismiss({
      ID: this.locationID,
      Name: this.locations.find(location => location.ID === this.locationID)?.Name,
    });
  }

  onClose(): void {
    this.modalController.dismiss();
  }

}
