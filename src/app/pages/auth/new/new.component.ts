import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { AlertController, IonDatetime, IonRouterOutlet, ModalController, NavController } from '@ionic/angular';

import { Location, User } from 'src/app/interfaces';
import { AuthService } from 'src/app/services';
import { environment } from 'src/environments/environment';
import t from 'src/locales';

import { LocationComponent } from './location/location.component';
import { NicknameComponent } from './nickname/nickname.component';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.scss'],
})
export class NewComponent implements OnInit {

  @ViewChild('birthdayDatetime') birthdayDatetime !: IonDatetime;

  avatar: File | null = null;
  avatarURL = '';
  nickname = '';
  birthday = '';
  locationID: number | null = null;
  locationName: string | null = null;
  isLoading = false;
  role = 1;

  constructor(
    private router: Router,
    private routerOutlet: IonRouterOutlet,
    private auth: AuthService,
    private http: HttpClient,
    private nav: NavController,
    private alert: AlertController,
    private modal: ModalController
  ) { }

  ngOnInit(): void {
    this.role = this.router.url.includes('cast') ? 0 : 1;
  }

  onFileChanged(event: Event): void {
    if (event.target instanceof HTMLInputElement && event.target.files && event.target.files[0]) {
      this.avatar = event.target.files[0];
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = () => {
        this.avatarURL = reader.result as string;
      };
    }
  }

  async onEditLocation(): Promise<void> {
    const modal = await this.modal.create({
      component: LocationComponent,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        locationID: this.locationID,
      },
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
    if (data) {
      this.locationID = data.ID;
      this.locationName = data.Name;
    }
  }

  getLocations(locationID: number): void {
    this.http.get<Location[]>(`${environment.API_URL}/locations?pid=0`).subscribe(locations => {
      const location = locations.find(l => l.ID === locationID);
      this.locationName = location?.Name ?? '';
    });
  }

  async presentAlert(msg: string): Promise<void> {
    const alert = await this.alert.create({
      message: msg,
      buttons: [t('OK')],
    });
    await alert.present();
  }

  async onEditNickname(): Promise<void> {
    const modal = await this.modal.create({
      component: NicknameComponent,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        nickname: this.nickname,
      },
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
    if (data) {
      this.nickname = data;
    }
  }

  onEditBirthday(): void {
    this.birthdayDatetime.open();
  }

  getMaxYear(): number {
    return new Date().getFullYear() - environment.START_AGE;
  }

  onSave(): void {
    if (!this.nickname) {
      this.presentAlert(t('Nickname is required'));
    } else if (!this.birthday) {
      this.presentAlert(t('Please select your birthday'));
    } else if (!this.locationID) {
      this.presentAlert(t('Please select your favorite place'));
    } else {
      this.auth.registUserSubject.next({
        Nickname: this.nickname,
        Birthday: this.birthday,
        LocationIDs: `,${this.locationID.toString()},`
      });
      if (this.role === 1) {
        this.nav.navigateForward('/sms');
      } else {
        this.nav.navigateForward('/cast/sms');
      }
    }
  }

}
