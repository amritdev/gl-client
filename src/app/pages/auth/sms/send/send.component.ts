import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { AlertController, NavController } from '@ionic/angular';
import { AuthService } from 'src/app/services';
import { environment } from 'src/environments/environment';
import t from 'src/locales';
import { UserRegisterRequest } from 'src/app/interfaces';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-send',
  templateUrl: './send.component.html',
  styleUrls: ['./send.component.scss'],
})
export class SendComponent implements OnInit, OnDestroy {

  role = '';
  isLoading = false;
  phoneNumber = '';
  mode = 'register';
  userInfo: UserRegisterRequest | null = null;
  lineID: string | null = null;
  userSubscription: Subscription | null = null;

  constructor(
    private router: Router,
    private authService: AuthService,
    private http: HttpClient,
    private navController: NavController,
    private alertController: AlertController,
  ) { }

  ngOnInit(): void {
    // get line id if line logged in
    this.lineID = localStorage.getItem('line_id');

    // check role
    this.role = this.router.url.includes('cast') ? 'cast' : '';

    // check login or register
    this.mode = this.router.url.includes('login') ? 'login' : 'register';

    if (this.userSubscription) {
      this.userSubscription = this.authService.registUserSubject.subscribe((userInfo) => {
        this.userInfo = userInfo ? { ...userInfo } : null;
        if (this.mode === 'register' && userInfo === null) {
          if (this.role === 'cast') {
            this.navController.navigateBack('/cast/new');
          } else {
            this.navController.navigateBack('/new');
          }
        }
      });
    }
  }

  ngOnDestroy(): void {
    this.userSubscription?.unsubscribe();
  }

  async presentAlert(msg: string): Promise<void> {
    const alert = await this.alertController.create({
      message: msg,
      buttons: [t('OK')],
    });
    await alert.present();
  }

  async onSendSMS(): Promise<void> {
    if (this.phoneNumber.trim() === '') {
      const alert = await this.alertController.create({
        message: t('Phone number is required'),
        buttons: [t('OK')],
      });
      await alert.present();
    } else {
      if (this.phoneNumber.trim().length !== 11) {
        const alert = await this.alertController.create({
          message: t('Phone number length should be 11'),
          buttons: [t('OK')],
        });
        await alert.present();

      } else {
        this.phoneNumber = this.phoneNumber.trim();
        this.isLoading = true;
        this.http.post<{
          created: boolean;
        }>(`${environment.API_URL}/users/sms`, {
          phone_number: `+81${this.phoneNumber}`,
          line_id: this.lineID ?? '',
        }).subscribe((res) => {
          this.isLoading = false;
          this.authService.currentPhoneNumberSubject.next({
            number: `+81${this.phoneNumber}`,
            created: res.created
          });
          if (this.role === 'cast') {
            if (this.mode === 'register') {
              this.navController.navigateForward('/cast/sms/verify');
            } else {
              this.navController.navigateForward('/login/sms/verify');
            }
          } else {
            if (this.mode === 'register') {
              this.navController.navigateForward('/sms/verify');
            } else {
              this.navController.navigateForward('/login/sms/verify');
            }
          }
        }, async (err) => {
          this.isLoading = false;
          let msg = '';
          if (err.status === 409) {
            msg = 'The phone number already exists. Please log in with this phone number.';
          } else if (err.status === 404) {
            msg = 'The phone number that does not exist.';
          } else {
            msg = 'Failed to send authentication code';
          }
          const alert = await this.alertController.create({
            message: t(msg),
            buttons: [t('OK')]
          });
          await alert.present();
        });
      }
    }
  }

  async onCancel(): Promise<void> {
    const alert = await this.alertController.create({
      message: t('Do you want to cancel line register?'),
      buttons: [t('Cancel'), {
        text: t('OK'),
        handler: () => {
          localStorage.removeItem('line_id');
          this.navController.navigateRoot('/');
        }
      }],
    });
    await alert.present();
  }
}
