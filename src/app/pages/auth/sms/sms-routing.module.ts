import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SendComponent } from './send/send.component';
import { VerifyComponent } from './verify/verify.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'send',
    pathMatch: 'full',
  },
  {
    path: 'send',
    component: SendComponent,
  },
  {
    path: 'send/login',
    component: SendComponent,
  },
  {
    path: 'verify',
    component: VerifyComponent,
  },
  {
    path: 'verify/login',
    component: VerifyComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SmsRoutingModule { }
