import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PipesModule } from 'src/app/shared';

import { SmsRoutingModule } from './sms-routing.module';
import { SendComponent } from './send/send.component';
import { VerifyComponent } from './verify/verify.component';

@NgModule({
  declarations: [
    SendComponent,
    VerifyComponent,
  ],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    PipesModule,
    SmsRoutingModule,
  ],
})
export class SmsModule { }
