import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { AlertController, NavController } from '@ionic/angular';

import { first } from 'rxjs/operators';

import { AuthService, WebsocketService } from 'src/app/services';
import t from 'src/locales';

@Component({
  selector: 'app-verify',
  templateUrl: './verify.component.html',
  styleUrls: ['./verify.component.scss'],
})
export class VerifyComponent implements OnInit {

  role = 'guest';
  isLoading = false;
  phoneNumber = '';
  verificationCode = '';
  mode = 'register';
  lineID: string | null = null;
  created = false;

  constructor(
    private router: Router,
    private authService: AuthService,
    private wsService: WebsocketService,
    private alert: AlertController,
    private navController: NavController,
  ) { }

  ngOnInit(): void {
    // get line id if line logged in
    this.lineID = localStorage.getItem('line_id');

    this.role = this.router.url.includes('cast') ? 'cast' : 'guest';
    this.mode = this.router.url.includes('login') ? 'login' : 'register';
    this.authService.currentPhoneNumberSubject.subscribe(numberObj => {
      if (numberObj){
        this.phoneNumber = numberObj.number;
        this.created = numberObj.created;
      }
    });
  }

  async onSMSVerify(): Promise<void> {
    if (this.verificationCode.trim() === '') {
      this.showAlert(t('Authentication code is required'));
    } else {
      this.isLoading = true;
      this.authService.smsLogin('sms', this.phoneNumber, this.verificationCode, this.role, this.lineID ?? '')
        .pipe(first())
        .subscribe(async res => {
          localStorage.removeItem('source');
          localStorage.removeItem('introducer_id');
          localStorage.removeItem('campaign_id');
          this.isLoading = false;
          if (res.data.IsBlocked) {
            localStorage.removeItem('token');
            this.showAlert(t('Account has been blocked'));
          } else if (res.data.Withdrawn) {
            localStorage.removeItem('token');
            this.showAlert(t('You are already withdrawn'));
          } else {
            this.wsService.init(res.data.ID);
            localStorage.removeItem('line_id');

            const token = localStorage.getItem('token');
            this.authService.saveTokenStatus(token ?? '', 'login', 200, 'login');

            if (!this.created && this.lineID){
              this.showAlert(t('LINE cooperation with the already registered account has been completed'));
            }

            this.navController.navigateRoot('/main');
          }
        }, async (err: HttpErrorResponse) => {
          if (err.status === 401) {
            if (err.error.msg && err.error.msg !== ''){
              if (err.error.msg === 'Authentication Error'){
                this.showAlert(t('Authentication code dose not match'));
              }else{
                this.showAlert(err.error.msg.replace(/(?:\r\n|\r|\n)/g, '<br>'));
              }
            }else{
              this.showAlert(t('Operation Failed'));
            }
          }else{
            this.showAlert(t('Operation Failed'));
          }
          this.isLoading = false;
        });
    }
  }

  async showAlert(msgStr: string): Promise<void> {
    const alert = await this.alert.create({
      message: msgStr,
      buttons: ['OK']
    });
    await alert.present();
  }

}
