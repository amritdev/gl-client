import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NavController, Platform } from '@ionic/angular';

import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-line',
  templateUrl: './line.component.html',
  styleUrls: ['./line.component.scss'],
})
export class LineComponent implements OnInit {

  constructor(
    private platform: Platform,
    private route: ActivatedRoute,
    private nav: NavController
  ) { }

  ngOnInit(): void {
    const code = this.route.snapshot.queryParamMap.get('code');
    const url = this.route.snapshot.url.join('/');
    if (code) {
      if (this.platform.is('mobileweb')) {
        location.href = `${environment.DEEPLINK_SCHEME}://glass.dating/s/${url}/${code}`;
      }
    }
    this.nav.navigateRoot('/');
  }

}
