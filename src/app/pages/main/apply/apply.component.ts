import { Component, OnInit, ViewChild, OnDestroy, ElementRef } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import * as moment from 'moment-timezone';
import { IonInfiniteScroll, AlertController, ToastController, NavController, Platform, ModalController, IonRouterOutlet } from '@ionic/angular';
import { AuthService, GeoLocationService, WebsocketService } from 'src/app/services';
import { Call, Tag, Class, User, Schedule, AdminData } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';
import t from 'src/locales';
import { Subscription } from 'rxjs';
import { Router, RouterEvent } from '@angular/router';
import { ScheduleComponent } from './schedule/schedule.component';
import { RemarkComponent } from './remark/remark.component';

@Component({
  selector: 'app-apply',
  templateUrl: './apply.component.html',
  styleUrls: ['./apply.component.scss'],
})
export class ApplyComponent implements OnInit, OnDestroy {

  @ViewChild(IonInfiniteScroll) ionInfScrollToday !: IonInfiniteScroll;
  @ViewChild(IonInfiniteScroll) ionInfScrollTomorrow !: IonInfiniteScroll;
  @ViewChild('applyHeader') applyHeader !: ElementRef;

  mode = 'today';
  id = 0;
  isPresent = false;
  isMatching = false;
  backPercent = 75;
  pointHalf = 0;
  offset = 0;
  castClass: Class | null = null;
  childTags: Tag[] = [];
  childTagsReady = false;
  guestLevels: Class[] = [];
  levelsReady = false;
  checking = false;
  isClicked = false;
  user: User | null = null;
  schedule: Schedule | undefined = undefined;
  today = '';
  canApply = true;
  admin!: AdminData;

  allLoaded: { [key: string]: boolean } = {
    today: false,
    tomorrow: false
  };

  pageIndex: { [key: string]: number } = {
    today: 0,
    tomorrow: 0
  };

  calls: { [key: string]: Call[] } = {
    today: [],
    tomorrow: []
  };

  onceLoaded: { [key: string]: boolean } = {
    today: false,
    tomorrow: false
  };

  pushTokenSubscription: Subscription | null = null;
  firebasePushToken = '';
  callSubscription: Subscription | null = null;
  joinSubscription: Subscription | null = null;
  routerSubscription: Subscription | null = null;
  curURL = '';

  constructor(
    private auth: AuthService,
    private alert: AlertController,
    private navi: NavController,
    private http: HttpClient,
    private router: Router,
    private webSocket: WebsocketService,
    private toastr: ToastController,
    private modal: ModalController,
    private routerOutlet: IonRouterOutlet,
    private platform: Platform,
    private gps: GeoLocationService
  ) {
    this.auth.currentUserSubject.subscribe((userinfo) => {
      if (userinfo) {
        this.id = userinfo.ID;
        // this.isPresent = userinfo.IsPresent;
        this.pointHalf = userinfo.PointHalf;
        this.castClass = userinfo.Class;
        this.backPercent = userinfo.BackPercent;
        this.user = userinfo;
        // console.log(this.castClass);

        if (this.routerSubscription === null) {
          this.routerSubscription = this.router.events.subscribe( e => {
            const routerEvent = e as RouterEvent;
            if (routerEvent.url && routerEvent.url !== this.curURL){
              // if url is changed then update user last connected at time
              if (routerEvent.url.endsWith('main/apply')){
                const nowDate = moment().tz('Asia/Tokyo');
                if (nowDate.hour() < 5){
                  nowDate.subtract(1, 'days');
                }
                this.today = nowDate.format('YYYY-MM-DD');

                if (this.user && this.user.Schedules) {
                  this.schedule = this.user.Schedules.find(item => item.Date === this.today);
                  // console.log(this.schedule);
                }
              }

              this.curURL = routerEvent.url;
            }
          });
        }
      }
    });

    if (this.callSubscription === null) {
      this.callSubscription = this.webSocket.callSubject.subscribe((_) => {
        if (this.onceLoaded.today && this.onceLoaded.tomorrow) {
          this.refresh();
        }
      });
    }

    if (this.joinSubscription === null) {
      this.joinSubscription = this.webSocket.joinSubject.subscribe((_) => {
        if (this.onceLoaded.today && this.onceLoaded.tomorrow) {
          this.refresh();
        }
      });
    }

    if (this.pushTokenSubscription === null) {
      this.pushTokenSubscription = this.auth.firebasePushToken.subscribe(token => {
        if (token !== '' && token !== this.firebasePushToken){
          if (this.platform.is('hybrid')){
            const deviceType = this.platform.is('ios') ? 'ios' : 'android';
            this.savePushToken(token, deviceType);
          }
        }
      });
    }
  }

  ngOnInit(): void {
    this.refresh();
    this.getChildTags();
    this.checkMatchingState();
    this.getGuestLevels();
    this.getAdminData();

    // show toastr
    // this.presentToastWithOptions(
    //   "", '<img src="assets/img/line-logo.png"><div class="bar-text">' +
    //   '<div class="bar-title">通知設定がされていません</div><div class="bar-promote">' +
    //   'メッセージや通知を見逃す状態です。<br/>通知設定をしてください。</div></div><div class="bar-register-button">' +
    //   '</div>');
    // this.showSettingConfirmDialog("通知設定がされていません", "メッセージや通知を見逃す状態です。<br/>通知設定を行いますか。");

    const deviceType = localStorage.getItem('fcm_device') ?? '';
    const pushToken = localStorage.getItem('fcm_token');
    if (pushToken && pushToken !== this.firebasePushToken){
      this.savePushToken(pushToken, deviceType);
      localStorage.removeItem('fcm_token');
      localStorage.removeItem('fcm_device');
    }
  }

  ngOnDestroy(): void{
    this.callSubscription?.unsubscribe();
    this.joinSubscription?.unsubscribe();
    this.pushTokenSubscription?.unsubscribe();
    this.routerSubscription?.unsubscribe();
  }

  savePushToken(token: string, deviceType: string): void {
    this.http.post(`${environment.API_URL}/users/push_token`, {
      push_token: token,
      device_type: deviceType
    }).subscribe(_ => {
      // console.log('Push Token Successfully Saved');
      // console.log(token);
      this.firebasePushToken = token;
      this.auth.firebasePushToken.next(token);
    });
  }

  getAge(birthday: string | null): number {
    // return birthday ? new Date().getFullYear() - new Date(birthday).getFullYear() : 0;
    if (birthday){
      return moment.tz('Asia/Tokyo').diff(birthday, 'years');
    }else{
      return 0;
    }
  }

  getAdminData(): void{
    this.http.get<AdminData>(`${environment.API_URL}/users/admin`).subscribe(res => {
      this.admin = res;
    });
  }

  async showSettingConfirmDialog(title: string, content: string): Promise<void> {
      const confirmation = await this.warn(
        `<div class = "alert-img-container">
          <img src = "assets/img/line-logo2.png" class = "line-img" />
        </div>` +
        `<strong class="message-see">${title}</strong>` +
        `<p class="alert-paragraph">${content}</p>`,
        'none', 'see-message-alert');
      if (confirmation) {
        // to register credit card
        this.navi.navigateForward('main/mypage/payment');
      }
  }

  async presentToastWithOptions(title: string, message: string): Promise<void> {
    const toast = await this.toastr.create({
      header: title,
      message,
      color: 'dark',
      position: 'bottom',
      cssClass: 'toast-custom-class',
      buttons: [
        {
          text: '設定する',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    toast.present();
  }

  getNotifyCount(): number {
    return this.webSocket.unreadNotices;
  }

  getChildTags(): void {
    this.http.get<Tag[]>(`${environment.API_URL}/admin/situations/children/0`).subscribe((cData) => {
      this.childTags = cData;
      this.childTagsReady = true;
    });
  }

  getGuestLevels(): void {
    this.http.get<Class[]>(`${environment.API_URL}/admin/levels/guest`).subscribe(res => {
      this.guestLevels = res;
      this.levelsReady = true;
    });
  }

  getLevelName(usedPoint: number): string {
    if (this.guestLevels.length > 0) {
      for (const levelItem of this.guestLevels) {
        if (usedPoint < levelItem.Point) {
          return levelItem.Name;
        } else {
          continue;
        }
      }
      return this.guestLevels[this.guestLevels.length - 1].Name;
    } else {
      return t('Undefined');
    }
  }

  // async presentToday(): Promise<void> {
  //   const confirmation = await this.warn(t('Are you sure to be present today?'));
  //   if (confirmation) {
  //     this.http.get<User>(`${environment.API_URL}/casts/present`).subscribe(res => {
  //       this.auth.currentUserSubject.next(res);
  //       this.showAlert(t('Successfully Presented!'));
  //     });
  //   }
  // }

  refresh(): void {
    this.pageIndex = {
      today: 0,
      tomorrow: 0
    };

    this.calls = {
      today: [],
      tomorrow: []
    };

    this.getCastCalls('today', null);
    this.getCastCalls('tomorrow', null);
  }

  checkMatchingState(): void {
    this.http.get<boolean>(`${environment.API_URL}/calls/matching?role=cast`).subscribe((res) => {
      this.isMatching = res;
      // if (this.isMatching) {
      //   console.log('YOU ARE MATCHING NOW...');
      // }
    }, (err: HttpErrorResponse) => {
      console.log(err);
      this.showAlert(t('Server Error'));
    });
  }

  async showAlert(msgStr: string): Promise<void> {
    const alert = await this.alert.create({
      message: msgStr,
      buttons: ['OK']
    });
    await alert.present();
  }

  // fetch cast call list
  getCastCalls(mode: string, event: Event | null): void {
    this.http.get<Call[]>(`${environment.API_URL}/calls/cast?mode=${mode}&page=${this.pageIndex[mode]}&offset=${this.offset}`)
      .subscribe(async (res) => {
        if (res.length < 10) {
          this.allLoaded[mode] = true;
        }

        this.calls[mode] = this.calls[mode].concat(res.map((callItem) => {
          if (callItem.ClassID === 0) {
            callItem.Classes = JSON.parse(callItem.MixStr ?? '[]');
          }
          if (callItem.SituationIDs && callItem.SituationIDs !== '') {
            callItem.SituationIDArray = callItem.SituationIDs.split(',').filter((sitItemStr: string) => {
              return sitItemStr !== '';
            }).map((itemStr: string) => {
              return parseInt(itemStr, 10);
            });
          } else {
            callItem.SituationIDArray = [];
          }
          return callItem;
        }));

        // console.log(this.calls);

        if (event && event.target) {
          if (mode === 'today') {
            this.ionInfScrollToday.complete();
          } else {
            this.ionInfScrollTomorrow.complete();
          }
        } else {
          this.onceLoaded[mode] = true;
        }

      }, (err: HttpErrorResponse) => {
        this.showAlert(t('Data Retrieve Failed!'));
      });
  }

  getButtonText(call: Call): string {
    if (call.Applied.findIndex((item) => item.ID === this.id) > -1) {
      return t('Already Applied');
    } else {
      if (call.Status === 'condition' || call.Status === 'waiting') {
        return t('Apply');
      } else {
        if (call.Applied.findIndex((item) => item.ID === this.id) > -1) {
          return t('Already Applied');
        } else {
          if (!call.KeepApplying) {
            if (call.Status === 'confirm') {
              return t('Matching Confirmed');
            } else if (call.Status === 'break') {
              return t('Meeting finished');
            } else {
              return t('Meeting now');
            }
          }else{
            return t('Apply');
          }
        }
      }
    }
  }

  isActive(call: Call): boolean {
    return call.Joined.findIndex((item) => item.ID === this.id) > -1;
  }

  getButtonState(call: Call): boolean {
    if (!call.KeepApplying && (call.Status === 'confirm' || call.Status === 'starting' || call.Status === 'break')) {
      return true;
    } else {
      return call.Joined.findIndex((item) => item.ID === this.id) > -1;
    }
  }

  getTotalCost(call: Call): number {
    let isDesired = false;
    if (call.Desired && this.castClass) {
      const ids = call.Desired.split(',');
      const desireIndex = ids.findIndex(item => {
        if (this.castClass) {
          return item !== '' && parseInt(item, 10) === this.id;
        } else {
          return false;
        }
      });
      isDesired = desireIndex > -1;
    }

    if (this.castClass) {
      // if cast has his own class
      if (isDesired){
        return ( this.castClass.Point * call.Period * 2 + 2000 ) * this.backPercent / 100;
      }else{
        return this.castClass.Point * call.Period * 2 * this.backPercent / 100;
      }
    } else {
      // unless
      if (call.ClassID > 0 && call.Class) {
        return call.Class.Point * call.Period * 2 * this.backPercent / 100;
      } else {
        return 0;
      }
    }
  }

  getExtendCost(call: Call): number {
    if (this.castClass) {
      return Math.floor(this.castClass.Point * 1.3 / 2 * this.backPercent / 100);
    } else {
      if (call.ClassID > 0) {
        if (call.Class) {
          return Math.floor(call.Class.Point * 1.3 / 2 * this.backPercent / 100);
        } else {
          return 0;
        }
      } else {
        return 0;
      }
    }
  }

  getMixValue(call: Call): number {
    let sum = 0;
    for (const classItem of call.Classes) {
      sum += classItem.number * classItem.class.Point;
    }
    return Math.ceil(sum / call.Person);
  }

  getCallSum(call: Call): number {
    if (call.ClassID === 0) {
      let sum = 0;
      for (const classItem of call.Classes) {
        sum += classItem.number * classItem.class.Point;
      }
      return sum;
    } else {
      return 0;
    }
  }

  getMeetLocationStr(call: Call): string {
    const returnVal = (call.BigLocation && call.BigLocation.ID > 0 ? `${call.BigLocation.Name}・` : '' ) +
      (call.Location && call.Location.ID > 0 ? call.Location.Name : call.OtherLocation );
    return returnVal;
  }

  getMeetTimeStr(call: Call, mode: string = 'date'): string {
    if (mode === 'date') {
      return moment(`${call.MeetTimeISO} +0900`, 'YYYY-MM-DD HH:mm:ss Z').format('MM月DD日 HH時mm分');
    } else {
      return moment(`${call.MeetTimeISO} +0900`, 'YYYY-MM-DD HH:mm:ss Z').format('HH時 mm分');
    }
  }

  getClassPerson(call: Call): string {
    const className = call.Class && call.ClassID > 0 ? call.Class.Name : t('Mix');
    return `${className} ${call.Person}名`;
  }

  getTags(call: Call): string {
    let returnStr = '';
    for (const tagItem of call.SituationIDArray) {
      const index = this.childTags.findIndex(item => item.ID === tagItem);
      if (index > -1) {
        returnStr += `#${this.childTags[index].Name} `;
      }
    }
    return returnStr;
  }

  getClassString(call: Call): string {
    if (call.Classes.length > 0) {
      const returnStr: string[] = [];
      for (const cItem of call.Classes) {
        if (cItem.number > 0) {
          returnStr.push(cItem.class.Name + ': ' + cItem.number + '人');
        }
      }
      return `(${returnStr.join(', ')})`;
    } else {
      return '';
    }
  }

  async apply(callId: number, callIndex: number, mode: string): Promise<void> {
    if (this.user && this.user.Role === 10){
      this.showAlert(t('You can apply to calls when you are cast'));
      return;
    }else{
      this.openRemarkModal(callId, callIndex, mode);
    }
  }

  execJoin(callId: number, itemIndex: number, mode: string, addedData: {
    arrival: string,
    remark: string
  }): void {
    this.http.post<Call>(`${environment.API_URL}/calls/join`, {
      call_id: callId,
      arrival: moment(addedData.arrival).tz('Asia/Tokyo'),
      remark: addedData.remark
    }).subscribe(async (res) => {

      if (res.ClassID === 0) {
        res.Classes = JSON.parse(res.MixStr ?? '[]');
      }

      if (res) {
        if (res.SituationIDs && res.SituationIDs !== '') {
          res.SituationIDArray = res.SituationIDs.split(',').filter((sitItemStr: string) => {
            return sitItemStr !== '';
          }).map((itemStr: string) => {
            return parseInt(itemStr, 10);
          });
        } else {
          res.SituationIDArray = [];
        }
      }

      // console.log(res);

      this.calls[mode][itemIndex] = res;
      this.canApply = true;
      this.gps.savePosition('call-apply', callId);

      this.showAlert(t('Successfully applied!'));
    }, (err: HttpErrorResponse) => {
      this.canApply = true;
      console.log(err);
      this.showAlert(t('Server Error'));
    });
  }

  async warn(message: string, title: string = '', cssClass: string = ''): Promise<boolean> {
    return new Promise(async (resolve) => {
      const confirm = await this.alert.create({
        header: title === 'none' ? '' : (title === '' ? t('Alert Notify') : t(title)),
        message: t(message),
        cssClass: cssClass !== '' ? cssClass : 'not-special',
        buttons: [
          {
            text: t('Cancel'),
            role: 'cancel',
            handler: () => {
              return resolve(false);
            },
          },
          {
            text: t('OK'),
            handler: () => {
              return resolve(true);
            },
          },
        ],
      });

      await confirm.present();
    });
  }

  loadData(mode: string, event: Event | null): void {
    this.pageIndex[mode]++;
    this.getCastCalls(mode, event);
  }

  gotoLine(): void{
    window.location.href = 'https://lin.ee/6fAJj9Tg';
  }

  userApplied(callItem: Call): boolean {
    if (this.user){
      const userID = this.user.ID;
      return callItem.Applied.some(item => item.ID === userID);
    }else {
      return false;
    }
  }

  userConfirmed(callItem: Call): boolean {
    if (this.user){
      const userID = this.user.ID;
      return callItem.Joined.some(item => item.ID === userID);
    }else {
      return false;
    }
  }

  getTimeRange(): string {
    if (this.schedule){
      if (this.schedule.From || this.schedule.To){
        let startHour = this.schedule.From ? moment(this.schedule.From).format('HH') : '05';
        if (parseInt(startHour, 10) < 5){
          startHour = (parseInt(startHour, 10) + 24).toString().padStart(2, '0');
        }
        const startMin = this.schedule.From ? moment(this.schedule.From).format('mm') : '00';
        let endHour = this.schedule.To ? moment(this.schedule.To).format('HH') : '28';
        if (parseInt(endHour, 10) < 5){
          endHour = (parseInt(endHour, 10) + 24).toString().padStart(2, '0');
        }
        const endMin = this.schedule.To ? moment(this.schedule.To).format('mm') : '59';
        return `(${startHour}:${startMin}~${endHour}:${endMin})`;
      }else{
        return '';
      }
    }else{
      return '';
    }
  }

  async openRemarkModal(callID: number, callIndex: number, mode: string): Promise<void> {
    const modal = await this.modal.create({
      component: RemarkComponent,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        call: this.calls[mode][callIndex]
      }
    });

    await modal.present();
    const { data } = await modal.onWillDismiss();
    if (data && data.arrival && this.user){
      // console.log(data);
      this.execJoin(callID, callIndex, mode, data);
    }
  }

  async openScheduleModal(): Promise<void> {
    const modal = await this.modal.create({
      component: ScheduleComponent,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        today: this.today,
        schedule: this.schedule,
        userID: this.user?.ID,
      }
    });

    await modal.present();
    const { data } = await modal.onWillDismiss();
    if (data && data.schedule && this.user){
      if (!data.schedule.Enabled && !data.schedule.From && !data.schedule.To){
        this.schedule = undefined;
        if (this.user.Schedules && this.user.Schedules.length > 0){
          const sIndex = this.user.Schedules.findIndex(item => item.Date === this.today);
          if (sIndex > -1){
            this.user.Schedules.splice(sIndex, 1);
            this.saveSchedule();
          }
        }
      }else{
        this.schedule = data.schedule as Schedule;
        if (this.user.Schedules && this.user.Schedules.length > 0) {
          const sIndex = this.user.Schedules.findIndex(item => item.Date === this.today);
          // console.log(sIndex);
          if (sIndex > -1){
            this.user.Schedules[sIndex] = this.schedule;
          }else{
            this.user.Schedules.push(this.schedule);
          }
        }else{
          this.user.Schedules = [this.schedule];
        }
        this.saveSchedule();
      }
    }
  }

  saveSchedule(): void {
    this.http.put<{
      data: Schedule[];
      today: boolean;
    }>(`${environment.API_URL}/users/profile/schedule`, this.user?.Schedules).subscribe(res => {
      if (this.user) {
        this.user.Schedules = res.data;
        this.auth.currentUserSubject.next(this.user);
        if (res.today) {
          this.gps.savePosition('today-free');
        }
      }
    }, async () => {
      const alert = await this.alert.create({
        message: t('Operation Failed'),
        buttons: [t('OK')],
      });
      await alert.present();
    });
  }
}
