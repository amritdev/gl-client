import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { PipesModule } from 'src/app/shared';
import { ApplyRoutingModule } from './apply-routing.module';
import { ApplyComponent } from './apply.component';
import { ScheduleComponent } from './schedule/schedule.component';
import { RemarkComponent } from './remark/remark.component';

@NgModule({
  declarations: [
    ApplyComponent,
    ScheduleComponent,
    RemarkComponent
  ],
  imports: [
    CommonModule,
    ApplyRoutingModule,
    IonicModule,
    PipesModule,
    FormsModule
  ]
})
export class ApplyModule { }
