import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import * as moment from 'moment-timezone';
import { Call, Class } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';
import t from 'src/locales';

@Component({
  selector: 'app-remark',
  templateUrl: './remark.component.html',
  styleUrls: ['./remark.component.scss'],
})
export class RemarkComponent implements OnInit {

  @Input() call !: Call;

  arrival = '';
  placeholder = '運営へコールに対して要望などがあればメッセージをお書きください。\n' +
    '例）今仕事中なので到着時間が遅れる可能性があります。\n' +
    '例）質問があるので電話でもいいですか？090xxxxxxxxなど';
  remark = '';
  guestLevels: Class[] = [];
  levelsReady = false;

  constructor(
    private modal: ModalController,
    private alert: AlertController,
    private http: HttpClient
  ) { }

  ngOnInit(): void {
    this.getGuestLevels();
    this.arrival = moment(`${this.call.MeetTimeISO} +0900`, 'YYYY-MM-DD HH:mm:ss Z').format('YYYY/MM/DD HH:mm:00');
  }

  onSave(): void {
    if (this.arrival === null) {
      this.showAlert(t('Arrival time is required'));

    }else{
      this.modal.dismiss({
        arrival: this.arrival,
        remark: this.remark,
      });
    }
  }

  onClose(): void {
    this.modal.dismiss();
  }

  async showAlert(msgStr: string): Promise<void> {
    const alert = await this.alert.create({
      message: msgStr,
      buttons: ['OK']
    });
    await alert.present();
  }

  async warn(message: string, title: string = ''): Promise<boolean> {
    return new Promise(async (resolve) => {
      const confirm = await this.alert.create({
        header: title === '' ? t('Alert Notify') : t(title),
        message: t(message),
        buttons: [
          {
            text: t('Cancel'),
            role: 'cancel',
            handler: () => {
              return resolve(false);
            },
          },
          {
            text: t('OK'),
            handler: () => {
              return resolve(true);
            },
          },
        ],
      });

      await confirm.present();
    });
  }

  getLevelName(usedPoint: number): string {
    if (this.guestLevels.length > 0) {
      for (const levelItem of this.guestLevels) {
        if (usedPoint < levelItem.Point) {
          return levelItem.Name;
        } else {
          continue;
        }
      }
      return this.guestLevels[this.guestLevels.length - 1].Name;
    } else {
      return t('Undefined');
    }
  }

  getGuestLevels(): void {
    this.http.get<Class[]>(`${environment.API_URL}/admin/levels/guest`).subscribe(res => {
      this.guestLevels = res;
      this.levelsReady = true;
    });
  }

  getMeetLocationStr(call: Call): string {
    const returnVal = (call.BigLocation && call.BigLocation.ID > 0 ? `${call.BigLocation.Name}・` : '' ) +
      (call.Location && call.Location.ID > 0 ? call.Location.Name : call.OtherLocation );
    return returnVal;
  }

  getMeetTimeStr(call: Call, mode: string = 'date'): string {
    if (mode === 'date') {
      return moment(`${call.MeetTimeISO} +0900`, 'YYYY-MM-DD HH:mm:ss Z').format('MM月DD日 HH時mm分');
    } else {
      return moment(`${call.MeetTimeISO} +0900`, 'YYYY-MM-DD HH:mm:ss Z').format('HH時 mm分');
    }
  }

  getClassPerson(call: Call): string {
    const className = call.Class && call.ClassID > 0 ? call.Class.Name : t('Mix');
    return `${className} ${call.Person}名`;
  }
}
