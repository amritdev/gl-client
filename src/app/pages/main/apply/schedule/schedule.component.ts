import { Component, Input, OnInit } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { Schedule } from 'src/app/interfaces';
import t from 'src/locales';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.scss'],
})
export class ScheduleComponent implements OnInit {
  @Input() schedule !: Schedule;
  @Input() userID = 0;
  @Input() today = '';

  hours: number[] = [];
  minutes: number[] = [];
  canDelete = true;

  constructor(
    private modal: ModalController,
    private alert: AlertController
  ) { }

  ngOnInit(): void {
    this.setHoursMinutes();
    if (this.schedule === undefined){
      this.canDelete = false;
      this.schedule = {
        Date: this.today,
        From: null,
        To: null,
        Midnight: false,
        Enabled: false,
        UserID: this.userID,
      };
    }
  }

    setHoursMinutes(): void {
    for (let hour = 5; hour < 29; hour++){
      this.hours.push(hour % 24);
    }

    for (let minutes = 0; minutes < 60; minutes++){
      this.minutes.push(minutes);
    }
  }

  onSave(): void {
    if (this.schedule.Enabled === false && !this.schedule.From && !this.schedule.To) {
      this.showAlert(t('Please set correctly'));
      return;
    }
    this.modal.dismiss({
      schedule: this.schedule,
    });
  }

  onClose(): void {
    this.modal.dismiss();
  }

  async onDelete(): Promise<void> {
    const confirmation = await this.warn(t('Are you sure to delete this schedule?'));
    if (confirmation){
      this.schedule.Enabled = false;
      this.schedule.From = null;
      this.schedule.To = null;

      this.modal.dismiss({
        schedule: this.schedule
      });
    }
  }

  async showAlert(msgStr: string): Promise<void> {
    const alert = await this.alert.create({
      message: msgStr,
      buttons: ['OK']
    });
    await alert.present();
  }

  async warn(message: string, title: string = ''): Promise<boolean> {
    return new Promise(async (resolve) => {
      const confirm = await this.alert.create({
        header: title === '' ? t('Alert Notify') : t(title),
        message: t(message),
        buttons: [
          {
            text: t('Cancel'),
            role: 'cancel',
            handler: () => {
              return resolve(false);
            },
          },
          {
            text: t('OK'),
            handler: () => {
              return resolve(true);
            },
          },
        ],
      });

      await confirm.present();
    });
  }
}
