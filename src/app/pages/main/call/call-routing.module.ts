import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TelecomGuard } from 'src/app/guards/telecom.guard';
import { CallComponent } from './call.component';

const routes: Routes = [
  {
    path: '',
    component: CallComponent,
    canActivate: [TelecomGuard]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CallRoutingModule { }
