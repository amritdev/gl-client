import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { ModalController, NavController, IonDatetime, AlertController, IonRouterOutlet, Platform } from '@ionic/angular';
import * as moment from 'moment-timezone';
import { Subscription } from 'rxjs';
import { ConditionService, WebsocketService, AuthService } from 'src/app/services';
import { Location, Class, User, Call, Tag } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';
import t from 'src/locales';
import { ReservationComponent } from './reservation/reservation.component';
import { ReviewComponent } from 'src/app/components/review/review.component';

interface Options {
  name: string;
  value: number;
}

interface ReservationInfo {
  place: string;
  name: string;
  id: number;
}
@Component({
  selector: 'app-call',
  templateUrl: './call.component.html',
  styleUrls: ['./call.component.scss'],
})
export class CallComponent implements OnInit, OnDestroy {
  @ViewChild('callAfterDatetime') callAfterDatetime !: IonDatetime;

  calls !: Call[];
  call !: Call;
  castExists = false;
  otherMeetTime = false;
  periodOther = false;
  childTags: Tag[] = [];
  childTagsReady = false;
  classList: Class[] = [];
  reverseClassList: Class[] = [];
  desiredCasts: User[] = [];
  familyCasts: User[] = [];
  meetTimes: string[] = [
    t('After 30 minutes from now'),
    t('After 60 minutes'),
    t('After 90 minutes'),
    t('Set By Date')
  ];

  persons: Options[] = [];
  periods: Options[] = [];
  parentLocations: Location[] = [];
  childLocations: Location[] = [];

  // condition subscriber
  condSubscriber: Subscription | null = null;
  userSubscription: Subscription | null = null;
  reloadSubscription: Subscription | null = null;

  // for slider
  slideOpts = {
    slidesPerView: 2.2,
    spaceBetween: 10
  };

  // for reservation
  selectedIndex = 0;

  // family IDs
  familyIDs = '';

  // telecom credit failed
  telecomCreditFailed = false;
  cardReady !: boolean;
  mixClassString = '';
  minClassPoint = 0;
  maxClassPoint = 0;
  user: User | null = null;
  reviewSubscription: Subscription | null = null;

  // deisre casts pagination
  page = 0;
  size = 10;
  offset = 0;
  desireAllLoaded = false;
  connectSubscription: Subscription | null = null;
  pushTokenSubscription: Subscription | null = null;

  // family casts pagination
  familyPage = 0;
  familySize = 10;
  familyAllLoaded = false;

  // firebase push token
  firebasePushToken = '';

  constructor(
    public cond: ConditionService,
    private http: HttpClient,
    private navi: NavController,
    private modal: ModalController,
    private alert: AlertController,
    private webSocket: WebsocketService,
    private routerOutlet: IonRouterOutlet,
    private auth: AuthService,
    private platform: Platform
  ) {
    if (this.cond.userLocation > 0){
      this.cond.initializeCondition();
    }

    if (this.condSubscriber === null) {
      this.condSubscriber = this.cond.callConditionSubject.subscribe((condition) => {
        this.cardReady = this.cond.cardReady;
        if (condition && condition.Status === 'initial') {
          this.call = JSON.parse(JSON.stringify(condition.Call));
          this.parentLocations = this.cond.pLocations;

          // get desired casts
          this.page = 0;
          this.offset = 0;
          this.desiredCasts = [];
          this.desireAllLoaded = false;
          this.getClasses();
          this.getChildLocations(this.cond.userLocation);
        }
      });
    }

    this.checkCastsExists();

    this.webSocket.callSubject.subscribe(_ => {
      this.calls = [];
      this.getCalls();
    });

    this.webSocket.joinSubject.subscribe(_ => {
      this.calls = [];
      this.getCalls();
    });

    if (this.reviewSubscription === null) {
      this.reviewSubscription = this.webSocket.reviewSubject.subscribe(roomID => {
        if (roomID > 0) {
          this.openReviewDialog(roomID);
        }
      });
    }

    if (this.userSubscription === null) {
      this.userSubscription = this.auth.currentUserSubject.subscribe(user => {
        if (user) {
          this.user = user;
          if (this.familyIDs !== user.FamilyIDs) {
            this.familyIDs = user.FamilyIDs;
            this.getFamilyCasts();
          }
          this.telecomCreditFailed = user.TelecomFailed;
        }
      });
    }

    if (this.reloadSubscription === null) {
      this.reloadSubscription = this.auth.reloadSubject.subscribe(res => {
        if (res) {
          this.page = 0;
          this.offset = 0;
          this.desiredCasts = [];
          this.desireAllLoaded = false;
          this.getClasses();
        }
      });
    }

    if (this.pushTokenSubscription === null) {
      this.pushTokenSubscription = this.auth.firebasePushToken.subscribe(token => {
        if (token !== '' && token !== this.firebasePushToken){
          if (this.platform.is('hybrid')){
            const deviceType = this.platform.is('ios') ? 'ios' : 'android';
            this.savePushToken(token, deviceType);
          }
        }
      });
    }
  }

  ngOnInit(): void {
    this.getPersons();
    this.getPeriods();
    this.getCalls();
    this.getChildTags();

    const pushToken = localStorage.getItem('fcm_token');
    const deviceType = localStorage.getItem('fcm_device') ?? '';
    if (pushToken && pushToken !== this.firebasePushToken){
      this.savePushToken(pushToken, deviceType);
      localStorage.removeItem('fcm_token');
      localStorage.removeItem('fcm_device');
    }
  }

  ngOnDestroy(): void {
    this.condSubscriber?.unsubscribe();
    this.reviewSubscription?.unsubscribe();
    this.userSubscription?.unsubscribe();
    this.reloadSubscription?.unsubscribe();
    this.connectSubscription?.unsubscribe();
    this.pushTokenSubscription?.unsubscribe();
  }

  savePushToken(token: string, deviceType: string): void {
    this.http.post(`${environment.API_URL}/users/push_token`, {
      push_token: token,
      device_type: deviceType
    }).subscribe(_ => {
      // console.log('Push Token Successfully Saved');
      // console.log(token);
      this.firebasePushToken = token;
      this.auth.firebasePushToken.next(token);
    });
  }

  async openReviewDialog(roomID: number): Promise<void> {
    const modal = await this.modal.create({
      component: ReviewComponent,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        roomID
      },
    });
    await modal.present();
    await modal.onDidDismiss();
  }

  async openSelectModal(type: string): Promise<void> {
    if (type === 'time') {
      const modal = await this.modal.create({
        component: ReservationComponent,
        swipeToClose: true,
        componentProps: {
          choices: this.meetTimes,
          firstValue: this.call.MeetTime,
          mode: type
        },
        cssClass: `call-choices-modal ${type}-modal`
      });
      await modal.present();
      await modal.onDidDismiss().then(res => {
        if (res.data){
          this.call.MeetTime = res.data.meetTime;
          this.call.TimeOther = res.data.timeOther;
          if (this.call.TimeOther){
            this.call.MeetTimeISO = res.data.meetTimeISO;
          }
        }
      });
    }else if (type === 'person') {
      const modal = await this.modal.create({
        component: ReservationComponent,
        swipeToClose: true,
        componentProps: {
          choices: this.persons,
          firstValue: this.call.Person,
          mode: type
        },
        cssClass: `call-choices-modal ${type}-modal`
      });
      await modal.present();
      await modal.onDidDismiss().then(res => {
        if (res.data){
          this.call.Person = res.data.person;
        }
      });
    }else if (type === 'period') {
      const modal = await this.modal.create({
        component: ReservationComponent,
        swipeToClose: true,
        componentProps: {
          choices: this.periods,
          firstValue: this.call.Period,
          mode: type
        },
        cssClass: `call-choices-modal ${type}-modal`
      });
      await modal.present();
      await modal.onDidDismiss().then(res => {
        if (res.data){
          this.call.Period = res.data.period;
        }
      });
    }else if (type === 'class') {
      const modal = await this.modal.create({
        component: ReservationComponent,
        swipeToClose: true,
        componentProps: {
          choices: this.classList,
          firstValue: this.call.ClassID,
          mixStr: `${this.minClassPoint}pt ~ ${this.maxClassPoint}pt / 30分`,
          person: this.call.Person,
          mode: type
        },
        cssClass: `call-choices-modal ${type}-modal`
      });
      await modal.present();
      await modal.onDidDismiss().then(res => {
        if (res.data){
          this.call.ClassID = res.data.classID;
          if (res.data.classID > 0) {
            const classIndex = this.classList.findIndex(item => item.ID === res.data.classID);
            if (classIndex > -1){
              this.call.Class = this.classList[classIndex];
            }
          }
        }
      });
    } else if (type === 'location') {
      const modal = await this.modal.create({
        component: ReservationComponent,
        swipeToClose: true,
        componentProps: {
          choices: this.childLocations,
          firstValue: this.call.LocationID,
          otherLoc: this.call.OtherLocation,
          mode: type,
          reservationName: this.call.Reservation ?? '',
          place: this.call.Place ?? ''
        },
        cssClass: `call-choices-modal ${type}-modal`
      });
      await modal.present();
      await modal.onDidDismiss().then(res => {
        if (res.data){
          this.call.LocationID = res.data.locationID;
          if (res.data.reservation.trim() !== '') {
            this.call.Reservation = res.data.reservation.trim();
          }
          if (res.data.place.trim() !== '') {
            this.call.Place = res.data.place.trim();
          }

          if (res.data.locationID > 0) {
            const classIndex = this.childLocations.findIndex(item => item.ID === res.data.locationID);
            if (classIndex > -1){
              this.call.Location = this.childLocations[classIndex];
            }
          }else{
            this.call.OtherLocation = res.data.otherLocation;
          }
        }
      });
    }
  }

  checkCastsExists(): void{
    this.http.get<boolean>(`${environment.API_URL}/casts/check`).subscribe(res => {
      this.castExists = res;
    });
  }

  getNotifyCount(): number {
    return this.webSocket.unreadNotices;
  }

  getFamilyCasts(): void {
    this.http.get<User[]>(`${environment.API_URL}/users/family?page=${this.familyPage}&size=${this.familySize}`).subscribe(casts => {
      if (casts.length < this.familySize) {
        this.familyAllLoaded = true;
      }
      this.familyCasts = this.familyCasts.concat(casts);
    });
  }

  getDesiredCasts(): void {
    this.http.post<User[]>(`${environment.API_URL}/casts/desire`, {
      ids: [],
      location: this.call.BigLocationID,
      page: this.page,
      size: this.size,
      offset: this.offset
    }).subscribe((data) => {
      if (data.length < this.size) {
        this.desireAllLoaded = true;
      }
      this.desiredCasts = this.desiredCasts.concat(data);

      if (this.connectSubscription === null){
        this.connectSubscription = this.webSocket.connectUserSubject.subscribe(res => {
          const desireIndex = this.desiredCasts.findIndex(item => item.ID === res.ID);

          if (desireIndex > -1){
            this.desiredCasts.splice(desireIndex, 1);
            this.desiredCasts.unshift(res);
          }else{
            this.desiredCasts.unshift(res);
            this.offset++;
          }
        });
      }
    }, (err: HttpErrorResponse) => {
      console.log(err);
    });
  }

  moreData(_: Event): void {
    if (!this.desireAllLoaded){
      this.page++;
      this.getDesiredCasts();
    }
  }

  moreFamilyData(_: Event): void {
    if (!this.familyAllLoaded){
      this.familyPage++;
      this.getFamilyCasts();
    }
  }

  getPersons(): void {
    this.persons = [];
    for (let i = 1; i <= 10; i++) {
      this.persons.push({
        name: `${i}名`,
        value: i
      });
    }
  }

  getPeriods(): void {
    this.periods = [];
    for (let i = 4; i < 11; i++) {
      this.periods.push({
        name: `${i}時間`,
        value: i
      });
    }
  }

  getClasses(): void {
    this.http.get<Class[]>(`${environment.API_URL}/admin/classes`).subscribe((res) => {
      this.classList = JSON.parse(JSON.stringify(res));
      // console.log(this.classList);
      this.reverseClassList = res.reverse();
      this.minClassPoint = this.classList[0].Point;
      if (this.classList.length >= 2) {
        this.maxClassPoint = this.classList[this.classList.length - 2].Point;
      }

      this.mixClassString = this.getPointRange();
      if (res.length > 0) {
        this.call.ClassID = this.classList[0].ID;
        this.call.Class = this.classList[0];

        this.call.Classes = [];
        for (const classItem of this.classList) {
          this.call.Classes.push({
            class: classItem,
            number: 0
          });
        }

        this.personChanged();
        this.getDesiredCasts();
      }
    });
  }

  getPointRange(): string {
    if (this.classList.length > 2) {
      const lastIndex = this.classList.length - 2;
      return `${this.classList[0].Point * this.call.Period * 2 * this.call.Person}P ~
        ${this.classList[lastIndex].Point * this.call.Period * 2 * this.call.Person}P / 30分`;
    } else {
      return '';
    }
  }

  selectMeetTime(): void {
    if (this.call.MeetTime === t('Set By Date')) {
      this.call.TimeOther = true;
      this.call.MeetTimeISO = new Date().toISOString();
    } else {
      this.call.TimeOther = false;
    }
  }

  getThisYear(): number {
    return new Date().getFullYear();
  }

  bigLocationChanged(): void {
    this.getDesiredCasts();
    this.getChildLocations(this.call.BigLocationID);
  }

  getChildLocations(pid: number): void {
    this.http.get<Location[]>(`${environment.API_URL}/locations?pid=${pid}`).subscribe(locations => {
      this.childLocations = locations;
      this.call.LocationID = this.childLocations[0].ID;
      this.call.Location = this.childLocations[0];
    });
  }

  getChildTags(): void {
    this.http.get<Tag[]>(`${environment.API_URL}/admin/situations/children/0`).subscribe((cData) => {
      this.childTags = cData;
      this.childTagsReady = true;
    });
  }

  childLocationChanged(): void {
    const index = this.childLocations.findIndex(item => item.ID === this.call.LocationID);
    if (index > -1) {
      this.call.Location = this.childLocations[index];
    } else {
      this.call.Location = null;
    }
  }

  classChanged(): void {
    const index = this.classList.findIndex(item => item.ID === this.call.ClassID);
    if (index > -1) {
      this.call.Class = this.classList[index];
    } else {
      this.call.Class = null;
    }
  }

  personChanged(): void {
    if (this.call.Classes.length > 1) {
      this.call.Classes[0].number = Math.floor(this.call.Person / 2);
      this.call.Classes[1].number = Math.ceil(this.call.Person / 2);
      this.call.Classes[2].number = 0;
    }
  }

  onMinus(index: number): void {
    const allNumber = this.call.Classes.reduce((a, b) => a + b.number, 0);
    if (this.call.Classes[index].number >= 1 && allNumber > 1) {
      this.call.Classes[index].number--;
    }
  }

  onPlus(index: number): void {
    if (this.call.Person - this.getSum() > 0) {
      this.call.Classes[index].number++;
    }
  }

  getSum(): number {
    let sum = 0;
    for (const classItem of this.call.Classes) {
      sum += classItem.number;
    }
    return sum;
  }

  getMixValue(): number {
    let sum = 0;
    for (const classItem of this.call.Classes) {
      sum += classItem.number * classItem.class.Point;
    }
    return Math.ceil(sum / this.getSum());
  }

  async goToSituaion(): Promise<void> {
    // this.condSubscriber?.unsubscribe();
    if (this.telecomCreditFailed) {
      this.showAlert(t('Pay with your telecom card failed. Consult with glass consultant.'));
      return;
    } else {
      if (!this.cardReady) {
        const confirmation = await this.warn(t('Credit card info is not registered. Are you gonna register your card?'), t('Call after Credit card registration'));
        if (confirmation) {
          // to register credit card
          this.navi.navigateForward('main/mypage/payment');
        }
      } else {
        if (this.call.LocationID === 0 && this.call.OtherLocation.trim() === '') {
          this.showAlert(t('Please input cast meet location correctly'));
        }else{
          // console.log(this.call);
          this.cond.callConditionSubject.next({
            Call: this.call,
            Status: 'update'
          });
          this.navi.navigateForward(['/main/call/situation']);
        }
      }
    }
  }

  async warn(message: string, title: string = ''): Promise<boolean> {
    return new Promise(async (resolve) => {
      const confirm = await this.alert.create({
        header: title === '' ? t('Alert Notify') : t(title),
        message: t(message),
        buttons: [
          {
            text: t('Cancel'),
            role: 'cancel',
            handler: () => {
              return resolve(false);
            },
          },
          {
            text: t('OK'),
            handler: () => {
              return resolve(true);
            },
          },
        ],
      });

      await confirm.present();
    });
  }

  callAfter(): void {
    this.call.TimeOther = true;
    this.call.MeetTimeISO = new Date().toISOString();
    this.call.MeetTime = t('Set By Date');
    this.callAfterDatetime.open();
  }

  afterTimeSelected(): void {
    this.condSubscriber?.unsubscribe();
    this.navi.navigateForward(['main/call/edit']);
  }

  getCalls(): void {
    this.http.get<Call[]>(`${environment.API_URL}/calls/guest`).subscribe((calls) => {
      this.calls = calls.map((callItem) => {
        if (callItem.ClassID === 0) {
          callItem.Classes = JSON.parse(callItem.MixStr ?? '[]');
        }
        if (callItem) {
          if (callItem.SituationIDs && callItem.SituationIDs !== '') {
            callItem.SituationIDArray = callItem.SituationIDs.split(',').filter((sitItemStr: string) => {
              return sitItemStr !== '';
            }).map((itemStr: string) => {
              return parseInt(itemStr, 10);
            });
          } else {
            callItem.SituationIDArray = [];
          }
        }
        return callItem;
      });
      // console.log(this.calls);
    });
  }

  getStatusText(status: string, isPrivate: boolean): string {
    if (status === 'condition' || status === 'waiting') {
      if (isPrivate) {
        return t('Now Waiting for accept');
      } else {
        return t('Now collecting casts');
      }
    } else if (status === 'confirm') {
      return t('Now collected casts');
    } else if (status === 'starting') {
      return t('Meeting now');
    } else if (status === 'break') {
      return t('Waiting for Pay');
    } else {
      return t('Undefined');
    }
  }

  getHelpText(status: string, isPrivate: boolean): string {
    if (status === 'condition') {
      return t('Please input meeting place');
    } else if (status === 'waiting') {
      if (isPrivate) {
        return t('Please wait until call is accepted');
      } else {
        return t('Please wait until casts collected');
      }
    } else if (status === 'confirm') {
      return t('Just wait until meeting starts');
    } else if (status === 'starting') {
      return t('Now have a good time');
    } else if (status === 'break') {
      return t('Please wait for point calculation from admin');
    } else {
      return t('Undefined');
    }
  }

  getButtonText(status: string): string {
    if (status === 'condition') {
      return t('Input Place');
    } else if (status === 'waiting') {
      return t('Go To Consult you');
    } else {
      return t('See Groupchat');
    }
  }

  getExpectedInvoice(call: Call): number {
    if (call.Invoices && call.Invoices.length > 0) {
      return call.Invoices[0].Amount;
    }else {
      return this.getTotalCost(call);
    }
  }

  getTotalCost(call: Call): number {
    if (call.ClassID > 0) {
      if (!call.IsPrivate) {
        if (call.Status === 'condition' || call.Status === 'waiting') {
          if (call.ClassID > 0 && call.Class) {
            return call.Class.Point * call.Period * call.Person * 2 + call.GiftPoint;
          } else {
            return this.getCallSum(call) * call.Period * 2 + call.GiftPoint;
          }
        } else {
          let totalPoint = 0;
          for (const castItem of call.Joined) {
            const curClass = this.classList.find(item => item.ID === castItem.ClassID);

            if (curClass) {
              totalPoint += curClass.Point;
            } else {
              totalPoint += castItem.PointHalf;
            }
          }
          totalPoint = totalPoint * 2 * call.Period + call.GiftPoint;
          return totalPoint;
        }
      } else {
        if (call.CastPoint > 0){
          return call.CastPoint * 2 * call.Period * call.Person;
        }else{
          const curCast = call.Joined.find(item => {
            return item.Role === 0;
          });
          if (curCast) {
            return curCast.PointHalf * call.Period * 2 * call.Person;
          } else {
            return 0;
          }
        }
      }
    } else {
      return this.classList[0].Point * call.Period * call.Person * 2 + call.GiftPoint;
    }
  }

  getCallSum(call: Call): number {
    if (call.ClassID === 0) {
      let sum = 0;
      for (const classItem of call.Classes) {
        sum += classItem.number * classItem.class.Point ;
      }
      return sum;
    } else {
      return 0;
    }
  }

  getMeetTimeStr(call: Call): string {
    return moment(`${call.MeetTimeISO} +0900`, 'YYYY-MM-DD HH:mm:ss Z').format('MM月 DD日 HH時 mm分');
  }

  getMeetLocationStr(call: Call): string {
    const returnVal = (call.BigLocation && call.BigLocation.ID > 0 ? `${call.BigLocation.Name}・` : '' ) +
      (call.Location && call.Location.ID > 0 ? call.Location.Name : call.OtherLocation );
    return returnVal;
  }

  getClassPerson(call: Call): string {
    const className = call.Class && call.ClassID > 0 ? call.Class.Name : t('Mix');
    return `${className} ${call.Person}名・${this.getTimeStr(call.Period)}`;
  }

  getTimeStr(period: number): string {
    let periodStr = `${period}時間`;
    if (Math.ceil(period) !== Math.floor(period)){
      periodStr = `${Math.floor(period)}時間 ${(period - Math.floor(period)) * 60}分`;
    }
    return periodStr;
  }

  getTags(call: Call): string {
    let returnStr = '';
    for (const tagItem of call.SituationIDArray) {
      const index = this.childTags.findIndex(item => item.ID === tagItem);
      if (index > -1) {
        returnStr += `#${this.childTags[index].Name} `;
      }
    }
    return returnStr;
  }

  getClassString(call: Call): string {
    if (call.Classes.length > 0) {
      const returnStr: string[] = [];
      for (const cItem of call.Classes) {
        if (cItem.number > 0) {
          returnStr.push(cItem.class.Name + ': ' + cItem.number + '人');
        }
      }
      return `(${returnStr.join(', ')})`;
    } else {
      return '';
    }
  }

  async editReservation(status: string, id: number, index: number): Promise<void> {
    if (status === 'condition') {
      this.selectedIndex = index;
      const modal = await this.modal.create({
        component: ReservationComponent,
        cssClass: 'input-place-modal',
        swipeToClose: true,
        presentingElement: this.routerOutlet.nativeEl,
        componentProps: {
          callId: id,
        },
      });
      await modal.present();
      await modal.onDidDismiss().then((res) => {
        if (res.data) {
          // console.log(res.data);
          if (res.data.place.trim() === '' || res.data.name.trim() === '') {
            this.showAlert(t('Input Valid Reservation Info'));
          } else {
            this.sendReservationInfo(res.data);
          }
        }
      });
    } else if (status === 'waiting') {
      // metalgear to do
      this.http.get<{
        RoomID: number
      }>(`${environment.API_URL}/users/admin/room`).subscribe((res) => {
        if (res.RoomID > 0) {
          this.navi.navigateForward(`/main/chat/detail/${res.RoomID}`);
        }
      }, (err: HttpErrorResponse) => {
        this.navi.navigateForward('/main/call');
      });
    } else {
      // metalgear to do
      const params = new HttpParams().append('id', id.toString());
      this.http.get<{
        RoomID: number
      }>(`${environment.API_URL}/calls/room`, { params }).subscribe((res) => {
        if (res.RoomID > 0) {
          this.navi.navigateForward(`/main/chat/detail/${res.RoomID}`);
        }
      }, (err: HttpErrorResponse) => {

        this.navi.navigateForward('/main/call');
      });
    }
  }

  sendReservationInfo(info: ReservationInfo): void {
    this.http.post<Call>(`${environment.API_URL}/calls/reservation`, info).subscribe((res) => {
      this.calls[this.selectedIndex] = res;
      if (res.ClassID === 0) {
        res.Classes = JSON.parse(res.MixStr ?? '[]');
      }

      if (res) {
        if (res.SituationIDs && res.SituationIDs !== '') {
          res.SituationIDArray = res.SituationIDs.split(',').filter((sitItemStr: string) => {
            return sitItemStr !== '';
          }).map((itemStr: string) => {
            return parseInt(itemStr, 10);
          });
        } else {
          res.SituationIDArray = [];
        }
      }
      this.showAlert(t('Successfully Sent!'));
    }, (err: HttpErrorResponse) => {
      console.log(err);
      this.showAlert(t('Reservation Failed!'));
    });
  }

  async showAlert(msgStr: string): Promise<void> {
    const alert = await this.alert.create({
      message: msgStr,
      buttons: ['OK']
    });
    await alert.present();
  }
}
