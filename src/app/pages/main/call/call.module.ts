import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PipesModule, ComponentsModule } from 'src/app/shared';

import { CallRoutingModule } from './call-routing.module';
import { CallComponent } from './call.component';
import { SituationComponent } from './situation/situation.component';
import { DesireComponent } from './desire/desire.component';
import { WarningComponent } from './warning/warning.component';
import { ConfirmComponent } from './confirm/confirm.component';
import { EditComponent } from './edit/edit.component';
import { GiftComponent } from './gift/gift.component';
import { ReservationComponent } from './reservation/reservation.component';

@NgModule({
  declarations: [
    CallComponent, SituationComponent, DesireComponent, WarningComponent, ConfirmComponent,
    EditComponent, GiftComponent, ReservationComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    PipesModule,
    ComponentsModule,
    FormsModule,
    CallRoutingModule,
  ],
})
export class CallModule { }
