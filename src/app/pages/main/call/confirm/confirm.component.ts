import { Component, OnInit } from '@angular/core';
import { formatDate } from '@angular/common';
import { HttpClient, HttpParams } from '@angular/common/http';

import { ModalController, IonRouterOutlet, AlertController, NavController } from '@ionic/angular';

import { ConditionService, AuthService, GeoLocationService } from 'src/app/services';
import { Call, Tag, Class } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';
import t from 'src/locales';
import { Subscription } from 'rxjs';
import { GiftComponent } from '../gift/gift.component';
import { EditComponent } from '../edit/edit.component';
import { SituationComponent } from '../situation/situation.component';
import * as moment from 'moment-timezone';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss'],
})
export class ConfirmComponent implements OnInit {

  condition !: Call;
  tagsReady = false;
  sitTags: Tag[] = [];
  cardReady !: boolean;
  isDisabled = false;
  guestLevels: Class[] = [];
  classList: Class[] = [];
  levelsReady = false;
  point = 0;
  pointUsed = 0;
  authSubscriber: Subscription | null = null;

  constructor(
    private http: HttpClient,
    private cond: ConditionService,
    private modal: ModalController,
    private routerOutlet: IonRouterOutlet,
    private alert: AlertController,
    private auth: AuthService,
    private navi: NavController,
    private gps: GeoLocationService
  ) {
    if (this.authSubscriber === null) {
      this.authSubscriber = this.auth.currentUserSubject.subscribe((user) => {
        if (user) {
          this.pointUsed = user.PointUsed;
        }
      });
    }

    this.cond.callConditionSubject.subscribe((condition) => {
      if (condition && condition.Status === 'update') {
        this.condition = condition.Call;
        this.getSitTagNames();
        this.cardReady = this.cond.cardReady;
        this.point = this.cond.userPoint;
      }
    });
  }

  ngOnInit(): void {
    this.getClasses();
  }

  getClasses(): void {
    this.http.get<Class[]>(`${environment.API_URL}/admin/classes`).subscribe((res) => {
      this.classList = res;
    });
  }

  getSitTagNames(): void {
    if (this.condition.SituationIDArray.length > 0) {
      const params = new HttpParams().append('tags', this.condition.SituationIDArray.toString());
      this.http.get<Tag[]>(`${environment.API_URL}/admin/situations/tags`, { params }).subscribe((tags) => {
        this.sitTags = tags;
        this.tagsReady = true;
      });
    } else {
      this.tagsReady = true;
    }
  }

  getTime(timestamp: string): string {
    // return formatDate(new Date(timestamp), 'yyyy年M月dd日 HH時mm分', 'en');
    return moment.tz(timestamp, 'Asia/Tokyo').format('YYYY年M月DD日 HH時mm分');
  }

  async editCondition(): Promise<void> {
    const modal = await this.modal.create({
      component: EditComponent,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        isDialog: true
      },
    });
    await modal.present();
    await modal.onDidDismiss();
  }

  async editSitTags(): Promise<void> {
    const modal = await this.modal.create({
      component: SituationComponent,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        isDialog: true
      },
    });
    await modal.present();
    await modal.onDidDismiss();
  }

  async showAlert(msgStr: string, header = ''): Promise<void> {
    if (header) {
      const alert = await this.alert.create({
        header,
        message: msgStr,
        buttons: ['OK']
      });
      await alert.present();
    }else{
      const alert = await this.alert.create({
        message: msgStr,
        buttons: ['OK']
      });
      await alert.present();
    }
  }

  async confirm(): Promise<void> {
    if (this.isDisabled === false) {
      this.isDisabled = true;
      if (!this.cardReady) {
        const confirmation = await this.warn(t('Credit card info is not registered. Are you gonna register your card?'), t('Call after Credit card registration'));
        if (confirmation) {
          // to register credit card
          this.navi.navigateForward('main/mypage/payment');
        }
      } else {
        // make MixStr and MixClassIDs
        if (this.condition.ClassID === 0) {
          this.condition.MixStr = JSON.stringify(this.condition.Classes);
          const mixClassIDs: number[] = this.condition.Classes.filter(classItem => {
            return classItem.number > 0;
          }).map((item) => {
            return item.class.ID;
          });
          this.condition.MixClassIDs = `,${mixClassIDs.join(',')},`;
        }

        // make situation and desired string
        if (this.condition.SituationIDArray.length > 0) {
          this.condition.SituationIDs = `,${this.condition.SituationIDArray.join(',')},`;
        }
        if (this.condition.DesiredArray.length > 0) {
          this.condition.Desired = `,${this.condition.DesiredArray.join(',')},`;
        }

        // calculate meet time
        if (!this.condition.TimeOther) {
          const now = new Date();
          const offset: number = parseInt(this.condition.MeetTime.replace(/[^\d.]/g, ''), 10);
          now.setMinutes(now.getMinutes() + offset);
          // this.condition.MeetTimeISO = formatDate(now, 'yyyy-MM-dd HH:mm:ss', 'en', '+900');
          this.condition.MeetTimeISO = moment.tz(now, 'Asia/Tokyo').format('YYYY-MM-DD HH:mm:ss');
        } else {
          const threshDate = new Date();
          threshDate.setMinutes(threshDate.getMinutes() + 25);
          if (new Date(this.condition.MeetTimeISO) < threshDate) {
            this.showAlert(t('You can not set meet time earlier than 30 minutes after now'));
            return;
          }
          // this.condition.MeetTimeISO = formatDate(this.condition.MeetTimeISO, 'yyyy-MM-dd HH:mm:ss', 'en', '+900');
          this.condition.MeetTimeISO = moment.tz(this.condition.MeetTimeISO, 'Asia/Tokyo').format('YYYY-MM-DD HH:mm:ss');
        }

        this.http.post<Call>(`${environment.API_URL}/calls`, this.condition).subscribe((res) => {
          this.navi.navigateForward('/main/call');
          this.gps.savePosition('call-create', res.ID);
        });
      }
    }
  }

  async warn(message: string, title: string = ''): Promise<boolean> {
    return new Promise(async (resolve) => {
      const confirm = await this.alert.create({
        header: title === '' ? t('Alert Notify') : t(title),
        message: t(message),
        buttons: [
          {
            text: t('Cancel'),
            role: 'cancel',
            handler: () => {
              return resolve(false);
            },
          },
          {
            text: t('OK'),
            handler: () => {
              return resolve(true);
            },
          },
        ],
      });

      await confirm.present();
    });
  }

  getSum(): number {
    let sum = 0;
    for (const classItem of this.condition.Classes) {
      sum += classItem.number * classItem.class.Point;
    }
    return sum;
  }

  getTotalPoint(): number {
    if (this.condition.ClassID > 0 && this.condition.Class) {
      return this.condition.Class.Point * this.condition.Period * 2 * this.condition.Person + this.condition.GiftPoint;
    } else {
      return this.getSum() * this.condition.Period * 2 + this.condition.GiftPoint;
    }
  }

  getTotalString(): string {
    if (this.condition) {
      if (this.classList.length > 2) {
        const lastIndex = this.classList.length - 2;
        return `${this.classList[0].Point * this.condition.Period * 2 * this.condition.Person}P ~
          ${this.classList[lastIndex].Point * this.condition.Period * 2 * this.condition.Person}P / 30分`;
      }
    }
    return '';
  }

  getExtendPoint(): number {
    if (this.condition.ClassID > 0) {
      if (this.condition.Class) {
        return this.condition.Class.Point * 1.3 / 2;
      } else {
        return 0;
      }
    } else {
      return this.getMixValue() * 1.3 / 2;
    }
  }

  async editGiftPoint(): Promise<void> {
    const modal = await this.modal.create({
      component: GiftComponent,
      cssClass: 'gift-modal ion-align-items-end'
    });
    await modal.present();
    await modal.onDidDismiss();
  }

  getPeople(): number {
    let sum = 0;
    for (const classItem of this.condition.Classes) {
      sum += classItem.number;
    }
    return sum;
  }

  getMixValue(): number {
    let sum = 0;
    for (const classItem of this.condition.Classes) {
      sum += classItem.number * classItem.class.Point;
    }
    return Math.ceil(sum / this.getPeople());
  }

  getClassString(): string {
    if (this.condition.Classes.length > 0) {
      const returnStr: string[] = [];
      for (const cItem of this.condition.Classes) {
        if (cItem.number > 0) {
          returnStr.push(cItem.class.Name + ': ' + cItem.number + '人');
        }
      }
      return `(${returnStr.join(', ')})`;
    } else {
      return '';
    }
  }

  getGuestLevels(): void {
    this.http.get<Class[]>(`${environment.API_URL}/admin/levels/guest`).subscribe(res => {
      this.guestLevels = res;
      this.levelsReady = true;
    });
  }

  guestAllowed(): boolean {
    if (this.levelsReady && this.guestLevels.length > 0) {
      return this.pointUsed >= this.guestLevels[0].Point;
    } else {
      return false;
    }
  }

  showNotify(typeStr: string): void {
    if (typeStr){
      this.showAlert(
        'キャストに匿名で予約を行う事ができます。 慣れ親しんだキャストに気づかれる事はありません。'
      );
    }
  }

}
