import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { IonInfiniteScroll, NavController } from '@ionic/angular';

import { Subscription } from 'rxjs';

import { ConditionService, AuthService, WebsocketService } from 'src/app/services';
import { Call, User } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-desire',
  templateUrl: './desire.component.html',
  styleUrls: ['./desire.component.scss'],
})
export class DesireComponent implements OnInit, OnDestroy {
  @ViewChild(IonInfiniteScroll) ionInfiniteScroll !: IonInfiniteScroll;

  condition !: Call;
  desiredCasts: User[] = [];

  // infinite scroll
  connectSubscription: Subscription | null = null;
  allLoaded = false;
  page = 0;
  offset = 0;

  // for slider
  slideOpts = {
    slidesPerView: 2.2,
    spaceBetween: 10
  };
  familyCasts: User[] = [];
  familyIDs = '';

  // condition subscriber
  condSubscriber: Subscription | null = null;

  // family casts pagination
  familyPage = 0;
  familySize = 10;
  familyAllLoaded = false;

  constructor(
    public cond: ConditionService,
    private http: HttpClient,
    private navi: NavController,
    private websocket: WebsocketService,
    private auth: AuthService
  ) {
    if (this.condSubscriber === null) {
      this.condSubscriber = this.cond.callConditionSubject.subscribe((condition) => {
        if (condition && condition.Status === 'update') {
          this.condition = condition.Call;
          // console.log(this.condition);
          this.desiredCasts = [];
          this.getDesiredCasts(null);
        }
      });
    }

    this.auth.currentUserSubject.subscribe(user => {
      if (user){
        if (this.familyIDs !== user.FamilyIDs){
          this.familyIDs = user.FamilyIDs;
          this.getFamilyCasts();
        }
      }
    });
  }

  ngOnInit(): void { }

  ngOnDestroy(): void {
    this.condSubscriber?.unsubscribe();
  }

  getFamilyCasts(): void {
    this.http.get<User[]>(`${environment.API_URL}/users/family?page=${this.familyPage}&size=${this.familySize}`).subscribe(casts => {
      if (casts.length < this.familySize) {
        this.familyAllLoaded = true;
      }
      this.familyCasts = this.familyCasts.concat(casts);
    });
  }

  moreFamilyData(_: Event): void {
    if (!this.familyAllLoaded){
      this.familyPage++;
      this.getFamilyCasts();
    }
  }

  getDesiredCasts(event: Event | null): void {
    const classIDs: number[] = this.condition.ClassID > 0 ? [this.condition.ClassID] : this.getMixIDs();
    this.http.post<User[]>(`${environment.API_URL}/casts/desire`, {
      ids: classIDs,
      location: this.condition.BigLocationID,
      page: this.page,
      size: environment.CAST_PER_PAGE,
      offset: this.offset
    }).subscribe((data) => {
      if (data.length < environment.CAST_PER_PAGE) {
        this.allLoaded = true;
      }
      this.desiredCasts = this.desiredCasts.concat(data);
      if (event && event.target) {
        this.ionInfiniteScroll.complete();
      }
      if (this.connectSubscription === null){
        this.connectSubscription = this.websocket.connectUserSubject.subscribe(res => {
          const desireIndex = this.desiredCasts.findIndex(item => item.ID === res.ID);

          if (desireIndex > -1){
            this.desiredCasts.splice(desireIndex, 1);
            this.desiredCasts.unshift(res);
          }else{
            this.desiredCasts.unshift(res);
            this.offset++;
          }
        });
      }
    }, (err: HttpErrorResponse) => {
      console.log(err);
    });
  }

  getMixIDs(): number[] {
    const retArray: number[] = [];
    for (const classItem of this.condition.Classes) {
      if (classItem.number > 0) {
        retArray.push(classItem.class.ID);
      }
    }
    return retArray;
  }

  loadData(event: Event | null): void {
    this.page++;
    this.getDesiredCasts(event);
  }

  // select cast
  selectCast(id: number): void {
    try {
      const index = this.condition.DesiredArray.indexOf(id);
      if (index > -1) {
        this.condition.DesiredArray.splice(index, 1);
      } else {
        this.condition.DesiredArray.push(id);
      }
    } catch (ex) {
      console.log(ex);
      this.condition.DesiredArray = [id];
    }
  }

  isMatching(id: number): boolean {
    return this.condition.DesiredArray.indexOf(id) > -1;
  }

  viewCast(profileID: string): void {
    this.navi.navigateForward(`main/profile/${profileID}`);
  }
}
