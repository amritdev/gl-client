import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { AlertController, ModalController, NavController, Platform } from '@ionic/angular';

import { Subscription } from 'rxjs';

import { ConditionService } from 'src/app/services';
import { Call, Class, Location } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';
import t from 'src/locales';
import * as moment from 'moment-timezone';

interface Options {
  name: string;
  value: number;
}

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
})
export class EditComponent implements OnInit, OnDestroy {

  @Input() isDialog = false;

  call !: Call;
  customDateStr !: string;

  meetTimes: string[] = [
    t('After 30 minutes from now'),
    t('After 60 minutes'),
    t('After 90 minutes'),
    t('Set By Date')
  ];

  persons: Options[] = [];
  periods: Options[] = [];
  parentLocations: Location[] = [];
  childLocations: Location[] = [];
  classList: Class[] = [];
  reverseClassList: Class[] = [];
  minClassPoint = 0;
  maxClassPoint = 0;

  // call subscriber
  condSubscriber: Subscription | null = null;

  // keyboard
  keyboardStyle = { width: '100%', height: '0px' };

  constructor(
    private modal: ModalController,
    private cond: ConditionService,
    private http: HttpClient,
    private alert: AlertController,
    private navi: NavController,
    private platform: Platform
  ) {
    if (this.condSubscriber === null) {
      this.condSubscriber = this.cond.callConditionSubject.subscribe((call) => {
        if (call && call.Status === 'update') {
          this.call = JSON.parse(JSON.stringify(call.Call));
          // console.log(this.call);
          this.getClasses();
          this.getChildLocations(this.cond.userLocation);

          if (call.Call.TimeOther) {
            this.customDateStr = moment.tz(call.Call.MeetTimeISO, 'Asia/Tokyo').format('YYYY-MM-DD[T]HH:mm:ss.SSS');
          }
        }
      });
    }

    this.platform.keyboardDidShow.subscribe((ev: {
      keyboardHeight: number;
    }) => {
      const { keyboardHeight } = ev;
      this.keyboardStyle.height = keyboardHeight + 'px';
    });

    this.platform.keyboardDidHide.subscribe(_ => {
      this.keyboardStyle.height = '0px';
    });
  }

  getChildLocations(pid: number): void {
    this.http.get<Location[]>(`${environment.API_URL}/locations?pid=${pid}`).subscribe(locations => {
      this.childLocations = locations;
    });
  }

  getClasses(): void {
    this.http.get<Class[]>(`${environment.API_URL}/admin/classes`).subscribe((res) => {
      this.classList = JSON.parse(JSON.stringify(res));
      this.reverseClassList = res.reverse();
      this.minClassPoint = this.classList[0].Point;
      if (this.classList.length >= 2) {
        this.maxClassPoint = this.classList[this.classList.length - 2].Point;
      }
    });
  }

  personChanged(): void {
    if (this.call.Classes.length > 1) {
      this.call.Classes[0].number = Math.floor(this.call.Person / 2);
      this.call.Classes[1].number = Math.ceil(this.call.Person / 2);
      this.call.Classes[2].number = 0;
    }
  }

  ngOnInit(): void {
    this.getPersons();
    this.getPeriods();
  }

  ngOnDestroy(): void {
    this.condSubscriber?.unsubscribe();
  }

  getPersons(): void {
    this.persons = [];
    for (let i = 1; i <= 30; i++) {
      this.persons.push({
        name: `${i}人`,
        value: i
      });
    }
  }

  getPeriods(): void {
    this.periods = [];
    for (let i = 1; i < 11; i++) {
      this.periods.push({
        name: `${i}時間`,
        value: i
      });
    }
  }

  onClose(): void {
    this.modal.dismiss();
  }

  onSave(): void {
    if (this.call.LocationID === 0 && this.call.OtherLocation.trim() === '') {
      this.showAlert(t('Please input cast meet location correctly'));
    }else{
      this.cond.callConditionSubject.next({
        Call: this.call,
        Status: 'update'
      });
      this.modal.dismiss();
    }
  }

  selectMeetTime(): void {
    if (this.call.MeetTime === t('Set By Date')) {
      this.call.TimeOther = true;
      this.call.MeetTimeISO = new Date().toISOString();
    } else {
      this.call.TimeOther = false;
    }
  }

  childLocationChanged(): void {
    const index = this.childLocations.findIndex(item => item.ID === this.call.LocationID);
    if (index > -1) {
      this.call.Location = this.childLocations[index];
    } else {
      this.call.Location = null;
    }
  }

  getThisYear(): number {
    return new Date().getFullYear();
  }

  classChanged(): void {
    const index = this.classList.findIndex(item => item.ID === this.call.ClassID);
    if (index > -1) {
      this.call.Class = this.classList[index];
    } else {
      this.call.Class = null;
    }
  }

  onMinus(index: number): void {
    if (this.call.Classes[index].number >= 1) {
      this.call.Classes[index].number--;
    }
  }

  onPlus(index: number): void {
    if (this.call.Person - this.getSum() > 0) {
      this.call.Classes[index].number++;
    }
  }

  getSum(): number {
    let sum = 0;
    for (const classItem of this.call.Classes) {
      sum += classItem.number;
    }
    return sum;
  }

  getMixValue(): number {
    let sum = 0;
    for (const classItem of this.call.Classes) {
      sum += classItem.number * classItem.class.Point;
    }
    return Math.ceil(sum / this.getSum());
  }

  goToNextStep(): void {
    this.cond.callConditionSubject.next({
      Call: this.call,
      Status: 'update'
    });
    this.navi.navigateForward('main/call/situation');
  }

  getPointRange(): string {
    if (this.classList.length > 2) {
      const lastIndex = this.classList.length - 2;
      return `${this.classList[0].Point * this.call.Period * 2 * this.call.Person}P ~
        ${this.classList[lastIndex].Point * this.call.Period * 2 * this.call.Person}P / 30分`;
    } else {
      return '';
    }
  }

  async showAlert(msgStr: string): Promise<void> {
    const alert = await this.alert.create({
      message: msgStr,
      buttons: ['OK']
    });
    await alert.present();
  }

}
