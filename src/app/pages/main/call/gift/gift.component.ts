import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ModalController } from '@ionic/angular';

import { ConditionService } from 'src/app/services';
import { Call, Gift } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-gift',
  templateUrl: './gift.component.html',
  styleUrls: ['./gift.component.scss'],
})
export class GiftComponent implements OnInit {

  condition !: Call;
  giftSum = 0;
  giftToday = 5000;
  gifts: Gift[] = [];

  constructor(
    private modal: ModalController,
    private cond: ConditionService,
    private http: HttpClient
  ) {
    this.cond.callConditionSubject.subscribe((condition) => {
      if (condition && condition.Status === 'update'){
        this.condition = condition.Call;
        // console.log(this.condition);
        this.giftSum = this.condition.GiftPoint;
      }
    });
  }

  ngOnInit(): void {
    this.getAvailableGifts();
    this.getMaxGifts();
  }

  getMaxGifts(): void{
    this.http.get<{
      Max: number
    }>(`${environment.API_URL}/calls/maxGift`).subscribe((res) => {
      this.giftToday = res.Max;
    });
  }

  confirmGift(): void{
    this.condition.GiftPoint = this.giftSum;
    this.cond.callConditionSubject.next({
      Call: this.condition,
      Status: 'update'
    });
    this.modal.dismiss();
  }

  getValue(): number{
    if (this.giftSum >= 50000){
      return 1;
    }else if (this.giftSum >= 10000){
      return 5 / 8;
    }else if (this.giftSum >= 5000 ){
      return 3 / 8;
    }else{
      return 0;
    }
  }

  getAvailableGifts(): void{
    this.http.get<Gift[]>(`${environment.API_URL}/admin/gifts/call`).subscribe((res) => {
      this.gifts = res;
    });
  }

  upSum(giftPoint: number): void{
    this.giftSum += giftPoint;
  }

  formatGift(): void{
    this.giftSum = 0;
  }

}
