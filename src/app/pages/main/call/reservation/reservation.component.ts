import { Component, OnInit, Input } from '@angular/core';
import { AlertController, ModalController, NavParams, Platform } from '@ionic/angular';
import { Class, Location } from 'src/app/interfaces';
import * as moment from 'moment-timezone';
import t from 'src/locales';

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.scss'],
})
export class ReservationComponent implements OnInit {

  @Input() choices: {name: string, value: number}[] | string[] | number[] | Class[] | Location[] = [];
  @Input() mode = 'time';
  @Input() firstValue: string | number = '';
  @Input() place = '';
  @Input() reservationName = '';
  @Input() otherLoc = '';
  @Input() mixStr = '';

  keyboardStyle = { width: '100%', height: '0px' };
  callId = 0;
  timeOther = false;
  meetTimeISO = '';
  periodOther = false;

  locations: Location[] = [];
  classes: Class[] = [];
  persons: {name: string; value: number; }[] = [];
  periods: {name: string; value: number; }[] = [];
  times: string[] = [];

  constructor(
    private modal: ModalController,
    private alert: AlertController,
    private platform: Platform,
    private navParams: NavParams
  ) {
    this.callId = this.navParams.get('callId');
    this.platform.keyboardDidShow.subscribe((ev: {
      keyboardHeight: number;
    }) => {
      const { keyboardHeight } = ev;
      this.keyboardStyle.height = keyboardHeight + 'px';
    });

    this.platform.keyboardDidHide.subscribe(_ => {
      this.keyboardStyle.height = '0px';
    });
  }

  ngOnInit(): void {
    switch (this.mode){
      case 'time':
        this.times = this.choices as string[];
        break;
      case 'person':
        this.persons = this.choices as {name: string; value: number; }[];
        break;
      case 'period':
        this.periods = this.choices as {name: string; value: number; }[];
        break;
      case 'class':
        this.classes = this.choices as Class[];
        break;
      case 'location':
        this.locations = this.choices as Location[];
        break;
    }
  }

  send(): void{
    switch (this.mode) {
      case 'time':
        if (this.timeOther){
          const now = moment().add(30, 'minute');
          const meetTime = moment(this.meetTimeISO);
          if (now >= meetTime){
            this.showAlert(t('You can not set meet time earlier than 30 minutes after now'));
            return;
          }
        }
        this.modal.dismiss({
          timeOther: this.timeOther,
          meetTimeISO: this.meetTimeISO,
          meetTime: this.firstValue
        });
        break;
      case 'person':
        this.modal.dismiss({
          person: this.firstValue
        });
        break;
      case 'period':
        this.modal.dismiss({
          period: this.firstValue
        });
        break;
      case 'class':
        this.modal.dismiss({
          classID: this.firstValue,
        });
        break;
      case 'location':
        if (this.firstValue === 0 && this.otherLoc.trim() === ''){
          this.showAlert(t('Please input meeting place'));
        }else{
          this.modal.dismiss({
            locationID: this.firstValue,
            place: this.place,
            reservation: this.reservationName,
            otherLocation: this.otherLoc.trim()
          });
        }
        break;
    }

  }

  onClose(): void{
    this.modal.dismiss();
  }

  timeButtonClicked(option: string): void {
    this.firstValue = option;
    if (option === t('Set By Date')) {
      this.timeOther = true;
      this.meetTimeISO = new Date().toISOString();
    } else {
      this.timeOther = false;
    }
  }

  periodButtonClicked(option: number): void {
    if (option <= 3) {
      this.firstValue = option;
      this.periodOther = false;
    }else {
      this.firstValue = 4;
      this.periodOther = true;
    }
  }

  getThisYear(): number {
    return new Date().getFullYear();
  }

  async showAlert(msgStr: string): Promise<void> {
    const alert = await this.alert.create({
      message: msgStr,
      buttons: ['OK']
    });
    await alert.present();
  }
}

