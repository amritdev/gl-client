import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { NavController, ModalController } from '@ionic/angular';
import { Subscription } from 'rxjs';

import { ConditionService, AuthService } from 'src/app/services';
import { Call, Tag, Class } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';

import { GiftComponent } from '../gift/gift.component';

@Component({
  selector: 'app-situation',
  templateUrl: './situation.component.html',
  styleUrls: ['./situation.component.scss'],
})
export class SituationComponent implements OnInit, OnDestroy {

  @Input() isDialog = false;

  situationCat: Tag[] = [];
  guestPrivileged = true;
  childTags: { [key: number]: Tag[] } = {};
  condition !: Call;
  tagsReady = false;
  sum = 0;
  guestLevels: Class[] = [];
  levelsReady = false;
  pointUsed = 0;

  // condition subscriber
  condSubscriber: Subscription | null = null;
  authSubscriber: Subscription | null = null;

  constructor(
    private http: HttpClient,
    private cond: ConditionService,
    private nav: NavController,
    private auth: AuthService,
    private modal: ModalController
  ) {
    if (this.authSubscriber === null) {
      this.authSubscriber = this.auth.currentUserSubject.subscribe((user) => {
        if (user) {
          this.pointUsed = user.PointUsed;
        }
      });
    }

    if (this.condSubscriber === null) {
      this.condSubscriber = this.cond.callConditionSubject.subscribe((condition) => {
        if (condition && condition.Status === 'update') {
          this.condition = JSON.parse(JSON.stringify(condition.Call));

          // get today situation from local storage
          const todayMoodStr = localStorage.getItem('today-situation');
          let todayMood: number[];
          if (todayMoodStr) {
            todayMood = JSON.parse(todayMoodStr);
          } else {
            todayMood = [];
          }

          this.condition.SituationIDArray = todayMood;
        }
      });
    }
  }

  ngOnInit(): void {
    this.getParentTags();
    this.getGuestLevels();
  }

  ngOnDestroy(): void {
    this.condSubscriber?.unsubscribe();
    this.authSubscriber?.unsubscribe();
  }

  getGuestLevels(): void {
    this.http.get<Class[]>(`${environment.API_URL}/admin/levels/guest`).subscribe(res => {
      this.guestLevels = res;
      this.levelsReady = true;
    });
  }

  guestAllowed(): boolean {
    if (this.levelsReady && this.guestLevels.length > 0) {
      return this.pointUsed >= this.guestLevels[0].Point;
    } else {
      return false;
    }
  }

  getParentTags(): void {
    this.http.get<Tag[]>(`${environment.API_URL}/admin/situations/parent`).subscribe(tags => {
      this.situationCat = tags;
      for (const [index, parentItem] of this.situationCat.entries()) {
        this.http.get<Tag[]>(`${environment.API_URL}/admin/situations/children/${parentItem.ID}`).subscribe((cdata) => {
          this.childTags[parentItem.ID] = cdata;
          if (index === this.situationCat.length - 1) {
            this.tagsReady = true;
          }
        });
      }
    });
  }

  clickTags(curTag: Tag): void {
    if (this.condition.SituationIDArray.includes(curTag.ID)) {
      this.condition.SituationIDArray.splice(this.condition.SituationIDArray.indexOf(curTag.ID, 0), 1);
    } else {
      this.condition.SituationIDArray.push(curTag.ID);
    }
  }

  goToNextStep(): void {
    // this.condSubscriber?.unsubscribe();
    // this.authSubscriber?.unsubscribe();
    localStorage.setItem('today-situation', JSON.stringify(this.condition.SituationIDArray));
    this.cond.callConditionSubject.next({
      Call: this.condition,
      Status: 'update'
    });
    if (this.condition.Person > 1){
      this.nav.navigateForward('/main/call/desire');
    }else{
      this.nav.navigateForward('/main/call/warning');
    }
  }

  onSave(): void {
    this.cond.callConditionSubject.next({
      Call: this.condition,
      Status: 'update'
    });
    this.modal.dismiss();
  }

  onClose(): void {
    this.modal.dismiss();
  }

  async presentGiftSelect(): Promise<void> {
    const modal = await this.modal.create({
      component: GiftComponent,
      cssClass: 'gift-modal ion-align-items-end'
    });
    await modal.present();
    await modal.onDidDismiss();
  }
}
