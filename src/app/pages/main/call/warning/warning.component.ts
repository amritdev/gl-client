import { Component, OnInit, OnDestroy } from '@angular/core';

import { Subscription } from 'rxjs';

import { ConditionService } from 'src/app/services';
import { Call } from 'src/app/interfaces';

@Component({
  selector: 'app-warning',
  templateUrl: './warning.component.html',
  styleUrls: ['./warning.component.scss'],
})
export class WarningComponent implements OnInit, OnDestroy {

  condition !: Call;
  gotCondition = false;

  // condition subscriber
  condSubscriber: Subscription | null = null;

  constructor(
    private cond: ConditionService
  ) {
    if (this.condSubscriber === null) {
      this.condSubscriber = this.cond.callConditionSubject.subscribe((condition) => {
        if (condition && condition.Status === 'update') {
          this.condition = condition.Call;
          this.gotCondition = true;
        }
      });
    }
  }

  getExtendPoint(): number {
    if (this.condition.ClassID > 0) {
      if (this.condition.Class) {
        return this.condition.Class.Point * 1.3 / 2;
      } else {
        return 0;
      }
    } else {
      return this.getMixValue() * 1.3 / 2;
    }
  }

  getSum(): number {
    let sum = 0;
    for (const classItem of this.condition.Classes) {
      sum += classItem.number;
    }
    return sum;
  }

  getMixValue(): number {
    let sum = 0;
    for (const classItem of this.condition.Classes) {
      sum += classItem.number * classItem.class.Point;
    }
    return Math.ceil(sum / this.getSum());
  }

  ngOnInit(): void { }

  ngOnDestroy(): void {
    this.condSubscriber?.unsubscribe();
  }

}
