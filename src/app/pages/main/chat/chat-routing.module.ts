import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InvoiceGuard, RoomGuard } from 'src/app/guards';

import { ChatComponent } from './chat.component';
import { DetailComponent } from './detail/detail.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { ShareComponent } from './share/share.component';

const routes: Routes = [
  {
    path: '',
    component: ChatComponent,
  },
  {
    path: 'detail/:id',
    component: DetailComponent,
    canActivate: [RoomGuard]
  },
  {
    path: 'invoice/:callid',
    component: InvoiceComponent
  },
  {
    path: 'share',
    component: ShareComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChatRoutingModule { }
