import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { IonInfiniteScroll, AlertController, NavController } from '@ionic/angular';
import { AuthService, ConditionService, WebsocketService } from 'src/app/services';
import { User, Room, ResRoom, Message } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';
import t from 'src/locales';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss'],
})
export class ChatComponent implements OnInit, OnDestroy {
  @ViewChild(IonInfiniteScroll) ionInfScrollAll !: IonInfiniteScroll;

  user: User | null = null;

  pageIndex = 0;
  offset = 0;
  isAllLoaded = false;
  onceLoaded = false;
  canNotSee = false;
  reloadSubscription: Subscription | null = null;
  roomRemoveSubscription: Subscription | null = null;
  roomSubscription: Subscription | null = null;
  messageSubscription: Subscription | null = null;
  withdrawnReloadSubscription: Subscription | null = null;

  segment: 'all' | 'favorite' = 'all';
  searchbarNickname: { [key: string]: string } = {
    all: '',
    favorite: '',
  };

  searchTerm$: { [key: string]: Subject<string> } = {
    all: new Subject<string>(),
    favorite: new Subject<string>(),
  };

  filter = '0';
  filterText = t('Message List');
  rooms: { [key: string]: ResRoom[] } = {
    all: [],
    favorite: []
  };

  constructor(
    private authService: AuthService,
    private http: HttpClient,
    private webSocket: WebsocketService,
    private navi: NavController,
    private alert: AlertController,
    public cond: ConditionService
  ) {

    this.authService.currentUserSubject.subscribe(user => {
      // if BlockedRoomIDs and FavoriteRoomIDs change
      if (this.user && user) {
        if (this.user.FavoriteRoomIDs !== user.FavoriteRoomIDs) {
          this.doRefresh('all', '');
          this.doRefresh('favorite', '');
        }
      }

      this.user = user ? { ...user } : null;
      // if (user) {
      //   if (user.Role === 1) {
      //     this.canNotSee = !(user.TelecomCredit && !user.TelecomFailed);
      //   } else {
      //     this.canNotSee = false;
      //   }
      // }
    });

    if (this.reloadSubscription === null) {
      this.reloadSubscription = this.authService.reloadSubject.subscribe(res => {
        if (res) {
          this.doRefresh('all', this.searchbarNickname.all);
          this.doRefresh('favorite', this.searchbarNickname.favorite);
        }
      });
    }

    if (this.roomSubscription === null) {
      this.roomSubscription = this.webSocket.roomSubject.subscribe((resRoom) => {
        if (this.onceLoaded) {
          const index = this.rooms.all.findIndex(item => item.Room.ID === resRoom.Room.ID);
          if (index > -1) {
            this.rooms.all[index] = resRoom;
          } else {
            this.rooms.all.unshift(resRoom);
            this.offset++;
          }
        }
      });
    }

    if (this.roomRemoveSubscription === null) {

      this.roomRemoveSubscription = this.webSocket.roomRemoveSubject.subscribe((resRoom) => {
        if (this.onceLoaded && this.user) {
          const allIndex = this.rooms.all.findIndex(item => item.Room.ID === resRoom.Room.ID);
          if (allIndex > -1) {
            if (resRoom.UserID === this.user.ID || resRoom.UserID === -1) {
              this.rooms.all.splice(allIndex, 1);
              this.offset--;

              // reset unread messages
              this.http.get<{
                data: number
              }>(`${environment.API_URL}/users/message/unread`).subscribe((res) => {
                this.webSocket.unreadMessages = res.data;
              });
            } else {
              const removeAllIndex = this.rooms.all[allIndex].Room.Users.findIndex(item => item.ID === resRoom.UserID);
              if (removeAllIndex > -1) {
                this.rooms.all[allIndex].Room.Users.splice(removeAllIndex, 1);
              }
            }
          }

          const favoriteIndex = this.rooms.favorite.findIndex(item => item.Room.ID === resRoom.Room.ID);
          if (favoriteIndex > -1) {
            if (resRoom.UserID === this.user.ID || resRoom.UserID === -1) {
              this.rooms.favorite.splice(favoriteIndex, 1);
            } else {
              const removeFavIndex = this.rooms.favorite[allIndex].Room.Users.findIndex(item => item.ID === resRoom.UserID);
              if (removeFavIndex > -1) {
                this.rooms.favorite[allIndex].Room.Users.splice(removeFavIndex, 1);
              }
            }
          }
        }
      });
    }

    if (this.messageSubscription === null) {
      this.messageSubscription = this.webSocket.messageSubject.subscribe(wsData => {
        // console.log(wsData);
        const data: Message = wsData.Data as Message;
        if (this.user && data.SenderID !== this.user.ID) {
          // console.log('MESSAGE ARRIVED');
          // console.log(data);
          for (const mode of ['all', 'favorite']) {
            const allIndex = this.rooms[mode].findIndex(item => item.Room.ID === data.RoomID);
            if (allIndex > -1) {
              this.rooms[mode][allIndex].Room.LastMessage = data.Content ?
                this.replaceURLs(data.Content) : (data.Image ? t('Image Sent') : t('Gift Sent'));
              const curRoomID = localStorage.getItem('curRoomID');
              if (curRoomID === null || (curRoomID && parseInt(curRoomID, 10) !== this.rooms[mode][allIndex].Room.ID)) {
                this.rooms[mode][allIndex].Unread++;
              }
              if (data.CreatedAt) {
                this.rooms[mode][allIndex].Room.UpdatedAt = data.CreatedAt;
                this.rooms[mode].unshift(this.rooms[mode].splice(allIndex, 1)[0]);
              }
            } else{
              if (mode === 'all') {
                const params = new HttpParams().append('room_id', data.RoomID.toString());
                this.http.get<ResRoom>(`${environment.API_URL}/users/check/room`, {params}).subscribe(res => {
                  // console.log(res);
                  this.rooms[mode].unshift(res);
                });
              }
            }
          }
        }
      });
    }

  }

  ngOnInit(): void {
    this.searchTerm$.all.pipe(
      debounceTime(400),
      distinctUntilChanged(),
    ).subscribe((searchTerm: string) => {
      this.doRefresh('all', searchTerm);
    });

    this.searchTerm$.favorite.pipe(
      debounceTime(400),
      distinctUntilChanged(),
    ).subscribe((searchTerm: string) => {
      this.doRefresh('favorite', searchTerm);
    });

    this.getChatrooms(null, this.segment, '');
  }

  ngOnDestroy(): void {
    this.reloadSubscription?.unsubscribe();
    this.roomRemoveSubscription?.unsubscribe();
    this.roomSubscription?.unsubscribe();
    this.messageSubscription?.unsubscribe();
  }

  async showAlert(msgStr: string): Promise<void> {
    const alert = await this.alert.create({
      message: msgStr,
      buttons: ['OK']
    });
    await alert.present();
  }

  replaceURLs(message: string): string {
    if (!message) { return ''; }

    const urlRegex = /(((https?:\/\/)|(www\.))[^\s]+)/g;
    return message.replace(urlRegex, (url) => {
      let hyperlink = url;
      if (!hyperlink.match('^https?:\/\/')) {
        hyperlink = 'http://' + hyperlink;
      }
      return '<a href="' + hyperlink + '" target="_blank" class="message-link">' + url + '</a>';
    });
  }

  getNotifyCount(): number {
    return this.webSocket.unreadNotices;
  }

  doRefresh(segment: 'all' | 'favorite', searchKeyword: string): void {
    if (segment === 'all') {
      this.pageIndex = 0;
      this.offset = 0;
      this.isAllLoaded = false;
    }
    this.rooms[segment] = [];
    this.getChatrooms(null, segment, searchKeyword);
  }

  getChatrooms(event: Event | null, segment: 'all' | 'favorite', searchKeyword: string): void {
    if (segment === 'all') {
      this.http.get<ResRoom[]>(`${environment.API_URL}/users/room?` +
        `mode=${segment}&keyword=${searchKeyword}&page=${this.pageIndex}&offset=${this.offset}&filter=${this.filter}`
        ).subscribe(rooms => {
          if (rooms.length < 15) {
            this.isAllLoaded = true;
          }
          // console.log(rooms);
          this.rooms[segment] = [...this.rooms[segment], ...rooms];
          // console.log(this.rooms);
          if (event && event.target) {
            this.ionInfScrollAll.complete();
          } else {
            this.onceLoaded = true;
          }

          if (event === null){
            if (this.withdrawnReloadSubscription === null){
              this.withdrawnReloadSubscription = this.webSocket.reloadSubject.subscribe(eventString => {
                if (eventString === 'message'){
                  this.doRefresh('all', this.searchbarNickname.all);
                  this.doRefresh('favorite', this.searchbarNickname.favorite);
                }
              });
            }
          }
        });
    } else {
      this.http.get<ResRoom[]>(`${environment.API_URL}/users/room?` +
        `mode=${segment}&keyword=${searchKeyword}`).subscribe(rooms => {
          // console.log(rooms);
          this.rooms[segment] = rooms;
        });
    }
  }

  filterCasts(users: User[]): User[] {
    return users.filter((item) => {
      return item.Role >= 0;
    });
  }

  getPartner(room: Room): User {
    return room.Users.find(user => user.ID !== this.user?.ID) ?? ({} as User);
  }

  onChangeSearchbarNickname(messageSegment: string): void {
    this.searchTerm$[messageSegment].next(this.searchbarNickname[messageSegment].trim());
  }

  onFilterChanged(event: Event): void {
    if (event instanceof CustomEvent) {
      this.filter = event.detail.value;
      this.searchbarNickname.all = '';
      this.searchbarNickname.favorite = '';
      this.doRefresh('all', '');
      this.doRefresh('favorite', '');
      switch (event.detail.value) {
        case '0':
          this.filterText = t('Message List');
          break;
        case '1':
          this.filterText = t('Unread Messages');
          break;
        case '2':
          this.filterText = t('Confluence Confirmed');
          break;
        case '3':
          this.filterText = t('Confluence Wating');
          break;
        case '4':
          this.filterText = t('Confluence History');
          break;
        default:
          this.filterText = t('Message List');
      }
    }
  }

  loadData(event: Event | null): void {
    this.pageIndex++;
    this.getChatrooms(event, 'all', this.searchbarNickname.all);
  }

  onSegmentChanged(): void {
    if (this.segment === 'favorite') {
      this.doRefresh('favorite', this.searchbarNickname.favorite);
    }
  }

  toDetail(id: number, index: number): void {

    if (this.user && this.user.TelecomFailed) {
      if (this.rooms[this.segment][index].Room.Type !== 'admin') {
        this.showAlert(t('Pay with your telecom card failed. Consult with glass consultant.'));
        return;
      }
    }
    this.webSocket.unreadMessages -= this.rooms[this.segment][index].Unread;
    this.rooms[this.segment][index].Unread = 0;

    localStorage.setItem('curRoomID', id.toString());
    let curIndex = 0;
    if (this.segment === 'all') {
      curIndex = this.rooms.favorite.findIndex(item => item.Room.ID === id);
      if (curIndex > -1) {
        this.rooms.favorite[curIndex].Unread = 0;
      }
    } else {
      curIndex = this.rooms.all.findIndex(item => item.Room.ID === id);
      if (curIndex > -1) {
        this.rooms.all[curIndex].Unread = 0;
      }
    }
    this.navi.navigateForward(`/main/chat/detail/${id}`);
  }

  getPartnerImage(room: Room): string {
    const partner = this.getPartner(room);
    if (partner !== undefined && partner.Images.length > 0 && partner.Withdrawn === false) {
      return partner.Images[0].Path;
    } else {
      return 'assets/img/withdrawn.png';
    }
  }

  gotoLine(): void{
    window.location.href = 'https://lin.ee/6fAJj9Tg';
  }
}
