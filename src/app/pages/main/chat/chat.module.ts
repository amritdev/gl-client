import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { PipesModule, DirectivesModule } from 'src/app/shared';
import { ChatRoutingModule } from './chat-routing.module';
import { ChatComponent } from './chat.component';
import { ShareComponent } from './share/share.component';
import { ValueComponent } from './value/value.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { DetailComponent } from './detail/detail.component';
import { ReportComponent } from './report/report.component';
import { MemoComponent } from './memo/memo.component';

@NgModule({
  declarations: [
    ChatComponent,
    ShareComponent,
    ValueComponent,
    InvoiceComponent,
    DetailComponent,
    ReportComponent,
    MemoComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    PipesModule,
    DirectivesModule,
    ChatRoutingModule,
  ],
})
export class ChatModule { }
