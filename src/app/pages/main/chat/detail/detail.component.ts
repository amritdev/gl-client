import { Component, OnInit, ViewChild, ElementRef, Inject, PLATFORM_ID, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { isPlatformBrowser, formatDate } from '@angular/common';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { ActionSheetButton } from '@ionic/core';
import {
  IonContent, IonInfiniteScroll, AlertController,
  NavController, ActionSheetController, ModalController, IonRouterOutlet, Platform, ToastController
} from '@ionic/angular';
import { ReviewComponent } from 'src/app/components/review/review.component';
import { ReportComponent } from '../report/report.component';
import * as moment from 'moment';
import { AuthService, ConditionService, GeoLocationService, WebsocketService } from 'src/app/services';
import { User, Room, Message, Image, Gift, Class, Join, GiftData, Call } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';
import t from 'src/locales';
import { Subscription } from 'rxjs';
import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations';
import { MemoComponent } from '../memo/memo.component';

interface GiftWithChildren extends Gift {
  Children?: Gift[];
}

interface Options {
  name: string;
  value: number;
}

export type FadeState = 'visible' | 'hidden';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
  animations: [
    trigger('textState', [
      state(
        'visible',
        style({
          opacity: '1'
        })
      ),
      state(
        'hidden',
        style({
          opacity: '0'
        })
      ),
      transition('* => visible', [animate('500ms ease-out')]),
      transition('visible => hidden', [animate('500ms ease-out')])
    ])
  ],
})
export class DetailComponent implements OnInit, OnDestroy {

  @ViewChild('ionContent') ionContent!: IonContent;
  @ViewChild('ionInfiniteScroll') ionInfiniteScroll!: IonInfiniteScroll;
  @ViewChild('fileInput') fileInput!: ElementRef;

  roomId = 0;
  consultant: User | null = null;
  user: User | null = null;
  partner: User | null = null;
  media: File | null = null;
  mediaURL = '';
  room: Room | null = null;
  roomType = 'private';
  call: Call | undefined;
  messages: Message[] = [];
  gifts: GiftWithChildren[] = [];
  selectedGiftTab = '';
  selectedGifts: Gift[] = [];
  isVisibleGiftContainer = false;
  isAllLoaded = false;
  pageIndex = 0;
  offset = 0;
  message = '';
  favoriteRoomIDs: number[] = [];
  blockedRoomIDs: number[] = [];
  guestLevels: Class[] = [];
  levelsReady = false;
  pointUsed = 0;
  isGuest = false;
  join: Join | null = null;
  canJoin = false;
  allInfoReady = false;
  canNotSee = false;
  maxNumber = 0;
  calls: Call[] = [];
  callHistoryVisible = false;

  // for suggestion
  persons: Options[] = [];
  periods: Options[] = [];

  suggestionPanel = false;
  meetTime = '';
  person = 1;
  period = 2;
  navigationID = 1;

  // for timer
  totalSec = 0;
  interval: number | null = null;

  messageSubscription: Subscription | null = null;
  authSubscription: Subscription | null = null;
  joinSubscription: Subscription | null = null;
  suggestSubscription: Subscription | null = null;
  roomRemoveSubscription: Subscription | null = null;
  roomSubscription: Subscription | null = null;
  callRemoveSubscription: Subscription | null = null;

  // modal variable
  reviewModal: HTMLIonModalElement | null = null;

  // fade in and out
  textState: FadeState;
  textVisible = true;
  isIOS = false;

  constructor(
    @Inject(PLATFORM_ID) platformID: object,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private wsService: WebsocketService,
    private http: HttpClient,
    private navi: NavController,
    private alert: AlertController,
    private routerOutlet: IonRouterOutlet,
    private modal: ModalController,
    public cond: ConditionService,
    private toastr: ToastController,
    private gps: GeoLocationService,
    private actionSheetController: ActionSheetController,
    private platform: Platform
  ) {
    this.textState = 'visible';
    this.isIOS = this.platform.is('ios') && !this.platform.is('mobileweb');

    const idParam = this.route.snapshot.paramMap.get('id');

    if (idParam) {
      this.roomId = parseInt(idParam, 10);
      this.messages = [];
      this.resetMessageUnreads(this.roomId);

      this.http.get<{
        'type': string;
        'maxNumber': number;
      }>(`${environment.API_URL}/users/room/${this.roomId}/type`).subscribe(res => {
        this.roomType = res.type;
        this.maxNumber = res.maxNumber;
        // console.log(res);

        if (isPlatformBrowser(platformID)) {
          if (!(/like Mac OS X/.test(window.navigator.userAgent))) {
            this.getMessages(this.roomId);
          }
        }
      });
    }

    // get consultant id and avatar

    this.http.get<User>(`${environment.API_URL}/users/consultant`).subscribe((res) => {
      this.consultant = res;
    });

    this.authService.giftSubject.subscribe(giftData => {
      if (giftData && this.room && this.call) {
        this.call.GiftPoint = giftData.Gift;
        this.call.GiftDetail = giftData.GiftStr;
        // console.log(this.call);
      }
    });
  }

  ngOnInit(): void {
    this.getPersons();
    this.getPeriods();
    this.getGuestLevels();
    if (this.authSubscription === null) {
      this.authService.currentUserSubject.subscribe(user => {
        if (user) {
          let isFirst = true;
          if (this.user) {
            isFirst = false;
          }
          this.user = user ? { ...user } : null;

          // check telecom failed
          if (user.TelecomFailed) {
            if (this.room && this.room.Type !== 'admin') {
              this.navi.navigateRoot('/main/chat');
            }
          }

          if (this.user && this.user.Role === 1) {
            this.pointUsed = this.user.PointUsed;
          }

          if (isFirst) {
            this.getRoom(this.roomId);
          }

          this.favoriteRoomIDs = JSON.parse(user?.FavoriteRoomIDs ?? '[]');
          this.blockedRoomIDs = JSON.parse(user?.BlockedRoomIDs ?? '[]');
          this.favoriteRoomIDs = this.favoriteRoomIDs.filter(roomID => roomID > 0);
          this.blockedRoomIDs = this.blockedRoomIDs.filter(roomID => roomID > 0);

          if (this.blockedRoomIDs.includes(this.roomId)){
            this.navi.navigateRoot('/main/chat');
          }
        }
      });
    }
    const navigation = this.router.getCurrentNavigation();
    this.navigationID = navigation?.id ?? 1;

    this.getGifts();
    setTimeout(() => {
      this.textState = 'hidden';
    }, 4000);
  }

  ngOnDestroy(): void {
    this.messageSubscription?.unsubscribe();
    this.authSubscription?.unsubscribe();
    this.joinSubscription?.unsubscribe();
    this.roomRemoveSubscription?.unsubscribe();
    this.roomSubscription?.unsubscribe();
    localStorage.removeItem('curRoomID');
  }

  async openReviewDialog(call: Call): Promise<void> {
    if (this.room) {
      // console.log(this.room.ID);
      if (this.reviewModal === null) {
        // console.log("now dialog opening");
        this.reviewModal = await this.modal.create({
          component: ReviewComponent,
          swipeToClose: (this.user?.Role ?? 0) === 1,
          backdropDismiss: this.user?.Role === 1,
          componentProps: {
            roomID: this.room.ID,
            call,
            role: this.user?.Role ?? 0,
            must: (this.user?.Role ?? 0) === 0
          },
        });
        if (this.user?.Role === 1){
          this.reviewModal.presentingElement = this.routerOutlet.nativeEl;
        }
        this.reviewModal.onDidDismiss().then(() => {
          this.reviewModal = null;
        });
        await this.reviewModal.present();
      }
    }
  }

  async suggestCall(): Promise<void> {
    if (this.user && this.partner) {
      if (this.user.Role === 1 && !this.user.TelecomCredit) {
        const confirmation = await this.warn(t('Credit card info is not registered. Are you gonna register your card?'), t('Call after Credit card registration'));
        if (confirmation) {
          // to register credit card
          this.navi.navigateForward('main/mypage/payment');
        }
      } else {
        let periodStr = `${this.period}時間\n`;
        if (Math.ceil(this.period) !== Math.floor(this.period)){
          periodStr = `${Math.floor(this.period)}時間${(this.period - Math.floor(this.period)) * 60}分\n`;
        }

        // make message
        const messageContent = '下記の提案いかがでしょうか？\n' +
          '日程：' + formatDate(this.meetTime, 'yyyy年 MM月 dd日 HH:mm', 'en', '+900') + '～\n' +
          '人数：' + this.person + '人、' +
          '時間：' + periodStr +
          '消費ポイント：' + (this.user.Role === 1 ? this.partner.PointHalf : this.user.PointHalf) * this.person * this.period * 2 + 'pt\n' +
          '延長ポイント：' + (this.user.Role === 1 ? this.partner.PointHalf : this.user.PointHalf) * 1.3 / 2 + 'pt / 15 分 ';

        this.sendMessage(messageContent);

        // suggest
        this.http.post<{
          'success': boolean,
          'call': Call
        }>(`${environment.API_URL}/calls/suggest`, {
          time: formatDate(this.meetTime, 'yyyy-MM-dd HH:mm:00', 'en', '+900'),
          period: this.period,
          person: this.person,
          roomId: this.roomId,
          partnerId: this.partner.ID
        }).subscribe((res) => {
          if (res.success) {
            this.suggestionPanel = false;
            this.call = res.call;
            const callIndex = this.calls.findIndex(item => item.ID === res.call.ID);
            if (callIndex > -1){
              this.calls[callIndex] = res.call;
            }else{
              this.calls.push(res.call);
            }
          } else {
            this.showAlert(t('You should respond to the suggestion of your partner first'));
          }
        }, (err: HttpErrorResponse) => {
          console.log(err);
        });
      }
    }
  }

  replaceURLs(message: string): string {
    if (!message) { return ''; }

    const urlRegex = /(((https?:\/\/)|(www\.))[^\s]+)/g;
    return message.replace(urlRegex, (url) => {
      let hyperlink = url;
      if (!hyperlink.match('^https?:\/\/')) {
        hyperlink = 'http://' + hyperlink;
      }
      return '<a href="' + hyperlink + '" target="_blank" class="message-link">' + url + '</a>';
    });
  }

  sendMessage(messageContent: string): void {
    // send message
    if (this.room && this.user) {
      const urlReplacedText = this.replaceURLs(messageContent);

      this.messages.push({
        Content: urlReplacedText,
        ImageID: null,
        Image: null,
        GiftID: null,
        Gift: null,
        RoomID: this.room.ID,
        Room: this.room,
        SenderID: this.user.ID,
        Sender: this.user,
        ReceiverID: this.user.ID,
        Receiver: this.user,
        Number: this.maxNumber + 1,
        CreatedAt: moment().format(),
      });
      this.scrollToBottom();
      const messages: Message[] = [];
      const receiverIDs: number[] = [];

      this.room.Users.forEach(user => {
        if (this.room) {
          if (!this.room.IsGroup || user.Role >= 0) {
            if (user.ID === this.user?.ID) {
              messages.push({
                Content: urlReplacedText,
                IsRead: true,
                RoomID: this.room ? this.room.ID : 0,
                SenderID: user.ID,
                ReceiverID: user.ID,
                Number: this.maxNumber + 1
              });
            } else {
              messages.push({
                Content: urlReplacedText,
                RoomID: this.room ? this.room.ID : 0,
                SenderID: this.user?.ID,
                ReceiverID: user.ID,
                Number: this.maxNumber + 1
              });
              receiverIDs.push(user.ID);
            }
          }
        }
      });
      this.http.post<{}>(`${environment.API_URL}/users/message`, messages).subscribe(
        _ => {
          this.maxNumber++;
        }
      );

      setTimeout(() => {
        this.http.post<{}>(`${environment.API_URL}/nsq`, {
          message: JSON.stringify({
            Type: 'MESSAGE',
            SenderID: this.user?.ID,
            ReceiverIDs: receiverIDs,
            Data: {
              Content: urlReplacedText,
              ImageID: null,
              Image: null,
              GiftID: null,
              Gift: null,
              RoomID: this.room?.ID,
              RoomTitle: this.getTitle(this.room),
              CreatedAt: moment().format(),
            },
          }),
        }).subscribe();
      }, 1000);
    }
  }

  async rejectSuggestion(): Promise<void> {
    this.suggestionPanel = false;
    if (this.user && this.partner && this.room && this.call) {

      const confirmation = await this.warn(t('Are you sure reject?'));
      if (confirmation) {
        // const messageContent = '残念ですが今回は見送りします。ご了承の程よろしくお願い致します。';
        // this.sendMessage(messageContent);

        // suggest
        this.http.post<{
          'success': boolean
        }>(`${environment.API_URL}/calls/reject`, {
          roomId: this.roomId,
          callId: this.call.ID,
          partnerId: this.partner.ID
        }).subscribe((res) => {
          if (res.success) {
            // success
          }
        }, (err: HttpErrorResponse) => {
          console.log(err);
        });
      }
    }
  }

  async confirmSuggestion(): Promise<void> {
    this.suggestionPanel = false;
    if (this.user && this.partner && this.room && this.call) {

      if (this.user.Role === 1 && !this.user.TelecomCredit) {
        const confirmation = await this.warn(t('Credit card info is not registered. Are you gonna register your card?'), t('Call after Credit card registration'));
        if (confirmation) {
          // to register credit card
          this.navi.navigateForward('main/mypage/payment');
        }
      } else {

        // suggest
        this.http.post<{
          'success': boolean
        }>(`${environment.API_URL}/calls/confirm`, {
          roomId: this.roomId,
          callId: this.call.ID,
          partnerId: this.partner.ID
        }).subscribe((res) => {
          if (res.success) {
            // success
          }
        }, (err: HttpErrorResponse) => {
          console.log(err);
        });
      }
    }
  }

  getPartner(room: Room): User {
    return room.Users.find(user => user.ID !== this.user?.ID) ?? ({} as User);
  }

  getAge(birthday: string | null): number {
    // return birthday ? new Date().getFullYear() - new Date(birthday).getFullYear() : 0;
    if (birthday){
      return moment.tz('Asia/Tokyo').diff(birthday, 'years');
    }else{
      return 0;
    }
  }

  getTitle(room: Room | null): string {
    if (room){
      if (room.IsGroup){
        return room.Title ?? t('Undefined');
      }else{
        if (this.user && this.user.Nickname) {
          if (this.user.Birthday && !this.user.BirthdayHidden) {
            return `${this.user.Nickname}  ${this.getAge(this.user.Birthday)}${t('Age')} ${this.cond.getLocationName(this.user)}`;
          } else {
            return `${this.user.Nickname} ${this.cond.getLocationName(this.user)}`;
          }
        } else {
          return '';
        }
      }
    }
    return '';
  }

  loadData($event: Event): void {
    this.getMessages(this.room?.ID ?? 0, $event);
  }

  getGuestLevels(): void {
    this.http.get<Class[]>(`${environment.API_URL}/admin/levels/guest`).subscribe(res => {
      this.guestLevels = res;
      // console.log(res);
      this.levelsReady = true;
    });
  }

  async seeMessage(): Promise<void> {
    if (this.room && this.partner) {
      // const confirmation = await this.warn(t('Credit card info is not registered. Are you gonna register your card?'),
      //   `クレカを登録して${this.getPartner(this.room).Nickname}さんからのメッセージを見てみよう `);
      const confirmation = await this.warn(
        `<div class = "alert-img-container">
            <ion-avatar>
              <img src = "${this.partner.Images[0].Path}" class = "alert-img" />
            </ion-avatar>
          </div>` +
        `<strong class="message-see">カードを登録して${this.partner.Nickname}さんからのメッセージを見てみよう!</strong>` +
        `<p class="alert-paragraph">${t('Credit Card Register is necessary.')}<br>${t('Are you sure register your card?')}<br>（${t('Message give and take is free')}）</p>` +
        `<img class = "card-list-img" src = "assets/img/cardlist.png" />`,
        'none', 'see-message-alert');
      if (confirmation) {
        // to register credit card
        this.navi.navigateForward('main/mypage/payment');
      }
    }
  }

  guestAllowed(): boolean {
    if (this.user && this.levelsReady && this.guestLevels.length > 0) {
      return this.pointUsed >= this.guestLevels[0].Point && this.user.Role === 1;
    } else {
      return false;
    }
  }

  resetMessageUnreads(roomId: number): void {
    this.http.get<{
      'success': boolean
    }>(`${environment.API_URL}/users/message/reset/${roomId}`).subscribe((res) => {
      if (res.success) {
        // successfully reset
      }
    }, (err: HttpErrorResponse) => {
      console.log(err);
      this.showAlert(t('Data Retrieve Failed!'));
    });
  }

  openSuggestPanel(): void {
    if (this.room && this.call) {
      this.http.get<{
        'success': boolean
      }>(`${environment.API_URL}/calls/check/${this.call.ID}`).subscribe((res) => {
        if (res.success) {
          // successfully reset
          this.meetTime = new Date().toISOString();
          this.suggestionPanel = !this.suggestionPanel;
        } else {
          this.showAlert(t('Please wait for the confirmation of ex-join'));
        }
      }, (err: HttpErrorResponse) => {
        console.log(err);
        this.showAlert(t('Data Retrieve Failed!'));
      });
    } else {
      this.meetTime = new Date().toISOString();
      this.suggestionPanel = !this.suggestionPanel;
      if (this.suggestionPanel) {
        this.isVisibleGiftContainer = false;
      }
    }

  }

  getThisYear(): number {
    return new Date().getFullYear();
  }

  getPersons(): void {
    this.persons = [];
    for (let i = 1; i <= 10; i++) {
      this.persons.push({
        name: `${i}人`,
        value: i
      });
    }
  }

  getPeriods(): void {
    this.periods = [];
    for (let i = 1; i < 24; i++) {
      this.periods.push({
        name: `${i}時間`,
        value: i
      });
      [15, 30, 45].forEach(item => {
        this.periods.push({
          name: `${i}時間${item}分`,
          value: i + item / 60
        });
      });
    }
  }

  async openReportModal(): Promise<void> {
    if (this.room && this.user){
      const reportTarget = this.room.Users.filter(item => item.ID !== this.user?.ID && item.Role >= 0);
      if (reportTarget.length > 0) {
        const modal = await this.modal.create({
          component: ReportComponent,
          swipeToClose: true,
          presentingElement: this.routerOutlet.nativeEl,
          componentProps: {
            users: reportTarget,
            role: this.user.Role
          },
        });
        await modal.present();
        const { data } = await modal.onWillDismiss();
        if (data) {
          this.showAlert(t('Successfully Reported'));
        }
      }
    }
  }

  async openMemoModal(): Promise<void> {
    if (this.room && this.user){
      const guests = this.room.Users.filter(item => item.ID !== this.user?.ID && item.Role === 1);
      if (guests.length > 0) {
        const modal = await this.modal.create({
          component: MemoComponent,
          swipeToClose: true,
          presentingElement: this.routerOutlet.nativeEl,
          componentProps: {
            guests,
            roomID: this.room.ID
          },
        });
        await modal.present();
        await modal.onWillDismiss();
      }
    }
  }

  async openMessageTypeDialog(): Promise<void> {
    const actionButtons: ActionSheetButton[] = [];

    // report for private room
    actionButtons.push({
      text: t('Report'),
      handler: () => {
        this.openReportModal();
      }
    });

    if (this.room && this.user && this.user.Role === 0) {
      actionButtons.push({
        text: t('Cast Memo'),
        handler: () => {
          this.openMemoModal();
        }
      });
    }

    if (this.room && !this.room.IsGroup){
      const partner = this.getPartner(this.room);
      if (this.blockedRoomIDs.includes(this.room?.ID ?? 0)) {
        actionButtons.push({
          text: t('Deblock'),
          handler: async () => {
            const alert = await this.alert.create({
              message: t('Are you going to deblock?'),
              buttons: [t('Cancel'), {
                text: t('OK'),
                handler: () => {
                  const index = this.blockedRoomIDs.findIndex(roomID => roomID === this.room?.ID);
                  this.blockedRoomIDs.splice(index, 1);
                  this.http.put<{}>(`${environment.API_URL}/users/room/block`, {
                    blocked_room_ids: JSON.stringify(this.blockedRoomIDs),
                    target_user_id: partner.ID,
                    room_id: this.room?.ID ?? 0,
                    is_blocked: false
                  }).subscribe(() => {
                    if (this.user) {
                      this.user.BlockedRoomIDs = JSON.stringify(this.blockedRoomIDs);
                      this.authService.currentUserSubject.next(this.user);
                      this.authService.reloadSubject.next(true);
                    }
                  });
                },
              }],
            });
            await alert.present();
          },
        });
      } else {
        actionButtons.push({
          text: t('Block'),
          handler: async () => {
            const alert = await this.alert.create({
              message: t('Are you going to block?'),
              buttons: [t('Cancel'), {
                text: t('OK'),
                handler: () => {
                  this.blockedRoomIDs.push(this.room?.ID ?? 0);
                  this.http.put<{}>(`${environment.API_URL}/users/room/block`, {
                    blocked_room_ids: JSON.stringify(this.blockedRoomIDs),
                    target_user_id: partner.ID,
                    room_id: this.room?.ID ?? 0,
                    is_blocked: true
                  }).subscribe(() => {
                    if (this.user) {
                      // console.log("BLOCK COMPLETE");
                      this.user.BlockedRoomIDs = JSON.stringify(this.blockedRoomIDs);
                      this.authService.currentUserSubject.next(this.user);
                      this.authService.reloadSubject.next(true);
                    }
                  });
                },
              }],
            });
            await alert.present();
          },
        });
      }

      if (this.favoriteRoomIDs.includes(this.room?.ID ?? 0)) {
        actionButtons.push({
          text: t('Remove from Favorites'),
          handler: () => {
            const index = this.favoriteRoomIDs.findIndex(roomID => roomID === this.room?.ID);
            this.favoriteRoomIDs.splice(index, 1);
            this.http.put<{}>(`${environment.API_URL}/users/room/favorite`, {
              favorite_room_IDs: JSON.stringify(this.favoriteRoomIDs),
            }).subscribe(() => {
              if (this.user) {
                this.user.FavoriteRoomIDs = JSON.stringify(this.favoriteRoomIDs);
                this.authService.currentUserSubject.next(this.user);
              }
            });
          },
        });
      } else {
        actionButtons.push({
          text: t('Add to Favorites'),
          handler: () => {
            this.favoriteRoomIDs.push(this.room?.ID ?? 0);
            this.http.put<{}>(`${environment.API_URL}/users/room/favorite`, {
              favorite_room_IDs: JSON.stringify(this.favoriteRoomIDs),
            }).subscribe(() => {
              if (this.user) {
                this.user.FavoriteRoomIDs = JSON.stringify(this.favoriteRoomIDs);
                this.authService.currentUserSubject.next(this.user);
              }
            });
          },
        });
      }
    }

    // add cancel button
    actionButtons.push({
      text: t('Cancel'),
      role: 'cancel',
    });

    const actionSheet = await this.actionSheetController.create({
      buttons: actionButtons,
    });
    await actionSheet.present();
  }

  filterCasts(users: User[]): User[] {
    return users.filter((item) => {
      return item.Role >= 0 && !item.Withdrawn;
    });
  }

  getRoom(roomID: number): void {
    this.http.get<Room>(`${environment.API_URL}/users/room/${roomID}`).subscribe(room => {
      this.room = room;
      if (this.room.IsGroup){
        this.call = this.room.Calls.length > 0 ? this.room.Calls[0] : undefined;
      }else{
        this.call = this.room.Calls.find(item => item.Status !== 'break' && item.Status !== 'end' && item.IsDeleted === false);
        this.calls = [... this.room.Calls.filter(item => item.IsDeleted === false)];
        this.callHistoryVisible = this.calls.length <= 2;
      }
      // console.log(this.room);
      // console.log(this.call);

      if (!this.room.IsGroup) {
        this.partner = this.getPartner(room);
        if (this.partner.Withdrawn || this.partner.IsBlocked){
          this.navi.navigateBack('/main/chat');
          this.authService.reloadSubject.next(true);
          this.showAlert(t('This user is already withdrawn'));
        }
      }

      if (this.call) {
        this.totalSec = this.call.Period * 3600;
      }

      if (this.room.IsDeleted){
        this.showAlert(t('This room has already been deleted'));
        this.navi.navigateForward(['/main/chat']);
      }

      if (this.user && this.user.TelecomFailed && this.room.Type !== 'admin') {
        this.showAlert(t('Pay with your telecom card failed. Consult with glass consultant.'));
        this.navi.navigateForward(['/main/chat']);
      }

      if (this.user && this.user.Role === 0 && this.room.Joins && this.room.Joins.length > 0) {
        const index = this.room.Joins.findIndex(
          item => {
            if (this.room) {
              return item.UserID === this.user?.ID && ((!this.room.IsGroup && !item.IsDone) || this.room.IsGroup);
            }else{
              return false;
            }
          }
        );
        if (index > -1) {
          this.join = this.room.Joins[index];
          if (this.call) {
            const elapsedSeconds = Math.floor((Date.now() - new Date(this.join.StartedAt).getTime()) / 1000);
            this.totalSec = this.call.Period * 60 * 60 - elapsedSeconds;
            this.startCountDown();
          }
        }
      }

      // console.log("now subscripting...");
      if (this.joinSubscription === null) {
        // console.log("now join subscription null");
        this.joinSubscription = this.wsService.joinSubject.subscribe((joinEvent) => {
          // console.log(joinEvent);
          if (this.user && this.room && joinEvent.join.RoomID === this.room.ID) {
            const joinEventNum = parseInt(joinEvent.event, 10);
            if (joinEventNum === this.user.ID || (joinEventNum === 0 && joinEvent.join.UserID === this.user.ID)) {
              this.join = joinEvent.join;
              if (joinEvent.type === 'start') {
                this.totalSec = this.call ? this.call.Period * 3600 : 0;
                this.startCountDown();
              } else {
                this.join.IsStarted = false;
                this.endCountDown();
                this.refreshGuestInfo();

                const lastCall = { ... this.call } as Call;
                if (lastCall.ID > 0) {
                  this.http.post<{
                    'success': boolean
                  }>(`${environment.API_URL}/users/lastroom`, {
                    last_room: lastCall.ID
                  }).subscribe(res => {
                    if (res.success) {
                      this.openReviewDialog(lastCall);
                    }
                  });
                }
              }
            }

            if (joinEvent.type === 'start') {
              this.room.Type = 'starting';
              if (this.call){
                this.call.Status = 'starting';
                const callIndex = this.calls.findIndex(item => item.ID === this.call?.ID);
                if (callIndex > -1) {
                  this.calls[callIndex].Status = 'starting';
                }
              }

              if (this.user.Role === 1) {
                this.gps.savePosition('call-start', this.call ? this.call.ID : 0);
              }
            }

            // if groupchat all end
            if (joinEvent.type === 'end' && (!this.room.IsGroup || joinEvent.event === '0')) {
              // console.log('now end');
              room.Type = 'end';

              this.endCountDown();
              if (this.join) {
                this.join.IsStarted = false;
              }

              if (this.user.Role === 1) {
                this.gps.savePosition('call-end', this.call ? this.call.ID : 0);
              }

              this.refreshGuestInfo();

              // update room status
              const lastCall = { ... this.call } as Call;
              this.getRoom(this.roomId);

              // if guest save his last room id
              if (this.user && this.user.Role === 1 && this.room) {
                this.join = joinEvent.join;

                if (this.join.ID > 0) {
                  this.http.post<{
                    'success': boolean
                  }>(`${environment.API_URL}/users/lastroom`, {
                    last_room: this.room.ID
                  }).subscribe(res => {
                    if (res.success) {
                      // successfully saved then open review dialog
                      if (lastCall.ID > 0) {
                        this.openReviewDialog(lastCall);
                      }
                    } else {
                      console.log('error updating family ids');
                    }
                  });
                }

              }
            }
          }
        });
      }

      // suggest subscription
      if (this.suggestSubscription === null) {
        this.suggestSubscription = this.wsService.suggestSubject.subscribe((callEvent) => {
          // console.log(callEvent);
          if (this.room && this.room.ID === callEvent.call.RoomID){
            if (callEvent.event === 'suggest') {
              this.call = callEvent.call;
              this.room.Type = 'waiting';
              if (this.call){
                const callIndex = this.calls.findIndex(item => item.ID === this.call?.ID);
                if (callIndex > -1) {
                  this.calls[callIndex].Status = 'waiting';
                }else{
                  this.calls.push(callEvent.call);
                }
              }
            } else if (callEvent.event === 'reject') {
              // console.log('reject event');
              this.room.Type = '';
              if (this.call){
                const callIndex = this.calls.findIndex(item => item.ID === this.call?.ID);
                if (callIndex > -1) {
                  this.calls.splice(callIndex, 1);
                }
              }
              this.call = undefined;

            } else if (callEvent.event === 'confirm') {
              // console.log('confirm event');
              this.room.Type = 'confirm';
              this.totalSec = callEvent.call.Period * 3600;
              this.call = callEvent.call;
              if (this.call){
                const callIndex = this.calls.findIndex(item => item.ID === this.call?.ID);
                if (callIndex > -1) {
                  this.calls[callIndex].Status = 'confirm';
                }
              }
            }
          }
        });
      }

      // room subscription
      if (this.roomSubscription === null) {
        this.roomSubscription = this.wsService.roomSubject.subscribe((resRoom) => {
          if (this.roomId === resRoom.Room.ID) {
            this.getRoom(this.roomId);
          }
        });
      }

      // room remove subscription
      if (this.roomRemoveSubscription === null) {
        this.roomRemoveSubscription = this.wsService.roomRemoveSubject.subscribe((roomRes) => {
          // console.log(roomRes);
          if (this.user && this.roomId === roomRes.Room.ID) {
            if (this.user.ID === roomRes.UserID || roomRes.UserID === -1) {
              this.navi.navigateForward('/main/chat');

              // reset unread messages
              this.http.get<{
                data: number
              }>(`${environment.API_URL}/users/message/unread`).subscribe((res) => {
                this.wsService.unreadMessages = res.data;
              });
            } else {
              // this is for room update
              this.join = null;
              this.endCountDown();
              this.getRoom(this.roomId);
            }
          }
        });
      }

      // call remove subscription
      if (this.callRemoveSubscription === null) {
        this.callRemoveSubscription = this.wsService.callSubject.subscribe((callEvent) => {
          if (this.room && this.call) {
            if (this.call.ID === callEvent.call.ID && callEvent.event === 'delete') {
              // call deleted
              this.join = null;
              this.endCountDown();
              this.getRoom(this.roomId);
            }
          }
        });
      }

      this.allInfoReady = true;
    });
  }

  // only for private rooms
  getInvoiceListStatus(): boolean{
    if (this.room && !this.room.IsGroup){
      if (this.calls.length === 0){
        return false;
      }else{
        if (this.calls.length === 1 && this.call && (this.call.Status === 'confirm' || this.call.Status === 'waiting')){
          return false;
        }
        return true;
      }
    }else{
      return false;
    }

  }

  endCountDown(): void {
    if (this.interval) {
      clearInterval(this.interval);
      this.interval = null;
    }
  }

  startCountDown(): void {
    if (this.interval === null) {
      this.interval = window.setInterval(() => {
        this.totalSec--;
      }, 1000);
    }
  }

  refreshGuestInfo(): void {
    this.http.get<User>(`${environment.API_URL}/users/info`).subscribe(user => {
      this.user = user;
      this.authService.currentUserSubject.next(user);
    });
  }

  async showAlert(msgStr: string): Promise<void> {
    const alert = await this.alert.create({
      message: msgStr,
      buttons: ['OK']
    });
    await alert.present();
  }

  getMessages(roomID: number, $event: Event | null = null): void {
    this.http.get<Message[]>(`${environment.API_URL}/users/message` +
      `?room_id=${roomID}&page=${this.pageIndex}&offset=${this.offset}`)
      .subscribe(messages => {
        // console.log(messages);

        if (this.pageIndex === 0) {
          this.scrollToBottom();
        }
        if (messages.length < 20) {
          this.isAllLoaded = true;
        }
        if (messages.length > 0) {
          this.messages.unshift(...messages.reverse());
          this.pageIndex++;
        }

        // this.canNotSee = this.canNotSeeMessages();

        // start message subscription
        if (this.messageSubscription === null) {
          this.messageSubscription = this.wsService.messageSubject.subscribe(wsData => {
            const data: Message = wsData.Data as Message;
            // console.log(wsData);
            if (this.user && this.room) {
              if (data.RoomID === this.room.ID) {
                this.wsService.unreadMessages--;
                this.resetMessageUnreads(roomID);

                // get sender
                let curSender = this.room.Users.find(user => user.ID === wsData.SenderID);
                // if sender is admin, and private room then
                if (wsData.SenderID && this.consultant) {
                  if (wsData.SenderID === this.consultant.ID && !this.room.IsGroup) {
                    curSender = this.consultant;
                  }
                }

                if (!this.canNotSee) {
                  this.messages.push({
                    ...data, ...{
                      Room: this.room,
                      SenderID: wsData.SenderID,
                      Sender: curSender,
                      ReceiverID: this.user.ID,
                      Receiver: this.user,
                    }
                  });
                  this.offset++;
                  this.scrollToBottom();
                }
              }else{
                if (data.ImageID !== null){
                  this.showToastr(data.RoomTitle ?? '', '画像が届きました。');
                }else if (data.GiftID !== null) {
                  this.showToastr(data.RoomTitle ?? '', 'ギフトが届きました。');
                }else{
                  if (data.Content && data.Content !== '') {
                    this.showToastr(data.RoomTitle ?? '', data.Content ?? '');
                  }
                }
              }
            }
          });
        }

        // complete infinite scroll
        if ($event && $event.target) {
          this.ionInfiniteScroll.complete();
        }
      });
  }

  async showToastr(title: string, content: string): Promise<void> {
    const toast = await this.toastr.create({
      header: title,
      message: content,
      color: 'dark',
      position: 'top',
      duration: 2000,
      cssClass: 'message-toast'
    });
    toast.present();
  }

  getGifts(): void {
    this.http.get<Gift[]>(`${environment.API_URL}/gifts`).subscribe(gifts => {
      if (gifts.length > 0) {
        this.gifts = gifts.filter(gift => gift.ParentID === 0);
        this.selectedGiftTab = this.gifts[0].Name ?? '';
        gifts.forEach(gift => {
          if (gift.ParentID !== 0) {
            const parentGift = this.gifts.find(item => item.ID === gift.ParentID);
            if (parentGift) {
              if (parentGift.Children) {
                parentGift.Children.push(gift);
              } else {
                parentGift.Children = [gift];
              }
            }
          }
        });
        this.selectedGifts = this.gifts[0].Children ?? [];
      }
    });
  }

  getTime(datetime: string | undefined): number {
    return datetime ? new Date(datetime).getTime() : 0;
  }

  scrollToBottom(): void {
    setTimeout(() => {
      this.ionContent?.scrollToBottom(100);
    }, 100);
  }

  onToggleGift(): void {
    this.isVisibleGiftContainer = !this.isVisibleGiftContainer;
    if (this.isVisibleGiftContainer) {
      this.suggestionPanel = false;
    }
  }

  onChangeGiftTab(): void {
    this.selectedGifts = this.gifts.find(gift => gift.Name === this.selectedGiftTab)?.Children ?? [];
  }

  onFileChanged(event: Event): void {
    if (event.target instanceof HTMLInputElement && event.target.files && event.target.files[0]) {
      this.media = event.target.files[0];
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = () => {
        this.mediaURL = reader.result as string;
      };
    }
  }

  onClearImage(): void {
    this.mediaURL = '';
    this.fileInput.nativeElement.value = '';
    this.media = null;
  }

  async onSend(): Promise<void> {
    if (this.room && this.user) {
      // if guest and has not a card
      if (this.user.Role === 1 && !this.user.TelecomCredit && this.roomType !== 'admin' && this.maxNumber >= 1) {
        let confirmation = false;
        if (!this.room.IsGroup && this.partner) {
          confirmation = await this.warn(
            `<div class = "alert-img-container">
                <ion-avatar>
                  <img src = "${this.partner.Images[0].Path}" class = "alert-img" />
                </ion-avatar>
              </div>` +
            `<strong class="message-see">カードを登録して${this.partner.Nickname}さんにメッセージを送ろう!</strong>` +
            `<p class="alert-paragraph">${t('Credit Card Register is necessary.')}<br>${t('Are you sure register your card?')}<br>（${t('Message give and take is free')}）</p>` +
            `<img class = "card-list-img" src = "assets/img/cardlist.png" />`,
            'none', 'see-message-alert');
        }else{
          confirmation = await this.warn(
            `<strong class="message-see">カードを登録してメッセージを送ろう!</strong>` +
            `<p class="alert-paragraph">${t('Credit Card Register is necessary.')}<br>${t('Are you sure register your card?')}<br>（${t('Message give and take is free')}）</p>` +
            `<img class = "card-list-img" src = "assets/img/cardlist.png" />`,
            'none', 'see-message-alert');
        }
        if (confirmation) {
          // to register credit card
          this.navi.navigateForward('main/mypage/payment');
        }
        return;
      }

      // check message
      let messageCheck = '';
      if (this.message.trim() !== '') {
        const params = new HttpParams().
          append('content', this.message.trim());
        try{
          messageCheck = await this.http.get<string>(`${environment.API_URL}/users/message/check`, {params}).toPromise();
        }catch (ex: unknown){
          this.showAlert(t('Operation Failed'));
          return;
        }
      }

      if (messageCheck !== ''){
        this.showAlert(t('Content can not include %s').replace('%s', messageCheck));
        return;
      }

      // else
      if (this.media) {
        const formData = new FormData();
        formData.append('image', this.media);
        this.http.post<Image>(`${environment.API_URL}/users/message/image`, formData).subscribe(image => {
          if (this.room && this.user) {
            this.messages.push({
              Content: null,
              ImageID: image.ID,
              Image: image,
              GiftID: null,
              Gift: null,
              RoomID: this.room.ID,
              Room: this.room,
              SenderID: this.user.ID,
              Sender: this.user,
              ReceiverID: this.user.ID,
              Receiver: this.user,
              Number: this.maxNumber + 1,
              CreatedAt: moment().format(),
            });
            this.scrollToBottom();
            const messages: Message[] = [];
            const receiverIDs: number[] = [];
            this.room.Users.forEach(user => {
              if (user.Role >= 0) {
                if (user.ID === this.user?.ID) {
                  messages.push({
                    ImageID: image.ID,
                    IsRead: true,
                    RoomID: this.room ? this.room.ID : 0,
                    SenderID: user.ID,
                    ReceiverID: user.ID,
                  });
                } else {
                  messages.push({
                    ImageID: image.ID,
                    RoomID: this.room ? this.room.ID : 0,
                    SenderID: this.user?.ID,
                    ReceiverID: user.ID,
                  });
                  receiverIDs.push(user.ID);
                }
              }
            });
            this.http.post<{}>(`${environment.API_URL}/users/message`, messages).subscribe(
              _ => {
                if (this.room && this.user && this.message.trim() !== '') {
                  this.sendMessage(this.message);
                  this.message = '';
                }else{
                  this.maxNumber++;
                }
              }
            );

            setTimeout(() => {
              this.http.post<{}>(`${environment.API_URL}/nsq`, {
                message: JSON.stringify({
                  Type: 'MESSAGE',
                  SenderID: this.user?.ID,
                  ReceiverIDs: receiverIDs,
                  Data: {
                    Content: null,
                    ImageID: image.ID,
                    Image: image,
                    GiftID: null,
                    Gift: null,
                    RoomID: this.room?.ID,
                    RoomTitle: this.getTitle(this.room),
                    CreatedAt: moment().format(),
                  },
                }),
              }).subscribe();
            }, 1000);
            this.onClearImage();
          }
        });
      } else {
        if (this.room && this.user && this.message.trim() !== '') {
          this.sendMessage(this.message);
          this.message = '';
        }
      }


      // save location
      this.gps.savePosition('message', this.call ? this.call.ID : 0);
    }
  }

  canNotSeeMessages(): boolean {
    if (this.user && this.user.Role === 1 && !this.user.TelecomCredit && this.roomType !== 'admin') {
      const index = this.messages.findIndex(item => {
        if (this.user) {
          return item.SenderID && item.SenderID !== this.user.ID;
        } else {
          return false;
        }
      });
      return index > -1;
    } else {
      return false;
    }
  }

  async onSendGift(gift: Gift): Promise<void> {
    if (this.user && this.room) {
      if (this.user.TelecomCredit === false) {
        const alert = await this.alert.create({
          message: t('Payment information is not registered'),
          buttons: [t('OK')],
        });
        await alert.present();
      } else if (this.room.IsGroup === false && this.user.Point < gift.Point) {
        const alert = await this.alert.create({
          message: t('Point is not enough. Are you gonna buy the point?'),
          buttons: [t('Cancel'), {
            text: t('OK'),
            handler: () => {
              this.navi.navigateForward('/main/mypage/point');
            },
          }],
        });
        await alert.present();
      } else {
        const alert = await this.alert.create({
          message: t('Are you sure send the gift?'),
          buttons: [t('Cancel'), {
            text: t('OK'),
            handler: () => {
              if (this.user && this.room) {
                this.messages.push({
                  Content: null,
                  ImageID: null,
                  Image: null,
                  GiftID: gift.ID,
                  Gift: gift,
                  RoomID: this.room?.ID,
                  Room: this.room,
                  SenderID: this.user.ID,
                  Sender: this.user,
                  ReceiverID: this.user.ID,
                  Receiver: this.user,
                  CreatedAt: moment().format(),
                });
                this.scrollToBottom();
                const messages: Message[] = [];
                const receiverIDs: number[] = [];
                this.room.Users.forEach(user => {
                  if (user.Role >= 0) {
                    if (user.ID === this.user?.ID) {
                      messages.push({
                        GiftID: gift.ID,
                        IsRead: true,
                        RoomID: this.room ? this.room.ID : 0,
                        SenderID: user.ID,
                        ReceiverID: user.ID,
                      });
                    } else {
                      messages.push({
                        GiftID: gift.ID,
                        RoomID: this.room ? this.room.ID : 0,
                        SenderID: this.user?.ID,
                        ReceiverID: user.ID,
                      });
                      receiverIDs.push(user.ID);
                    }
                  }
                });
                this.http.post<{}>(`${environment.API_URL}/users/message`, messages).subscribe();
                this.http.post<{}>(`${environment.API_URL}/nsq`, {
                  message: JSON.stringify({
                    Type: 'MESSAGE',
                    SenderID: this.user.ID,
                    ReceiverIDs: receiverIDs,
                    Data: {
                      Content: null,
                      ImageID: null,
                      Image: null,
                      GiftID: gift.ID,
                      Gift: gift,
                      RoomID: this.room.ID,
                      RoomTitle: this.getTitle(this.room),
                      CreatedAt: moment().format(),
                    },
                  }),
                }).subscribe();
              }
              this.isVisibleGiftContainer = false;
            },
          }],
        });
        await alert.present();
      }
    }
  }

  getLevelName(usedPoint: number): string {
    if (this.guestLevels.length > 0) {
      for (const levelItem of this.guestLevels) {
        if (usedPoint < levelItem.Point) {
          return levelItem.Name;
        } else {
          continue;
        }
      }
      return this.guestLevels[this.guestLevels.length - 1].Name;
    } else {
      return t('Undefined');
    }
  }

  getLevelNum(usedPoint: number): number {
    if (this.guestLevels.length > 0) {
      for (const [index, levelItem] of this.guestLevels.entries()) {
        if (usedPoint < levelItem.Point) {
          return index;
        } else {
          continue;
        }
      }
      return this.guestLevels.length;
    } else {
      return 0;
    }
  }

  repeatArray(n: number): number[] {
    return Array(n);
  }

  getStatusText(): string {
    if (this.room && this.user) {
      // group chat
      if (this.room.IsGroup) {
        // confirm
        if (this.room.Type === 'confirm') {
          return t('Join Confirmed');
          // starting
        } else if (this.room.Type === 'starting') {
          // guest
          if (this.user.Role === 1) {
            return t('Joining Now');
            // cast
          } else {
            if (this.join) {
              if (this.join.IsStarted) {
                return t('Joining Now');
              } else {
                return t('Join Ended');
              }
            } else {
              return t('Join Confirmed');
            }
          }
        } else if (this.room.Type === 'end') {
          return t('Join Finished');
        } else {
          return t('Undefined');
        }
        // private call
      } else {
        // no reservation
        if (this.room.Type === '') {
          return t('No Reservation');
        } else if (this.room.Type === 'waiting') {
          return t('Suggestion Appeared');
        } else if (this.room.Type === 'confirm') {
          return t('Join Confirmed');
        } else if (this.room.Type === 'starting') {
          return t('Joining Now');
        } else if (this.room.Type === 'end') {
          return t('Join Ended');
        } else {
          return t('Undefined');
        }
      }
    } else {
      return t('Undefined');
    }
  }

  getStatusTitle(glass: Call): string {
    if (glass.Status === 'confirm') {
      return t('Matching Ended');
    } else if (glass.Status === 'break') {
      return t('Waiting for Payment');
    } else if (glass.Status === 'end') {
      return t('Payment Completed');
    } else if (glass.Status === 'starting') {
      return t('Joining Now');
    } else if (glass.Status === 'waiting') {
      return t('Now Waiting for accept');
    } else {
      return t('Undefined');
    }
  }

  getHelpText(): string {
    if (this.room && this.user) {
      if (this.room.Type === 'confirm' || this.room.Type === 'starting') {
        if (this.call) {
          // return `${formatDate(this.call.MeetTimeISO, 'M/dd HH:mm', 'en', 'Asia/Tokyo')} ` +
          //   `${this.call.Person}人 ${this.call.Period}時間`;
          if (this.call.IsPrivate){
            return `${moment(`${this.call.MeetTimeISO} +0900`, 'YYYY-MM-DD HH:mm:ss Z').format('M/D HH:mm')} ` +
              `${this.getTimeStr(this.call.Period)}`;

          }else{
            return `${moment(`${this.call.MeetTimeISO} +0900`, 'YYYY-MM-DD HH:mm:ss Z').format('M/D HH:mm')} ` +
              `${this.call.Person}人 ${this.call.Period}時間`;
          }
        } else {
          return '';
        }
      } else {
        if (!this.room.IsGroup) {
          if (this.room.Type === 'waiting') {
            if (this.call) {
              if (this.call.SuggesterID === this.user.ID) {
                return t('Please wait until partner responds');
              } else {
                return t('Please respond to suggestion');
              }
            } else {
              return t('Call does not exist');
            }
          } else if (this.room.Type === 'end') {
            return t('Join has been Finished');
          } else {
            return t('Join can start after you confirm schedule');
          }
        } else {
          return t('Join has been Finished');
        }
      }
    } else {
      return '';
    }
  }

  getTimeStr(period: number): string {
    let periodStr = `${period}時間`;
    if (Math.ceil(period) !== Math.floor(period)){
      periodStr = `${Math.floor(period)}時間 ${(period - Math.floor(period)) * 60}分`;
    }
    return periodStr;
  }

  isJoining(): boolean {
    if (this.room && this.user) {
      // private
      if (!this.room.IsGroup) {
        return this.room.Type === 'starting';
        // public
      } else {
        // cast
        if (this.user.Role === 0) {
          return this.join ? this.join.IsStarted : false;
          // guest
        } else {
          return this.room.Type === 'starting';
        }
      }
    } else {
      return false;
    }
  }

  canControl(): boolean {
    if (this.room && this.user && this.user.Role === 0) {
      if (this.room.Type === '' || this.room.Type === 'waiting' || this.room.Type === 'end') {
        return false;
      }
      if (this.room.IsGroup) {
        if (this.join) {
          return this.room.Type !== 'end' && this.join.IsStarted;
        } else {
          return true;
        }
      } else {
        return this.room.Type !== 'end';
      }
    } else {
      return false;
    }
  }

  getRemainTime(): string {
    let val = this.totalSec;
    if (val < 0) { val *= -1; }
    const hour = Math.floor(val / 60 / 60);
    const minute = Math.floor(val % (60 * 60) / 60);
    const second = val % 60;

    return this.justifyNumber(hour) + ' : ' + this.justifyNumber(minute) + ' : ' + this.justifyNumber(second);
  }

  justifyNumber(d: number): string {
    if (d < 0) { d *= -1; }
    if (d < 10) {
      return `0${d}`;
    } else {
      return d.toString();
    }
  }

  isExceed(): boolean {
    return this.totalSec < 0;
  }

  remainTen(): boolean {
    // return this.totalSec < 10 * 60;
    return true;
  }

  async finishJoin(): Promise<void> {
    const confirmation = await this.warn(t('Are you sure finish joining?'));
    if (confirmation && this.user && this.room && this.join) {
      this.canJoin = true;
      this.http.post<{
        'success': boolean
      }>(`${environment.API_URL}/calls/end`, {
        joinId: this.join.ID,
        roomId: this.room.ID
      }).subscribe((res) => {
        if (res.success) {
          // successfully ended
          this.gps.savePosition('call-end', this.call ? this.call.ID : 0);
        } else {
          if (this.join) {
            this.join.IsStarted = false;
          }
          this.showAlert(t('Not allowed'));
        }
        this.canJoin = false;
      }, (err: HttpErrorResponse) => {
        console.log(err);
        this.canJoin = false;
      });
    }
  }

  async startJoin(): Promise<void> {
    const confirmation = await this.warn(t('Are you sure start joining?'));
    if (confirmation && this.user && this.room && this.call) {
      // console.log(this.call);
      this.canJoin = true;
      this.join = {
        ID: 0,
        UserID: this.user.ID,
        RoomID: this.room.ID,
        CallID: this.call.ID,
        IsStarted: true,
        Commented: false,
        IsExtended: false,
        IsPassed: false,
        IsDone: false,
        StartedAt: formatDate(new Date(), 'yyyy-MM-DD HH:mm:ss', 'en')
      };

      this.http.post<{
        'success': boolean
      }>(`${environment.API_URL}/calls/start`, {
        join: this.join,
        roomId: this.room.ID
      }).subscribe((res) => {
        if (res.success) {
          // successfully started
          if (this.room) {
            this.room.Type = 'starting';
            this.gps.savePosition('call-start', this.call ? this.call.ID : 0);
          }
        } else {
          this.join = null;
          this.showAlert(t('Not allowed'));
        }
        this.canJoin = false;
      }, (err: HttpErrorResponse) => {
        console.log(err);
        this.join = null;
        this.canJoin = false;
      });
    }
  }

  async warn(message: string, title: string = '', cssClass: string = ''): Promise<boolean> {
    return new Promise(async (resolve) => {
      const confirm = await this.alert.create({
        header: title === 'none' ? '' : (title === '' ? t('Alert Notify') : t(title)),
        message: t(message),
        cssClass: cssClass !== '' ? cssClass : 'not-special',
        buttons: [
          {
            text: t('Cancel'),
            role: 'cancel',
            handler: () => {
              return resolve(false);
            },
          },
          {
            text: t('OK'),
            handler: () => {
              return resolve(true);
            },
          },
        ],
      });

      await confirm.present();
    });
  }

  shareGift(): void {
    if (this.room && this.user) {
      if (this.call) {
        const giftData: GiftData = {
          ID: this.room.ID,
          Casts: this.room.Users.filter(userItem => {
            if (this.user) {
              return userItem.ID !== this.user.ID && userItem.Role === 0;
            } else {
              return false;
            }
          }),
          Gift: this.call.GiftPoint,
          GiftStr: this.call.GiftDetail ?? '',
          CallID: this.call.ID,
          GiftInfo: []
        };

        // console.log(giftData);
        this.authService.giftSubject.next(giftData);
        this.navi.navigateForward('/main/chat/share');
      }
    }
  }

  async cancelCall(): Promise<void>{
    if (this.user && this.call && this.call.Status === 'confirm') {
      if (this.user.Role === 1){
        const confirmation = await this.warn(
          '本当にキャンセルしますか？<br>ゲスト様都合の悪質なキャンセルの場合有償100％のキャンセル料が発生する場合があります。\
        キャストと合意の上でのキャンセルをお願い致します。', '');
        if (confirmation) {
          this.executeCancel(this.call.ID);
        }
      }
      if (this.user.Role === 0){
        const confirmation = await this.warn(
          '本当にキャンセルしますか？<br> \
          キャスト都合の悪質なキャンセルの場合、キャンセル料補填として1000Pマイナスさせて頂く可能性があります。<br>\
          ゲスト様とメッセージとの合意の上でキャンセルをしてください。', '');
        if (confirmation) {
          this.executeCancel(this.call.ID);
        }
      }
    }
  }

  executeCancel(callID: number): void {
    this.http.delete<boolean>(`${environment.API_URL}/calls/${callID}`).subscribe((res) => {
      if (res) {

      } else {
        this.showAlert('You can not cancel this call');
      }
    });
  }

  async willSend(): Promise<void> {
    if (this.user && this.user.Role === 1 && this.roomType !== 'admin' && this.room){
      if (!this.user.TelecomCredit && this.maxNumber >= 1){
        let confirmation = false;
        if (!this.room.IsGroup && this.partner) {
          confirmation = await this.warn(
            `<div class = "alert-img-container">
                <ion-avatar>
                  <img src = "${this.partner.Images[0].Path}" class = "alert-img" />
                </ion-avatar>
              </div>` +
            `<strong class="message-see">カードを登録して${this.partner.Nickname}さんにメッセージを送ろう!</strong>` +
            `<p class="alert-paragraph">${t('Credit Card Register is necessary.')}<br>${t('Are you sure register your card?')}<br>（${t('Message give and take is free')}）</p>` +
            `<img class = "card-list-img" src = "assets/img/cardlist.png" />`,
            'none', 'see-message-alert');
        }else{
          confirmation = await this.warn(
            `<strong class="message-see">カードを登録してメッセージを送ろう!</strong>` +
            `<p class="alert-paragraph">${t('Credit Card Register is necessary.')}<br>${t('Are you sure register your card?')}<br>（${t('Message give and take is free')}）</p>` +
            `<img class = "card-list-img" src = "assets/img/cardlist.png" />`,
            'none', 'see-message-alert');
        }

        if (confirmation) {
          // to register credit card
          this.navi.navigateForward('main/mypage/payment');
        }
      }
    }
  }

}
