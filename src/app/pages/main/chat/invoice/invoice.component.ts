import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Call, Join, User } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';
import { InvoiceDetail, Invoice } from 'src/app/interfaces';
import t from 'src/locales';
import { IonBackButtonDelegate, AlertController, NavController } from '@ionic/angular';
import { AuthService } from 'src/app/services';

interface CastInvoice{
  Cast: User;
  Invoice: InvoiceDetail | null;
}

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.scss'],
})
export class InvoiceComponent implements OnInit {

  @ViewChild(IonBackButtonDelegate, { static: false }) backButton !: IonBackButtonDelegate;
  callId = 0;
  call: Call | null = null;
  invoice: Invoice | null = null;
  invoiceDetail: InvoiceDetail[] | null = [];
  invoiceId = 0;
  joins: Join[] = [];
  castInvoices: CastInvoice[] = [];
  role = 1;
  user: User | null = null;

  constructor(
    private route: ActivatedRoute,
    private http: HttpClient,
    private alert: AlertController,
    private auth: AuthService,
    private navi: NavController
  ) {
    const idParam = this.route.snapshot.paramMap.get('callid');

    this.auth.currentUserSubject.subscribe(user => {
      if (user){
        this.role = user.Role;
        this.user = user;
      }
    });

    if (idParam) {
      this.callId = parseInt(idParam, 10);
      // console.log(this.callId);
    }
  }

  ngOnInit(): void {
    this.getCallData();
  }

  ionViewDidEnter(): void {
    this.setBackButtonAction();
  }

  setBackButtonAction(): void {
    this.backButton.onClick = () => {
      this.navi.back();
    };
  }

  getCallData(): void{
    this.http.get<Call>(`${environment.API_URL}/calls/detail/${this.callId}`).subscribe(res => {
      this.call = res;
      // console.log(this.call);
      if (this.call.Status === 'end'){
        this.http.get<number>(`${environment.API_URL}/invoices/call_id?id=${this.call.ID}`).subscribe(id => {
          this.invoiceId = id;
          // console.log(this.invoiceId);
          if (this.invoiceId > 0){
            this.getInvoiceData();
          }
        });
      }else if (this.call.Status === 'starting' || this.call.Status === 'break') {
        this.http.get<{
          invoice: Invoice
        }>(`${environment.API_URL}/calls/invoice?id=${this.call.ID}`).subscribe(invoiceRes => {
          this.invoice = invoiceRes.invoice;
          // console.log(this.invoice);

          if (this.call){
            // guest
            if (this.role === 1){

              this.call.Joined.forEach(item => {
                const castInvoice: CastInvoice = {
                  Cast: item,
                  Invoice: null,
                };

                if (this.invoice && this.invoice.Detail){
                  const detailIndex = this.invoice.Detail.findIndex(invoiceDetailItem => {
                    return invoiceDetailItem.UserID === item.ID;
                  });

                  if (detailIndex > -1){
                    castInvoice.Invoice = this.invoice.Detail[detailIndex];
                  }
                }

                this.castInvoices.push(castInvoice);
              });
            // cast
            }else{
              if (this.user && this.invoice && this.invoice.Detail && this.invoice.Detail.length > 0){
                const castInvoice: CastInvoice = {
                  Cast: this.user,
                  Invoice: this.invoice.Detail[0]
                };
                this.castInvoices.push(castInvoice);
              }
            }
          }
        }, (err: HttpErrorResponse) => {
          if (err.status === 400){
            this.showAlert('Bad Request');
          }
        });
      }
    });
  }

  async showAlert(msgStr: string): Promise<void> {
    const alert = await this.alert.create({
      message: msgStr,
      buttons: ['OK']
    });
    await alert.present();
  }

  getInvoiceData(): void{
    this.http.get<Invoice>(`${environment.API_URL}/users/invoice/${this.invoiceId}`).subscribe(invoice => {
      this.invoice = invoice;
      this.invoiceDetail = invoice.Detail;
    });
  }

  getStatusTitle(glass: Call): string {
    if (glass.Status === 'confirm') {
      return t('Matching Ended');
    } else if (glass.Status === 'break') {
      return t('Waiting for Payment');
    } else if (glass.Status === 'end') {
      return t('Payment Completed');
    } else if (glass.Status === 'starting') {
      return t('Joining Now');
    } else if (glass.Status === 'waiting') {
      return t('Now Waiting for accept');
    } else {
      return t('Undefined');
    }
  }

  getExpiredTime(joinTime: number, period: number): number {
    if (joinTime > period * 60){
      return Math.floor((joinTime - period * 60) / 15) * 15;
    }else{
      return 0;
    }
  }
}
