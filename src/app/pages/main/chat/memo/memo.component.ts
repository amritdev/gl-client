import { HttpClient, HttpParams } from '@angular/common/http';
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { IonInfiniteScroll, ModalController } from '@ionic/angular';
import { Review, User } from 'src/app/interfaces';
import { ConditionService } from 'src/app/services';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-memo',
  templateUrl: './memo.component.html',
  styleUrls: ['./memo.component.scss'],
})
export class MemoComponent implements OnInit {
  @ViewChild(IonInfiniteScroll) ionInfiniteScroll !: IonInfiniteScroll;

  @Input() guests: User[] = [];
  @Input() roomID = 0;

  guest !: User;

  // infinite scroll
  allLoaded = false;
  page = 0;
  offset = 0;
  pageSize = 10;
  reviews: Review[] = [];

  constructor(
    private modal: ModalController,
    private http: HttpClient,
    public cond: ConditionService
  ) { }

  ngOnInit(): void {
    if (this.guests.length > 0) {
      this.guest = this.guests[0];
      this.getReviews(null);
    }else {
      this.onClose();
    }
  }

  onClose(): void {
    this.modal.dismiss();
  }

  getReviews(event: Event | null): void {
    if (this.guest){
      const params = new HttpParams()
        .append('guest_id', this.guest.ID.toString())
        .append('room_id', this.roomID.toString())
        .append('page', this.page.toString());
      this.http.get<Review[]>(`${environment.API_URL}/reviews/specific`, {params}).subscribe(res => {
        if (res.length < this.pageSize) {
          this.allLoaded = true;
        }
        this.reviews = this.reviews.concat(res);
        // console.log(this.reviews);

        if (event && event.target) {
          this.ionInfiniteScroll.complete();
        }
      });
    }
  }

  loadData(event: Event | null): void {
    this.page++;
    this.getReviews(event);
  }
}
