import { Component, OnInit, Input } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { ModalController, AlertController } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import t from 'src/locales';
import { User } from 'src/app/interfaces';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss'],
})
export class ReportComponent implements OnInit {

  @Input() users: User[] = [];
  @Input() role = 1;
  targetID = 0;
  reportContent = '';
  typeStr = 'default';

  constructor(
    private http: HttpClient,
    private modalController: ModalController,
    private alert: AlertController,
  ) { }

  ngOnInit(): void {
    if (this.users.length > 0){
      this.targetID = this.users[0].ID;
    }
  }

  onSave(): void {
    if (this.targetID === 0){
      this.presentAlert(t('Please select target user'));
    }else if (this.reportContent.trim() === ''){
      this.presentAlert(t('Please input report content'));
    }else{
      this.http.post<{}>(`${environment.API_URL}/users/report`, {
        target_id: this.targetID,
        content: this.reportContent.trim(),
        type: this.typeStr
      }).subscribe(() => {
        this.modalController.dismiss(true);
      }, (_: HttpErrorResponse) => {
        this.presentAlert(t('Operation Failed'));
      });
    }
  }

  async presentAlert(msg: string): Promise<void> {
    const alert = await this.alert.create({
      message: msg,
      buttons: [t('OK')],
    });
    await alert.present();
  }

  onClose(): void {
    this.modalController.dismiss(false);
  }

  async showAlert(msgStr: string): Promise<void> {
    const alert = await this.alert.create({
      message: msgStr,
      buttons: ['OK']
    });
    await alert.present();
  }
}
