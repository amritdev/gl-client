import { Component, OnInit } from '@angular/core';
import { ValueComponent } from '../value/value.component';
import { AuthService } from 'src/app/services';
import { ModalController, NavController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { GiftData, GiftValue } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-share',
  templateUrl: './share.component.html',
  styleUrls: ['./share.component.scss'],
})
export class ShareComponent implements OnInit {

  giftData !: GiftData;

  constructor(
    private auth: AuthService,
    private modal: ModalController,
    private http: HttpClient,
    private navi: NavController
  ) {
    this.auth.giftSubject.subscribe((giftData) => {
      if (giftData){
        this.giftData = giftData;

        this.giftData.GiftInfo = JSON.parse(this.giftData.GiftStr !== '' ? this.giftData.GiftStr : '[]') as GiftValue[];
        if (this.giftData.GiftInfo.length === 0){
          const samePoint: number = Math.floor(this.giftData.Gift / this.giftData.Casts.length);
          this.giftData.Casts.map(item => {
            this.giftData.GiftInfo.push({
              ID: item.ID,
              Point: samePoint
            });
          });
        }
      }
    });
  }

  ngOnInit(): void {}

  async changeGiftPoint(): Promise<void>{
    const modal = await this.modal.create({
      component: ValueComponent,
      cssClass: 'gift-modal ion-align-items-end'
    });
    await modal.present();
    await modal.onDidDismiss();
  }

  getGift(id: number): number{
    const giftItem = this.giftData.GiftInfo.find((item) => item.ID === id);
    if (giftItem !== undefined){
      return giftItem.Point;
    }else{
      return 0;
    }
  }

  getBackURI(): string{
    return `/main/chat/detail/${this.giftData.ID}`;
  }

  onMinus(id: number): void{
    const giftItem = this.giftData.GiftInfo.find((item) => item.ID === id);
    if (giftItem !== undefined){
      giftItem.Point = Math.max(0, giftItem.Point - 100);
    }
  }

  onPlus(id: number): void{
    const index = this.giftData.GiftInfo.findIndex((item) => item.ID === id);
    if (index > -1){
      let marginVal = this.giftData.Gift;
      for (const giftItem of this.giftData.GiftInfo){
        if (giftItem.ID !== this.giftData.GiftInfo[index].ID){
          marginVal -= giftItem.Point;
        }
      }
      if (this.giftData.GiftInfo[index].Point < marginVal){
        this.giftData.GiftInfo[index].Point += 100;
      }
    }
  }

  confirmSharePoint(): void{
    let giftPoint = 0;
    this.giftData.GiftInfo.map((item) => {
      giftPoint += item.Point;
    });
    this.giftData.Gift = giftPoint;

    this.http.post<{
      'success': boolean
    }>(`${environment.API_URL}/calls/gift`, {
      id: this.giftData.CallID,
      gift: this.giftData.Gift,
      giftStr: JSON.stringify(this.giftData.GiftInfo)
    }).subscribe((res) => {
      if (res.success){
        this.giftData.GiftStr = JSON.stringify(this.giftData.GiftInfo);
        this.auth.giftSubject.next(this.giftData);
        this.navi.navigateRoot([`/main/chat/detail/${this.giftData.ID}`]);
      }
    });
  }

}
