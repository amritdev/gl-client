import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ModalController } from '@ionic/angular';

import { AuthService } from 'src/app/services';
import { Gift, GiftData } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-value',
  templateUrl: './value.component.html',
  styleUrls: ['./value.component.scss'],
})
export class ValueComponent implements OnInit {

  giftData !: GiftData;
  giftSum = 0;
  giftToday = 5000;
  gifts: Gift[] = [];

  constructor(
    private modal: ModalController,
    private auth: AuthService,
    private http: HttpClient
  ) {
    this.auth.giftSubject.subscribe(giftData => {
      if (giftData){
        this.giftData = giftData;
        this.giftSum = this.giftData.Gift;
      }
    });
  }

  ngOnInit(): void {
    this.getAvailableGifts();
    this.getMaxGifts();
  }

  getMaxGifts(): void{
    this.http.get<{
      Max: number
    }>(`${environment.API_URL}/calls/maxGift`).subscribe((res) => {
      this.giftToday = res.Max;
    });
  }

  confirmGift(): void{
    this.giftData.Gift = this.giftSum;
    this.modal.dismiss();
  }

  getValue(): number{
    if (this.giftSum >= 50000){
      return 1;
    }else if (this.giftSum >= 10000){
      return 5 / 8;
    }else if (this.giftSum >= 5000 ){
      return 3 / 8;
    }else{
      return 0;
    }
  }

  getAvailableGifts(): void{
    this.http.get<Gift[]>(`${environment.API_URL}/admin/gifts/call`).subscribe((res) => {
      this.gifts = res;
    });
  }

  upSum(giftPoint: number): void{
    this.giftSum += giftPoint;
  }

  formatGift(): void{
    this.giftSum = 0;
  }

}
