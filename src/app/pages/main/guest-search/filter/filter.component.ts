import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { NavController, AlertController } from '@ionic/angular';

import { AuthService, GuestFilterService } from 'src/app/services';
import { Detail, Class, GuestSearchFilter, Location } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';
import t from 'src/locales';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss'],
})
export class FilterComponent implements OnInit {

  // filter condition
  filterCond: GuestSearchFilter | null = null;

  // guest levels
  guestLevels: Class[] = [];
  levelsReady = false;

  // subscriptions
  searchFilterSubscription: unknown = null;
  residences: Detail[] = [];
  locations: Location[] = [];

  constructor(
    private http: HttpClient,
    private cond: GuestFilterService,
    private nav: NavController,
    private auth: AuthService,
    private alert: AlertController
  ) {
    this.auth.currentUserSubject.subscribe((user) => {
      if (user){
        if (user.LocationIDs){
          // const locationIDs = user.LocationIDs.split(',').filter(item => {
          //   return item !== '';
          // }).map(item => {
          //   return parseInt(item, 10);
          // });
          if (this.searchFilterSubscription === null) {
            this.searchFilterSubscription = this.cond.guestFilterSubject.subscribe((filter) => {
              if (filter) {
                this.filterCond = filter;
                // if (locationIDs.length > 0 && !this.cond.isCleared){
                //   this.filterCond.Location = locationIDs[0];
                // }
              }
            });
          }
        }
      }

    });

  }

  ngOnInit(): void {
    this.getGuestLevels();
    // this.getResidences();
    this.getLocations();
  }

  getGuestLevels(): void {
    this.http.get<Class[]>(`${environment.API_URL}/admin/levels/guest`).subscribe(res => {
      this.guestLevels = res;
      this.levelsReady = true;
    });
  }

  getResidences(): void {
    this.http.get<Detail[]>(`${environment.API_URL}/admin/details/residence`).subscribe((data) => {
      this.residences = data;
    }, (err: HttpErrorResponse) => {
      console.log(err);
    });
  }

  getLocations(): void {
    this.http.get<Location[]>(`${environment.API_URL}/locations?pid=0`).subscribe(locations => {
      this.locations = locations;
    }, async () => {
      const alert = await this.alert.create({
        message: t('Operation Failed'),
        buttons: [t('OK')],
      });
      await alert.present();
    });
  }

  execSearch(): void {
    if (this.filterCond) {
      this.filterCond.SearchStr = [];

      // location
      if (this.filterCond.Location > 0) {
        const locationIndex = this.locations.findIndex(item => this.filterCond && item.ID === this.filterCond.Location);
        if (locationIndex > -1) {
          this.filterCond.SearchStr = [this.locations[locationIndex].Name];
        }
      }

      // class
      if (this.filterCond.Level.length > 0) {
        this.filterCond.Level.forEach((classID: number) => {
          const levelIndex = this.guestLevels.findIndex(item => item.ID === classID);
          if (this.filterCond && levelIndex > -1){
            this.filterCond.SearchStr.push(this.guestLevels[levelIndex].Name);
          }
        });
      }

      // birthday this month
      if (this.filterCond.Recently){
        this.filterCond.SearchStr.push(`${t('Recently')}${t('Registered')}`);
      }

      localStorage.setItem('guestFilterCond', JSON.stringify(this.filterCond));
    }

    this.cond.guestFilterSubject.next(this.filterCond);
    this.nav.navigateBack('/main/guest-search');
    this.cond.isCleared = true;
  }

  clearFilter(): void {
    this.cond.isCleared = true;
    this.cond.reset();
  }
}
