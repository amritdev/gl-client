import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GuestSearchComponent } from './guest-search.component';
import { FilterComponent } from './filter/filter.component';

const routes: Routes = [
  {
    path: '',
    component: GuestSearchComponent,
  },
  {
    path: 'filter',
    component: FilterComponent
  },
  {
    path: 'rank',
    loadChildren: () => import('../rank/rank.module').then(m => m.RankModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GuestSearchRoutingModule { }
