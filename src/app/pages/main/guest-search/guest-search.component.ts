import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { IonInfiniteScroll, IonContent } from '@ionic/angular';
import { User, GuestSearchFilter, Class, Location } from 'src/app/interfaces';
import { AuthService, ConditionService, GuestFilterService, WebsocketService } from 'src/app/services';
import { environment } from 'src/environments/environment';
import t from 'src/locales';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-guest-search',
  templateUrl: './guest-search.component.html',
  styleUrls: ['./guest-search.component.scss'],
})
export class GuestSearchComponent implements OnInit, OnDestroy {

  @ViewChild('ionContent') ionContent!: IonContent;
  @ViewChild(IonInfiniteScroll) ionInfiniteScroll !: IonInfiniteScroll;

  // guest levels
  guestLevels: Class[] = [];
  levelsReady = false;
  userLocationIDs: number[] = [];
  locations: Location[] = [];

  // favorite guests
  favoriteGuests: number[] = [];
  isFavoriteDataReady = false;

  // recent guests
  recentGuests: User[] = [];
  filteredGuests: User[] = [];

  // for slider
  slideOpts = {
    slidesPerView: 2.7,
    spaceBetween: 10
  };

  // for searching guests
  guestFilter: GuestSearchFilter | null = null;

  // infinite scroll
  allLoaded = false;
  page = 0;
  offset = 0;
  connectSubscription: Subscription | null = null;

  // full guests
  isFullGuestsReady = false;
  fullGuests: User[] = [];

  // search query
  searchText = '';

  // subscriptions
  userSubscription: Subscription | null = null;
  searchFilterSubscription: Subscription | null = null;
  reloadSubscription: Subscription | null = null;
  withdrawnReloadSubscription: Subscription | null = null;
  user: User | null = null;
  isFirst = true;

  // block user
  blockUserIDs: number[] = [];

  constructor(
    private auth: AuthService,
    private http: HttpClient,
    private cond: GuestFilterService,
    public callCond: ConditionService,
    private ws: WebsocketService,
    private router: Router
  ) {
    this.getLocations();

    if (this.reloadSubscription === null) {
      this.reloadSubscription = this.auth.reloadSubject.subscribe(res => {
        if (res) {
          this.reloadGuests();
        }
      });
    }
  }

  ngOnInit(): void {
    this.getRecentGuests();
    this.getGuestLevels();
    this.getBlockUserIDs();
  }

  ngOnDestroy(): void {
    this.userSubscription?.unsubscribe();
    this.searchFilterSubscription?.unsubscribe();
    this.reloadSubscription?.unsubscribe();
    this.withdrawnReloadSubscription?.unsubscribe();
  }

  getBlockUserIDs(): void {
    this.http.get<number[]>(`${environment.API_URL}/users/block/all`).subscribe(res => {
      // console.log(res);
      this.blockUserIDs = res;
    });
  }

  reloadGuests(): void {
    this.getRecentGuests();
    this.filteredGuests = [];
    this.allLoaded = false;
    this.page = 0;
    this.offset = 0;
    this.execSearch(null);
  }

  getLocations(): void {
    this.http.get<Location[]>(`${environment.API_URL}/locations?pid=0`).subscribe(locations => {
      this.locations = locations;

      // user subscription after locations fetched
      if (this.userSubscription === null) {
        this.userSubscription = this.auth.currentUserSubject.subscribe((user) => {
          if (user){
            this.user = user;
            if (user.Favorites) {
              this.favoriteGuests = JSON.parse(user.Favorites);
            }
            this.isFavoriteDataReady = true;

            if (user.LocationIDs) {

              this.userLocationIDs = user.LocationIDs.split(',').filter(item => {
                return item !== '';
              }).map(item => {
                return parseInt(item, 10);
              });

              if (this.searchFilterSubscription === null) {
                this.searchFilterSubscription = this.cond.guestFilterSubject.subscribe((filter) => {
                  if (filter) {
                    this.guestFilter = filter;
                    this.allLoaded = false;
                    this.filteredGuests = [];
                    this.page = 0;
                    this.offset = 0;
                    if (this.userLocationIDs.length > 0 && !this.cond.isCleared && this.isFirst && !this.cond.isPrevExist) {
                      this.guestFilter.Location = this.userLocationIDs[0];
                      const locationIndex = this.locations.findIndex(item => this.guestFilter && item.ID === this.guestFilter.Location);
                      if (locationIndex > -1) {
                        this.guestFilter.SearchStr = [this.locations[locationIndex].Name];
                      }
                      this.isFirst = false;
                    }
                    this.execSearch(null);

                    // make search text
                    if (this.guestFilter.SearchStr === undefined){
                      this.guestFilter.SearchStr = [];
                      this.searchText = '';
                    }else {
                      this.searchText = this.guestFilter.SearchStr.join(', ');
                    }
                  }
                });
              }
            }

          }

        });
      }
    });
  }

  getGuestLevels(): void {
    this.http.get<Class[]>(`${environment.API_URL}/admin/levels/guest`).subscribe(res => {
      this.guestLevels = res;
      this.levelsReady = true;
    });
  }

  getRecentGuests(): void {
    this.http.get<User[]>(`${environment.API_URL}/guests/recent`).subscribe(res => {
      this.recentGuests = res;
    });
  }

  execSearch(event: Event | null): void {
    // if guestlevel array does not exist, wait
    if (this.guestLevels.length === 0) {
      setTimeout(() => {
        this.execSearch(event);
      }, 500);
      return;
    }

    // guest level condition generation
    const levelCondStr: string[] = [];
    if (this.guestFilter) {
      if (this.guestFilter.Level.length > 0) {
        for (const levelID of this.guestFilter.Level) {
          const curIndex = this.guestLevels.findIndex(item => item.ID === levelID);
          if (curIndex === 0) {
            levelCondStr.push(`(point_used < ${this.guestLevels[0].Point})`);
          } else if (curIndex === this.guestLevels.length - 1) {
            levelCondStr.push(`(point_used >= ${this.guestLevels[curIndex - 1].Point})`);
          } else {
            levelCondStr.push(
              `(point_used < ${this.guestLevels[curIndex].Point} AND point_used >= ${this.guestLevels[curIndex - 1].Point})`
            );
          }
        }
        this.guestFilter.LevelStr = `(${levelCondStr.join(' OR ')})`;
      }
    }
    // console.log(`(${levelCondStr.join(" OR ")})`);

    this.http.post<User[]>(`${environment.API_URL}/guests/filter/${this.page}/${this.offset}`, this.guestFilter).subscribe((data) => {
      if (data.length < environment.CAST_PER_PAGE) {
        this.allLoaded = true;
      }

      this.filteredGuests = this.filteredGuests.concat(data);
      if (event && event.target) {
        this.ionInfiniteScroll.complete();
      }

      // connected user subscription
      if (this.connectSubscription === null) {
        this.connectSubscription = this.ws.connectUserSubject.subscribe(res => {
          if (this.guestFilter && this.auth.checkUser(res, 'cast', this.guestFilter.Location, this.blockUserIDs, this.guestLevels)) {
            const guestIndex = this.filteredGuests.findIndex(item => item.ID === res.ID);

            if (guestIndex > -1){
              this.filteredGuests.splice(guestIndex, 1);
              this.filteredGuests.unshift(res);
            }else{
              this.filteredGuests.unshift(res);
              this.offset++;
            }
          }
        });
      }

      // reload user subsubcription
      if (this.withdrawnReloadSubscription === null) {
        this.withdrawnReloadSubscription = this.ws.reloadSubject.subscribe(eventStr => {
          if (eventStr === 'search') {
            this.reloadGuests();
          }
        });
      }
    }, (err: HttpErrorResponse) => {
      console.log(err);
    });
  }

  loadData(event: Event | null): void {
    this.page++;
    this.execSearch(event);
  }

  getNotifyCount(): number {
    return this.ws.unreadNotices;
  }

  getLevelName(usedPoint: number): string {
    if (this.guestLevels.length > 0) {
      for (const levelItem of this.guestLevels) {
        if (usedPoint < levelItem.Point) {
          return levelItem.Name;
        } else {
          continue;
        }
      }
      return this.guestLevels[this.guestLevels.length - 1].Name;
    } else {
      return t('Undefined');
    }
  }

  getLevelNum(usedPoint: number): number {
    if (this.guestLevels.length > 0) {
      for (const [index, levelItem] of this.guestLevels.entries()) {
        if (usedPoint < levelItem.Point) {
          return index;
        } else {
          continue;
        }
      }
      return this.guestLevels.length;
    } else {
      return 0;
    }
  }

  isFavorite(id: number): boolean {
    return this.favoriteGuests.includes(id);
  }

  gotoLine(): void{
    window.location.href = 'https://lin.ee/6fAJj9Tg';
  }

  clearSearch(event: Event): void {
    event.preventDefault();
    event.stopPropagation();

    this.scrollToTop();
    this.cond.isCleared = true;
    const formattedFilter = this.cond.reset();
    localStorage.setItem('guestFilterCond', JSON.stringify(formattedFilter));
  }

  goToFilterPage(): void {
    this.router.navigate(['/main/guest-search/filter']);
  }

  scrollToTop(): void {
    this.ionContent?.scrollToTop();
  }
}
