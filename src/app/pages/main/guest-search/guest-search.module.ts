import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PipesModule, ComponentsModule } from 'src/app/shared';

import { GuestSearchRoutingModule } from './guest-search-routing.module';
import { GuestSearchComponent } from './guest-search.component';
import { FilterComponent } from './filter/filter.component';

@NgModule({
  declarations: [
    GuestSearchComponent,
    FilterComponent
  ],
  imports: [
    CommonModule,
    GuestSearchRoutingModule,
    IonicModule,
    FormsModule,
    PipesModule,
    ComponentsModule
  ]
})
export class GuestSearchModule { }
