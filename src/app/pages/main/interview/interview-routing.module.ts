import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InterviewComponent } from './interview.component';
import { ProfileComponent } from './profile/profile.component';
import { LineComponent } from './line/line.component';

const routes: Routes = [
  {
    path: '',
    // component: InterviewComponent,
    redirectTo: 'profile',
    pathMatch: 'full'
  },
  {
    path: 'profile',
    component: ProfileComponent
  },
  {
    path: 'line',
    component: LineComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InterviewRoutingModule { }
