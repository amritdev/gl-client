import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { AlertController, NavController } from '@ionic/angular';

import * as moment from 'moment';

import { AuthService } from 'src/app/services';
import { Detail } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';
import t from 'src/locales';

interface DetailWithChildren extends Detail {
  Children?: Detail[];
}

@Component({
  selector: 'app-interview',
  templateUrl: './interview.component.html',
  styleUrls: ['./interview.component.scss'],
})
export class InterviewComponent implements OnInit {

  details: DetailWithChildren[] = [];
  detailArr: number[] = [];
  interviewDate1 = '';
  interviewTime1 = '';
  interviewDate2 = '';
  interviewTime2 = '';
  interviewDate3 = '';
  interviewTime3 = '';
  interviewDatetimes: string[] = [];
  image1: File | null = null;
  image1URL = '';
  image2: File | null = null;
  image2URL = '';
  image3: File | null = null;
  image3URL = '';
  experience: string[] = [];
  ask = '';

  constructor(
    private authService: AuthService,
    private http: HttpClient,
    private navController: NavController,
    private alertController: AlertController,
  ) { }

  ngOnInit(): void {
    this.getDetails();
  }

  async presentAlert(msg: string): Promise<void> {
    const alert = await this.alertController.create({
      message: msg,
      buttons: [t('OK')],
    });
    await alert.present();
  }

  getDetails(): void {
    this.http.get<Detail[]>(`${environment.API_URL}/details`).subscribe(details => {
      let parentDetail = details.find(detail => detail.Name === '身長');
      if (parentDetail) {
        this.details.push(parentDetail);
      }
      parentDetail = details.find(detail => detail.Name === '体系');
      if (parentDetail) {
        this.details.push(parentDetail);
      }
      parentDetail = details.find(detail => detail.Name === '職業');
      if (parentDetail) {
        this.details.push(parentDetail);
      }
      this.details.forEach(detail => {
        detail.Children = details.filter(item => item.ParentID === detail.ID) ?? [];
      });
    });
  }

  getSelectedValue(detail: DetailWithChildren): number {
    const selectedDetail = detail.Children?.find(item => this.detailArr.includes(item.ID));
    return selectedDetail ? selectedDetail.ID : 0;
  }

  onSelect(event: Event, beforeValue: number): void {
    if (event instanceof CustomEvent) {
      const index = this.detailArr.findIndex(ID => ID === beforeValue);
      if (index > -1) {
        this.detailArr.splice(index, 1);
      }
      this.detailArr.push(event.detail.value);
    }
  }

  onFileChanged(event: Event, type: number): void {
    if (event.target instanceof HTMLInputElement && event.target.files && event.target.files[0]) {
      if (type === 1) {
        this.image1 = event.target.files[0];
      } else if (type === 2) {
        this.image2 = event.target.files[0];
      }
      // } else if (type === 3) {
      //   this.image3 = event.target.files[0];
      // }
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = () => {
        if (type === 1) {
          this.image1URL = reader.result as string;
        } else if (type === 2) {
          this.image2URL = reader.result as string;
        }
        // } else if (type === 3) {
        //   this.image3URL = reader.result as string;
        // }
      };
    }
  }

  onSave(): void {
    if (this.interviewDate1 === '' || this.interviewTime1 === '') {
      this.presentAlert(t('Please select interview hope day'));
    } else if (!this.image1 || !this.image2) {
    // } else if (!this.image1 || !this.image2 || !this.image3) {
      this.presentAlert(t('Please upload images'));
    } else if (this.detailArr.length < 1 || this.experience.length < 1) {
      this.presentAlert(t('Please select basic information'));
    } else {
      let datetime = moment(this.interviewDate1).format('MM月 DD日') + ' ' + moment(this.interviewTime1).format('HH時 mm分');
      this.interviewDatetimes.push(datetime);
      if (this.interviewDate2 !== '' && this.interviewTime2 !== '') {
        datetime = moment(this.interviewDate2).format('MM月 DD日') + ' ' + moment(this.interviewTime2).format('HH時 mm分');
        this.interviewDatetimes.push(datetime);
      }
      if (this.interviewDate3 !== '' && this.interviewTime3 !== '') {
        datetime = moment(this.interviewDate3).format('MM月 DD日') + ' ' + moment(this.interviewTime3).format('HH時 mm分');
        this.interviewDatetimes.push(datetime);
      }
      const formData = new FormData();
      formData.append('datetimes', JSON.stringify(this.interviewDatetimes));
      formData.append('image1', this.image1);
      formData.append('image2', this.image2);
      // formData.append('image3', this.image3);
      formData.append('detail', JSON.stringify(this.detailArr).replace('[', ',').replace(']', ','));
      formData.append('experience', JSON.stringify(this.experience));
      formData.append('ask', this.ask.trim());
      this.http.post<{}>(`${environment.API_URL}/users/interview`, formData).subscribe(() => {
        this.presentAlert(t('The application was successful. Pleaes wait for the contact from the concierge.'));
        localStorage.removeItem('token');
        this.navController.navigateRoot('/cast');
      }, () => {
        this.presentAlert(t('Operation Failed'));
      });
    }
  }

  async onCancel(): Promise<void> {
    const alert = await this.alertController.create({
      message: t('Do you want to cancel your application?'),
      buttons: [t('Cancel'), {
        text: t('OK'),
        handler: () => {
          this.http.post<{}>(`${environment.API_URL}/users/profile/role`, {}).subscribe(() => {
            this.authService.logout();
            this.navController.navigateRoot('/');
          }, async () => {
            const otherAlert = await this.alertController.create({
              message: t('Operation Failed'),
              buttons: [t('OK')],
            });
            await otherAlert.present();
          });
        }
      }],
    });
    await alert.present();
  }

}
