import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PipesModule } from 'src/app/shared';

import { InterviewRoutingModule } from './interview-routing.module';
import { InterviewComponent } from './interview.component';
import { ProfileComponent } from './profile/profile.component';
import { LineComponent } from './line/line.component';

@NgModule({
  declarations: [
    InterviewComponent,
    ProfileComponent,
    LineComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    PipesModule,
    InterviewRoutingModule,
  ]
})
export class InterviewModule { }
