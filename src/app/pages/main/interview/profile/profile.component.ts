import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

import { AlertController, NavController } from '@ionic/angular';

import { Location, User } from 'src/app/interfaces';
import { AuthService } from 'src/app/services';
import { environment } from 'src/environments/environment';
import t from 'src/locales';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {

  isLoading = false;
  locations: Location[] = [];
  nickname = '';
  locationID = 0;

  constructor(
    private auth: AuthService,
    private http: HttpClient,
    private nav: NavController,
    private alert: AlertController
  ) {
    this.auth.currentUserSubject.subscribe((user) => {
      if (user && user.Nickname) {
        this.nav.navigateRoot('/main/interview/line');
      }
    });
  }

  ngOnInit(): void {
    this.getLocations();
  }

  async presentAlert(msg: string): Promise<void> {
    const alert = await this.alert.create({
      message: msg,
      buttons: [t('OK')],
    });
    await alert.present();
  }

  getLocations(): void {
    this.http.get<Location[]>(`${environment.API_URL}/locations?pid=0`).subscribe((locations) => {
      this.locations = locations;
    });
  }

  onSave(): void {
    if (this.nickname.trim() === '') {
      this.presentAlert(t('Please input name'));
    } else if (this.locationID < 1) {
      this.presentAlert(t('Please select registration area'));
    } else {
      this.http.put<User>(`${environment.API_URL}/users/interview/profile`, {
        nickname: this.nickname.trim(),
        location_id: this.locationID.toString()
      }).subscribe((user) => {
        this.auth.currentUserSubject.next(user);
        this.nav.navigateForward('/main/interview/line');
      }, (err: HttpErrorResponse) => {
        if (err.status === 409) {
          this.presentAlert(t('Name already exists'));
        } else {
          this.presentAlert(t('Operation Failed'));
        }
      });
    }
  }

}
