import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { IonInfiniteScroll, NavController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { Class, User } from 'src/app/interfaces';
import { AuthService, ConditionService, WebsocketService } from 'src/app/services';
import { environment } from 'src/environments/environment';
import t from 'src/locales';

@Component({
  selector: 'app-like',
  templateUrl: './like.component.html',
  styleUrls: ['./like.component.scss'],
})
export class LikeComponent implements OnInit, OnDestroy {

  @ViewChild(IonInfiniteScroll) ionInfiniteScroll !: IonInfiniteScroll;

  userSubscription: Subscription | null = null;

  // guest levels
  guestLevels: Class[] = [];
  levelsReady = false;

  // guests
  guests: User[] = [];
  favoriteGuests: User[] = [];

  // infinite scroll
  allLoaded = false;
  connectSubscription: Subscription | null = null;
  page = 0;
  offset = 0;
  pageSize = 10;

  constructor(
    private http: HttpClient,
    private auth: AuthService,
    private navi: NavController,
    private websocket: WebsocketService,
    public cond: ConditionService
  ) {
    if (this.userSubscription === null) {
      this.userSubscription = this.auth.currentUserSubject.subscribe(user => {
        if (user) {
          this.getFavoriteGuests();
        }
      });
    }
  }

  ngOnInit(): void {
    this.auth.enabled = false;
    this.getGuestLevels();
    this.execSearch(null);
  }

  ngOnDestroy(): void {
    this.auth.enabled = true;
  }

  goToNext(): void{
    this.goToMainPage();
  }

  goToMainPage(): void {
    this.auth.enabled = true;

    // save tutorial viewed time
    this.http.get<User>(`${environment.API_URL}/users/like`).subscribe(res => {
      this.userSubscription?.unsubscribe();
      this.auth.currentUserSubject.next(res);

      // go to main page
      this.navi.navigateRoot('/main/guest-search');
    });
  }

  getFavoriteGuests(): void {
    this.http.get<User[]>(`${environment.API_URL}/users/favorite`).subscribe(res => {
      // console.log(res);
      this.favoriteGuests = res;
    });
  }

  getGuestLevels(): void {
    this.http.get<Class[]>(`${environment.API_URL}/admin/levels/guest`).subscribe(res => {
      this.guestLevels = res;
      this.levelsReady = true;
    });
  }

  getLevelName(usedPoint: number): string {
    if (this.guestLevels.length > 0) {
      for (const levelItem of this.guestLevels) {
        if (usedPoint < levelItem.Point) {
          return levelItem.Name;
        } else {
          continue;
        }
      }
      return this.guestLevels[this.guestLevels.length - 1].Name;
    } else {
      return t('Undefined');
    }
  }

  getLevelNum(usedPoint: number): number {
    if (this.guestLevels.length > 0) {
      for (const [index, levelItem] of this.guestLevels.entries()) {
        if (usedPoint < levelItem.Point) {
          return index;
        } else {
          continue;
        }
      }
      return this.guestLevels.length;
    } else {
      return 0;
    }
  }

  execSearch(event: Event | null): void {
    this.http.get<User[]>(`${environment.API_URL}/guests/all`).subscribe((res) => {
      // if (res.length < this.pageSize) {
      //   this.allLoaded = true;
      // }
      this.guests = this.guests.concat(res);
      // console.log(this.guests);

      if (event && event.target) {
        this.ionInfiniteScroll.complete();
      }

      if (this.connectSubscription === null){
        this.connectSubscription = this.websocket.connectUserSubject.subscribe(connector => {
          const desireIndex = this.guests.findIndex(item => item.ID === connector.ID);

          if (desireIndex > -1){
            this.guests.splice(desireIndex, 1);
            this.guests.unshift(connector);
          }else{
            this.guests.unshift(connector);
            this.offset++;
          }
        });
      }
    }, (err: HttpErrorResponse) => {
      console.log(err);
    });
  }


  isFavorite(id: number): boolean{
    return this.favoriteGuests.findIndex(item => item.ID === id) > -1;
  }

  loadData(event: Event | null): void {
    this.page++;
    this.execSearch(event);
  }

  getArray(length: number): Array<unknown>{
    return new Array(length);
  }

  changeFavorite(guest: User): void{
    try {
      const index = this.favoriteGuests.findIndex(item => item.ID === guest.ID);
      if (index > -1) {
        this.favoriteGuests.splice(index, 1);
      } else {
        if (this.favoriteGuests.length < 20){
          this.favoriteGuests.push(guest);
        }
      }
      // console.log(this.favoriteGuests);
    } catch (ex) {
      console.log(ex);
      this.favoriteGuests = [guest];
    }
  }

  executeLike(): void {
    let itemArray: number[] = this.favoriteGuests.map((item: User) => {
      return item.ID;
    });

    const newGuests = this.guests.filter(item => !itemArray.includes(item.ID)).map(item => item.ID);
    itemArray = itemArray.concat(newGuests);

    if (itemArray.length > 0) {
      const guestStr = JSON.stringify(itemArray);
      const formData = new FormData();
      formData.append('favorite_users', guestStr);

      this.http.post<boolean>(`${environment.API_URL}/users/profile/register/like`, formData).subscribe(res => {
        if (res){
          this.goToNext();
        }
      });
    }else{
      this.goToNext();
    }
  }

  viewProfile(guestID: string): void {
    this.navi.navigateForward(`/main/profile/${guestID}`);
  }
}
