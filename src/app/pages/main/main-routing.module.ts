import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {
  RegisterGuard,
  GuestGuard,
  CastGuard,
  CallGuard,
  TutorialGuard,
  AntiTutorialGuard,
  AntiRegisterGuard,
  ProfileGuard,
  LikeGuard,
  AntiLikeGuard,
  AntiReviewGuard,
  SightGuard
} from 'src/app/guards';
import { MainComponent } from './main.component';
import { ProfileComponent } from './profile/profile.component';
import { SituationComponent } from './call/situation/situation.component';
import { DesireComponent } from './call/desire/desire.component';
import { WarningComponent } from './call/warning/warning.component';
import { ConfirmComponent } from './call/confirm/confirm.component';
import { EditComponent } from './call/edit/edit.component';
import { NoticeComponent } from './notice/notice.component';
import { TutorialComponent } from './tutorial/tutorial.component';
import { LikeComponent } from './like/like.component';
import { ReviewComponent } from './review/review.component';
import { ReviewGuard } from 'src/app/guards/review.guard';
import { DetailComponent } from './notice/detail/detail.component';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    canActivate: [RegisterGuard],
    children: [
      {
        path: '',
        redirectTo: 'call',
        pathMatch: 'full',
      },
      {
        path: 'apply',
        loadChildren: () => import('./apply/apply.module').then(m => m.ApplyModule),
        canActivate: [CastGuard, ReviewGuard, TutorialGuard]
      },
      {
        path: 'call',
        loadChildren: () => import('./call/call.module').then(m => m.CallModule),
        canActivate: [GuestGuard, TutorialGuard]
      },
      {
        path: 'search',
        loadChildren: () => import('./search/search.module').then(m => m.SearchModule),
        canActivate: [GuestGuard]
      },
      {
        path: 'guest-search',
        loadChildren: () => import('./guest-search/guest-search.module').then(m => m.GuestSearchModule),
        canActivate: [CastGuard, ReviewGuard, LikeGuard]
      },
      {
        path: 'chat',
        loadChildren: () => import('./chat/chat.module').then(m => m.ChatModule),
        canActivate: [ReviewGuard]
      },
      {
        path: 'tweet',
        loadChildren: () => import('./tweet/tweet.module').then(m => m.TweetModule),
        canActivate: [ReviewGuard]
      },
      {
        path: 'mypage',
        loadChildren: () => import('./mypage/mypage.module').then(m => m.MypageModule),
        canActivate: [ReviewGuard]
      },
    ],
  },
  {
    path: 'tutorial',
    component: TutorialComponent,
    canActivate: [SightGuard, AntiTutorialGuard],
  },
  {
    path: 'review',
    component: ReviewComponent,
    canActivate: [SightGuard, AntiReviewGuard],
  },
  {
    path: 'like',
    component: LikeComponent,
    canActivate: [SightGuard, AntiLikeGuard],
  },
  {
    path: 'register',
    loadChildren: () => import('./register/register.module').then(m => m.RegisterModule),
    canActivate: [AntiRegisterGuard]
  },
  // {
  //   path: 'interview',
  //   loadChildren: () => import('./interview/interview.module').then(m => m.InterviewModule),
  //   canLoad: [RegisterGuard],
  // },
  // {
  //   path: 'qrcode',
  //   component: QrcodeComponent,
  //   canActivate: [RegisterGuard],
  // },
  // {
  //   path: 'rank',
  //   loadChildren: () => import('./rank/rank.module').then(m => m.RankModule),
  //   canActivate: [RegisterGuard],
  // },
  {
    path: 'profile/:id',
    component: ProfileComponent,
    canActivate: [SightGuard, RegisterGuard, ProfileGuard],
  },
  {
    path: 'call/situation',
    component: SituationComponent,
    canActivate: [RegisterGuard, CallGuard],
  },
  {
    path: 'call/desire',
    component: DesireComponent,
    canActivate: [RegisterGuard, CallGuard],
  },
  {
    path: 'call/warning',
    component: WarningComponent,
    canActivate: [RegisterGuard, CallGuard],
  },
  {
    path: 'call/confirm',
    component: ConfirmComponent,
    canActivate: [RegisterGuard, CallGuard],
  },
  {
    path: 'call/edit',
    component: EditComponent,
    canActivate: [RegisterGuard, CallGuard],
  },
  {
    path: 'notice',
    component: NoticeComponent,
    canActivate: [SightGuard, RegisterGuard],
    // children: [
    //   {
    //     path: 'detail/:id',
    //     component: DetailComponent
    //   }
    // ]
  },
  {
    path: 'notice/detail/:id',
    component: DetailComponent,
    canActivate: [SightGuard, RegisterGuard],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MainRoutingModule { }
