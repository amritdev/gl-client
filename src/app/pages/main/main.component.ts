import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { AuthService, GeoLocationService, WebsocketService } from 'src/app/services';
import { environment } from 'src/environments/environment';
import { NavController, Platform } from '@ionic/angular';
import { Router, RouterEvent } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { Subscription } from 'rxjs';
import * as moment from 'moment';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
})
export class MainComponent implements OnInit, OnDestroy {

  role = 1;
  sight = '';
  teleFailed = false;
  curUrl = '';
  lineID = '';
  email = '';
  callSubscription: Subscription | null = null;
  roomSubscription: Subscription | null = null;
  userSubscription: Subscription | null = null;
  routerSubscription: Subscription | null = null;
  isIOS = false;
  isIPhone = false;
  isProfileDetail = false;
  lastConnectedAt: string | null = null;
  isChatDetail = false;

  constructor(
    private auth: AuthService,
    private http: HttpClient,
    private websocket: WebsocketService,
    private nav: NavController,
    private router: Router,
    private titleService: Title,
    private platform: Platform,
    private gps: GeoLocationService
  ) {
    this.isIOS = this.platform.is('ios') && !this.platform.is('mobileweb');
    this.isIPhone = this.platform.is('iphone');

    if (this.userSubscription === null) {
      this.userSubscription = this.auth.currentUserSubject.subscribe(user => {
        if (user) {
          this.role = user.Role ?? 1;
          this.teleFailed = user.TelecomFailed;
          this.lineID = user.LineID ?? '';
          this.email = user.Email ?? '';
          this.sight = user.Sight;
          this.lastConnectedAt = user.LastConnectedAt;
          if (this.teleFailed) {
            this.nav.navigateRoot('/main/chat');
          }
        }
      });
    }

    if (this.routerSubscription === null) {
      this.routerSubscription = this.router.events.subscribe( e => {
        const routerEvent = e as RouterEvent;
        if (routerEvent.url && routerEvent.url !== this.curUrl){
          // if url is changed then update user last connected at time
          if (routerEvent.url !== this.curUrl){
            this.updateLastConnectionTime();
          }

          this.curUrl = routerEvent.url;
          this.isChatDetail = this.curUrl.includes('/main/chat/detail/')
          || this.curUrl.includes('/main/chat/invoice') || this.curUrl.includes('/main/chat/share');

          switch (this.curUrl){
            case '/main/call':
              this.setTitle('glass キャストを呼ぶ コール');
              break;
            case '/main/search':
              this.setTitle('glass キャストを探す');
              break;
            case '/main/apply':
              this.setTitle('glass コールを探す');
              break;
            case '/main/guest-search':
              this.setTitle('glass ゲストを探す');
              break;
            case '/main/chat':
              this.setTitle('glass メッセージ');
              break;
            case '/main/tweet':
              this.setTitle('glass つぶやきを見る');
              break;
            case '/main/mypage':
              // if (this.role === 0){
              //   this.auth.updatePointVal();
              // }
              this.setTitle('glass マイページを見る');
              break;
            default:
              break;
          }
        }
      });
    }

    this.updateUnreadMessages();

    if (this.callSubscription === null) {
      this.callSubscription = this.websocket.callSubject.subscribe((callEvent) => {
        if (callEvent.event === 'delete'){
          this.updateUnreadMessages();
        }
      });
    }

    if (this.roomSubscription === null) {
      this.roomSubscription = this.websocket.roomSubject.subscribe(_ => {
        this.updateUnreadMessages();
      });
    }
  }

  updateLastConnectionTime(): void {
    let shouldBeUpdated = false;
    if (this.lastConnectedAt === null){
      shouldBeUpdated = true;
    }else{
      const date = moment(this.lastConnectedAt);
      const nowTime = moment();
      if (nowTime.diff(date, 'minutes') > 10){
        shouldBeUpdated = true;
      }
    }
    if (shouldBeUpdated && localStorage.getItem('token') !== null){
      this.http.get<string>(`${environment.API_URL}/users/update_connection`).subscribe(res => {
        this.lastConnectedAt = res;
        this.gps.savePosition('connection');
        this.doRefreshToken();
      });
    }
  }

  doRefreshToken(): void {
    this.http.get<{
      token: string;
    }>(`${environment.API_URL}/users/refresh_token`).subscribe(res => {
      localStorage.setItem('token', `Bearer ${res.token}`);
    });
  }

  updateUnreadMessages(): void {
    this.http.get<{
      data: number
    }>(`${environment.API_URL}/users/message/unread`).subscribe((res) => {
      this.websocket.unreadMessages = res.data;
    });
  }

  ngOnInit(): void { }

  ngOnDestroy(): void {
    this.userSubscription?.unsubscribe();
    this.callSubscription?.unsubscribe();
    this.roomSubscription?.unsubscribe();
    this.routerSubscription?.unsubscribe();
  }

  getUnread(): number {
    return this.websocket.unreadMessages;
  }

  getTweetUnreads(): number {
    // console.log(this.websocket.unreadTweets);
    return this.websocket.unreadTweets;
  }

  public setTitle(newTitle: string): void {
    this.titleService.setTitle(newTitle);
  }

}
