import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { IonicModule } from '@ionic/angular';
import { QRCodeModule } from 'angularx-qrcode';
import { PipesModule, ComponentsModule } from 'src/app/shared';
import { JwtInterceptor, ErrorInterceptor } from 'src/app/interceptors';
import { NgxIonicImageViewerModule } from 'ngx-ionic-image-viewer';
import { MainRoutingModule } from './main-routing.module';
import { MainComponent } from './main.component';
import { QrcodeComponent } from './mypage/qrcode/qrcode.component';
import { ProfileComponent } from './profile/profile.component';
import { NoticeComponent } from './notice/notice.component';
import { TutorialComponent } from './tutorial/tutorial.component';
import { LikeComponent } from './like/like.component';
import { DetailComponent } from './notice/detail/detail.component';
import { ReviewComponent } from './review/review.component';

@NgModule({
  declarations: [
    MainComponent,
    QrcodeComponent,
    ProfileComponent,
    NoticeComponent,
    DetailComponent,
    TutorialComponent,
    ReviewComponent,
    LikeComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    HttpClientModule,
    QRCodeModule,
    PipesModule,
    NgxIonicImageViewerModule,
    MainRoutingModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
  ],
})
export class MainModule { }
