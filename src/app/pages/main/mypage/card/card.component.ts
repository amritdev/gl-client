import { HttpClient } from '@angular/common/http';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { Image } from 'src/app/interfaces';
import { AuthService } from 'src/app/services';
import { environment } from 'src/environments/environment';
import t from 'src/locales';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent implements OnInit, OnDestroy {

  cardImages: Image[] | null = null;
  isChecked = false;
  userSubscription: Subscription | null = null;
  imageURLs: string[] = [];
  images: File[] = [];
  onSaving = false;

  constructor(
    private http: HttpClient,
    private auth: AuthService,
    private alert: AlertController
  ){
    if (this.userSubscription === null) {
      this.userSubscription = this.auth.currentUserSubject.subscribe(user => {
        if (user){
          if (user.Cards){
            this.imageURLs = user.Cards.map(item => {
              return item.Path;
            });
          }
          this.isChecked = user.IsChecked;
        }
      });
    }
  }

  ngOnInit(): void{}

  ngOnDestroy(): void{
    this.userSubscription?.unsubscribe();
  }

  onFileChanged(event: Event): void {
    if (event.target instanceof HTMLInputElement && event.target.files && event.target.files[0]) {
      if (event.target.files.length > 2){
        this.presentAlert(t('You can upload up to 2 card images'));
        return;
      }
      const fileLength = event.target.files.length;

      this.imageURLs = [];
      this.images = [];
      for (let i = 0; i < fileLength; i++) {
        const reader = new FileReader();
        reader.readAsDataURL(event.target.files[i]);
        this.images.push(event.target.files[i]);
        reader.onload = () => {
          this.imageURLs.push(reader.result as string);
        };
      }
    }
  }

  async presentAlert(msg: string): Promise<void> {
    const alert = await this.alert.create({
      message: msg,
      buttons: [t('OK')],
    });
    await alert.present();
  }

  uploadCard(): void {
    if (this.images.length > 0) {
      this.onSaving = true;
      const formData = new FormData();
      this.images.forEach(imageItem => {
        formData.append('avatar', imageItem);
      });
      this.http.post<{
        success: boolean,
        cards: Image[]
      }>(`${environment.API_URL}/users/profile/register/cards`, formData).subscribe(res => {
        if (res.success){
          this.imageURLs = res.cards.map(item => {
            return item.Path;
          });
          this.presentAlert(t('Cards Registered Successfully'));
        }else{
          this.presentAlert(t('Operation Failed'));
        }
        this.onSaving = false;
      }, () => {
        this.presentAlert(t('Operation Failed'));
      });
    }
  }
}
