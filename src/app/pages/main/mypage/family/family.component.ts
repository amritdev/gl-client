import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { User, UserCall, Invoice, Call } from 'src/app/interfaces';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { formatDate } from '@angular/common';
import t from 'src/locales';
import { AuthService, ConditionService } from 'src/app/services';
import { AlertController, IonInfiniteScroll, IonRouterOutlet, ModalController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import * as moment from 'moment-timezone';
import { ReviewComponent } from 'src/app/components/review/review.component';

@Component({
  selector: 'app-family',
  templateUrl: './family.component.html',
  styleUrls: ['./family.component.scss'],
})
export class FamilyComponent implements OnInit, OnDestroy {

  @ViewChild(IonInfiniteScroll) ionInfiniteScroll !: IonInfiniteScroll;
  @ViewChild(IonInfiniteScroll) ionInfiniteScrollFamily !: IonInfiniteScroll;

  segment = 'family';
  familyCasts: UserCall[] = [];
  user: User | null = null;
  familyIDs = '';

  // for invoice infinite scroll
  invoices: Invoice[] = [];
  pageIndex = 0;
  isAllLoaded = false;

  authSubscription: Subscription | null = null;

  // family casts
  familyPage = 0;
  familySize = 10;
  familyAllLoaded = false;

  constructor(
    private auth: AuthService,
    private http: HttpClient,
    private alert: AlertController,
    public cond: ConditionService,
    private modal: ModalController,
    private routerOutlet: IonRouterOutlet,
  ) {
    if (this.authSubscription === null) {
      this.authSubscription = this.auth.currentUserSubject.subscribe(user => {
        this.user = user ? { ...user } : null;
        if (user) {
          this.familyIDs = user.FamilyIDs;
        }
        this.getFamilyCasts(null);
      });
    }

  }

  ngOnInit(): void {
    this.getInvoices(null);
  }

  ngOnDestroy(): void {
    this.authSubscription?.unsubscribe();
  }

  getInvoices(event: Event | null = null): void {
    this.http.get<Invoice[]>(`${environment.API_URL}/invoices/call?page=${this.pageIndex}`).subscribe(invoices => {
      if (invoices && invoices.length === 0) {
        this.isAllLoaded = true;
      }
      this.invoices = this.invoices.concat(invoices);
      // console.log(this.invoices);
      if (event && event.target) {
        this.ionInfiniteScroll.complete();
      }
    });
  }

  getFamilyCasts(event: Event | null): void {
    this.http.get<UserCall[]>(`${environment.API_URL}/users/family/call?page=${this.familyPage}&size=${this.familySize}`)
      .subscribe(casts => {
      if (casts.length < this.familySize) {
        this.familyAllLoaded = true;
      }

      this.familyCasts = this.familyCasts.concat(casts);
      // console.log(casts);
      if (event && event.target) {
        this.ionInfiniteScrollFamily.complete();
      }
    });
  }

  getTimeStr(metAt: string): string {
    if (metAt !== '') {
      return `${formatDate(metAt, 'MM月dd日', 'en')}に合流`;
    } else {
      return `合流時間不明`;
    }
  }

  async dislikeCast(castID: number, index: number): Promise<void> {
    const confirmation = await this.warn(t('Are you sure remove current cast from your family list?'));
    if (confirmation) {
      if (this.familyIDs.includes(`,${castID},`)) {
        const idArray = this.familyIDs.split(',');
        idArray.pop();
        idArray.shift();
        const removedArray = idArray.filter(item => {
          return item !== castID.toString();
        });

        if (removedArray.length === 0) {
          this.familyIDs = '';
        } else {
          this.familyIDs = `,${removedArray.join(',')},`;
        }

        this.http.post<{
          'success': boolean
        }>(`${environment.API_URL}/users/family`, {
          familyIDs: this.familyIDs
        }).subscribe(res => {
          if (res.success) {
            // successfully saved
            if (this.user) {
              this.user.FamilyIDs = this.familyIDs;
              this.auth.currentUserSubject.next(this.user);
            }
            this.familyCasts.splice(index, 1);
          } else {
            console.log('error updating family ids');
          }
        });
      }

    }
  }

  async likeCast(castID: number): Promise<void> {
    const confirmation = await this.warn(t('Are you sure add current cast from your family list?'));
    if (confirmation) {
      if (!this.familyIDs.includes(`,${castID},`)) {
        const idArray = this.familyIDs.split(',');
        idArray.pop();
        idArray.shift();
        idArray.push(castID.toString());

        this.familyIDs = `,${idArray.join(',')},`;
        this.http.post<{
          'success': boolean
        }>(`${environment.API_URL}/users/family`, {
          familyIDs: this.familyIDs
        }).subscribe(async res => {
          if (res.success) {
            // successfully saved
            if (this.user) {
              this.user.FamilyIDs = this.familyIDs;
              this.auth.currentUserSubject.next(this.user);
            }
            this.showAlert(t('Successfully added to family casts'));
          } else {
            console.log('error updating family ids');
          }
        });
      }

    }
  }

  async dislikeCallCast(castID: number): Promise<void> {
    const confirmation = await this.warn(t('Are you sure remove current cast from your family list?'));
    if (confirmation) {
      if (this.familyIDs.includes(`,${castID},`)) {
        const idArray = this.familyIDs.split(',');
        idArray.pop();
        idArray.shift();
        const removedArray = idArray.filter(item => {
          return item !== castID.toString();
        });

        if (removedArray.length === 0) {
          this.familyIDs = '';
        } else {
          this.familyIDs = `,${removedArray.join(',')},`;
        }

        this.http.post<{
          'success': boolean
        }>(`${environment.API_URL}/users/family`, {
          familyIDs: this.familyIDs
        }).subscribe(res => {
          if (res.success) {
            // successfully saved
            if (this.user) {
              this.user.FamilyIDs = this.familyIDs;
              this.auth.currentUserSubject.next(this.user);
            }
          } else {
            console.log('error updating family ids');
          }
        });
      }

    }
  }

  async showAlert(msgStr: string): Promise<void> {
    const alert = await this.alert.create({
      message: msgStr,
      buttons: ['OK']
    });
    await alert.present();
  }

  async warn(message: string, title: string = ''): Promise<boolean> {
    return new Promise(async (resolve) => {
      const confirm = await this.alert.create({
        header: title === '' ? t('Alert Notify') : t(title),
        message: t(message),
        buttons: [
          {
            text: t('Cancel'),
            role: 'cancel',
            handler: () => {
              return resolve(false);
            },
          },
          {
            text: t('OK'),
            handler: () => {
              return resolve(true);
            },
          },
        ],
      });

      await confirm.present();
    });
  }

  getCallLabel(call: Call): string {
    if (call.ID > 0) {
      // const timeStr = formatDate(call.MeetTimeISO, 'yyyy年M月dd日', 'en', 'Asia/Tokyo');
      const timeStr = moment(`${call.MeetTimeISO} +0900`, 'YYYY-MM-DD HH:mm:ss Z').format('YYYY年MM月DD日');
      if (call.Location && call.Location.Name !== '') {
        return `${timeStr}・${call.Location ? call.Location.Name : ''}`;
      } else {
        return timeStr;
      }
    } else {
      return t('Unknown Call');
    }
  }

  isLike(castID: number): boolean {
    return this.familyIDs.includes(`,${castID},`);
  }

  loadData(event: Event): void {
    this.pageIndex++;
    this.getInvoices(event);
  }

  loadFamilyData(event: Event): void {
    this.familyPage++;
    this.getFamilyCasts(event);
  }

  getCastPath(user: User): string {
    if (user.Images.length > 0) {
      return user.Images[0].Path;
    } else {
      return 'assets/img/avatar.svg';
    }
  }

  openReviewPanel(invoice: Invoice): void {
    // console.log(invoice);
    this.openReviewDialog(invoice);
  }

  async openReviewDialog(invoice: Invoice): Promise<void> {
    if (invoice && invoice.Call.Room){
      const reviewModal = await this.modal.create({
        component: ReviewComponent,
        swipeToClose: true,
        presentingElement: this.routerOutlet.nativeEl,
        componentProps: {
          roomID: invoice.Call.Room.ID,
          call: invoice.Call,
          role: this.user?.Role ?? 0
        },
      });
      await reviewModal.present();
    }

  }

}
