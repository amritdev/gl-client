import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PipesModule } from 'src/app/shared';
import { HelpRoutingModule } from './help-routing.module';
import { HelpComponent } from './help.component';
import { OtherComponent } from './other/other.component';
import { UsageComponent } from './usage/usage.component';


@NgModule({
  declarations: [
    HelpComponent,
    OtherComponent,
    UsageComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    PipesModule,
    HelpRoutingModule
  ]
})
export class HelpModule { }
