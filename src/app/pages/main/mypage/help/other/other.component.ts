import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AlertController, NavController } from '@ionic/angular';
import { AuthService } from 'src/app/services';
import { environment } from 'src/environments/environment';
import t from 'src/locales';

@Component({
  selector: 'app-other',
  templateUrl: './other.component.html',
  styleUrls: ['./other.component.scss'],
})
export class OtherComponent implements OnInit {

  role = 1;

  constructor(
    private auth: AuthService,
    private navController: NavController,
    private alert: AlertController,
    private http: HttpClient
  ) { }

  ngOnInit(): void {
    this.auth.currentUserSubject.subscribe(user => {
      if (user){
        this.role = user.Role;
      }
    });
  }

  async logOut(): Promise<void> {
    const confirmation = await this.warn(t('Are you sure logout?'));
    if (confirmation){
      this.auth.logout();
      if (this.role === 1) {
        this.navController.navigateRoot('/');
      } else if (this.role === 0 || this.role === 10) {
        this.navController.navigateRoot('/');
      } else {
        this.navController.navigateRoot('/agent');
      }
    }
  }

  async outGlass(): Promise<void> {
    const confirmation = await this.warn(t('Are you sure withdraw from glass?'));
    if (confirmation){
      const roleStr = this.role === 1 ? 'guest' : 'cast';
      this.http.get<boolean>(`${environment.API_URL}/calls/matching?role=${roleStr}`).subscribe(async (res) => {
        if (res){
          this.showAlert(t('You are matching now so you can not withdraw'));
        }else {
          this.withdraw();
        }
      });
    }
  }

  withdraw(): void {
    this.http.get<{}>(`${environment.API_URL}/users/withdraw`).subscribe(_ => {
      this.auth.logout();
      if (this.role === 1) {
        this.navController.navigateRoot('/');
      } else if (this.role === 0 || this.role === 10) {
        this.navController.navigateRoot('/');
      } else {
        this.navController.navigateRoot('/agent');
      }
    });
  }

  async showAlert(msgStr: string): Promise<void> {
    const alert = await this.alert.create({
      message: msgStr,
      buttons: ['OK']
    });
    await alert.present();
  }

  async warn(message: string, title: string = ''): Promise<boolean> {
    return new Promise(async (resolve) => {
      const confirm = await this.alert.create({
        header: title === '' ? t('Alert Notify') : t(title),
        message: t(message),
        buttons: [
          {
            text: t('Cancel'),
            role: 'cancel',
            handler: () => {
              return resolve(false);
            },
          },
          {
            text: t('OK'),
            handler: () => {
              return resolve(true);
            },
          },
        ],
      });

      await confirm.present();
    });
  }

}
