import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Config } from 'src/app/interfaces';

@Component({
  selector: 'app-usage',
  templateUrl: './usage.component.html',
  styleUrls: ['./usage.component.scss'],
})
export class UsageComponent implements OnInit, OnDestroy {

  role = 0;
  userSubscription: Subscription | null = null;
  config: Config | null = null;

  constructor(
    private auth: AuthService,
    private http: HttpClient
  ) {
    if (this.userSubscription === null) {
      this.userSubscription = this.auth.currentUserSubject.subscribe(user => {
        if (user){
          this.role = user.Role;
        }
      });
    }
   }

  ngOnInit(): void {
    this.getConfig();
  }

  getConfig(): void {
    this.http.get<Config>(`${environment.API_URL}/users/config`).subscribe((config) => {
      this.config = config;
    });
  }

  ngOnDestroy(): void {
    this.userSubscription?.unsubscribe();
  }

}
