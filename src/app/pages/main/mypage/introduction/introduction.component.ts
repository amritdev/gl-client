import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { AuthService } from 'src/app/services';
import { User, Intro, Config } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';
import { AlertController } from '@ionic/angular';
import t from 'src/locales';

@Component({
  selector: 'app-introduction',
  templateUrl: './introduction.component.html',
  styleUrls: ['./introduction.component.scss'],
})
export class IntroductionComponent implements OnInit {

  user: User | null = null;
  inviteURL = '';
  intros: Intro[] = [];
  config: Config | null = null;
  segment = '0';

  constructor(
    private authService: AuthService,
    private http: HttpClient,
    private alert: AlertController
  ) { }

  ngOnInit(): void {
    this.authService.currentUserSubject.subscribe(user => {
      this.user = user ? { ...user } : null;
      this.getConfig();
    });
    this.getIntros();
  }

  setInviteURL(): void{
    this.inviteURL = `${environment.SITE_URL}?source=iv&id=${this.user?.ProfileID}&c_id=${this.config?.IntroCampaignID}`;
  }

  getConfig(): void {
    this.http.get<Config>(`${environment.API_URL}/users/config`).subscribe((config) => {
      this.config = config;
      this.setInviteURL();
    });
  }

  getIntros(): void {
    this.http.get<Intro[]>(`${environment.API_URL}/users/intro`).subscribe(intros => {
      this.intros = intros;
    });
  }

  getEarnedPoints(): number {
    let amount = 0;
    this.intros.forEach((i) => {
      if (i.IsPointAdded) {
        amount += i.AddedPoint;
      }
    });
    return amount;
  }

  getWaitingPoints(): number {
    let amount = 0;
    this.intros.forEach((i) => {
      if (!i.IsPointAdded && this.user) {
        if (i.Visitor.Role === 1) {
          amount += this.user.Role === 2 ? this.user.GuestPoint : this.config?.GuestPoint ?? 0;
        } else {
          amount += this.user.Role === 2 ? this.user.CastPoint : this.config?.CastPoint ?? 0;
        }
      }
    });
    return amount;
  }

  onCopyToClipboard(): void {
    const inputBox = document.createElement('textarea');
    inputBox.style.position = 'fixed';
    inputBox.style.left = '0';
    inputBox.style.top = '0';
    inputBox.style.opacity = '0';
    inputBox.value = this.inviteURL;
    document.body.appendChild(inputBox);
    inputBox.focus();
    inputBox.select();
    document.execCommand('copy');
    document.body.removeChild(inputBox);
    this.presentAlert(t('Successfully copied to clipboard'));
  }

  async presentAlert(msg: string): Promise<void> {
    const alert = await this.alert.create({
      message: msg,
      buttons: [t('OK')],
    });
    await alert.present();
  }
}
