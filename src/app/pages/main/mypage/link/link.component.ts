import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlertController, NavController, Platform } from '@ionic/angular';
import { AuthService } from 'src/app/services';
import { User } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';
import t from 'src/locales';

@Component({
  selector: 'app-link',
  templateUrl: './link.component.html',
  styleUrls: ['./link.component.scss'],
})
export class LinkComponent implements OnInit {
  lineID = '';

  constructor(
    private platform: Platform,
    private route: ActivatedRoute,
    private http: HttpClient,
    private auth: AuthService,
    private nav: NavController,
    private alert: AlertController
  ) {
    this.auth.currentUserSubject.subscribe(user => {
      if (user && user.LineID){
        this.lineID = user.LineID;
        // console.log(this.lineID);
      }
    });
  }

  ngOnInit(): void {
    const code = this.route.snapshot.queryParamMap.get('code');
    if (code) {
      this.http.post<User>(`${environment.API_URL}/users/link/${code}?mode=link`, {}).subscribe((user) => {
        if (this.lineID === ''){
          this.presentAlert(t('LINE official account has been added as a friend.'));
          this.lineID = user.LineID ?? '';
        }else{
          this.presentAlert(t('LINE Account successfully added'));
        }
        this.auth.currentUserSubject.next(user);
        this.nav.navigateBack('/main/mypage');
      }, (err) => {
        if (err.status === 409) {
          this.presentAlert(t('You already have a LINE account.'));
        } else {
          this.presentAlert(t('Operation Failed'));
        }
        this.nav.navigateBack('/main/mypage');
      });
    }
    this.route.queryParamMap.subscribe((params) => {
      if (params.get('line')) {
        const lineCode = params.get('line');
        this.http.post<User>(`${environment.API_URL}/users/link/${lineCode}?mode=link&platform=mobile`, {}).subscribe((user) => {
          if (this.lineID === ''){
            this.presentAlert(t('LINE official account has been added as a friend.'));
            this.lineID = user.LineID ?? '';
          }else{
            this.presentAlert(t('LINE Account successfully added'));
          }
          this.auth.currentUserSubject.next(user);
          this.nav.navigateBack('/main/mypage');
        }, (err) => {
          if (err.status === 409) {
            this.presentAlert(t('You already have a LINE account.'));
          } else {
            this.presentAlert(t('Operation Failed'));
          }
          this.nav.navigateBack('/main/mypage');
        });
      }
    });
  }

  async presentAlert(msg: string): Promise<void> {
    const alert = await this.alert.create({
      message: msg,
      buttons: [t('OK')],
    });
    await alert.present();
  }

  associateWithLINE(): void {
    let redirectURL = `${environment.SITE_URL}/main/mypage/link`;
    if (this.platform.is('hybrid')) {
      redirectURL = `${environment.SITE_URL}/line/mypage/link`;
    }
    location.href = 'https://access.line.me/oauth2/v2.1/authorize?'
        + 'response_type=code&'
        + `client_id=${environment.LINE_CHANNEL_ID}&`
        + `redirect_uri=${redirectURL}&`
        + `state=${Math.random().toString(36).substring(2, 15)}&`
        + 'scope=profile%20openid%20email&'
        + 'bot_prompt=aggressive';
  }

}
