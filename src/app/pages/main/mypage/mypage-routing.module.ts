import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MypageComponent } from './mypage.component';
import { PaymentComponent } from './payment/payment.component';
import { FamilyComponent } from './family/family.component';
import { IntroductionComponent } from './introduction/introduction.component';
import { LinkComponent } from './link/link.component';
import { SalesComponent } from './sales/sales.component';
import { PhraseComponent } from './phrase/phrase.component';
import { QrcodeComponent } from './qrcode/qrcode.component';
import { CardComponent } from './card/card.component';
import { ReportComponent } from './report/report.component';

const routes: Routes = [
  {
    path: '',
    component: MypageComponent,
  },
  {
    path: 'profile',
    loadChildren: () => import('./profile/profile.module').then(m => m.ProfileModule),
  },
  {
    path: 'payment',
    component: PaymentComponent,
  },
  {
    path: 'family',
    component: FamilyComponent,
  },
  {
    path: 'qrcode',
    component: QrcodeComponent
  },
  {
    path: 'report',
    component: ReportComponent
  },
  {
    path: 'point',
    loadChildren: () => import('./point/point.module').then(m => m.PointModule),
  },
  {
    path: 'help',
    loadChildren: () => import('./help/help.module').then(m => m.HelpModule),
  },
  {
    path: 'setting',
    loadChildren: () => import('./setting/setting.module').then(m => m.SettingModule),
  },
  {
    path: 'intro',
    component: IntroductionComponent,
  },
  {
    path: 'link',
    component: LinkComponent,
  },
  {
    path: 'phrase',
    component: PhraseComponent
  },
  {
    path: 'card',
    component: CardComponent
  },
  {
    path: 'sales',
    redirectTo: 'sales/guest',
    pathMatch: 'full'
  },
  {
    path: 'sales/:type',
    component: SalesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MypageRoutingModule { }
