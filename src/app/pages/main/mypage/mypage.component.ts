import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { AuthService, WebsocketService } from 'src/app/services';
import { User, Class } from 'src/app/interfaces';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import t from 'src/locales';
import { AlertController, NavController } from '@ionic/angular';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-mypage',
  templateUrl: './mypage.component.html',
  styleUrls: ['./mypage.component.scss'],
})
export class MypageComponent implements OnInit, OnDestroy {

  user: User | null = null;
  levels: Class[] = [];
  levelsReady = false;
  familyCasts: User[] = [];
  nextLevelVal = 0;
  nextLevelName = '';
  curLevelVal = 0;
  curLevelName = '';
  isBarVisible = true;
  role = 1;
  sight = '';
  teleFailed = true;
  userSubscription: Subscription | null = null;
  // pointSubscription: Subscription | null = null;
  userPoint = 0;

  constructor(
    private route: ActivatedRoute,
    private authService: AuthService,
    private webSocket: WebsocketService,
    private navController: NavController,
    private http: HttpClient,
    private alert: AlertController
  ) {}

  ngOnInit(): void {
    const telecomcredit = this.route.snapshot.queryParamMap.get('telecomcredit') ?? '';
    if (telecomcredit === 'success') {
      this.presentAlert(t('Credit card verification is complete'));
    }
    this.route.queryParamMap.subscribe((params) => {
      if (params.get('telecom')) {
        this.presentAlert(t('Credit card verification is complete'));
      }
    });
    if (this.userSubscription === null) {
      this.userSubscription = this.authService.currentUserSubject.subscribe(user => {
        this.user = user ? { ...user } : null;
        this.role = user?.Role ?? 1;
        this.sight = user?.Sight ?? '';
        this.teleFailed = user?.TelecomFailed ?? true;
        if (this.user) {
          if (this.user.Role === 1) {
            this.getLevels('guest');
            this.getFamilyCasts();
            if (this.user.PointUsed === 0) {
              this.isBarVisible = false;
            }
            this.userPoint = this.user.PointUsed;
          }

          // start subscripting point value if cast
          // if (this.role === 0 && this.pointSubscription === null)   {
          //   this.pointSubscription = this.authService.pointSubject.subscribe(pointVal => {
          //     this.userPoint = pointVal;
          //     this.getLevels('cast');
          //   });
          // }
          if (this.role === 0){
            this.userPoint = this.user.PointUsed;
            this.getLevels('cast');
          }
        }
      });
    }

  }

  ngOnDestroy(): void {
    this.userSubscription?.unsubscribe();
    // this.pointSubscription?.unsubscribe();
  }

  async presentAlert(msg: string): Promise<void> {
    const alert = await this.alert.create({
      message: msg,
      buttons: [t('OK')],
    });
    await alert.present();
  }

  getLevels(role: string): void {
    this.http.get<Class[]>(`${environment.API_URL}/admin/levels/${role}`).subscribe(res => {
      this.levels = res;
      // console.log(res);
      this.levelsReady = true;
      if (this.user) {
        if (this.role === 1) {
          this.getLevelName(this.user.PointUsed);
        }else{
          this.getLevelName(this.userPoint);
        }
      }
    });
  }

  getNotifyCount(): number {
    return this.webSocket.unreadNotices;
  }

  getLevelName(usedPoint: number): string {
    if (this.levels.length > 0) {
      for (const [index, levelItem] of this.levels.entries()) {
        if (usedPoint < levelItem.Point) {
          this.curLevelName = levelItem.Name;

          // set next level name
          if (index === this.levels.length - 1) {
            this.isBarVisible = false;
          } else {
            this.nextLevelName = this.levels[index + 1].Name;
          }

          return levelItem.Name;
        } else {
          continue;
        }
      }
      this.isBarVisible = false;
      return this.levels[this.levels.length - 1].Name;
    } else {
      return t('Undefined');
    }
  }

  getValue(): number {
    if (this.user) {
      if (this.userPoint === this.curLevelVal) {
        return 0.01;
      } else {
        return (this.userPoint - this.curLevelVal) / (this.nextLevelVal - this.curLevelVal);
      }
    } else {
      return 0;
    }
  }

  getLevelNum(usedPoint: number): number {
    if (this.levels.length > 0) {
      for (const [index, levelItem] of this.levels.entries()) {
        if (usedPoint < levelItem.Point) {

          // set current and next level point value
          this.nextLevelVal = levelItem.Point;
          if (index === 0) {
            this.curLevelVal = 0;
          } else {
            this.curLevelVal = this.levels[index - 1].Point;
          }

          return index;
        } else {
          continue;
        }
      }
      return this.levels.length - 1;
    } else {
      return 0;
    }
  }

  getFamilyCasts(): void {
    this.http.get<User[]>(`${environment.API_URL}/users/family`).subscribe(casts => {
      this.familyCasts = casts;
    });
  }

  getArray(length: number): Array<unknown> {
    return new Array(length);
  }

  async logOut(): Promise<void> {
    const confirmation = await this.warn(t('Are you sure logout?'));
    if (confirmation){
      this.authService.logout();
      if (this.role === 1) {
        this.navController.navigateRoot('/');
      } else if (this.role === 0 || this.role === 10) {
        this.navController.navigateRoot('/');
      } else {
        this.navController.navigateRoot('/agent');
      }
    }
  }

  gotoLine(): void{
    window.location.href = 'https://lin.ee/6fAJj9Tg';
  }

  async warn(message: string, title: string = ''): Promise<boolean> {
    return new Promise(async (resolve) => {
      const confirm = await this.alert.create({
        header: title === '' ? t('Alert Notify') : t(title),
        message: t(message),
        buttons: [
          {
            text: t('Cancel'),
            role: 'cancel',
            handler: () => {
              return resolve(false);
            },
          },
          {
            text: t('OK'),
            handler: () => {
              return resolve(true);
            },
          },
        ],
      });

      await confirm.present();
    });
  }
}
