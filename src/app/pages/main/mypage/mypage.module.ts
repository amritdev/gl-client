import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { QRCodeModule } from 'angularx-qrcode';
import { PipesModule } from 'src/app/shared';
import { MypageRoutingModule } from './mypage-routing.module';
import { MypageComponent } from './mypage.component';
import { PaymentComponent } from './payment/payment.component';
import { FamilyComponent } from './family/family.component';
import { IntroductionComponent } from './introduction/introduction.component';
import { LinkComponent } from './link/link.component';
import { SalesComponent } from './sales/sales.component';
import { PhraseComponent } from './phrase/phrase.component';
import { CardComponent } from './card/card.component';
import { ReportComponent } from './report/report.component';

@NgModule({
  declarations: [
    MypageComponent,
    PaymentComponent,
    FamilyComponent,
    IntroductionComponent,
    LinkComponent,
    SalesComponent,
    PhraseComponent,
    CardComponent,
    ReportComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    PipesModule,
    MypageRoutingModule,
    QRCodeModule,
  ],
})
export class MypageModule { }
