import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { AlertController, Platform } from '@ionic/angular';

import { AuthService } from 'src/app/services';
import { User } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';
import t from 'src/locales';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss'],
})
export class PaymentComponent implements OnInit {

  user: User | null = null;
  clientIP = environment.TELECOM_CLIENT_IP;
  redirectURL = environment.SITE_URL + '/main/mypage';
  phoneNumber = '';

  constructor(
    private platform: Platform,
    private authService: AuthService,
    private http: HttpClient,
    private alert: AlertController
  ) { }

  ngOnInit(): void {
    if (this.platform.is('hybrid')) {
      this.redirectURL = this.redirectURL.replace('https://', `${environment.DEEPLINK_SCHEME}://`);
    }
    this.authService.currentUserSubject.subscribe(user => {
      if (user) {
        this.user = user;
        this.phoneNumber = this.user.PhoneNumber ? this.user.PhoneNumber.replace('+81', '') : '';
      }
    });
  }

  removeCreditCard(): void {
    this.http.delete<{}>(`${environment.API_URL}/users/telecom/${this.user?.ID}`).subscribe(() => {
      if (this.user) {
        this.user.TelecomCredit = false;
        this.authService.currentUserSubject.next(this.user);
      }
    }, async () => {
      const alert = await this.alert.create({
        message: t('Operation Failed'),
        buttons: [t('OK')],
      });
      await alert.present();
    });
  }

}
