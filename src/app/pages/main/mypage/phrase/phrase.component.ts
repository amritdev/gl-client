import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { AlertController, NavController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { User } from 'src/app/interfaces';
import { AuthService } from 'src/app/services';
import { environment } from 'src/environments/environment';
import t from 'src/locales';

@Component({
  selector: 'app-phrase',
  templateUrl: './phrase.component.html',
  styleUrls: ['./phrase.component.scss'],
})
export class PhraseComponent implements OnInit, OnDestroy {

  phrase = '';
  phraseStatus = false;
  userSubscription: Subscription | null = null;
  user !: User;

  constructor(
    private auth: AuthService,
    private alert: AlertController,
    private http: HttpClient,
    private nav: NavController
  ) {
    if (this.userSubscription === null) {
      this.userSubscription = this.auth.currentUserSubject.subscribe(user => {
        if (user) {
          this.user = user;
          this.phrase = user.FixedPhrase ?? '';
          this.phraseStatus = user.PhraseStatus;
        }
      });
    }
  }

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.userSubscription?.unsubscribe();
  }

  savePhrase(): void {
    // if (this.phrase.trim() === '') {
    //   this.presentAlert(t('Please input phrase text correctly'));
    //   return;
    // }else{
    this.http.put<{}>(`${environment.API_URL}/users/profile`, {
      phrase_status: this.phraseStatus,
      fixed_phrase: this.phrase.trim()
    }).subscribe(_ => {
      this.user.PhraseStatus = this.phraseStatus;
      this.user.FixedPhrase = this.phrase.trim();
      this.nav.navigateBack('/main/mypage');
    }, (err: HttpErrorResponse) => {
      if (err.status === 409) {
        this.presentAlert(t('Nickname already exists'));
      } else {
        this.presentAlert(t('Operation Failed'));
      }
    });
    // }
  }

  async presentAlert(msg: string): Promise<void> {
    const alert = await this.alert.create({
      message: msg,
      buttons: [t('OK')],
    });
    await alert.present();
  }
}
