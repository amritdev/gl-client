import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';

import { AlertController, IonInfiniteScroll } from '@ionic/angular';

import { AuthService } from 'src/app/services';
import { Transfer, User } from 'src/app/interfaces';
import t from 'src/locales';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-applicate',
  templateUrl: './applicate.component.html',
  styleUrls: ['./applicate.component.scss'],
})
export class ApplicateComponent implements OnInit, OnDestroy {
  @ViewChild(IonInfiniteScroll) ionInfiniteScroll !: IonInfiniteScroll;

  user: User | null = null;
  userSubscription: Subscription | null = null;
  transferType = 'bank';
  transferPoint = '';
  remainingPoint = 0;
  transfers: Transfer[] = [];

  // infinite scroll
  allLoaded = false;
  page = 0;
  pageSize = 10;

  parseInt = parseInt;
  firstJoined = false;

  constructor(
    private auth: AuthService,
    private http: HttpClient,
    private alert: AlertController
  ) { }

  ngOnInit(): void {
    if (this.userSubscription === null) {
      this.userSubscription = this.auth.currentUserSubject.subscribe((user) => {
        this.user = user ? { ...user } : null;
        if (user) {
          this.remainingPoint = user.Point - user.RequestedPoint;
          if (this.transferType === 'bank') {
            this.transferPoint = (this.remainingPoint).toString();
          } else {
            this.transferPoint = '';
          }
          this.firstJoined = user.FirstJoinAt !== null;
          this.refresh();
        }
      });
    }

  }

  ngOnDestroy(): void {
    this.userSubscription?.unsubscribe();
  }

  refresh(): void {
    this.page = 0;
    this.transfers = [];
    this.allLoaded = false;
    this.getTransferHistory(null);
  }

  getTransferHistory(event: Event | null): void {
    this.http.get<Transfer[]>(`${environment.API_URL}/users/transfer?page=${this.page}`).subscribe((transfers) => {
      if (transfers.length < this.pageSize) {
        this.allLoaded = true;
      }
      // console.log(transfers);

      this.transfers = this.transfers.concat(transfers);
      if (event && event.target) {
        this.ionInfiniteScroll.complete();
      }
    });
  }

  async presentAlert(msg: string): Promise<void> {
    const alert = await this.alert.create({
      message: msg,
      buttons: [t('OK')],
    });
    await alert.present();
  }

  onTransferTypeChanged(): void {
    if (this.user) {
      if (this.transferType === 'bank') {
        this.transferPoint = (this.remainingPoint * this.user.PointRatio).toString();
      } else {
        this.transferPoint = '';
      }
    }
  }

  async transfer(): Promise<void> {
    if (this.user) {
      const point = parseInt(this.transferPoint || '0', 10);
      if (point > this.remainingPoint) {
        this.presentAlert(t('Transfer application points cannot be larger than remaining points'));
      } else if (point < 1000) {
        this.presentAlert(t('You must apply for a deposit of 1000 yen or more.'));
      } else if (this.user.RequestedPoint >= this.user.Point) {
        this.presentAlert(t('The withdrawal application limit has been exceeded.'));
      } else if (!this.firstJoined) {
        this.presentAlert(t('You can not apply if you did not join once'));
      } else {
        const alert = await this.alert.create({
          message: `${point}Pを入金申請しますか？`,
          buttons: [
            t('Cancel'),
            {
              text: t('OK'),
              handler: () => {
                this.http.post(`${environment.API_URL}/users/transfer`, {
                  point,
                  type: this.transferType
                }).subscribe(async (_) => {
                  const ohterAlert = await this.alert.create({
                    message: t('The deposit application has been completed.'),
                    buttons: [t('OK')]
                  });
                  await ohterAlert.present();
                }, (err: HttpErrorResponse) => {
                  if (err.status === 400) {
                    this.presentAlert(t('You have no account info registered yet'));
                  }
                });
              }
            }
          ]
        });
        await alert.present();
      }
    }
  }

  loadData(event: Event | null): void {
    this.page++;
    this.getTransferHistory(event);
  }


}
