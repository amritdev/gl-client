import { Component, Input, OnInit } from '@angular/core';

import { ModalController, AlertController } from '@ionic/angular';

import t from 'src/locales';

@Component({
  selector: 'app-bank',
  templateUrl: './bank.component.html',
  styleUrls: ['./bank.component.scss'],
})
export class BankComponent implements OnInit {

  @Input() data: {
    BankName: string;
    BankBranchName: string;
    BankAccountType: string;
    BankAccount: string;
    BankAccountHolder: string;
  } = {
      BankName: '',
      BankBranchName: '',
      BankAccountType: '',
      BankAccount: '',
      BankAccountHolder: ''
    };

  constructor(
    private modalController: ModalController,
    private alertController: AlertController,
  ) { }

  ngOnInit(): void { }

  async presentAlert(msg: string): Promise<void> {
    const alert = await this.alertController.create({
      message: msg,
      buttons: [t('OK')],
    });
    await alert.present();
  }

  onSave(): void {
    if (this.data.BankName.trim() === '') {
      this.presentAlert(t('Please input bank name'));
    } else if (this.data.BankBranchName.trim() === '') {
      this.presentAlert(t('Please input bank branch name'));
    } else if (this.data.BankAccountType.trim() === '') {
      this.presentAlert(t('Please input bank account type'));
    } else if (this.data.BankAccountHolder.trim() === '') {
      this.presentAlert(t('Please input bank account holder'));
    } else if (this.data.BankName.trim() === '') {
      this.presentAlert(t('Please input bank account'));
    } else {
      this.modalController.dismiss(this.data);
    }
  }

  onClose(): void {
    this.modalController.dismiss();
  }

}
