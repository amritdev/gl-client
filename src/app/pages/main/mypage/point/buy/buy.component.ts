import { Component, OnInit } from '@angular/core';
import { formatNumber } from '@angular/common';
import { HttpClient } from '@angular/common/http';

import { AlertController, LoadingController } from '@ionic/angular';

import { AuthService } from 'src/app/services';
import { User, Point } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';
import t from 'src/locales';

@Component({
  selector: 'app-buy',
  templateUrl: './buy.component.html',
  styleUrls: ['./buy.component.scss'],
})
export class BuyComponent implements OnInit {

  user: User | null = null;
  points: Point[] = [];

  constructor(
    private authService: AuthService,
    private http: HttpClient,
    private alertController: AlertController,
    private loadingController: LoadingController,
  ) { }

  ngOnInit(): void {
    this.authService.currentUserSubject.subscribe(user => {
      this.user = user ? { ...user } : null;
    });
    this.getPoints();
  }

  getPoints(): void {
    this.http.get<Point[]>(`${environment.API_URL}/points`).subscribe(points => {
      this.points = points;
    });
  }

  async onBuyPoint(point: number): Promise<void> {
    if (this.user?.TelecomCredit) {
      const alert = await this.alertController.create({
        message: formatNumber(point, 'en-US') + t('Would you like to purchase points?'),
        buttons: [t('Cancel'), {
          text: t('OK'),
          handler: async () => {
            const loading = await this.loadingController.create({
              message: t('Please wait...'),
            });
            await loading.present();
            this.http.post<{}>(`${environment.API_URL}/users/point`, {
              send_id: this.user ? new Date(this.user.CreatedAt).getTime() : 0,
              point,
            }).subscribe(async () => {
              await loading.dismiss();
              if (this.user) {
                this.user.Point += point;
              }
              this.authService.currentUserSubject.next(this.user);
            }, async () => {
              await loading.dismiss();
              const otherAlert = await this.alertController.create({
                message: t('Purchase Failed'),
                buttons: [t('OK')],
              });
              await otherAlert.present();
            });
          },
        }],
      });
      await alert.present();
    } else {
      const alert = await this.alertController.create({
        message: t('Payment information is not registered'),
        buttons: [t('OK')],
      });
      await alert.present();
    }
  }

}
