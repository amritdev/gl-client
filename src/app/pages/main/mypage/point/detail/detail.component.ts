import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ReviewComponent } from 'src/app/components/review/review.component';
import { InvoiceDetail, Invoice, Call } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';
import { IonRouterOutlet, ModalController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
})
export class DetailComponent implements OnInit, OnDestroy {

  title: string | null = '';
  invoice: Invoice | null = null;
  invoiceDetail: InvoiceDetail[] | null = [];
  userSubscription: Subscription | null = null;
  role = 1;

  constructor(
    private route: ActivatedRoute,
    private http: HttpClient,
    private modal: ModalController,
    private routerOutlet: IonRouterOutlet,
    private auth: AuthService
  ) {
    if (this.userSubscription === null) {
      this.userSubscription = this.auth.currentUserSubject.subscribe(user => {
        if (user){
          this.role = user.Role;
        }
      });
    }
  }

  ngOnInit(): void {
    const invoiceID = this.route.snapshot.paramMap.get('id');
    this.http.get<Invoice>(`${environment.API_URL}/users/invoice/${invoiceID}`).subscribe(invoice => {
      this.invoice = invoice;
      this.invoiceDetail = invoice.Detail;
      // console.log(this.invoice);
      if (invoice.Call.Room && invoice.Detail && invoice.Detail.length > 0) {
        this.title = invoice.Call.Room.Title ?? invoice.Detail[0].User.Nickname + 'とのglassコール';
      }
    });
  }

  ngOnDestroy(): void {
    this.userSubscription?.unsubscribe();
  }

  getExpiredTime(joinTime: number, period: number): number {
    if (joinTime > period * 60){
      return Math.floor((joinTime - period * 60) / 15) * 15;
    }else{
      return 0;
    }
  }

  openReviewPanel(): void {
    if (this.invoice){
      this.openReviewDialog(this.invoice.Call);
    }
  }

  async openReviewDialog(call: Call): Promise<void> {
    if (this.invoice && this.invoice.Call.Room){
      // console.log(this.invoice.Call.Room);
      const reviewModal = await this.modal.create({
        component: ReviewComponent,
        swipeToClose: true,
        componentProps: {
          roomID: this.invoice.Call.Room.ID,
          call,
          role: this.role
        },
      });
      await reviewModal.present();
    }

  }

}
