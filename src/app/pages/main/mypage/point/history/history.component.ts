import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { formatNumber } from '@angular/common';
import { HttpClient } from '@angular/common/http';

import { IonInfiniteScroll, ModalController, IonRouterOutlet, NavController, AlertController } from '@ionic/angular';

import { AuthService } from 'src/app/services';
import { User, Invoice } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';
import t from 'src/locales';
import * as moment from 'moment-timezone';
import { ReceiptComponent } from '../receipt/receipt.component';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss'],
})
export class HistoryComponent implements OnInit, OnDestroy {

  @ViewChild(IonInfiniteScroll) ionInfiniteScroll!: IonInfiniteScroll;

  user: User | null = null;
  invoices: Invoice[] = [];
  pageIndex = 0;
  isAllLoaded = false;
  segment = 'done';
  userSubscription: Subscription | null = null;

  constructor(
    private routerOutlet: IonRouterOutlet,
    private authService: AuthService,
    private http: HttpClient,
    private navController: NavController,
    private modalController: ModalController,
    private alertController: AlertController
  ) {
    if (this.userSubscription === null) {
      this.userSubscription = this.authService.currentUserSubject.subscribe(user => {
        this.user = user ? { ...user } : null;
      });
    }
  }

  ngOnInit(): void {
    this.getInvoices();
  }

  ngOnDestroy(): void {
    this.userSubscription?.unsubscribe();
  }

  onSegmentChanged(): void {
    this.refresh();
  }

  refresh(): void {
    this.pageIndex = 0;
    this.isAllLoaded = false;
    this.invoices = [];
    this.getInvoices(null);
  }

  loadData(event: Event): void {
    this.pageIndex++;
    this.getInvoices(event);
  }

  getInvoices(event: Event | null = null): void {
    this.http.get<Invoice[]>(`${environment.API_URL}/invoices?page=${this.pageIndex}&type=${this.segment}`).subscribe(invoices => {
      if (invoices && invoices.length === 0) {
        this.isAllLoaded = true;
      }
      this.invoices = this.invoices.concat(invoices);
      if (event) {
        this.ionInfiniteScroll.complete();
      }
    });
  }

  async onExportReceipt(invoice: Invoice): Promise<void> {
    const modal = await this.modalController.create({
      component: ReceiptComponent,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
    });
    // console.log(invoice);
    await modal.present();
    const { data } = await modal.onDidDismiss();
    if (data) {
      this.navController.navigateForward('/main/mypage/point/pdf', {
        state: {
          address: data,
          invoiceID: invoice.ID,
          point: Math.floor(invoice.Amount * (this.user ? this.user.PointRatio : 1.1)),
          nickname: this.user?.Nickname,
          date: moment.tz(invoice.CreatedAt, 'Asia/Tokyo').format('YYYY-MM-DD')
        },
      });
    }
  }

  async transferPoint(): Promise<void> {
    if (this.user) {
      if (this.user.Point < 10000) {
        const alert = await this.alertController.create({
          message: t('You must apply for a deposit of 10000 yen or more.'),
          buttons: [t('OK')]
        });
        await alert.present();
      } else {
        const alert = await this.alertController.create({
          message: `${formatNumber(this.user.Point, 'en-US')}Pを入金申請しますか？`,
          buttons: [
            t('Cancel'),
            {
              text: t('Bank Transfer'),
              handler: () => {
                this.http.post<{}>(`${environment.API_URL}/users/transfer`, {
                  type: 'bank'
                }).subscribe(async () => {
                  const ohterAlert = await this.alertController.create({
                    message: t('The deposit application has been completed.'),
                    buttons: [t('OK')]
                  });
                  await ohterAlert.present();
                });
              }
            },
            {
              text: t('Paypal'),
              handler: () => {
                this.http.post<{}>(`${environment.API_URL}/users/transfer`, {
                  type: 'paypal'
                }).subscribe(async () => {
                  const otherAlert = await this.alertController.create({
                    message: t('The deposit application has been completed.'),
                    buttons: [t('OK')]
                  });
                  await otherAlert.present();
                });
              }
            }
          ]
        });
        await alert.present();
      }
    }
  }

}
