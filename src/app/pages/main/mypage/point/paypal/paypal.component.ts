import { Component, Input, OnInit } from '@angular/core';

import { ModalController, AlertController } from '@ionic/angular';

import t from 'src/locales';

@Component({
  selector: 'app-paypal',
  templateUrl: './paypal.component.html',
  styleUrls: ['./paypal.component.scss'],
})
export class PaypalComponent implements OnInit {

  @Input() paypalAccount = '';

  constructor(
    private modalController: ModalController,
    private alertController: AlertController,
  ) { }

  ngOnInit(): void { }

  async onSave(): Promise<void> {
    if (this.paypalAccount.trim() === '') {
      const alert = await this.alertController.create({
        message: t('Please input paypal account'),
        buttons: [t('OK')],
      });
      await alert.present();
    } else {
      this.modalController.dismiss({ PaypalAccount: this.paypalAccount });
    }
  }

  onClose(): void {
    this.modalController.dismiss();
  }

}
