import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { jsPDF } from 'jspdf';
// import html2canvas from 'html2canvas';
import domtoimage from 'dom-to-image';
import * as moment from 'moment-timezone';

@Component({
  selector: 'app-pdf',
  templateUrl: './pdf.component.html',
  styleUrls: ['./pdf.component.scss'],
})
export class PdfComponent implements OnInit {

  address = '';
  invoiceID = 0;
  nickname = '';
  point = 0;
  now = '';

  constructor(
    private router: Router,
  ) { }

  ngOnInit(): void {
    const data = this.router.getCurrentNavigation();
    this.address = data?.extras.state?.address ?? '';
    this.invoiceID = data?.extras.state?.invoiceID ?? '';
    this.nickname = data?.extras.state?.nickname ?? '';
    this.point = data?.extras.state?.point ?? '';
    this.now = data?.extras.state?.date ?? '';
    // this.generatePDF();
  }

  generatePDF(): void {
    try {
      const el = document.getElementById('contentToConvert') as HTMLInputElement;
      const options = {background: 'white', height: 800, width: 800};
      domtoimage.toPng(el, options).then((dataUrl) => {
        // console.log(dataUrl);
        // Initialize JSPDF
        const doc = new jsPDF('l', 'mm', 'b6');
        // Add image Url to PDF
        doc.addImage(dataUrl, 'PNG', 40, 20, 220, 220);
        doc.save(`receipt_${moment.tz('Asia/Tokyo').format('YYYYMMDD_HHmmss')}.pdf`);

      }, (err) => {
        console.log(err);
      });

    } catch (ex) { }
  }

}
