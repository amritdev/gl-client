import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BuyComponent } from './buy/buy.component';
import { HistoryComponent } from './history/history.component';
import { DetailComponent } from './detail/detail.component';
import { ReceiptComponent } from './receipt/receipt.component';
import { PdfComponent } from './pdf/pdf.component';
import { TransferComponent } from './transfer/transfer.component';
import { ReviewComponent } from './review/review.component';
import { ApplicateComponent } from './applicate/applicate.component';
import { InvoiceGuard } from 'src/app/guards';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'history',
    pathMatch: 'full',
  },
  {
    path: 'buy',
    component: BuyComponent,
  },
  {
    path: 'history',
    component: HistoryComponent,
  },
  {
    path: 'detail/:id',
    component: DetailComponent,
    canActivate: [InvoiceGuard]
  },
  {
    path: 'receipt',
    component: ReceiptComponent,
  },
  {
    path: 'pdf',
    component: PdfComponent,
  },
  {
    path: 'transfer',
    component: TransferComponent,
  },
  {
    path: 'review',
    component: ReviewComponent
  },
  {
    path: 'applicate',
    component: ApplicateComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PointRoutingModule { }
