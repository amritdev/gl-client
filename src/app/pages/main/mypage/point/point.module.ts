import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PipesModule } from 'src/app/shared';

import { PointRoutingModule } from './point-routing.module';
import { BuyComponent } from './buy/buy.component';
import { HistoryComponent } from './history/history.component';
import { DetailComponent } from './detail/detail.component';
import { ReceiptComponent } from './receipt/receipt.component';
import { PdfComponent } from './pdf/pdf.component';
import { TransferComponent } from './transfer/transfer.component';
import { BankComponent } from './bank/bank.component';
import { PaypalComponent } from './paypal/paypal.component';
import { ReviewComponent } from './review/review.component';
import { ApplicateComponent } from './applicate/applicate.component';

@NgModule({
  declarations: [
    BuyComponent,
    HistoryComponent,
    DetailComponent,
    ReceiptComponent,
    PdfComponent,
    TransferComponent,
    BankComponent,
    PaypalComponent,
    ReviewComponent,
    ApplicateComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    PipesModule,
    PointRoutingModule,
  ]
})
export class PointModule { }
