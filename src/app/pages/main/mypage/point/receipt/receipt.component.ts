import { Component, OnInit } from '@angular/core';

import { ModalController, AlertController } from '@ionic/angular';

import t from 'src/locales';

@Component({
  selector: 'app-receipt',
  templateUrl: './receipt.component.html',
  styleUrls: ['./receipt.component.scss'],
})
export class ReceiptComponent implements OnInit {

  address = '';

  constructor(
    private modalController: ModalController,
    private alertController: AlertController,
  ) { }

  ngOnInit(): void { }

  async onExport(): Promise<void> {
    if (this.address.trim() === '') {
      const alert = await this.alertController.create({
        message: t('Please input receipt address'),
        buttons: [t('OK')],
      });
      await alert.present();
    } else {
      this.modalController.dismiss(this.address);
    }
  }

  onClose(): void {
    this.modalController.dismiss();
  }

}
