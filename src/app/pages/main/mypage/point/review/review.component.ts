import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from 'src/app/services';
import { HttpClient } from '@angular/common/http';
import { IonInfiniteScroll } from '@ionic/angular';
import t from 'src/locales';
import { User, Review } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.scss'],
})
export class ReviewComponent implements OnInit {

  @ViewChild(IonInfiniteScroll) ionInfiniteScroll!: IonInfiniteScroll;
  user !: User;
  reviews: Review[] = [];
  pageIndex = 0;
  isAllLoaded = false;

  constructor(
    private auth: AuthService,
    private http: HttpClient
  ) {
    this.auth.currentUserSubject.subscribe(user => {
      if (user){
        this.user = user;
      }
    });
  }

  loadData(event: Event): void {
    this.pageIndex++;
    this.getReviews(event);
  }

  getReviews(event: Event | null = null): void {
    this.http.get<Review[]>(`${environment.API_URL}/reviews?page=${this.pageIndex}`).subscribe(reviews => {
      if (reviews && reviews.length === 0) {
        this.isAllLoaded = true;
      }
      this.reviews = this.reviews.concat(reviews);
      // console.log(reviews);
      if (event) {
        this.ionInfiniteScroll.complete();
      }
    });
  }

  ngOnInit(): void{
    this.getReviews();
  }

}
