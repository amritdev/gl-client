import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ModalController, IonRouterOutlet, AlertController } from '@ionic/angular';

import t from 'src/locales';

import { BankComponent } from '../bank/bank.component';
import { PaypalComponent } from '../paypal/paypal.component';
import { environment } from 'src/environments/environment';
import { AuthService } from 'src/app/services';
import { User } from 'src/app/interfaces';

@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.component.html',
  styleUrls: ['./transfer.component.scss'],
})
export class TransferComponent implements OnInit {

  user: User | null = null;

  constructor(
    private routerOutlet: IonRouterOutlet,
    private authService: AuthService,
    private http: HttpClient,
    private modalController: ModalController,
    private alertController: AlertController,
  ) { }

  ngOnInit(): void {
    this.authService.currentUserSubject.subscribe(user => {
      this.user = user ? { ...user } : null;
    });
  }

  async openBankModal(): Promise<void> {
    const modal = await this.modalController.create({
      component: BankComponent,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        data: {
          BankName: this.user?.BankName ?? '',
          BankBranchName: this.user?.BankBranchName ?? '',
          BankAccountType: this.user?.BankAccountType ?? '',
          BankAccount: this.user?.BankAccount ?? '',
          BankAccountHolder: this.user?.BankAccountHolder ?? ''
        },
      },
    });
    await modal.present();
    const { data } = await modal.onDidDismiss();
    if (data) {
      this.http.put<{}>(`${environment.API_URL}/users/info/bank`, {
        bank_name: data.BankName,
        bank_branch_name: data.BankBranchName,
        bank_account_type: data.BankAccountType,
        bank_account_holder: data.BankAccountHolder,
        bank_account: data.BankAccount,
      }).subscribe(() => {
        if (this.user) {
          this.user.BankName = data.BankName;
          this.user.BankBranchName = data.BankBranchName;
          this.user.BankAccountType = data.BankAccountType;
          this.user.BankAccount = data.BankAccount;
          this.user.BankAccountHolder = data.BankAccountHolder;
          this.authService.currentUserSubject.next(this.user);
        }
      }, async () => {
        const alert = await this.alertController.create({
          message: t('Operation Failed'),
          buttons: [t('OK')],
        });
        await alert.present();
      });
    }
  }

  async openPaypalModal(): Promise<void> {
    const modal = await this.modalController.create({
      component: PaypalComponent,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        paypalAccount: this.user?.PaypalAccount ?? '',
      },
    });
    await modal.present();
    const { data } = await modal.onDidDismiss();
    if (data) {
      this.http.put<{}>(`${environment.API_URL}/users/info/paypal`, {
        paypal_account: data.PaypalAccount,
      }).subscribe(() => {
        if (this.user) {
          this.user.PaypalAccount = data.PaypalAccount;
          this.authService.currentUserSubject.next(this.user);
        }
      }, async () => {
        const alert = await this.alertController.create({
          message: t('Operation Failed'),
          buttons: [t('OK')],
        });
        await alert.present();
      });
    }
  }

}
