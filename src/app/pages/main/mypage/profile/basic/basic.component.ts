import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ModalController, AlertController } from '@ionic/angular';

import { Detail } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';
import t from 'src/locales';

interface DetailWithChildren extends Detail {
  Children: Detail[];
}

@Component({
  selector: 'app-basic',
  templateUrl: './basic.component.html',
  styleUrls: ['./basic.component.scss'],
})
export class BasicComponent implements OnInit {

  @Input() detail: string | null = null;
  @Input() details: DetailWithChildren[] = [];
  @Input() role = 0;

  detailArr: string[] = [];

  constructor(
    private http: HttpClient,
    private modalController: ModalController,
    private alertController: AlertController,
  ) { }

  ngOnInit(): void {
    const detailArr = this.detail?.split(',') ?? [];
    this.detailArr = detailArr.filter(item => item !== '');
    this.getDetails();
  }

  getDetails(): void {
    this.http.get<Detail[]>(`${environment.API_URL}/details`).subscribe(details => {
      this.details.forEach(detail => {
        detail.Children = details.filter(item => item.ParentID === detail.ID) ?? [];
      });
      // console.log(this.details);
    });
  }

  getSelectedValue(detail: DetailWithChildren): number {
    const selectedDetail = detail.Children?.find(item => this.detailArr.includes(item.ID.toString()));
    return selectedDetail ? selectedDetail.ID : 0;
  }

  onSelect(event: Event, beforeValue: number): void {
    if (event instanceof CustomEvent) {
      const index = this.detailArr.findIndex(ID => ID === beforeValue.toString());
      if (index > -1) {
        this.detailArr.splice(index, 1);
      }
      if (event.detail.value.toString() !== '0'){
        this.detailArr.push(event.detail.value.toString());
      }
    }
  }

  onSave(): void {
    let userDetail = '';
    this.detailArr.forEach(item => {
      userDetail += ',' + item.toString();
    });
    userDetail += ',';
    this.http.put<{}>(`${environment.API_URL}/users/profile`, {
      detail: userDetail,
    }).subscribe(() => {
      this.modalController.dismiss(userDetail);
    }, async () => {
      const alert = await this.alertController.create({
        message: t('Operation Failed'),
        buttons: [t('OK')],
      });
      await alert.present();
    });
  }

  onClose(): void {
    this.modalController.dismiss();
  }
}
