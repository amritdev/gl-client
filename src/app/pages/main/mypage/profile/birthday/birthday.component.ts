import { HttpClient } from '@angular/common/http';
import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-birthday',
  templateUrl: './birthday.component.html',
  styleUrls: ['./birthday.component.scss'],
})
export class BirthdayComponent implements OnInit {

  @Input() birthday: string | null = null;

  constructor(
    private http: HttpClient,
    private modal: ModalController,
  ) { }

  ngOnInit(): void {

  }

  getMaxYear(): number {
    return new Date().getFullYear() - environment.START_AGE;
  }

  onSave(): void {
    // console.log(this.birthday);
    this.http.put<{}>(`${environment.API_URL}/users/profile`, {
      birthday: this.birthday,
    }).subscribe(() => {
      this.modal.dismiss(this.birthday);
    });
  }

  onClose(): void {
    this.modal.dismiss();
  }


}
