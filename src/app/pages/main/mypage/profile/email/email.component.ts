import { Component, OnInit, Input } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { ModalController, AlertController } from '@ionic/angular';

import { environment } from 'src/environments/environment';
import t from 'src/locales';

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.scss'],
})
export class EmailComponent implements OnInit {

  @Input() email: string | null = null;

  constructor(
    private http: HttpClient,
    private modalController: ModalController,
    private alert: AlertController,
  ) { }

  ngOnInit(): void {}

  isValidEmail(): boolean {
    if (!this.email || this.email.trim().length < 1) {
      return false;
    }
    if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.email)) {
      return false;
    }
    return true;
  }

  onSave(): void {
    this.http.put<{}>(`${environment.API_URL}/users/profile`, {
      email: this.email,
    }).subscribe(() => {
      this.modalController.dismiss(this.email);
    }, (err: HttpErrorResponse) => {
      if (err.status === 409) {
        this.presentAlert(t('Email already exists'));
      } else {
        this.presentAlert(t('Operation Failed'));
      }
    });
  }

  async presentAlert(msg: string): Promise<void> {
    const alert = await this.alert.create({
      message: msg,
      buttons: [t('OK')],
    });
    await alert.present();
  }

  onClose(): void {
    this.modalController.dismiss();
  }

}
