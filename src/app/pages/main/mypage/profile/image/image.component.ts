import { Component, OnInit, Input } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { ModalController, AlertController } from '@ionic/angular';

import { environment } from 'src/environments/environment';
import t from 'src/locales';
import { Image } from 'src/app/interfaces';


@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss'],
})
export class ImageComponent implements OnInit {

  @Input() title = '';
  @Input() imageID = 0;
  @Input() imageURL = 'assets/img/avatar.svg';

  image: File | null = null;
  blocked = false;

  constructor(
    private http: HttpClient,
    private modalController: ModalController,
    private alertController: AlertController,
  ) { }

  ngOnInit(): void { }

  onFileChanged(event: Event): void {
    if (event.target instanceof HTMLInputElement && event.target.files && event.target.files[0]) {
      this.image = event.target.files[0];
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (_) => {
        this.imageURL = reader.result as string;
      };
    }
  }

  onSave(): void {
    if (this.image) {
      const formData = new FormData();
      formData.append('image_id', this.imageID.toString());
      formData.append('image', this.image);
      this.http.put<Image>(`${environment.API_URL}/users/profile/image`, formData).subscribe(image => {
        this.modalController.dismiss({
          type: 'UPDATE',
          image,
        });
      }, async () => {
        const alert = await this.alertController.create({
          message: t('Operation Failed'),
          buttons: [t('OK')],
        });
        await alert.present();
      });
    }
  }

  async onDelete(): Promise<void> {
    const alert = await this.alertController.create({
      message: t('Are you sure to delete?'),
      buttons: [t('Cancel'), {
        text: t('OK'),
        handler: () => {
          this.blocked = true;
          this.http.delete<{}>(`${environment.API_URL}/users/profile/image/${this.imageID}`).subscribe(() => {
            this.modalController.dismiss({
              type: 'DELETE',
            });
          }, async (err: HttpErrorResponse) => {
            if (err.status === 400) {
              const errAlert = await this.alertController.create({
                message: t('Can not delete last profile image'),
                buttons: [t('OK')],
              });
              await errAlert.present();
            } else {
              const errAlert = await this.alertController.create({
                message: t('Operation Failed'),
                buttons: [t('OK')],
              });
              await errAlert.present();
            }
          });
        },
      }],
    });
    await alert.present();
  }

  onClose(): void {
    this.modalController.dismiss();
  }

}
