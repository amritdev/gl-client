import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ModalController, AlertController } from '@ionic/angular';

import { Location } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';
import t from 'src/locales';

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss'],
})
export class LocationComponent implements OnInit {

  @Input() locationIDs = '';
  @Input() locations: Location[] = [];

  isChecked: boolean[] = [];
  isDisabled: boolean[] = [];
  mainLocationID = 0;

  constructor(
    private http: HttpClient,
    private modalController: ModalController,
    private alertController: AlertController,
  ) { }

  ngOnInit(): void {
    let locationArr = this.locationIDs.split(',') ?? [];
    locationArr = locationArr.filter(item => item !== '');
    this.locations.forEach(location => {
      if (locationArr.includes(location.ID.toString())) {
        this.isChecked.push(true);
        this.isDisabled.push(false);
      } else {
        this.isChecked.push(false);
        if (locationArr.length === 3) {
          this.isDisabled.push(true);
        } else {
          this.isDisabled.push(false);
        }
      }
    });
    this.mainLocationID = parseInt(locationArr[0], 10);
  }

  getCheckedCount(): number {
    return this.isChecked.filter(b => b === true).length;
  }

  getCheckedLocations(): Location[] {
    const locations: Location[] = [];
    this.isChecked.forEach((b, i) => {
      if (b) {
        locations.push(this.locations[i]);
      }
    });
    return locations;
  }

  onLocationsChanged(): void {
    if (this.isChecked.filter(b => b === true).length === 3) {
      this.isChecked.forEach((b, i) => {
        this.isDisabled[i] = !b;
      });
    } else {
      this.isDisabled.forEach((b, i) => {
        this.isDisabled[i] = false;
      });
    }
    const locations = this.getCheckedLocations();
    if (locations.length < 1) {
      this.mainLocationID = 0;
    } else if (!locations.find(l => l.ID === this.mainLocationID)) {
      this.mainLocationID = locations[0].ID;
    }
  }

  onSave(): void {
    const locationIDsArr: number[] = [];
    this.getCheckedLocations().forEach(l => {
      if (l.ID === this.mainLocationID) {
        locationIDsArr.unshift(l.ID);
      } else {
        locationIDsArr.push(l.ID);
      }
    });
    const locationIDs = JSON.stringify(locationIDsArr).replace('[', ',').replace(']', ',');
    this.http.put<{}>(`${environment.API_URL}/users/profile`, {
      location_ids: locationIDs,
    }).subscribe(() => {
      this.modalController.dismiss(locationIDs);
    }, async () => {
      const alert = await this.alertController.create({
        message: t('Operation Failed'),
        buttons: [t('OK')],
      });
      await alert.present();
    });
  }

  onClose(): void {
    this.modalController.dismiss();
  }

}
