import { Component, OnInit, Input } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { ModalController, AlertController } from '@ionic/angular';

import { environment } from 'src/environments/environment';
import t from 'src/locales';

@Component({
  selector: 'app-nickname',
  templateUrl: './nickname.component.html',
  styleUrls: ['./nickname.component.scss'],
})
export class NicknameComponent implements OnInit {

  @Input() nickname: string | null = null;

  constructor(
    private http: HttpClient,
    private modal: ModalController,
    private alert: AlertController,
  ) { }

  ngOnInit(): void { }

  onSave(): void {
    this.http.put<{}>(`${environment.API_URL}/users/profile`, {
      nickname: this.nickname?.trim(),
    }).subscribe(() => {
      this.modal.dismiss(this.nickname?.trim());
    }, (err: HttpErrorResponse) => {
      if (err.status === 409) {
        this.presentAlert(t('Nickname already exists'));
      } else {
        this.presentAlert(t('Operation Failed'));
      }
    });
  }

  async presentAlert(msg: string): Promise<void> {
    const alert = await this.alert.create({
      message: msg,
      buttons: [t('OK')],
    });
    await alert.present();
  }

  onClose(): void {
    this.modal.dismiss();
  }

}
