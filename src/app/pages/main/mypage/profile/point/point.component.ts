import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ModalController, AlertController } from '@ionic/angular';

import { environment } from 'src/environments/environment';
import t from 'src/locales';
import { Class } from 'src/app/interfaces';

@Component({
  selector: 'app-point',
  templateUrl: './point.component.html',
  styleUrls: ['./point.component.scss'],
})
export class PointComponent implements OnInit {

  @Input() pointHalf = 0;
  infPoint = 500;
  pointValues: number[] = [];

  constructor(
    private http: HttpClient,
    private modalController: ModalController,
    private alertController: AlertController,
  ) { }

  ngOnInit(): void {
    this.getClasses();
  }

  onSave(): void {
    this.http.put<{}>(`${environment.API_URL}/users/profile`, {
      point_half: Math.floor(this.pointHalf),
    }).subscribe(() => {
      this.modalController.dismiss(this.pointHalf);
    }, async () => {
      const alert = await this.alertController.create({
        message: t('Operation Failed'),
        buttons: [t('OK')],
      });
      await alert.present();
    });
  }

  onClose(): void {
    this.modalController.dismiss();
  }

  getClasses(): void {
    this.http.get<Class[]>(`${environment.API_URL}/admin/classes`).subscribe((res) => {
      const classList: Class[] = JSON.parse(JSON.stringify(res));
      if (classList.length > 0){
        this.infPoint = classList[0].Point;
      }
      this.pointValues = this.getValues(this.infPoint);
    });
  }

  getValues(startVal: number): number[]{
    const values = [];
    let included = false;
    for (let i = 1; i <= 200; i++){
      if (100 * i >= startVal && !included){
        if (100 * i > startVal){
          values.push(startVal);
        }
        included = true;
      }
      if (included){
        values.push(100 * i);
      }
    }
    return values;
  }

  valueSelected(newVal: number): void{
    this.pointHalf = newVal;
  }
}
