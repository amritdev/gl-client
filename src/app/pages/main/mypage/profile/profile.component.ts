import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IonRouterOutlet, ModalController, AlertController, NavController } from '@ionic/angular';
import { ItemReorderEventDetail } from '@ionic/core';
import { AuthService } from 'src/app/services';
import { User, Detail, Image, Tag, Location } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';
import t from 'src/locales';
import { ImageComponent } from './image/image.component';
import { VideoComponent } from './video/video.component';
import { NicknameComponent } from './nickname/nickname.component';
import { EmailComponent } from './email/email.component';
import { PointComponent } from './point/point.component';
import { WordComponent } from './word/word.component';
import { AboutComponent } from './about/about.component';
import { BirthdayComponent } from './birthday/birthday.component';
import { BasicComponent } from './basic/basic.component';
import { ViewComponent } from './view/view.component';
import { ScheduleComponent } from './schedule/schedule.component';
import { TagComponent } from './tag/tag.component';
import { LocationComponent } from './location/location.component';

interface DetailWithValue extends Detail {
  Value?: string;
}
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {

  user: User | null = null;
  details: Detail[] = [];
  userDetails: DetailWithValue[] = [];
  image = environment.SITE_URL + '/assets/cast.png';
  sight = '';
  tags: Tag[] = [];
  detailsStr = '';
  tagStr = '';
  locations: Location[] = [];
  isLoading = false;
  realName = '';
  realNameFrigana = '';
  coporateName = '';
  coporateNameFrigana = '';
  orderFixed = true;

  constructor(
    private routerOutlet: IonRouterOutlet,
    private authService: AuthService,
    private http: HttpClient,
    private navController: NavController,
    private modalController: ModalController,
    private alertController: AlertController,
  ) { }

  ngOnInit(): void {
    this.authService.currentUserSubject.subscribe(user => {
      this.user = user ? { ...user } : null;
      if (user) {
        this.realName = user.RealName ?? '';
        this.realNameFrigana = user.RealNameFrigana ?? '';
        this.coporateName = user.CoporateName ?? '';
        this.coporateNameFrigana = user.CoporateNameFrigana ?? '';
        this.sight = user.Sight;
        this.getAllDetails();

        if (user.Images.length > 0){
          this.image = user.Images[0].Path;
        }
      }


    });
    this.getParentTags();
    this.getParentDetails();
    this.getParentLocations();
  }

  async presentAlert(msg: string): Promise<void> {
    const alert = await this.alertController.create({
      message: msg,
      buttons: [t('OK')],
    });
    await alert.present();
  }

  getParentTags(): void {
    this.tagStr = '';
    this.http.get<Tag[]>(`${environment.API_URL}/tags?pid=0`).subscribe(tags => {
      this.tags = tags;
      this.tags.forEach(tag => {
        this.tagStr += tag.Name + '、';
      });
    });
  }

  getAllDetails(): void {
    this.http.get<Detail[]>(`${environment.API_URL}/details`).subscribe(details => {
      const parentDetails: DetailWithValue[] = details.filter(d => d.ParentID === 0);
      const detailArr = this.user?.Detail?.split(',').filter(item => item !== '') ?? [];
      detailArr.forEach(detailID => {
        const detail = details.find(item => item.ID.toString() === detailID);
        if (detail) {
          const index = parentDetails.findIndex(item => item.ID === detail.ParentID);
          if (index > -1) {
            parentDetails[index].Value = detail.Name;
          }
        }
      });
      this.userDetails = [...parentDetails];
    });
  }

  getParentDetails(): void {
    this.detailsStr = '';
    this.http.get<Detail[]>(`${environment.API_URL}/details?pid=0`).subscribe(details => {
      this.details = details;
      details.forEach(detail => {
        this.detailsStr += detail.Name + '、';
      });
    });
  }

  getParentLocations(): void {
    this.http.get<Location[]>(`${environment.API_URL}/locations?pid=0`).subscribe(locations => {
      this.locations = locations;
    });
  }

  getLocationsAsString(): string {
    let locationArr = this.user?.LocationIDs?.split(',') ?? [];
    locationArr = locationArr.filter(item => item !== '');
    let locationStr = '';
    locationArr.forEach(l => {
      const location = this.locations.find(item => item.ID.toString() === l);
      if (location) {
        locationStr += location.Name + '、';
      }
    });
    locationStr = locationStr.substring(0, locationStr.length - 1);
    return locationStr;
  }

  onFileChanged(event: Event): void {
    if (event.target instanceof HTMLInputElement && event.target.files && event.target.files[0]) {
      // check file size
      const limitSize = 5;
      const alertText = `You can not upload more than %s megabytes sized image`;
      if (event.target.files[0].size / 1024 / 1024 > limitSize) {
        this.presentAlert(t(alertText).replace('%s', limitSize.toString()));
        return;
      }

      const formData = new FormData();
      formData.append('image', event.target.files[0]);
      this.http.post<{
        update: boolean;
        image: Image;
      }>(`${environment.API_URL}/users/profile/image`, formData).subscribe(res => {
        if (this.user) {
          // console.log(res);
          if (res.update){
            this.user.Images[0] = res.image;
          }else{
            this.user.Images.push(res.image);
          }
        }
        this.authService.currentUserSubject.next(this.user);
        if (event.target instanceof HTMLInputElement) {
          event.target.value = '';
        }
      }, async () => {
        const alert = await this.alertController.create({
          message: t('Operation Failed'),
          buttons: [t('OK')],
        });
        await alert.present();
        if (event.target instanceof HTMLInputElement) {
          event.target.value = '';
        }
      });
    }
  }

  async openPreviewModal(): Promise<void> {
    const modal = await this.modalController.create({
      component: ViewComponent,
    });
    await modal.present();
  }

  async openScheduleModal(): Promise<void> {
    const modal = await this.modalController.create({
      component: ScheduleComponent,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
    });
    await modal.present();
  }

  async openImageModal(index: number): Promise<void> {
    const modal = await this.modalController.create({
      component: ImageComponent,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        title: t('Image') + (index + 1).toString(),
        imageID: this.user?.Images[index].ID ?? 0,
        imageURL: this.user?.Images[index].Path ?? 'assets/img/avatar.svg',
      },
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
    if (this.user && data?.type === 'UPDATE') {
      this.user.Images[index] = data.image;
      this.authService.currentUserSubject.next(this.user);
    }
    if (data?.type === 'DELETE') {
      this.user?.Images.splice(index, 1);
      this.authService.currentUserSubject.next(this.user);
    }
  }

  async openVideoModal(index: number): Promise<void> {
    const modal = await this.modalController.create({
      component: VideoComponent,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        title: index === -1 ? t('New Video') : t('Video') + (index + 1).toString(),
        image: this.image,
        videoID: index === -1 ? 0 : (this.user?.Videos[index].ID ?? 0),
        videoURL: index === -1 ? '' : (this.user?.Videos[index].Path ?? ''),
      },
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
    if (data?.type === 'DELETE') {
      this.user?.Videos.splice(index, 1);
      this.authService.currentUserSubject.next(this.user);
    }
    if (data?.type === 'CREATE') {
      this.showAlert(t('Video successfully added'));
    }
  }

  async openNicknameModal(): Promise<void> {
    const modal = await this.modalController.create({
      component: NicknameComponent,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        nickname: this.user?.Nickname ?? null,
      },
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
    if (this.user && data) {
      this.user.Nickname = data;
      this.authService.currentUserSubject.next(this.user);
    }
  }

  async openEmailModal(): Promise<void> {
    const modal = await this.modalController.create({
      component: EmailComponent,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        email: this.user?.Email ?? null,
      },
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
    if (this.user && data) {
      this.user.Email = data;
      this.authService.currentUserSubject.next(this.user);
    }
  }

  async openPointModal(): Promise<void> {
    this.http.get<{
      success: boolean;
    }>(`${environment.API_URL}/users/check/status`).subscribe(async res => {
      if (res.success) {
        const modal = await this.modalController.create({
          component: PointComponent,
          swipeToClose: true,
          presentingElement: this.routerOutlet.nativeEl,
          componentProps: {
            pointHalf: this.user?.PointHalf ?? null,
          },
        });
        await modal.present();
        const { data } = await modal.onWillDismiss();
        if (this.user && data) {
          this.user.PointHalf = data;
          this.authService.currentUserSubject.next(this.user);
        }
      } else {
        this.showAlert(t('You have ongoing private room'));
      }
    });
  }

  async showAlert(msgStr: string): Promise<void> {
    const alert = await this.alertController.create({
      message: msgStr,
      buttons: ['OK']
    });
    await alert.present();
  }

  async openWordModal(): Promise<void> {
    const modal = await this.modalController.create({
      component: WordComponent,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        word: this.user?.Word ?? null,
      },
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
    if (this.user && data) {
      this.user.Word = data;
      this.authService.currentUserSubject.next(this.user);
    }
  }

  async openAboutModal(): Promise<void> {
    const modal = await this.modalController.create({
      component: AboutComponent,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        about: this.user?.About ?? null,
      },
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
    if (this.user && data) {
      this.user.About = data;
      this.authService.currentUserSubject.next(this.user);
    }
  }

  async openTagModal(): Promise<void> {
    const modal = await this.modalController.create({
      component: TagComponent,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        tag: this.user?.Tags ?? null,
        tags: this.tags,
      },
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
    if (this.user && data) {
      this.user.Tags = data;
      this.authService.currentUserSubject.next(this.user);
    }
  }

  async openBasicModal(): Promise<void> {
    const modal = await this.modalController.create({
      component: BasicComponent,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        detail: this.user?.Detail ?? null,
        details: this.details,
        role: this.user?.Role ?? 0
      },
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
    if (this.user && data) {
      this.user.Detail = data;
      this.authService.currentUserSubject.next(this.user);
    }
  }

  async openBirthdayModal(): Promise<void> {
    const modal = await this.modalController.create({
      component: BirthdayComponent,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        birthday: this.user?.Birthday
      },
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
    if (this.user && data) {
      this.user.Birthday = data;
      this.authService.currentUserSubject.next(this.user);
    }
  }

  async openLocationModal(): Promise<void> {
    const modal = await this.modalController.create({
      component: LocationComponent,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        locationIDs: this.user?.LocationIDs ?? '',
        locations: this.locations,
      },
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
    if (this.user && data) {
      this.user.LocationIDs = data;
      this.authService.currentUserSubject.next(this.user);
    }
  }

  onSaveAgentProfile(): void {
    if (this.realName.trim() === '') {
      this.presentAlert(t('Real name is required'));
    } else if (this.realNameFrigana.trim() === '') {
      this.presentAlert(t('Real name (frigana) is required'));
    } else {
      this.isLoading = true;
      this.http.post<User>(`${environment.API_URL}/users/profile/agent/register`, {
        real_name: this.realName,
        real_name_frigana: this.realNameFrigana,
        coporate_name: this.coporateName,
        coporate_name_frigana: this.coporateNameFrigana
      }).subscribe((user) => {
        this.isLoading = false;
        this.authService.currentUserSubject.next(user);
        this.navController.navigateBack('/main/mypage');
      }, () => {
        this.isLoading = false;
        this.presentAlert(t('Operation Failed'));
      });
    }
  }

  toggleBirthdayHidden(): void {
    if (this.user) {
      this.http.put<{}>(`${environment.API_URL}/users/profile`, {
        birthday_hidden: this.user.BirthdayHidden ? 'true' : 'false',
      }).subscribe(() => {
        this.authService.currentUserSubject.next(this.user);
      });
    }
  }

  reorderItems(ev: Event, typeStr: string): void {
    // The `from` and `to` properties contain the index of the item
    // when the drag started and ended, respectively
    const reorderEvent = ev as CustomEvent<ItemReorderEventDetail>;
    console.log('Dragged from index', reorderEvent.detail.from, 'to', reorderEvent.detail.to);


    this.http.post<Image[]>(`${environment.API_URL}/users/swap`,
      { start: reorderEvent.detail.from, end: reorderEvent.detail.to, type: typeStr }).subscribe((res) => {
        if (this.user) {
          if (typeStr === 'image'){
            this.user.Images = res;
          }else {
            this.user.Videos = res;
          }
          this.authService.currentUserSubject.next(this.user);
        }
      });

    // Finish the reorder and position the item in the DOM based on
    // where the gesture ended. This method can also be called directly
    // by the reorder group
    reorderEvent.detail.complete();
  }

}
