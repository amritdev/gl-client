import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { PipesModule } from 'src/app/shared';

import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileComponent } from './profile.component';
import { ImageComponent } from './image/image.component';
import { VideoComponent } from './video/video.component';
import { NicknameComponent } from './nickname/nickname.component';
import { EmailComponent } from './email/email.component';
import { PointComponent } from './point/point.component';
import { WordComponent } from './word/word.component';
import { AboutComponent } from './about/about.component';
import { BasicComponent } from './basic/basic.component';
import { ViewComponent } from './view/view.component';
import { ScheduleComponent } from './schedule/schedule.component';
import { TagComponent } from './tag/tag.component';
import { LocationComponent } from './location/location.component';
import { BirthdayComponent } from './birthday/birthday.component';

@NgModule({
  declarations: [
    ProfileComponent,
    ImageComponent,
    VideoComponent,
    NicknameComponent,
    EmailComponent,
    WordComponent,
    AboutComponent,
    BasicComponent,
    BirthdayComponent,
    ViewComponent,
    ScheduleComponent,
    TagComponent,
    PointComponent,
    LocationComponent,
  ],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    PipesModule,
    ProfileRoutingModule,
  ],
})
export class ProfileModule { }
