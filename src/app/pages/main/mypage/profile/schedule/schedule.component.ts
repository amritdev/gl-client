import { Component, OnInit } from '@angular/core';
import { formatDate } from '@angular/common';
import { HttpClient } from '@angular/common/http';

import { ModalController, AlertController } from '@ionic/angular';

import { AuthService, GeoLocationService } from 'src/app/services';
import { Schedule, User } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';
import t from 'src/locales';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.scss'],
})
export class ScheduleComponent implements OnInit {

  user: User | null = null;
  datetimes: Date[] = [];
  daysOfWeek = ['日', '月', '火', '水', '木', '金', '土'];
  hours: number[] = [];
  minutes: number[] = [];
  scheduleByKey: {
    [key: string]: {
      from: string | null;
      to: string | null;
      midnight: boolean;
      enabled: boolean;
    };
  } = {};

  constructor(
    private authService: AuthService,
    private http: HttpClient,
    private modalController: ModalController,
    private alertController: AlertController,
    private gps: GeoLocationService
  ) { }

  ngOnInit(): void {
    this.setHoursMinutes();

    for (let i = 0; i < 14; i++) {
      const tmpDate = new Date();
      tmpDate.setDate(tmpDate.getDate() + i);
      this.datetimes.push(tmpDate);
      this.scheduleByKey[this.getDateKey(tmpDate)] = {
        from: null,
        to: null,
        midnight: false,
        enabled: false,
      };
    }
    this.authService.currentUserSubject.subscribe(user => {
      this.user = user ? { ...user } : null;
      user?.Schedules?.forEach(item => {
        if (this.scheduleByKey[item.Date]) {
          this.scheduleByKey[item.Date] = {
            from: item.From,
            to: item.To,
            midnight: item.Midnight,
            enabled: item.Enabled,
          };
        }
      });
    });
  }

  getDateKey(datetime: Date): string {
    return formatDate(datetime, 'yyyy-MM-dd', 'en', 'Asia/Tokyo');
  }

  setHoursMinutes(): void {
    for (let hour = 5; hour < 29; hour++){
      this.hours.push(hour % 24);
    }

    for (let minutes = 0; minutes < 60; minutes++){
      this.minutes.push(minutes);
    }
  }

  setMidNight(dateKey: string, isSet: boolean): void {
    this.scheduleByKey[dateKey].midnight = isSet;
  }

  onClear(datetime: Date): void {
    const date = this.getDateKey(datetime);
    if (this.scheduleByKey[date]) {
      this.scheduleByKey[date] = {
        from: '',
        to: '',
        midnight: false,
        enabled: false,
      };
    }
  }

  onSave(): void {
    const schedules: Schedule[] = [];
    Object.keys(this.scheduleByKey).forEach(date => {
      if (this.scheduleByKey[date].enabled || (this.scheduleByKey[date].from || this.scheduleByKey[date].to)) {
        schedules.push({
          Date: date,
          From: this.scheduleByKey[date].from,
          To: this.scheduleByKey[date].to,
          Midnight: this.scheduleByKey[date].midnight,
          Enabled: this.scheduleByKey[date].enabled,
          UserID: this.user?.ID ?? null,
        });
      }
    });
    this.http.put<{
      data: Schedule[],
      today: boolean;
    }>(`${environment.API_URL}/users/profile/schedule`, schedules).subscribe(res => {
      if (this.user) {
        this.user.Schedules = res.data;
        this.authService.currentUserSubject.next(this.user);
        if (res.today){
          this.gps.savePosition(
            'today-free'
          );
        }
      }
      this.modalController.dismiss();
    }, async () => {
      const alert = await this.alertController.create({
        message: t('Operation Failed'),
        buttons: [t('OK')],
      });
      await alert.present();
    });
  }

  onClose(): void {
    this.modalController.dismiss();
  }

}
