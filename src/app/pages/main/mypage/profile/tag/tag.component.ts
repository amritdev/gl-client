import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ModalController, AlertController } from '@ionic/angular';

import { Tag } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';
import t from 'src/locales';

interface TagWithChildren extends Tag {
  Children: Tag[];
}

@Component({
  selector: 'app-tag',
  templateUrl: './tag.component.html',
  styleUrls: ['./tag.component.scss'],
})
export class TagComponent implements OnInit {

  @Input() tag: string | null = null;
  @Input() tags: TagWithChildren[] = [];

  tagArr: string[] = [];

  constructor(
    private http: HttpClient,
    private modalController: ModalController,
    private alertController: AlertController,
  ) { }

  ngOnInit(): void {
    const tagArr = this.tag?.split(',') ?? [];
    this.tagArr = tagArr.filter(item => item !== '');
    this.getTags();
  }

  getTags(): void {
    this.http.get<Tag[]>(`${environment.API_URL}/tags`).subscribe(tags => {
      this.tags.forEach(tag => {
        tag.Children = tags.filter(item => item.ParentID === tag.ID) ?? [];
      });
    });
  }

  getSelectedValue(tag: TagWithChildren): number[] {
    const selectedTagIDs: number[] = [];
    const selectedTags = tag.Children?.filter(item => this.tagArr.includes(item.ID.toString()));
    if (selectedTags) {
      selectedTags.forEach(item => {
        selectedTagIDs.push(item.ID);
      });
    }
    return selectedTagIDs;
  }

  onSelect(event: Event, beforeValue: number[]): void {
    if (event instanceof CustomEvent) {
      beforeValue.forEach(item => {
        const index = this.tagArr.findIndex(ID => ID === item.toString());
        if (index > -1) {
          this.tagArr.splice(index, 1);
        }
      });
      event.detail.value.forEach((item: number) => {
        this.tagArr.push(item.toString());
      });
    }
  }

  onSave(): void {
    let userTag = '';
    this.tagArr.forEach(item => {
      userTag += ',' + item.toString();
    });
    userTag += ',';
    this.http.put<{}>(`${environment.API_URL}/users/profile`, {
      tags: userTag,
    }).subscribe(() => {
      this.modalController.dismiss(userTag);
    }, async () => {
      const alert = await this.alertController.create({
        message: t('Operation Failed'),
        buttons: [t('OK')],
      });
      await alert.present();
    });
  }

  onClose(): void {
    this.modalController.dismiss();
  }
}
