import { HttpClient } from '@angular/common/http';
import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { AlertController, ModalController } from '@ionic/angular';
import { User } from 'src/app/interfaces';
import { AuthService } from 'src/app/services';
import { environment } from 'src/environments/environment';
import t from 'src/locales';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.scss'],
})
export class VideoComponent implements OnInit {

  @Input() title = '';
  @Input() videoID = 0;
  @Input() videoURL = '';
  @Input() image = '';

  video: File | null = null;
  isBlocked = false;
  isSaving = false;

  constructor(
    private modalController: ModalController,
    private alert: AlertController,
    private http: HttpClient,
    private sanitizer: DomSanitizer,
    private auth: AuthService
  ) { }

  ngOnInit(): void {}

  async onDelete(): Promise<void> {
    const alert = await this.alert.create({
      message: t('Are you sure to delete?'),
      buttons: [t('Cancel'), {
        text: t('OK'),
        handler: () => {
          this.http.delete<{}>(`${environment.API_URL}/users/profile/video/${this.videoID}`).subscribe(() => {
            this.modalController.dismiss({
              type: 'DELETE',
            });
          }, _ => {
            this.presentAlert(t('Operation Failed'));
          });
        },
      }],
    });
    await alert.present();
  }

  onClose(): void {
    this.modalController.dismiss();
  }

  onFileChanged(event: Event): void {
    this.isBlocked = true;
    if (event.target instanceof HTMLInputElement && event.target.files && event.target.files[0]) {
      this.video = event.target.files[0];
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (e) => {
        try{
          // @ts-ignore
          this.videoURL = this.sanitizer.bypassSecurityTrustUrl(e.target.result);
        }catch (ex){
          console.log(ex);
        }

      };
    }
  }

  getDuration(event: Event): void {
    if (this.video) {
      if (event.target instanceof HTMLMediaElement && event.target.duration > 10.9999) { // 10.99999
        this.presentAlert(t('Please upload a video less than 10 seconds'));
        this.videoURL = '';
        this.isBlocked = false;
      } else {
        this.isSaving = true;
        const formData = new FormData();
        formData.append('video', this.video, this.video.name);
        if (this.videoID === 0){
          this.http.post<User>(`${environment.API_URL}/users/profile/video`, formData).subscribe(res => {
            this.isSaving = false;
            this.auth.currentUserSubject.next(res);
            this.modalController.dismiss({
              type: 'CREATE',
            });
          }, _ => {
            this.isSaving = false;
            this.isBlocked = false;
            this.presentAlert('Video upload failed');
          });
        }else{
          // formData.append('video_id', this.videoID.toString());
          // this.http.put<User>(`${environment.API_URL}/users/profile/video`, formData).subscribe(res => {
          //   this.auth.currentUserSubject.next(res);
          //   this.modalController.dismiss({
          //     type: 'UPDATE',
          //   });
          // }, _ => {
          //   this.presentAlert("Video upload failed");
          // });
        }
      }
    }
  }

  async presentAlert(msg: string): Promise<void> {
    const alert = await this.alert.create({
      message: msg,
      buttons: [t('OK')],
    });
    await alert.present();
  }
}
