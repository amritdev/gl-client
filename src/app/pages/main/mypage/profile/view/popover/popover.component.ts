import { Component, OnInit } from '@angular/core';

import { NavParams } from '@ionic/angular';

@Component({
  selector: 'app-popover',
  templateUrl: './popover.component.html',
  styleUrls: ['./popover.component.scss'],
})
export class PopoverComponent implements OnInit {

  text = '';

  constructor(
    private navParams: NavParams
  ) {
    this.text = this.navParams.get('text');
  }

  ngOnInit(): void { }

}
