import { Component, OnInit, ViewChild } from '@angular/core';
import { formatDate } from '@angular/common';
import { HttpClient } from '@angular/common/http';

import { ModalController, IonSlides, PopoverController, Platform } from '@ionic/angular';

import { AuthService } from 'src/app/services';
import { User, Detail, Tag, Tweet, Gift, UserGift, Video, Image } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';

import { PopoverComponent } from './popover/popover.component';

interface DetailWithValue extends Detail {
  Value?: string;
}

interface GiftHistory {
  ID: number;
  Count: number;
}

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss'],
})
export class ViewComponent implements OnInit {

  @ViewChild('slides') slides!: IonSlides;

  user: User | null = null;
  details: DetailWithValue[] = [];
  tweets: Tweet[] = [];
  blockedTweetIDs: string | null = null;
  blockedTweetUserIDs: string | null = null;
  tags: string[] = [];
  activatedSlide = 0;
  thumbSlideOpts = {
    spaceBetween: 10,
    slidesPerView: 4.5,
  };
  headerOpacity = 0;
  daysOfWeek: string[] = ['日', '月', '火', '水', '木', '金', '土'];
  calendar: Date[][] = [[], []];
  scheduleByKey: {
    [key: string]: {
      from: string | null;
      to: string | null;
      midnight: boolean;
    };
  } = {};
  selectedWeek = 0;
  isIOS = false;
  selectedImage = '';
  selectedVideo = '';
  muted = true;

  gifts: Gift[] = [];
  userGifts: UserGift[] = [];

  constructor(
    private authService: AuthService,
    private http: HttpClient,
    private modalController: ModalController,
    private popoverController: PopoverController,
    private platform: Platform
  ) {
    this.isIOS = this.platform.is('ios') && !this.platform.is('mobileweb');
    this.getGifts();
  }

  ngOnInit(): void {
    for (let i = 0; i < 14; i++) {
      const tmpDate = new Date();
      tmpDate.setDate(tmpDate.getDate() + i);
      if (i < 7) {
        this.calendar[0].push(tmpDate);
      } else {
        this.calendar[1].push(tmpDate);
      }
      this.scheduleByKey[this.getDateKey(tmpDate)] = {
        from: '',
        to: '',
        midnight: false
      };
    }
    this.authService.currentUserSubject.subscribe(user => {
      this.user = user ? { ...user } : null;
      user?.Schedules?.forEach(item => {
        if (this.scheduleByKey[item.Date]) {
          this.scheduleByKey[item.Date] = {
            from: item.From,
            to: item.To,
            midnight: item.Midnight,
          };
        }
      });
      this.blockedTweetIDs = user?.BlockedTweetIDs ?? null;
      this.blockedTweetUserIDs = user?.BlockedTweetUserIDs ?? null;
      if (user) {

        if (user.Images.length > 0){
          this.selectedImage = user.Images[0].Path;
        }

        // this.userGifts = [];
        this.getRecentTweets(user.ID);

        // if (user.Role === 0 && user.GiftReceived !== null) {
        //   const giftHistory = JSON.parse(user.GiftReceived) as GiftHistory[];
        //   giftHistory.forEach(item => {
        //     const giftIndex = this.gifts.findIndex(giftItem => giftItem.ID === item.ID);
        //     if (giftIndex > -1){
        //       this.userGifts.push({
        //         Gift: this.gifts[giftIndex],
        //         Count: item.Count
        //       });
        //     }
        //   });
        // }
      }
    });
    this.getParentTags();
    this.getAllDetails();
  }

  getRecentTweets(userID: number): void {
    this.http.get<Tweet[]>(`${environment.API_URL}/users/tweet/recent/${userID}`).subscribe(tweets => {
      this.tweets = tweets;
    });
  }

  getGifts(): void {
    this.http.get<Gift[]>(`${environment.API_URL}/gifts`).subscribe(gifts => {
      this.gifts = gifts;
    });
  }

  getDateKey(datetime: Date): string {
    return formatDate(datetime, 'yyyy-MM-dd', 'en', 'Asia/Tokyo');
  }

  getMidnightStatus(datetime: Date): string {
    if (this.scheduleByKey[this.getDateKey(datetime)]
      && this.scheduleByKey[this.getDateKey(datetime)].from
      && this.scheduleByKey[this.getDateKey(datetime)].to) {
      if (this.scheduleByKey[this.getDateKey(datetime)].midnight) {
        return 'radio-button-on';
      } else {
        return 'radio-button-off';
      }
    } else {
      return 'remove';
    }
  }

  getParentTags(): void {
    this.http.get<Tag[]>(`${environment.API_URL}/tags`).subscribe(tags => {
      tags.forEach(tag => {
        if (this.user?.Tags?.includes(',' + tag.ID.toString() + ',')) {
          this.tags.push(tag.Name);
        }
      });
    });
  }

  getAllDetails(): void {
    this.http.get<Detail[]>(`${environment.API_URL}/details`).subscribe(details => {
      const parentDetails: DetailWithValue[] = details.filter(d => d.ParentID === 0);
      const detailArr = this.user?.Detail?.split(',').filter(item => item !== '') ?? [];
      detailArr.forEach(detailID => {
        const detail = details.find(item => item.ID.toString() === detailID);
        if (detail) {
          const index = parentDetails.findIndex(item => item.ID === detail.ParentID);
          if (index > -1) {
            parentDetails[index].Value = detail.Name;
          }
        }
      });
      this.details = [...parentDetails];
    });
  }

  getHeartCount(tweet: Tweet): number {
    return tweet.FavoriteUserIDs ? JSON.parse(tweet.FavoriteUserIDs).length + tweet.Bonus : tweet.Bonus;
  }

  getHeartStatus(tweet: Tweet): boolean {
    return tweet.FavoriteUserIDs ? JSON.parse(tweet.FavoriteUserIDs).includes(this.user?.ID) : 0;
  }

  isValidTweet(tweet: Tweet): boolean {
    const blocketTweetIDsArr = this.blockedTweetIDs ? JSON.parse(this.blockedTweetIDs) : [];
    const blockedTweetUserIDsArr = this.blockedTweetUserIDs ? JSON.parse(this.blockedTweetUserIDs) : [];
    if (blocketTweetIDsArr.includes(tweet.ID)) {
      return false;
    }
    if (blockedTweetUserIDsArr.includes(tweet.UserID)) {
      return false;
    }
    return true;
  }

  onToggleHeart(tweet: Tweet, index: number): void {
    const favoriteUserIDsArr = tweet.FavoriteUserIDs ? JSON.parse(tweet.FavoriteUserIDs) : [];
    let isFavorite = false;
    if (this.getHeartStatus(tweet)) {
      const favoriteUserIDsArrIndex = favoriteUserIDsArr.findIndex((id: number) => id === this.user?.ID);
      favoriteUserIDsArr.splice(favoriteUserIDsArrIndex, 1);
      isFavorite = false;
    } else {
      favoriteUserIDsArr.push(this.user?.ID);
      isFavorite = true;
    }
    this.tweets[index].FavoriteUserIDs = JSON.stringify(favoriteUserIDsArr);
    this.http.put<{}>(`${environment.API_URL}/tweets/favorite`, {
      tweet_id: tweet.ID,
      favorite_user_ids: JSON.stringify(favoriteUserIDsArr),
      is_favorite: isFavorite,
    }).subscribe();
  }

  async onPopover(event: Event, datetime: Date): Promise<void> {
    const from = this.scheduleByKey[this.getDateKey(datetime)].from;
    const to = this.scheduleByKey[this.getDateKey(datetime)].to;
    if (from && to) {
      const popover = await this.popoverController.create({
        component: PopoverComponent,
        componentProps: {
          text: formatDate(from, 'HH:mm', 'en', 'Asia/Tokyo') + ' ～ ' + formatDate(to, 'HH:mm', 'en', 'Asia/Tokyo')
        },
        event,
      });
      await popover.present();
    }
  }

  onScroll(event: Event): void {
    if (event instanceof CustomEvent) {
      this.headerOpacity = (event.detail.scrollTop > 50) ? 1 : 0;
    }
  }

  onSlideChange(): void {
    this.slides.getActiveIndex().then(index => {
      this.activatedSlide = index;
    });
  }

  onClose(): void {
    this.modalController.dismiss();
  }

  getStatusClass(): string {
    if (this.user) {
      if (this.user.Status) {
        return 'green';
      } else {
        const curTime = Date.now();
        const leftAt = new Date(this.user.LeftAt ?? '').getTime();
        const seconds = (curTime - leftAt) / 1000;
        if (seconds < 3600) {
          return 'green';
        } else if (seconds < 3600 * 24) {
          return 'yellow';
        } else if (seconds < 3600 * 24 * 3) {
          return 'gray';
        } else {
          return '';
        }
      }
    } else {
      return '';
    }
  }

  videoSelected(video: Video): void {
    this.selectedVideo = video.Path;
    this.selectedImage = '';

    // if exists then remove
    const curProfileVideo = document.querySelector('.avatar-slide .profile-video');
    if (curProfileVideo){
      curProfileVideo.remove();
    }

    this.muted = true;

    // create video tag
    const videoElement: HTMLVideoElement = document.createElement('video');
    const avatarSlide = document.querySelector('.avatar-slide');
    videoElement.loop = true;
    videoElement.muted = true;
    videoElement.autoplay = true;
    videoElement.playsInline = true;
    videoElement.poster = this.user ? this.user.Images[0].Path : 'assets/img/cast.png';
    videoElement.className = 'profile-video';
    videoElement.style.width = '100%';
    videoElement.style.height = '500px';
    videoElement.style.objectFit = 'cover';
    videoElement.style.alignSelf = 'center';
    if (avatarSlide){
      avatarSlide.appendChild(videoElement);
      const source: HTMLSourceElement = document.createElement('source');
      source.src = video.Path;
      source.type = 'video/mp4';
      videoElement.appendChild(source);
    }
  }

  imageSelected(image: Image): void{
    this.selectedImage = image.Path;
    this.selectedVideo = '';

    // if exists then remove
    const curProfileVideo = document.querySelector('.avatar-slide .profile-video');
    if (curProfileVideo){
      curProfileVideo.remove();
    }
  }

  controlSound(): void{
    this.muted = !this.muted;
    const curProfileVideo = document.querySelector('.avatar-slide .profile-video') as HTMLVideoElement;
    if (curProfileVideo) {
      curProfileVideo.muted = this.muted;
    }
  }

}
