import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-qrcode',
  templateUrl: './qrcode.component.html',
  styleUrls: ['./qrcode.component.scss'],
})
export class QrcodeComponent implements OnInit {

  qrdata = '';

  constructor(
    private authService: AuthService,
  ) { }

  ngOnInit(): void {
    this.authService.currentUserSubject.subscribe(user => {
      this.qrdata = `${environment.SITE_URL}/main/mypage/profile/${user?.ID}`;
    });
  }

}
