import { HttpClient, HttpParams } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { User, Report } from 'src/app/interfaces';
import { AuthService } from 'src/app/services';
import * as moment from 'moment-timezone';
import { NavController } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { Subscription } from 'rxjs';

interface MonthType {
  name: string;
  value: string;
}

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss'],
})
export class ReportComponent implements OnInit, OnDestroy {

  segment: 'monthly' | 'all' = 'monthly';
  curMonth = '';
  user: User | null = null;
  monthArray: MonthType[] = [];
  reports: Report[] = [];
  userSubscription: Subscription | null = null;

  constructor(
    private http: HttpClient,
    private navi: NavController,
    private auth: AuthService
  ) {
    if (this.userSubscription === null) {
      this.userSubscription = this.auth.currentUserSubject.subscribe(user => {
        if (user){
          this.user = user;
          if (user.CastStart === null){
            this.navi.navigateRoot('/main/mypage');
          }
        }
      });
    }
  }

  ngOnInit(): void {
    if (this.user){
      this.getMonthArray(
        moment(this.user.CastStart).tz('Asia/Tokyo'), moment.tz('Asia/Tokyo'));
    }
  }

  ngOnDestroy(): void {
    this.userSubscription?.unsubscribe();
  }

  getMonthArray(dateStart: moment.Moment, dateEnd: moment.Moment): void {
    while (dateEnd > dateStart || dateStart.format('M') === dateEnd.format('M')) {
      this.monthArray.push({
        name: dateStart.format('YYYY年 MM月'),
        value: dateStart.format('YYYY-MM')
      });
      dateStart.add(1, 'month');
    }
    this.monthArray.reverse();
    this.curMonth = this.monthArray[0].value;
    this.getStat();
  }

  getStat(): void {
    let params = new HttpParams()
      .append('type', this.segment);
    if (this.segment !== 'all') {
      params = params.append('month', this.curMonth);
    }

    this.http.get<Report[]>(`${environment.API_URL}/users/report`, { params }).subscribe(res => {
      this.reports = res;
      // console.log(res);
    });
  }

  monthChanged(): void {
    this.getStat();
  }

  typeChanged(): void {
    this.getStat();
  }

  getPercentVal(now: number, prev: number): string {
    if (prev < now) {
      const perVal = (now - prev) / prev * 100;
      if (perVal < 1) {
        return `(<span class="delta-up">${perVal.toFixed(2)}%🡡</span> 先月比)`;
      }else{
        return `(<span class="delta-up">${Math.floor(perVal)}%🡡</span> 先月比)`;
      }
    }else if (prev > now){
      const perVal = (prev - now) / prev * 100;
      if (perVal < 1) {
        return `(<span class="delta-down">${perVal.toFixed(2)}%🡣</span> 先月比)`;
      }else{
        return `(<span class="delta-down">${Math.floor(perVal)}%🡣</span> 先月比)`;
      }
    }else{
      return `(<span class="delta-none">±0%</span> 先月比)`;
    }
  }
}
