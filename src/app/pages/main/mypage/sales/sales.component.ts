import { HttpClient, HttpParams } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SalesData } from 'src/app/interfaces';
import { IonInfiniteScroll } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import * as moment from 'moment-timezone';

@Component({
  selector: 'app-sales',
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.scss'],
})
export class SalesComponent implements OnInit {

  @ViewChild(IonInfiniteScroll) ionInfiniteScroll!: IonInfiniteScroll;
  typeStr = 'guest';
  salesData: SalesData[] = [];
  from = '';
  to = '';
  pageIndex = 0;
  isAllLoaded = false;
  pageSize = 10;

  constructor(
    private route: ActivatedRoute,
    private http: HttpClient
  ) {
    this.typeStr = this.route.snapshot.paramMap.get('type') ?? 'guest';
  }

  ngOnInit(): void {
    this.getSalesData();
  }

  loadData(event: Event): void {
    this.pageIndex++;
    this.getSalesData(event);
  }

  getSalesData(event: Event | null = null): void {
    const filterParams = new HttpParams()
      .append('page', this.pageIndex.toString())
      .append('size', `${this.pageSize}`)
      .append('from', this.from !== '' ? moment(this.from).tz('Asia/Tokyo').format('YYYY-MM-DD') : '')
      .append('to', this.to !== '' ? moment(this.to).tz('Asia/Tokyo').format('YYYY-MM-DD') : '')
      .append('mode', this.typeStr);

    this.http.get<SalesData[]>(`${environment.API_URL}/calls/intros`, { params: filterParams }).subscribe(res => {
      if (res && res.length === 0) {
        this.isAllLoaded = true;
      }
      this.salesData = this.salesData.concat(res);
      if (event) {
        this.ionInfiniteScroll.complete();
      }
      // console.log(res);
    });
  }

  search(): void {
    this.pageIndex = 0;
    this.salesData = [];
    this.getSalesData();
  }

}
