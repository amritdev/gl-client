import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';
import { AlertController, IonInfiniteScroll } from '@ionic/angular';
import t from 'src/locales';
import { AuthService } from 'src/app/services';

@Component({
  selector: 'app-block',
  templateUrl: './block.component.html',
  styleUrls: ['./block.component.scss'],
})
export class BlockComponent implements OnInit {
  @ViewChild(IonInfiniteScroll) ionInfiniteScroll !: IonInfiniteScroll;

  blockedUsers: User[] = [];
  page = 0;
  allLoaded = false;
  size = 20;

  constructor(
    private http: HttpClient,
    private alert: AlertController,
    private auth: AuthService
  ) { }

  ngOnInit(): void {
    this.getBlockedUsers();
  }

  getBlockedUsers(event: Event | null = null): void {
    this.http.get<User[]>(`${environment.API_URL}/users/block?page=${this.page}&size=${this.size}`).subscribe(res => {
      if (res && res.length === 0) {
        this.allLoaded = true;
      }
      this.blockedUsers = this.blockedUsers.concat(res);
      if (event && event.target) {
        this.ionInfiniteScroll.complete();
      }
    });
  }

  loadData(event: Event): void {
    this.page++;
    this.getBlockedUsers(event);
  }

  async unblock(userID: number, index: number): Promise<void> {
    const confirmation = await this.warn(t('Are you sure to unblock this user?'));
    if (confirmation){
      this.http.get<boolean>(`${environment.API_URL}/users/unblock/${userID}`).subscribe(res => {
        if (res){
          this.blockedUsers.splice(index, 1);
          this.auth.reloadSubject.next(true);
        }
      });
    }
  }

  async warn(message: string, title: string = '', cssClass: string = ''): Promise<boolean> {
    return new Promise(async (resolve) => {
      const confirm = await this.alert.create({
        header: title === 'none' ? '' : (title === '' ? t('Alert Notify') : t(title)),
        message: t(message),
        cssClass: cssClass !== '' ? cssClass : 'not-special',
        buttons: [
          {
            text: t('Cancel'),
            role: 'cancel',
            handler: () => {
              return resolve(false);
            },
          },
          {
            text: t('OK'),
            handler: () => {
              return resolve(true);
            },
          },
        ],
      });

      await confirm.present();
    });
  }
}
