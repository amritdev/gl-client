import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { AlertController, IonRouterOutlet, ModalController } from '@ionic/angular';
import { AuthService } from 'src/app/services';
import { User } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';
import t from 'src/locales';
import {
  SettingEmailBooleanKeyType,
  SettingLineBooleanKeyType,
  SettingAppPushBooleanKeyType
} from 'src/app/interfaces/setting';
import { EmailComponent } from './email/email.component';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-notify',
  templateUrl: './notify.component.html',
  styleUrls: ['./notify.component.scss'],
})
export class NotifyComponent implements OnInit, OnDestroy {

  user: User | null = null;
  emailFields: SettingEmailBooleanKeyType[] = [
    'EmailAutoChargeNotify', 'EmailAutoDelayNotify',
    'EmailAutoRemoveNotify', 'EmailTweetLike',
    'EmailLike', 'EmailMessage', 'EmailJoinLeaveNotify', 'EmailAutoCallNotify',
    'EmailAdsNotify', 'EmailCallNotify'];

  lineFields: SettingLineBooleanKeyType[] = [
    'LineAutoChargeNotify', 'LineAutoDelayNotify',
    'LineAutoRemoveNotify', 'LineTweetLike',
    'LineLike', 'LineMessage', 'LineJoinLeaveNotify', 'LineAutoCallNotify',
    'LineNewGuestNotify', 'LineAdsNotify', 'LineCallNotify'];

  appPushFields: SettingAppPushBooleanKeyType[] = [
      'PushAutoChargeNotify', 'PushAutoDelayNotify',
      'PushAutoRemoveNotify', 'PushTweetLike',
      'PushLike', 'PushMessage', 'PushJoinLeaveNotify', 'PushAutoCallNotify',
      'PushAdsNotify', 'PushCallNotify'];
  pushToken = '';
  pushStatus = false;
  pushTokenSubscription: Subscription | null = null;
  pushEnabled = false;

  constructor(
    private authService: AuthService,
    private http: HttpClient,
    private alertController: AlertController,
    private modalController: ModalController,
    private routerOutlet: IonRouterOutlet
  ) {
    if (this.pushTokenSubscription === null) {
      this.pushTokenSubscription = this.authService.firebasePushToken.subscribe(token => {
        this.pushToken = token;
        if (token !== '') {
          this.pushEnabled = true;
          this.checkState();
        }
      });
    }
  }

  ngOnInit(): void {
    this.authService.currentUserSubject.subscribe(user => {
      this.user = user ? { ...user } : null;
    });
  }

  ngOnDestroy(): void {
    this.pushTokenSubscription?.unsubscribe();
  }

  checkState(): void {
    const params = new HttpParams().
      append('token', this.pushToken);
    this.http.get<boolean>(`${environment.API_URL}/users/token/status`, {params}).subscribe(res => {
      this.pushStatus = res;
    }, (err: HttpErrorResponse) => {
      if (err.status === 400) {
        this.pushEnabled = false;
      }
    });
  }

  statusChanged(status: boolean): void {
    this.http.put<boolean>(`${environment.API_URL}/users/token/status`, {
      token: this.pushToken,
      status
    }).subscribe(_ => {
      this.pushStatus = status;
    });
  }

  setState(): void {
    const params = new HttpParams().
      append('token', this.pushToken);
    this.http.put(`${environment.API_URL}/users/token/status`, {params}).subscribe();
  }

  onChange(): void {
    const data = this.user?.Setting ?? null;
    this.http.put<{}>(`${environment.API_URL}/users/setting`, data).subscribe(() => {
      this.authService.currentUserSubject.next(this.user);
    }, async () => {
      const alert = await this.alertController.create({
        message: t('Operation Failed'),
        buttons: [t('OK')],
      });
      await alert.present();
    });
  }

  isEmailAll(): boolean {
    if (this.user && this.user.Setting) {
      return this.user.Setting.EmailLike && this.user.Setting.EmailMessage && this.user.Setting.EmailTweetLike &&
        this.user.Setting.EmailAdsNotify && this.user.Setting.EmailCallNotify;
    } else {
      return false;
    }
  }

  isLineAll(): boolean {
    if (this.user && this.user.Setting) {
      return this.user.Setting.LineLike && this.user.Setting.LineMessage && this.user.Setting.LineTweetLike &&
        this.user.Setting.LineAdsNotify && this.user.Setting.LineCallNotify;
    } else {
      return false;
    }
  }

  isAppPushAll(): boolean {
    if (this.user && this.user.Setting) {
      return this.user.Setting.PushLike && this.user.Setting.PushMessage && this.user.Setting.PushTweetLike &&
        this.user.Setting.PushAdsNotify && this.user.Setting.PushCallNotify;
    } else {
      return false;
    }
  }

  lineAnyTrue(): boolean{
    if (this.user && this.user.Setting){
      return this.user.Setting.LineLike || this.user.Setting.LineMessage || this.user.Setting.LineTweetLike ||
        this.user.Setting.LineAdsNotify || this.user.Setting.LineCallNotify;
    }else{
      return false;
    }

  }

  pushAnyTrue(): boolean{
    if (this.user && this.user.Setting){
      return this.user.Setting.PushLike || this.user.Setting.PushMessage || this.user.Setting.PushTweetLike ||
        this.user.Setting.PushAdsNotify || this.user.Setting.PushCallNotify;
    }else{
      return false;
    }

  }

  emailAnyTrue(): boolean{
    if (this.user && this.user.Setting){
      return this.user.Setting.EmailLike || this.user.Setting.EmailMessage || this.user.Setting.EmailTweetLike ||
        this.user.Setting.EmailAdsNotify || this.user.Setting.EmailCallNotify;
    }else{
      return false;
    }

  }

  onAllChange(isTrue: boolean, mode: string): void {
    // console.log(isTrue);
    if (mode === 'email') {
      this.emailFields.forEach(item => {
        if (this.user && this.user.Setting) {
          this.user.Setting[item] = isTrue;
        }
      });
    } else if (mode === 'line') {
      this.lineFields.forEach(item => {
        if (this.user && this.user.Setting) {
          this.user.Setting[item] = isTrue;
        }
      });
    } else if (mode === 'push') {
      this.appPushFields.forEach(item => {
        if (this.user && this.user.Setting) {
          this.user.Setting[item] = isTrue;
        }
      });
    }

    this.onChange();
  }

  async openEmailModal(): Promise<void> {
    const modal = await this.modalController.create({
      component: EmailComponent,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        email: this.user?.Email ?? null,
      },
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
    if (this.user && data) {
      this.user.Email = data;
      this.authService.currentUserSubject.next(this.user);
    }
  }
}
