import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { User } from 'src/app/interfaces';
import { AuthService } from 'src/app/services';
import t from 'src/locales';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-private',
  templateUrl: './private.component.html',
  styleUrls: ['./private.component.scss'],
})
export class PrivateComponent implements OnInit {

  rankingVisible = true;


  constructor(
    private auth: AuthService,
    private http: HttpClient,
    private alert: AlertController
  ) {
    this.auth.currentUserSubject.subscribe(user => {
      if (user && user.Setting){
        this.rankingVisible = user.Setting.RankingVisible;
      }
    });
  }

  ngOnInit(): void {}

  async showAlert(msgStr: string): Promise<void> {
    const alert = await this.alert.create({
      message: msgStr,
      buttons: ['OK']
    });
    await alert.present();
  }

  onChange(): void {
    this.http.put<User>(`${environment.API_URL}/users/setting/private`, {
      ranking: this.rankingVisible ? 'true' : 'false'
    }).subscribe(res => {
      this.auth.currentUserSubject.next(res);
    }, async () => {
      this.showAlert(t('Operation Failed'));
    });
  }

}
