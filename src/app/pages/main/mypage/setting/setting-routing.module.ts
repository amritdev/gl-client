import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BlockComponent } from './block/block.component';
import { NotifyComponent } from './notify/notify.component';
import { PrivateComponent } from './private/private.component';
import { SettingComponent } from './setting.component';

const routes: Routes = [
  {
    path: '',
    component: SettingComponent
  },
  {
    path: 'notify',
    component: NotifyComponent
  },
  {
    path: 'private',
    component: PrivateComponent
  },
  {
    path: 'block',
    component: BlockComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingRoutingModule { }
