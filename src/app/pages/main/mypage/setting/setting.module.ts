import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { PipesModule } from 'src/app/shared';
import { SettingRoutingModule } from './setting-routing.module';
import { NotifyComponent } from './notify/notify.component';
import { EmailComponent } from './notify/email/email.component';
import { SettingComponent } from './setting.component';
import { PrivateComponent } from './private/private.component';
import { BlockComponent } from './block/block.component';

@NgModule({
  declarations: [
    NotifyComponent,
    EmailComponent,
    SettingComponent,
    PrivateComponent,
    BlockComponent
  ],
  imports: [
    CommonModule,
    SettingRoutingModule,
    PipesModule,
    FormsModule,
    IonicModule
  ]
})
export class SettingModule { }
