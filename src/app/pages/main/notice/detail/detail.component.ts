import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Notification } from 'src/app/interfaces';
import * as moment from 'moment';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
})
export class DetailComponent implements OnInit {
  title = '';
  content = '';
  category = '';
  createdAt = '';
  daysOfWeek = ['日', '月', '火', '水', '木', '金', '土'];
  noticeID = 0;

  constructor(
    private route: ActivatedRoute,
    private http: HttpClient
  ) {
    const idParam = this.route.snapshot.paramMap.get('id');

    if (idParam) {
      this.noticeID = parseInt(idParam, 10);
      this.getNoticeData();
    }
  }

  ngOnInit(): void {}

  getNoticeData(): void {
    this.http.get<Notification>(`${environment.API_URL}/notifications/detail/${this.noticeID}`).subscribe(res => {
      this.title = res.Title;
      this.content = res.Content;
      this.category = res.Category?.Name ?? '';
      this.createdAt = res.CreatedAt ?? '';
    });
  }

  getDateWeekday(): string {
    moment.locale('jp');
    const createdDate = moment.tz(this.createdAt, 'Asia/Tokyo');
    return `${createdDate.format('YYYY年MM月DD日')}（${this.daysOfWeek[createdDate.day()]}）${createdDate.format('HH:mm')}`;
  }
}
