import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AlertController, IonInfiniteScroll, NavController, IonBackButtonDelegate, ModalController, IonRouterOutlet } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { AuthService, ConditionService, WebsocketService } from 'src/app/services';
import { Notice, Room, User, Message, Notification } from 'src/app/interfaces';
import * as moment from 'moment';
import { environment } from 'src/environments/environment';
import t from 'src/locales';
import { DetailComponent } from './detail/detail.component';

@Component({
  selector: 'app-notice',
  templateUrl: './notice.component.html',
  styleUrls: ['./notice.component.scss'],
})
export class NoticeComponent implements OnInit, OnDestroy {

  @ViewChild(IonInfiniteScroll) ionInfScrollFoot !: IonInfiniteScroll;
  @ViewChild(IonInfiniteScroll) ionInfScrollAdmin !: IonInfiniteScroll;
  @ViewChild(IonInfiniteScroll) ionInfScrollNotify !: IonInfiniteScroll;
  @ViewChild(IonBackButtonDelegate, { static: false }) backButton !: IonBackButtonDelegate;

  self: User | null = null;
  mode = 'foot';
  role = true;
  favorites: number[] = [];
  daysOfWeek = ['日', '月', '火', '水', '木', '金', '土'];

  allLoaded: { [key: string]: boolean } = {
    foot: false,
    admin: false,
    notify: false
  };

  pageIndex: { [key: string]: number } = {
    foot: 0,
    admin: 0,
    notify: 0
  };

  notices: { [key: string]: Notice[] } = {
    foot: [],
    admin: []
  };
  notifications: Notification[] = [];

  offset: { [key: string]: number } = {
    foot: 0,
    admin: 0,
    notify: 0
  };

  // modal variable
  detailModal: HTMLIonModalElement | null = null;

  noticeSubscription: Subscription | null = null;
  notifSubscription: Subscription | null = null;
  reloadSubscription: Subscription | null = null;

  constructor(
    private auth: AuthService,
    private http: HttpClient,
    private alert: AlertController,
    private webSocket: WebsocketService,
    private modal: ModalController,
    public cond: ConditionService,
    private navi: NavController,
    private routerOutlet: IonRouterOutlet,
  ) {
    this.auth.currentUserSubject.subscribe((user) => {
      if (user) {
        this.self = user;
        this.role = user.Role === 1 ? true : false;
        this.getFavorites();
      }
    });
  }

  ngOnInit(): void {
    this.refresh('foot');
    this.refresh('admin');
    this.refresh('notify');
    this.webSocket.unreadNotices = this.webSocket.adminNotices;
  }

  ngOnDestroy(): void {
    this.noticeSubscription?.unsubscribe();
    this.reloadSubscription?.unsubscribe();
    this.notifSubscription?.unsubscribe();
  }

  getFavorites(): void {
    this.http.get<User[]>(`${environment.API_URL}/users/favorite/all`).subscribe(res => {
      this.favorites = res.map(item => item.ID);
      // console.log(this.favorites);
    });
  }

  ionViewDidEnter(): void {
    this.setBackButtonAction();
  }

  setBackButtonAction(): void {
    this.backButton.onClick = () => {
      this.navi.back();
    };
  }

  getFromAdmin(): number {
    return this.webSocket.adminNotices;
  }

  getFromNotify(): number {
    return this.webSocket.notifyNotices;
  }

  refresh(mode: string): void {
    this.pageIndex[mode] = 0;
    this.offset[mode] = 0;
    this.notices[mode] = [];
    this.allLoaded[mode] = false;
    this.getNotices(null, mode);
  }

  getNotices(event: Event | null, mode: string): void {
    if (mode === 'foot' || mode === 'admin') {
      this.http.get<Notice[]>(`${environment.API_URL}/notices?mode=${mode}&page=${this.pageIndex[mode]}&offset=${this.offset[mode]}`)
      .subscribe(async (res) => {
        if (res.length < 20) {
          this.allLoaded[mode] = true;
        }
        this.notices[mode] = [...this.notices[mode], ...res];
        // console.log(this.notices);

        if (event && event.target) {
          if (mode === 'foot') {
            this.ionInfScrollFoot.complete();
          } else {
            this.ionInfScrollAdmin.complete();
          }
        }

        if (event === null) {
          // start reload subscription
          if (mode === 'foot' && this.reloadSubscription === null){
            this.reloadSubscription = this.webSocket.reloadSubject.subscribe(eventStr => {
              if (eventStr === 'notice'){
                this.refresh('foot');
              }
            });
          }

          // start notice subscription
          if (this.noticeSubscription === null) {
            this.noticeSubscription = this.webSocket.noticeSubject.subscribe((newNotice) => {
              if (newNotice) {
                // count calculation
                if (this.mode === 'admin') {
                  this.webSocket.adminNotices = 0;
                }
                this.webSocket.unreadNotices--;

                // deal with notices
                if (newNotice.Type === 'foot' || newNotice.Type === 'tweet') {
                  const index = this.notices.foot.findIndex(item => item.ID === newNotice.ID);
                  if (index > -1) {
                    this.notices.foot.splice(index, 1);
                    this.notices.foot.unshift(newNotice);
                  } else {
                    this.notices.foot.unshift(newNotice);
                    this.offset.foot++;
                  }
                }

                if (newNotice.Type === 'admin'){
                  this.notices.admin.unshift(newNotice);
                  this.offset.admin++;
                }
              }
            });
          }
        }
      });
    }else if (mode === 'notify') {
      this.http.get<Notification[]>(`${environment.API_URL}/notifications?page=${this.pageIndex[mode]}&offset=${this.offset[mode]}`)
      .subscribe(async (res) => {
        if (res.length < 20) {
          this.allLoaded[mode] = true;
        }
        this.notifications = [...this.notifications, ...res];
        // console.log(this.notifications);

        if (event && event.target) {
          this.ionInfScrollNotify.complete();
        }

        if (event === null) {
          if (this.notifSubscription === null) {
            this.notifSubscription = this.webSocket.notificationSubject.subscribe(newNotif => {
              if (newNotif) {
                if (this.mode === 'notify'){
                  this.webSocket.notifyNotices = 0;
                  this.webSocket.unreadNotices--;
                }

                const index = this.notifications.findIndex(item => item.ID === newNotif.ID);
                if (index > -1) {
                  this.notifications.splice(index, 1);
                  this.notifications.unshift(newNotif);
                } else {
                  this.notifications.unshift(newNotif);
                  this.offset.notify++;
                }
              }
            });
          }
        }
      });
    }
  }

  getNoticeAbout(noticeItem: Notice): string {
    let returnStr = '';
    if (noticeItem.FromUser) {
      returnStr = noticeItem.FromUser.About ?
        (noticeItem.FromUser.About.length > 70 ? noticeItem.FromUser.About.slice(0, 70) + '...' : noticeItem.FromUser.About) :
        t('Cast Standard Greeting');
    }
    return returnStr;
  }

  getAdminImage(admin: User): string {
    if (admin.Images && admin.Images.length > 0) {
      return admin.Images[0].Path;
    } else {
      return 'assets/img/admin.png';
    }
  }

  segmentChanged(): void {
    if (this.mode === 'admin') {
      this.webSocket.unreadNotices -= this.webSocket.adminNotices;
      this.webSocket.adminNotices = 0;
    }else if (this.mode === 'notify') {
      this.webSocket.unreadNotices -= this.webSocket.notifyNotices;
      this.webSocket.notifyNotices = 0;
    }
  }

  loadData(mode: string, event: Event | null): void {
    this.pageIndex[mode]++;
    this.getNotices(event, mode);
  }

  isFavoriteUser(id: number): boolean {
    return this.favorites.includes(id);
  }

  onFavorite(event: Event, id: number): void {
    event.stopPropagation();
    event.preventDefault();

    this.http.post<Room>(`${environment.API_URL}/users/message/like`, {
      partner_id: id
    }).subscribe((room) => {
      if (this.self){
        const messages: Message[] = [];
        messages.push({
          Content: 'いいね ♡',
          IsRead: true,
          RoomID: room.ID,
          SenderID: this.self.ID,
          ReceiverID: this.self.ID,
        });
        messages.push({
          Content: 'いいね ♡',
          RoomID: room.ID,
          SenderID: this.self.ID,
          ReceiverID: id,
        });
        this.http.post<{}>(`${environment.API_URL}/users/message`, messages).subscribe();
        this.http.post<{}>(`${environment.API_URL}/nsq`, {
          message: JSON.stringify({
            Type: 'MESSAGE',
            SenderID: this.self.ID,
            ReceiverIDs: [id],
            Data: {
              Content: 'いいね ♡',
              ImageID: null,
              Image: null,
              GiftID: null,
              Gift: null,
              RoomID: room.ID,
              CreatedAt: moment().format(),
            },
          }),
        }).subscribe();

        // update user
        this.http.get<User[]>(`${environment.API_URL}/users/favorite`).subscribe(res => {
          if (this.self){
            this.self.Favorites = JSON.stringify(res.map(item => item.ID));
            this.auth.currentUserSubject.next(this.self);
          }
        });
      }
    });
  }

  onSend(event: Event, notice: Notice): void {
    event.stopPropagation();
    event.preventDefault();
    this.http.get<Room[]>(`${environment.API_URL}/users/message/like/${notice.FromUserID}`).subscribe(rooms => {
      if (rooms.length > 0) {
        this.navi.navigateForward(`/main/chat/detail/${rooms[0].ID}`);
      } else {
        return;
      }
    });
  }

  getDateWeekday(createdAt: string | undefined): string {
    if (createdAt) {
      moment.locale('jp');
      const createdDate = moment.tz(createdAt, 'Asia/Tokyo');
      return `${createdDate.format('MM月DD日')}（${this.daysOfWeek[createdDate.day()]}）`;
    }else{
      return '';
    }
  }

  getDateString(createdAt: string | undefined): string {
    if (createdAt){
      return moment.tz(createdAt, 'Asia/Tokyo').format('YYYY-MM-DD');
    }else{
      return '';
    }
  }
}
