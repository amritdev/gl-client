import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { formatDate, Location } from '@angular/common';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalController, IonSlides, NavController, AlertController, PopoverController } from '@ionic/angular';
import * as moment from 'moment';
import { User, Detail, Tag, Room, Tweet, Message, Gift, Class, UserGift, Video, Image } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';
import { AuthService } from 'src/app/services';
import { Platform } from '@ionic/angular';
import t from 'src/locales';

import { PopoverComponent } from './popover/popover.component';

interface DetailWithValue extends Detail {
  Value?: string;
}

interface GiftHistory {
  ID: number;
  Count: number;
}
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {

  // @ViewChild('slides') slides!: IonSlides;
  @ViewChild('slides') thumbSlides !: IonSlides;
  @ViewChild('profileVideo') profileVideo !: ElementRef;

  profileStringId = '';
  profileId = 0;
  image = environment.SITE_URL + '/assets/cast.png';
  self: User | null = null;
  favorites: number[] = [];
  user: User | null = null;
  tweets: Tweet[] = [];
  blockedTweetIDs: string | null = null;
  blockedTweetUserIDs: string | null = null;
  navigationID = 1;
  details: DetailWithValue[] = [];
  tags: string[] = [];
  muted = true;
  // activatedSlide = 0;
  thumbSlideOpts = {
    spaceBetween: 10,
    slidesPerView: 4.5,
  };
  headerOpacity = 0;
  daysOfWeek: string[] = ['日', '月', '火', '水', '木', '金', '土'];
  calendar: Date[][] = [[], []];
  scheduleByKey: {
    [key: string]: {
      enabled: boolean;
      from: string | null;
      to: string | null;
      midnight: boolean;
    };
  } = {};
  selectedWeek = 0;
  roomID = 0;
  gifts: Gift[] = [];
  userGifts: UserGift[] = [];

  // for displaying levels
  guestLevels: Class[] = [];
  levelsReady = false;
  selectedImage = '';
  selectedVideo = '';
  isIOS = false;

  // footer enabled
  enabled = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private http: HttpClient,
    private alert: AlertController,
    private location: Location,
    private navController: NavController,
    private modalController: ModalController,
    private alertController: AlertController,
    private popoverController: PopoverController,
    private platform: Platform
  ) {
    this.isIOS = this.platform.is('ios') && !this.platform.is('mobileweb');

    const idParam = this.route.snapshot.paramMap.get('id');
    if (idParam) {
      this.profileStringId = idParam;
    }

    this.getGifts();

    this.platform.backButton.subscribeWithPriority(10, (_) => {
      this.onBack();
    });
  }

  ngOnInit(): void {
    this.enabled = this.authService.enabled;

    for (let i = 0; i < 14; i++) {
      const tmpDate = new Date();
      tmpDate.setDate(tmpDate.getDate() + i);
      if (i < 7) {
        this.calendar[0].push(tmpDate);
      } else {
        this.calendar[1].push(tmpDate);
      }
      this.scheduleByKey[this.getDateKey(tmpDate)] = {
        enabled: false,
        from: '',
        to: '',
        midnight: false
      };
    }
    const navigation = this.router.getCurrentNavigation();
    this.navigationID = navigation?.id ?? 1;

    if (this.profileStringId !== '') {
      this.http.get<number>(`${environment.API_URL}/users/profile/${this.profileStringId}`).subscribe(id => {
        this.profileId = id;
        this.getUserInfo(this.profileId);
        this.getPrivateRoom(this.profileId);
        this.getRecentTweets(this.profileId);
      });
    }

    this.authService.currentUserSubject.subscribe(user => {
      this.self = user ? { ...user } : null;
      this.blockedTweetIDs = user?.BlockedTweetIDs ?? null;
      this.blockedTweetUserIDs = user?.BlockedTweetUserIDs ?? null;
      if (user){
        this.getFavorites();
      }
    });
  }

  getGifts(): void {
    this.http.get<Gift[]>(`${environment.API_URL}/gifts`).subscribe(gifts => {
      this.gifts = gifts;
    });
  }

  getFavorites(): void {
    this.http.get<User[]>(`${environment.API_URL}/users/favorite/all`).subscribe(res => {
      this.favorites = res.map(item => item.ID);
      // console.log(this.favorites);
    });
  }

  async presentAlert(msg: string): Promise<void> {
    const alert = await this.alert.create({
      message: msg,
      buttons: [t('OK')],
    });
    await alert.present();
  }

  getUserInfo(userID: number): void {
    this.http.get<User>(`${environment.API_URL}/users/info/${userID}`).subscribe(user => {
      this.userGifts = [];
      this.user = user;

      // if profile user is withdrawn
      if (this.user.Withdrawn || this.user.IsBlocked) {
        this.authService.reloadSubject.next(true);
        const alertMessage = t('This user is already withdrawn');
        this.location.back();
        this.presentAlert(alertMessage);
      }

      if (this.user.Images.length > 0){
        this.image = this.user.Images[0].Path;
        this.setDetailImage(this.user.Images[0]);
      }

      if (this.user.Role === 1) {
        this.getGuestLevels();
      }

      if (this.user.Role === 0 && this.user.GiftReceived !== null) {
        const giftHistory = JSON.parse(this.user.GiftReceived) as GiftHistory[];
        giftHistory.forEach(item => {
          const giftIndex = this.gifts.findIndex(giftItem => giftItem.ID === item.ID);
          if (giftIndex > -1){
            this.userGifts.push({
              Gift: this.gifts[giftIndex],
              Count: item.Count
            });
          }
        });
        // console.log(this.userGifts);
      }

      if (this.user.Role < 0) {
        this.router.navigate(['/main']);
      }

      user?.Schedules?.forEach(item => {
        if (this.scheduleByKey[item.Date]) {
          this.scheduleByKey[item.Date] = {
            enabled: item.Enabled,
            from: item.From,
            to: item.To,
            midnight: item.Midnight,
          };
        }
      });

      if (user) {
        this.getParentTags();
        this.getAllDetails();
      }
    });
  }

  setDetailImage(curImage: Image): void {
    let imageKey = '';
    if (curImage.Path.includes('/assets/img/')){
      this.selectedImage = curImage.Path;
    }else{
      // get image key
      if (curImage.Key === null){
        const urlPath = curImage.Path.split('/');
        if (urlPath.length > 0) {
          imageKey = urlPath.pop() ?? '';
        }
      }else{
        imageKey = curImage.Key;
      }
      // check image key
      const params = new HttpParams().append('key', imageKey);
      this.http.get<boolean>(`${environment.API_URL}/users/image/check`, {params}).subscribe(res => {
        if (res){
          this.selectedImage = curImage.Path.replace(imageKey, imageKey + '-1500');
        }else{
          this.selectedImage = curImage.Path;
        }
      });
    }
  }

  getGuestLevels(): void {
    this.http.get<Class[]>(`${environment.API_URL}/admin/levels/guest`).subscribe(res => {
      this.guestLevels = res;
      this.levelsReady = true;
    });
  }

  getLevelName(usedPoint: number): string {
    if (this.guestLevels.length > 0) {
      for (const levelItem of this.guestLevels) {
        if (usedPoint < levelItem.Point) {
          return levelItem.Name;
        } else {
          continue;
        }
      }
      return this.guestLevels[this.guestLevels.length - 1].Name;
    } else {
      return t('Undefined');
    }
  }

  getLevelNum(usedPoint: number): number {
    if (this.guestLevels.length > 0) {
      for (const [index, levelItem] of this.guestLevels.entries()) {
        if (usedPoint < levelItem.Point) {
          return index;
        } else {
          continue;
        }
      }
      return this.guestLevels.length - 1;
    } else {
      return 0;
    }
  }

  getArray(length: number): Array<unknown> {
    return new Array(length);
  }

  getPrivateRoom(userID: number): void {
    this.http.get<Room[]>(`${environment.API_URL}/users/message/like/${userID}`).subscribe(rooms => {
      this.roomID = rooms.length > 0 ? rooms[0].ID : 0;
    });
  }

  getRecentTweets(userID: number): void {
    this.http.get<Tweet[]>(`${environment.API_URL}/users/tweet/recent/${userID}`).subscribe(tweets => {
      this.tweets = tweets;
    });
  }

  getDateKey(datetime: Date): string {
    return formatDate(datetime, 'yyyy-MM-dd', 'en', 'Asia/Tokyo');
  }

  getMidnightStatus(datetime: Date): string {
    if (this.scheduleByKey[this.getDateKey(datetime)] && this.scheduleByKey[this.getDateKey(datetime)].enabled){
      if (this.scheduleByKey[this.getDateKey(datetime)].midnight) {
        return 'radio-button-on';
      } else {
        return 'radio-button-off';
      }
    } else {
      return 'remove';
    }
  }

  getParentTags(): void {
    this.http.get<Tag[]>(`${environment.API_URL}/tags`).subscribe(tags => {
      tags.forEach(tag => {
        if (this.user?.Tags?.includes(',' + tag.ID.toString() + ',')) {
          this.tags.push(tag.Name);
        }
      });
    });
  }

  getAllDetails(): void {
    this.http.get<Detail[]>(`${environment.API_URL}/details`).subscribe(details => {
      const parentDetails: DetailWithValue[] = details.filter(d => d.ParentID === 0);
      const detailArr = this.user?.Detail?.split(',').filter(item => item !== '') ?? [];
      detailArr.forEach(detailID => {
        const detail = details.find(item => item.ID.toString() === detailID);
        if (detail) {
          const index = parentDetails.findIndex(item => item.ID === detail.ParentID);
          if (index > -1) {
            parentDetails[index].Value = detail.Name;
          }
        }
      });
      this.details = [...parentDetails];
    });
  }

  getHeartCount(tweet: Tweet): number {
    return tweet.FavoriteUserIDs ? JSON.parse(tweet.FavoriteUserIDs).length + tweet.Bonus : tweet.Bonus;
  }

  getHeartStatus(tweet: Tweet): boolean {
    return tweet.FavoriteUserIDs ? JSON.parse(tweet.FavoriteUserIDs).includes(this.self?.ID) : 0;
  }

  isFavoriteUser(): boolean {
    if (this.self && this.user) {
      if (this.favorites.includes(this.user?.ID ?? 0)) {
        return true;
      }else {
        const userFavorites = JSON.parse(this.user.Favorites ?? '[]');
        return userFavorites.includes(this.self.ID);
      }
    }
    return false;
  }

  isValidTweet(tweet: Tweet): boolean {
    const blocketTweetIDsArr = this.blockedTweetIDs ? JSON.parse(this.blockedTweetIDs) : [];
    const blockedTweetUserIDsArr = this.blockedTweetUserIDs ? JSON.parse(this.blockedTweetUserIDs) : [];
    if (blocketTweetIDsArr.includes(tweet.ID)) {
      return false;
    }
    if (blockedTweetUserIDsArr.includes(tweet.UserID)) {
      return false;
    }
    return true;
  }

  onToggleHeart(tweet: Tweet, index: number): void {
    const favoriteUserIDsArr = tweet.FavoriteUserIDs ? JSON.parse(tweet.FavoriteUserIDs) : [];
    let isFavorite = false;
    if (this.getHeartStatus(tweet)) {
      const favoriteUserIDsArrIndex = favoriteUserIDsArr.findIndex((id: number) => id === this.self?.ID);
      favoriteUserIDsArr.splice(favoriteUserIDsArrIndex, 1);
      isFavorite = false;
    } else {
      favoriteUserIDsArr.push(this.self?.ID);
      isFavorite = true;
    }
    this.tweets[index].FavoriteUserIDs = JSON.stringify(favoriteUserIDsArr);
    this.http.put<{}>(`${environment.API_URL}/tweets/favorite`, {
      tweet_id: tweet.ID,
      favorite_user_ids: JSON.stringify(favoriteUserIDsArr),
    }).subscribe();
  }

  async onPopover(event: Event, datetime: Date): Promise<void> {
    const from = this.scheduleByKey[this.getDateKey(datetime)].from;
    const to = this.scheduleByKey[this.getDateKey(datetime)].to;
    if (from && to) {
      const popover = await this.popoverController.create({
        component: PopoverComponent,
        componentProps: {
          text: formatDate(from, 'HH:mm', 'en', 'Asia/Tokyo') + ' ～ ' + formatDate(to, 'HH:mm', 'en', 'Asia/Tokyo')
        },
        event,
      });
      await popover.present();
    }
  }

  onScroll(event: Event): void {
    if (event instanceof CustomEvent) {
      this.headerOpacity = (event.detail.scrollTop > 50) ? 1 : 0;
    }
  }

  // onSlideChange(): void {
  //   this.slides.getActiveIndex().then(index => {
  //     this.activatedSlide = index;
  //   });
  // }

  onClose(): void {
    this.modalController.dismiss();
  }

  onBack(): void {
    if (this.navigationID < 2) {
      this.navController.navigateBack('/mypage');
    } else {
      console.log('BACK BACK');
      this.navController.back();
    }
  }

  onFavorite(): void {
    this.http.post<Room>(`${environment.API_URL}/users/message/like`, {
      partner_id: this.user?.ID,
    }).subscribe(room => {
      if (this.self) {
        this.roomID = room.ID;
        const messages: Message[] = [];
        messages.push({
          Content: 'いいね ♡',
          IsRead: true,
          RoomID: room.ID,
          SenderID: this.self.ID,
          ReceiverID: this.self.ID,
        });
        messages.push({
          Content: 'いいね ♡',
          RoomID: room.ID,
          SenderID: this.self.ID,
          ReceiverID: this.user?.ID,
        });
        this.http.post<{}>(`${environment.API_URL}/users/message`, messages).subscribe();
        this.http.post<{}>(`${environment.API_URL}/nsq`, {
          message: JSON.stringify({
            Type: 'MESSAGE',
            SenderID: this.self.ID,
            ReceiverIDs: [this.user?.ID],
            Data: {
              Content: 'いいね ♡',
              ImageID: null,
              Image: null,
              GiftID: null,
              Gift: null,
              RoomID: room.ID,
              CreatedAt: moment().format(),
            },
          }),
        }).subscribe();

        // update user
        this.http.get<User[]>(`${environment.API_URL}/users/favorite`).subscribe(res => {
          if (this.self){
            this.self.Favorites = JSON.stringify(res.map(item => item.ID));
            this.authService.currentUserSubject.next(this.self);
          }
        });
      }
    }, async () => {
      const alert = await this.alertController.create({
        message: t('Operation Failed'),
        buttons: [t('OK')],
      });
      await alert.present();
    });
  }

  getStatusClass(): string {
    if (this.user) {
      if (this.user.Status) {
        return 'green';
      } else {
        const curTime = Date.now();
        const leftAt = new Date(this.user.LeftAt ?? '').getTime();
        const seconds = (curTime - leftAt) / 1000;
        if (seconds < 3600) {
          return 'green';
        } else if (seconds < 3600 * 24) {
          return 'yellow';
        } else if (seconds < 3600 * 24 * 3) {
          return 'gray';
        } else {
          return '';
        }
      }
    } else {
      return '';
    }
  }

  videoSelected(video: Video): void {
    this.selectedVideo = video.Path;
    this.selectedImage = '';

    // if exists then remove
    const curProfileVideo = document.querySelector('.avatar-slide .profile-video');
    if (curProfileVideo){
      curProfileVideo.remove();
    }

    this.muted = true;

    // create video tag
    const videoElement: HTMLVideoElement = document.createElement('video');
    const avatarSlide = document.querySelector('.avatar-slide');
    videoElement.loop = true;
    videoElement.muted = true;
    videoElement.autoplay = true;
    videoElement.playsInline = true;
    videoElement.poster = this.user ? this.user.Images[0].Path : 'assets/img/cast.png';
    videoElement.className = 'profile-video';
    videoElement.style.width = '100%';
    videoElement.style.height = '500px';
    videoElement.style.objectFit = 'cover';
    videoElement.style.alignSelf = 'center';
    if (avatarSlide){
      avatarSlide.appendChild(videoElement);
      const source: HTMLSourceElement = document.createElement('source');
      source.src = video.Path;
      source.type = 'video/mp4';
      videoElement.appendChild(source);
    }
  }

  imageSelected(image: Image): void{
    this.setDetailImage(image);
    this.selectedVideo = '';

    // if exists then remove
    const curProfileVideo = document.querySelector('.avatar-slide .profile-video');
    if (curProfileVideo){
      curProfileVideo.remove();
    }
  }

  controlSound(): void{
    this.muted = !this.muted;
    const curProfileVideo = document.querySelector('.avatar-slide .profile-video') as HTMLVideoElement;
    if (curProfileVideo) {
      curProfileVideo.muted = this.muted;
    }
  }

  controlPlayStatus(index: number, event: Event): void {
    event.preventDefault();
    event.stopPropagation();

    const curProfileVideo: HTMLVideoElement = document.querySelector(`.slide-video #profile-video-${index}`) as HTMLVideoElement;
    if (curProfileVideo.paused){
      curProfileVideo.play();
    }else{
      curProfileVideo.pause();
    }
  }
}
