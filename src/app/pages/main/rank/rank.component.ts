import { Component, OnDestroy, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { ConditionService } from 'src/app/services';
import { Location, User } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-rank',
  templateUrl: './rank.component.html',
  styleUrls: ['./rank.component.scss'],
})
export class RankComponent implements OnInit, OnDestroy {

  role = 1;

  // cast or guest
  isCast = 'true';

  // mode
  mode: { [key: string]: number } = {
    casts: 0,
    guests: 0
  };
  period: { [key: string]: number } = {
    casts: 4,
    guests: 4
  };
  rankingList: { [key: string]: User[] } = {
    casts: [],
    guests: []
  };

  // locations
  rankLocation = 0;
  locationReady = true;
  parentLocations: Location[] = [];
  userSubscription: Subscription | null = null;

  constructor(
    public callCond: ConditionService,
    private http: HttpClient
  ) {
    if (this.userSubscription === null) {
      // this.userSubscription = this.auth.currentUserSubject.subscribe(user => {
      //   if (user) {
      //     this.role = user.Role;
      //     if (this.role === 0) {
      //       this.isCast = 'false';
      //     }
      //     this.getParentLocations();
      //   }
      // });
    }
  }

  ngOnInit(): void {
    this.getParentLocations();
  }

  ngOnDestroy(): void {
    this.userSubscription?.unsubscribe();
  }

  initialize(): void {
    this.rankingList.casts = [];
    this.rankingList.guests = [];
    this.getRankingResult('casts');
    this.getRankingResult('guests');
  }

  getParentLocations(): void {
    this.http.get<Location[]>(`${environment.API_URL}/locations/rank?pid=0`).subscribe(
      res => {
        if (res.length > 0) {
          // this.parentLocations = res;
          this.parentLocations = [];
          this.locationReady = true;
          this.initialize();
        }
      }, (err: HttpErrorResponse) => {
        console.log(err);
      });
  }

  changed(isCast: number): void {
    if (isCast === 1) {
      this.rankingList.casts = [];
    } else {
      this.rankingList.guests = [];
    }
    this.getRankingResult(isCast === 1 ? 'casts' : 'guests');
  }

  getRankingResult(part: string): void {
    this.http.get<User[]>(
      `${environment.API_URL}/${part}/rank?mode=${this.mode[part]}&period=${this.period[part]}&location=${this.rankLocation}`
    ).subscribe((data) => {
      // console.log(data);
      this.rankingList[part] = data;
    });
  }

  locationChanged(): void {
    this.getRankingResult(this.isCast === 'true' ? 'casts' : 'guests');
  }
}
