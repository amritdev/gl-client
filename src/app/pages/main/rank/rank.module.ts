import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PipesModule } from 'src/app/shared';

import { RankRoutingModule } from './rank-routing.module';
import { RankComponent } from './rank.component';

@NgModule({
  declarations: [
    RankComponent,
  ],
  imports: [
    CommonModule,
    RankRoutingModule,
    IonicModule,
    PipesModule,
    FormsModule,
  ],
})
export class RankModule { }
