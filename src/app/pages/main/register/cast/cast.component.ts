import { Component, OnInit, Input } from '@angular/core';
import { ModalController, AlertController } from '@ionic/angular';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { User } from 'src/app/interfaces';
import t from 'src/locales';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-cast',
  templateUrl: './cast.component.html',
  styleUrls: ['./cast.component.scss'],
})
export class CastComponent implements OnInit {

  @Input() favoriteCasts: User[] = [];

  casts: User[] = [];
  isFullCastsReady = false;

  constructor(
    private modal: ModalController,
    private http: HttpClient,
    private alert: AlertController
  ) { }

  ngOnInit(): void {
    this.getCasts();
  }

  getCasts(): void{
    this.http.get<User[]>(`${environment.API_URL}/casts`).subscribe((casts) => {
      this.casts = casts;
      // console.log(casts);
      this.isFullCastsReady = true;
    }, (err: HttpErrorResponse) => {
      console.log(err);
      this.showAlert('Get Casts failed');
    });
  }

  onSave(): void {
    this.modal.dismiss(this.favoriteCasts);
  }

  onClose(): void {
    this.modal.dismiss();
  }

  async showAlert(msgStr: string): Promise<void> {
    const alert = await this.alert.create({
        message: msgStr,
        buttons: ['OK']
    });
    await alert.present();
  }

  changeFavorite(cast: User): void{
    try {
      const index = this.favoriteCasts.findIndex(item => item.ID === cast.ID);
      if (index > -1) {
        this.favoriteCasts.splice(index, 1);
      } else {
        if (this.favoriteCasts.length < 5){
          this.favoriteCasts.push(cast);
        }
      }
    } catch (ex) {
      console.log(ex);
      this.favoriteCasts = [cast];
    }
  }

  isFavorite(id: number): boolean{
    return this.favoriteCasts.findIndex(item => item.ID === id) > -1;
  }

  getArray(length: number): Array<unknown>{
    return new Array(length);
  }

}
