import { Component, OnInit, Input } from '@angular/core';

import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.scss'],
})
export class EmailComponent implements OnInit {

  @Input() email: string | null = null;

  constructor(
    private modalController: ModalController,
  ) { }

  ngOnInit(): void {}

  isValidEmail(): boolean {
    if (!this.email || this.email.trim().length < 1) {
      return false;
    }
    if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.email)) {
      return false;
    }
    return true;
  }

  onSave(): void {
    this.modalController.dismiss(this.email?.trim());
  }

  onClose(): void {
    this.modalController.dismiss();
  }

}
