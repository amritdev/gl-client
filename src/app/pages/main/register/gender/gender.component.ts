import { Component, OnInit, Input } from '@angular/core';

import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-gender',
  templateUrl: './gender.component.html',
  styleUrls: ['./gender.component.scss'],
})
export class GenderComponent implements OnInit {

  @Input() gender = '';

  constructor(
    private modalController: ModalController,
  ) { }

  ngOnInit(): void {}

  onSave(): void {
    this.modalController.dismiss(this.gender);
  }

  onClose(): void {
    this.modalController.dismiss();
  }

}
