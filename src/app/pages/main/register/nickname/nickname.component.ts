import { HttpClient } from '@angular/common/http';
import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

import { AlertController, ModalController } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import t from 'src/locales';

@Component({
  selector: 'app-nickname',
  templateUrl: './nickname.component.html',
  styleUrls: ['./nickname.component.scss'],
})
export class NicknameComponent implements OnInit {

  @Input() nickname: string | null = null;

  isNew = false;

  constructor(
    private router: Router,
    private modalController: ModalController,
    private http: HttpClient,
    private alertController: AlertController
  ) { }

  ngOnInit(): void {
    this.isNew = this.router.url.includes('new');
  }

  onSave(): void {
    const url = this.isNew ? `${environment.API_URL}/users/profile/nickname/regist` : `${environment.API_URL}/users/profile/nickname`;
    this.http.post<{
      success: boolean
    }>(url, {
      nickname: this.nickname?.trim()
    }).subscribe((res) => {
      if (res.success) {
        this.modalController.dismiss(this.nickname?.trim());
      } else {
        this.presentAlert(t('Nickname already exists'));
      }
    });
  }

  onClose(): void {
    this.modalController.dismiss();
  }

  async presentAlert(msg: string): Promise<void> {
    const alert = await this.alertController.create({
      message: msg,
      buttons: [t('OK')],
    });
    await alert.present();
  }

}
