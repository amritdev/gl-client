import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';

import { IonRouterOutlet, ModalController, AlertController, NavController, IonDatetime } from '@ionic/angular';

import { AuthService } from 'src/app/services';
import { User, Location } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';
import t from 'src/locales';

import { NicknameComponent } from './nickname/nickname.component';
import { EmailComponent } from './email/email.component';
import { GenderComponent } from './gender/gender.component';
import { LocationComponent } from './location/location.component';
import { CastComponent } from './cast/cast.component';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit, OnDestroy {
  @ViewChild('birthdayDatetime') birthdayDatetime !: IonDatetime;

  avatar: File | null = null;
  hasAvatar = false;
  avatarURL: string | null = null;
  nickname: string | null = null;
  email: string | null = null;
  gender = '';
  birthday: string | null = null;
  locationID = 0;
  locationName: string | null = null;
  locations: Location[] = [];
  casts = '';
  isLoading = false;
  role = 0;
  castArray: User[] = [];
  userReady = true;
  realName = '';
  realNameFrigana = '';
  coporateName = '';
  coporateNameFrigana = '';
  userSubscription: Subscription | null = null;

  constructor(
    private routerOutlet: IonRouterOutlet,
    private authService: AuthService,
    private http: HttpClient,
    private navController: NavController,
    private modalController: ModalController,
    private alertController: AlertController,
  ) {
    if (this.userSubscription === null){
      this.userSubscription = this.authService.currentUserSubject.subscribe(user => {
        if (user) {
          this.nickname = user.Nickname;
          this.email = user.Email;
          this.birthday = user.Birthday;
          this.role = user.Role;
          this.locationID = user.LocationIDs ? parseInt(user.LocationIDs.split(',')[1], 10) : 0;
          if (user.Images && user.Images.length > 0) {
            this.hasAvatar = true;
            this.avatarURL = user.Images[0].Path;
          }
          this.realName = user.RealName ?? '';
          this.realNameFrigana = user.RealNameFrigana ?? '';
          this.coporateName = user.CoporateName ?? '';
          this.coporateNameFrigana = user.CoporateNameFrigana ?? '';
          this.userReady = true;
          this.getUserLocation(this.locationID);
          this.userSubscription?.unsubscribe();
        }
      });
    }
  }

  ngOnInit(): void {
    this.getLocations();
  }

  ngOnDestroy(): void {
    this.userSubscription?.unsubscribe();
  }

  canDeactivate(): boolean {
    if (this.role === 0 || this.role === 1 || this.role === 10) {
      console.log(this.nickname);
      if (this.nickname && this.locationID > 0 && this.birthday && this.hasAvatar) {
        this.navController.navigateRoot('/main');
        return false;
      }
      return true;
    } else if (this.role === 2) {
      if (this.realName && this.realNameFrigana) {
        this.navController.navigateRoot('/main');
        return false;
      }
      return true;
    } else {
      this.navController.navigateRoot('/');
      return true;
    }
  }

  getUserLocation(locationID: number): void {
    this.http.get<Location[]>(`${environment.API_URL}/locations?pid=0`).subscribe(locations => {
      const location = locations.find(l => l.ID === locationID);
      this.locationName = location?.Name ?? '';
    });
  }

  async presentAlert(msg: string): Promise<void> {
    const alert = await this.alertController.create({
      message: msg,
      buttons: [t('OK')],
    });
    await alert.present();
  }

  onFileChanged(event: Event): void {
    if (event.target instanceof HTMLInputElement && event.target.files && event.target.files[0]) {
      this.avatar = event.target.files[0];
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = () => {
        this.avatarURL = reader.result as string;
      };
    }
  }

  getLocations(): void {
    this.http.get<Location[]>(`${environment.API_URL}/locations?pid=0`).subscribe(locations => {
      this.locations = locations;
    }, async () => {
      const alert = await this.alertController.create({
        message: t('Operation Failed'),
        buttons: [t('OK')],
      });
      await alert.present();
    });
  }

  async onEditNickname(): Promise<void> {
    const modal = await this.modalController.create({
      component: NicknameComponent,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        nickname: this.nickname,
      },
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
    if (data) {
      this.nickname = data;
    }
  }

  async onEditEmail(): Promise<void> {
    const modal = await this.modalController.create({
      component: EmailComponent,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        email: this.email,
      },
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
    if (data) {
      this.email = data;
    }
  }

  async onEditGender(): Promise<void> {
    const modal = await this.modalController.create({
      component: GenderComponent,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        gender: this.gender,
      },
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
    if (data !== undefined) {
      this.gender = data;
    }
  }

  async onEditLocation(): Promise<void> {
    const modal = await this.modalController.create({
      component: LocationComponent,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        locationID: this.locationID,
      },
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
    if (data) {
      this.locationID = data.ID;
      this.locationName = data.Name;
    }
  }

  async onEditCasts(): Promise<void> {
    const modal = await this.modalController.create({
      component: CastComponent,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        favoriteCasts: this.castArray,
      }
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
    if (data) {
      this.castArray = data;
      const itemArray: number[] = this.castArray.map((item: User) => {
        return item.ID;
      });

      if (itemArray.length > 0) {
        this.casts = JSON.stringify(itemArray);
      } else {
        this.casts = '';
      }
    }
  }

  onEditBirthday(): void {
    this.birthdayDatetime.open();
  }

  onSave(): void {
    if (!this.nickname || this.nickname.trim() === '') {
      this.nickname = '';
      this.presentAlert(t('Nickname is required'));
    } else if (!this.birthday) {
      this.presentAlert(t('Please select your birthday'));
    } else if (!this.locationID) {
      this.presentAlert(t('Please select your favorite place'));
    } else if (this.gender === '') {
      this.presentAlert(t('Please select gender'));
    } else {
      this.isLoading = true;

      const formData = new FormData();
      formData.append('nickname', this.nickname.trim());
      formData.append('gender', this.gender);
      formData.append('birthday', this.birthday);
      formData.append('location_ids', `,${this.locationID.toString()},`);
      if (this.avatar) {
        formData.append('avatar', this.avatar);
      }
      if (this.email) {
        formData.append('email', this.email);
      }

      this.http.post<{
        success: string;
        updated: User | null;
      }>(`${environment.API_URL}/users/profile/register`, formData).subscribe(res => {
        if (res.success) {
          this.authService.currentUserSubject.next(res.updated);
          if (this.role === 0) {
            this.navController.navigateRoot('/cast_regist_refresh');
          } else {
            this.navController.navigateRoot('/guest_regist_refresh');
          }
        } else {
          this.presentAlert(t('Nickname already exists'));
        }
        this.isLoading = false;
      }, () => {
        this.isLoading = false;
        this.presentAlert(t('Operation Failed'));
      });
    }
  }

  async onCancel(): Promise<void> {
    const alert = await this.alertController.create({
      message: t('Do you want to cancel your registeration?'),
      buttons: [t('Cancel'), {
        text: t('OK'),
        handler: () => {
          // this.http.post<{}>(`${environment.API_URL}/users/profile/register/cancel`, {}).subscribe(() => {
          //   this.authService.logout();
          //   this.navController.navigateRoot('/');
          // }, async () => {
          //   const otherAlert = await this.alertController.create({
          //     message: t('Operation Failed'),
          //     buttons: [t('OK')],
          //   });
          //   await otherAlert.present();
          // });
          this.authService.logout();
          this.navController.navigateRoot('/');
        }
      }],
    });
    await alert.present();
  }

  getMaxYear(): number {
    return new Date().getFullYear() - environment.START_AGE;
  }

  onSaveAgent(): void {
    if (this.realName.trim() === '') {
      this.presentAlert(t('Real name is required'));
    } else if (this.realNameFrigana.trim() === '') {
      this.presentAlert(t('Real name (frigana) is required'));
    } else {
      this.isLoading = true;
      this.http.post<User>(`${environment.API_URL}/users/profile/agent/register`, {
        real_name: this.realName,
        real_name_frigana: this.realNameFrigana,
        coporate_name: this.coporateName,
        coporate_name_frigana: this.coporateNameFrigana
      }).subscribe((user) => {
        this.isLoading = false;
        this.authService.currentUserSubject.next(user);
        this.navController.navigateRoot('/main');
      }, () => {
        this.isLoading = false;
        this.presentAlert(t('Operation Failed'));
      });
    }
  }
}
