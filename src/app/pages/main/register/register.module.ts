import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PipesModule, ComponentsModule } from 'src/app/shared';

import { RegisterRoutingModule } from './register-routing.module';
import { RegisterComponent } from './register.component';
import { NicknameComponent } from './nickname/nickname.component';
import { EmailComponent } from './email/email.component';
import { GenderComponent } from './gender/gender.component';
import { LocationComponent } from './location/location.component';
import { CastComponent } from './cast/cast.component';

@NgModule({
  declarations: [
    RegisterComponent,
    LocationComponent,
    NicknameComponent,
    EmailComponent,
    GenderComponent,
    CastComponent,
  ],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    PipesModule,
    ComponentsModule,
    RegisterRoutingModule,
  ],
})
export class RegisterModule { }
