import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services';
import { Call, Review, User } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import * as moment from 'moment-timezone';
import { AlertController, NavController } from '@ionic/angular';
import t from 'src/locales';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.scss'],
})
export class ReviewComponent implements OnInit, OnDestroy {

  source !: User;
  lastCallID = 0;
  call: Call | null = null;
  guest: User | null = null;
  review: Review | null = null;
  daysOfWeek = ['日', '月', '火', '水', '木', '金', '土'];
  userSubscription: Subscription | null = null;

  constructor(
    private auth: AuthService,
    private http: HttpClient,
    private alert: AlertController,
    private navi: NavController
  ) {
    if (this.userSubscription === null){
      this.userSubscription =  this.auth.currentUserSubject.subscribe((user) => {
        if (user) {
          this.source = user;
          if (this.source.Role === 0){
            this.lastCallID = this.source.LastRoomID;
            this.getCallData();
          }
        }
      });
    }
  }

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.userSubscription?.unsubscribe();
  }

  getCallData(): void {
    const params = new HttpParams().append('id', this.lastCallID.toString());
    this.http.get<Call>(`${environment.API_URL}/calls`, {params}).subscribe(res => {
      this.call = res;
      this.guest = this.call.User;
      if (this.guest) {
        this.review = {
          ID: 0,
          IsGood: null,
          TypeStr: null,
          UserID: this.guest.ID,
          SourceID: this.source.ID,
          Source: this.source,
          CallID: this.call?.ID,
          HowGuest: '',
          HowUp: '',
          Comment: ''
        };
      }
    });
  }

  async showAlert(msgStr: string): Promise<void> {
    const alert = await this.alert.create({
      message: msgStr,
      buttons: ['OK']
    });
    await alert.present();
  }

  onSave(): void {
    if (this.review){
      if (this.review.IsGood === null){
        this.showAlert(t('Please give the guest review'));
        return;
      }else{
        if (this.review.HowGuest.trim() === '' || this.review.HowUp.trim() === '') {
          this.showAlert('内容をすべて入力してください');
          return;
        }
      }

      if (this.review.IsGood !== null) {
        this.review.Comment = this.review.Comment.trim();
        this.review.HowGuest = this.review.HowGuest.trim();
        this.review.HowUp = this.review.HowUp.trim();

        this.http.post<{
          'success': boolean
        }>(`${environment.API_URL}/reviews`, this.review).subscribe((res) => {
          if (res.success) {
            // successfully saved
            if (this.source) {
              if (this.source.Role === 0 && this.source.LastRoomID > 0 && this.source.LastRoomID === this.call?.ID){
                this.http.post<{
                  'success': boolean
                }>(`${environment.API_URL}/users/lastroom`, {
                  last_room: 0
                }).subscribe(result => {
                  if (result) {
                    this.source.LastRoomID = 0;
                    this.userSubscription?.unsubscribe();
                    this.auth.currentUserSubject.next(this.source);
                    this.navi.navigateRoot('/main');
                  }
                });
              }
            }
          }
        });
      }
    }


  }

  getTime(): string {
    if (this.call) {
      const meetTime = moment(`${this.call.MeetTimeISO} +0900`, 'YYYY-MM-DD HH:mm:ss Z');
      return meetTime.format('MM月DD日') +
        `(${this.daysOfWeek[meetTime.day()]}) ${meetTime.format('HH:mm')}~`;
    } else {
      return '';
    }
  }

  onMinus(): void {
    if (this.review){
      this.review.IsGood = false;
      this.review.Comment = '';
      this.review.TypeStr = '長く待たされた';
    }
  }

  onPlus(): void {
    if (this.review){
      this.review.IsGood = true;
      this.review.Comment = '';
      this.review.TypeStr = '';
    }
  }

  isActive(review: Review, result: boolean): boolean {
    if (review.IsGood !== null) {
      return review.IsGood === result;
    } else {
      return false;
    }
  }


}
