import { Component, OnInit } from '@angular/core';
import { formatDate } from '@angular/common';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { NavController, AlertController } from '@ionic/angular';
import { AuthService, CastFilterService } from 'src/app/services';
import { Detail, Class, CastSearchFilter, ScheduleDay, Location } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';
import t from 'src/locales';
import * as moment from 'moment-timezone';

interface PointRange {
  lower: number;
  upper: number;
}

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss'],
})
export class FilterComponent implements OnInit {

  residences: Detail[] = [];
  locations: Location[] = [];
  classes: Class[] = [];
  filterCond: CastSearchFilter | null = null;
  parentTags: Detail[] = [];
  childTags: { [key: number]: Detail[] } = {};
  Weeks: ScheduleDay[][] = [];
  daysOfTheWeek: string[] = ['日', '月', '火', '水', '木', '金', '土'];
  todaysDayList: string[] = [];
  details: Detail[] = [];
  curRange: PointRange = {
    lower: 0,
    upper: 20000
  };

  curYear = 0;
  curMonth = 0;

  // subscriptions
  searchFilterSubscription: unknown = null;

  constructor(
    private http: HttpClient,
    private cond: CastFilterService,
    private nav: NavController,
    private auth: AuthService,
    private alert: AlertController
  ) {
    this.auth.currentUserSubject.subscribe((user) => {
      if (user) {
        if (user.LocationIDs) {
          // const locationIDs = user.LocationIDs.split(',').filter(item => {
          //   return item !== '';
          // }).map(item => {
          //   return parseInt(item, 10);
          // });

          if (this.searchFilterSubscription === null) {
            this.searchFilterSubscription = this.cond.castFilterSubject.subscribe((filter) => {
              if (filter) {
                // console.log(filter);
                this.filterCond = filter;
                this.curRange.lower = this.filterCond.PointMin;
                this.curRange.upper = this.filterCond.PointMax;
              }
            });
          }
        }
      }
    });

  }

  ngOnInit(): void {
    this.setTwoWeeks();
    // this.getResidences();
    this.getLocations();
    this.getClasses();
    this.getTags();
  }

  setTwoWeeks(): void {
    const startDay = new Date().getDay();
    const curDate = new Date();
    let curWeekData: ScheduleDay[] = [];
    for (let j = 0; j < 2; j++) {
      curWeekData = [];
      for (let i = startDay; i < startDay + 7; i++) {
        if (j === 0) {
          this.todaysDayList.push(this.daysOfTheWeek[i % 7]);
        }
        curWeekData.push({
          date: formatDate(curDate, 'yyyy-MM-dd', 'en'),
          day: curDate.getDate()
        });
        this.curYear += curDate.getFullYear();
        this.curMonth += curDate.getMonth();
        curDate.setDate(curDate.getDate() + 1);
      }
      this.Weeks.push(curWeekData);
    }
    this.curYear = Math.round(this.curYear / 14);
    this.curMonth = Math.round(this.curMonth / 14);
  }

  getResidences(): void {
    this.http.get<Detail[]>(`${environment.API_URL}/admin/details/residence`).subscribe((data) => {
      this.residences = data;
    }, (err: HttpErrorResponse) => {
      console.log(err);
    });
  }

  getLocations(): void {
    this.http.get<Location[]>(`${environment.API_URL}/locations?pid=0`).subscribe(locations => {
      this.locations = locations;
    }, async () => {
      const alert = await this.alert.create({
        message: t('Operation Failed'),
        buttons: [t('OK')],
      });
      await alert.present();
    });
  }

  getClasses(): void {
    this.http.get<Class[]>(`${environment.API_URL}/admin/classes`).subscribe((data) => {
      this.classes = data;
    }, (err: HttpErrorResponse) => {
      console.log(err);
    });
  }

  getTags(): void {
    this.http.get<Detail[]>(`${environment.API_URL}/admin/tags/parent`).subscribe((pdata) => {
      this.parentTags = pdata;
      for (const parentItem of this.parentTags) {
        this.http.get<Detail[]>(`${environment.API_URL}/admin/tags/children/${parentItem.ID}`).subscribe((cdata) => {
          this.childTags[parentItem.ID] = cdata;
          this.details = this.details.concat(... cdata);
        });
      }
    }, (err: HttpErrorResponse) => {
      console.log(err);
    });
  }

  clickTags(curTag: Detail): void {
    if (this.filterCond?.Tag.includes(curTag.ID)) {
      this.filterCond?.Tag.splice(this.filterCond.Tag.indexOf(curTag.ID, 0), 1);
    } else {
      this.filterCond?.Tag.push(curTag.ID);
    }
  }

  selectScheduleDate(selectedDate: string): void {
    if (this.filterCond?.Schedule.includes(selectedDate)) {
      this.filterCond.Schedule.splice(this.filterCond.Schedule.indexOf(selectedDate, 0), 1);
    } else {
      this.filterCond?.Schedule.push(selectedDate);
    }
  }

  execSearch(): void {
    if (this.filterCond) {
      this.filterCond.PointMin = this.curRange.lower;
      this.filterCond.PointMax = this.curRange.upper;
    }

    // get info make search string
    if (this.filterCond) {
      this.filterCond.SearchStr = [];

      // location
      if (this.filterCond.Location > 0) {
        const locationIndex = this.locations.findIndex(item => this.filterCond && item.ID === this.filterCond.Location);
        if (locationIndex > -1) {
          this.filterCond.SearchStr = [this.locations[locationIndex].Name];
        }
      }

      // class
      if (this.filterCond.Class.length > 0) {
        this.filterCond.Class.forEach((classID: number) => {
          const classIndex = this.classes.findIndex(item => item.ID === classID);
          if (this.filterCond && classIndex > -1){
            this.filterCond.SearchStr.push(this.classes[classIndex].Name);
          }
        });
      }

      // tags
      if (this.filterCond.Tag.length > 0) {
        this.filterCond.Tag.forEach((tagID: number) => {
          const detailIndex = this.details.findIndex(item => item.ID === tagID);
          if (this.filterCond && detailIndex > -1){
            this.filterCond.SearchStr.push(this.details[detailIndex].Name);
          }
        });
      }

      // schedule
      if (this.filterCond.Schedule.length > 0){
        let prevMonth = '0';
        const scheduleLengthArray: string[] = [];
        this.filterCond.Schedule.forEach((scheduleItem: string) => {
          const month = moment(scheduleItem).format('M');
          const day = moment(scheduleItem).format('D');
          if (month !== prevMonth) {
            scheduleLengthArray.push(moment(scheduleItem).format('M/D'));
            prevMonth = month;
          }else{
            scheduleLengthArray.push(day);
          }
        });
        if (scheduleLengthArray.length > 0) {
          this.filterCond.SearchStr.push(scheduleLengthArray.join('•'));
        }
      }

      // point
      if (this.filterCond.PointMax < 20000 || this.filterCond.PointMin > 0) {
        this.filterCond.SearchStr.push(
          `${new Intl.NumberFormat().format(this.filterCond.PointMin)}P〜${new Intl.NumberFormat().format(this.filterCond.PointMax)}P`);
      }

      // birthday this month
      if (this.filterCond.Recently){
        this.filterCond.SearchStr.push(`${t('Recently')}${t('Registered')}`);
      }

      if (this.filterCond.Birthday){
        this.filterCond.SearchStr.push(`${t('This Month')}${t('Birthday')}`);
      }

      if (this.filterCond.TodayFree){
        this.filterCond.SearchStr.push(`${t('Have Time Today')}`);
      }

      localStorage.setItem('castFilterCond', JSON.stringify(this.filterCond));
    }

    this.cond.castFilterSubject.next(this.filterCond);
    this.nav.navigateBack('/main/search');
    this.cond.isCleared = true;
  }

  clearFilter(): void {
    this.cond.isCleared = true;
    this.cond.reset();
  }

}
