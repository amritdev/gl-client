import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SearchComponent } from './search.component';
import { FilterComponent } from './filter/filter.component';
import { TelecomGuard } from 'src/app/guards/telecom.guard';

const routes: Routes = [
  {
    path: '',
    component: SearchComponent,
    canActivate: [TelecomGuard]
  },
  {
    path: 'filter',
    component: FilterComponent,
    canActivate: [TelecomGuard]
  },
  {
    path: 'rank',
    loadChildren: () => import('../rank/rank.module').then(m => m.RankModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SearchRoutingModule { }
