import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { IonInfiniteScroll, IonContent, Platform } from '@ionic/angular';
import { AuthService, CastFilterService, ConditionService, WebsocketService } from 'src/app/services';
import { Detail, User, CastSearchFilter, Location } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import * as moment from 'moment-timezone';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit, OnDestroy {

  @ViewChild('ionContent') ionContent!: IonContent;
  @ViewChild(IonInfiniteScroll) ionInfiniteScroll!: IonInfiniteScroll;

  // recent casts
  recentCasts: User[] = [];
  filteredCasts: User[] = [];
  userLocationIDs: number[] = [];
  locations: Location[] = [];

  // for slider
  slideOpts = {
    slidesPerView: 2.7,
    spaceBetween: 10
  };

  // for searching casts
  castFilter: CastSearchFilter | null = null;
  selectedTags: Detail[] = [];

  // infinite scroll
  allLoaded = false;
  page = 0;
  offset = 0;
  connectSubscription: Subscription | null = null;

  // full casts
  isFullCastsReady = false;
  fullCasts: User[] = [];

  // search query
  searchText = '';

  // subscriptions
  userSubscription: Subscription | null = null;
  searchFilterSubscription: Subscription | null = null;
  reloadSubscription: Subscription | null = null;
  withdrawnReloadSubscription: Subscription | null = null;
  user: User | null = null;
  isFirst = true;

  // app
  isWebBrowser = false;

  // block user ids
  blockUserIDs: number[] = [];

  constructor(
    private auth: AuthService,
    private http: HttpClient,
    private cond: CastFilterService,
    public callCond: ConditionService,
    private ws: WebsocketService,
    private router: Router,
    private platform: Platform
  ) {
    this.isWebBrowser = this.platform.is('mobileweb');

    this.getLocations();

    if (this.reloadSubscription === null) {
      this.reloadSubscription = this.auth.reloadSubject.subscribe(res => {
        if (res) {
          this.reloadCasts();
        }
      });
    }
  }

  ngOnInit(): void {
    this.getRecentCasts();
    this.getFullCasts();
    this.getBlockUserIDs();
  }

  ngOnDestroy(): void {
    this.reloadSubscription?.unsubscribe();
    this.searchFilterSubscription?.unsubscribe();
    this.userSubscription?.unsubscribe();
    this.withdrawnReloadSubscription?.unsubscribe();
  }

  getBlockUserIDs(): void {
    this.http.get<number[]>(`${environment.API_URL}/users/block/all`).subscribe(res => {
      // console.log(res);
      this.blockUserIDs = res;
    });
  }

  reloadCasts(): void {
    this.getRecentCasts();
    this.filteredCasts = [];
    this.allLoaded = false;
    this.page = 0;
    this.offset = 0;
    this.execSearch(null);
  }

  getLocations(): void {
    this.http.get<Location[]>(`${environment.API_URL}/locations?pid=0`).subscribe(locations => {
      this.locations = locations;

      // user subscription after locations fetched
      if (this.userSubscription === null) {
        this.userSubscription = this.auth.currentUserSubject.subscribe((user) => {
          if (user){
            this.user = user;
            if (user.LocationIDs) {

              this.userLocationIDs = user.LocationIDs.split(',').filter(item => {
                return item !== '';
              }).map(item => {
                return parseInt(item, 10);
              });

              if (this.searchFilterSubscription === null) {
                this.searchFilterSubscription = this.cond.castFilterSubject.subscribe((filter) => {
                  if (filter) {
                    // console.log(filter);
                    this.castFilter = filter;

                    // reset
                    this.filteredCasts = [];
                    this.page = 0;
                    this.offset = 0;
                    this.allLoaded = false;

                    if (this.userLocationIDs.length > 0 && !this.cond.isCleared && this.isFirst && !this.cond.isPrevExist) {
                      this.castFilter.Location = this.userLocationIDs[0];
                      const locationIndex = this.locations.findIndex(item => this.castFilter && item.ID === this.castFilter.Location);
                      if (locationIndex > -1) {
                        this.castFilter.SearchStr = [this.locations[locationIndex].Name];
                      }
                      this.isFirst = false;
                    }

                    if (filter.Tag.length > 0){
                      this.selectedTags = [];
                      filter.Tag.forEach((tagID: number) => {
                        const detailIndex = this.cond.details.findIndex(item => item.ID === tagID);
                        if (detailIndex > -1){
                          this.selectedTags.push(this.cond.details[detailIndex]);
                        }
                      });
                    } else {
                      this.selectedTags = [];
                    }

                    this.execSearch(null);

                    // make search text
                    if (this.castFilter.SearchStr === undefined){
                      this.castFilter.SearchStr = [];
                      this.searchText = '';
                    }else {
                      this.searchText = this.castFilter.SearchStr.join(', ');
                    }
                  }
                });
              }
            }

          }

        });
      }
    });
  }

  getRecentCasts(): void {
    this.http.get<User[]>(`${environment.API_URL}/casts/recent`).subscribe(res => {
      this.recentCasts = res;
    });
  }

  getNotifyCount(): number {
    return this.ws.unreadNotices;
  }

  getFullCasts(): void {
    this.isFullCastsReady = true;
  }

  removeTags(index: number): void{
    const tagName = this.selectedTags[index].Name;
    this.selectedTags.splice(index, 1);
    if (this.castFilter) {
      this.castFilter.Tag = this.selectedTags.map(item => item.ID);
      this.castFilter.SearchStr = this.castFilter.SearchStr.filter(item => item !== tagName);
      this.cond.castFilterSubject.next(this.castFilter);
    }
  }

  execSearch(event: Event | null): void {
    // console.log(this.castFilter);

    this.http.post<User[]>(`${environment.API_URL}/casts/filter/${this.page}/${this.offset}`, this.castFilter).subscribe((data) => {
      if (data.length < environment.CAST_PER_PAGE) {
        this.allLoaded = true;
      }
      this.filteredCasts = this.filteredCasts.concat(data);
      if (event && event.target) {
        this.ionInfiniteScroll.complete();
      }

      // connected user subscription
      if (this.connectSubscription === null) {
        this.connectSubscription = this.ws.connectUserSubject.subscribe(res => {
          if (this.castFilter && this.auth.checkUser(res, 'guest', this.castFilter.Location, this.blockUserIDs, [])) {
            const castsIndex = this.filteredCasts.findIndex(item => item.ID === res.ID);
            if (castsIndex > -1){
              this.filteredCasts.splice(castsIndex, 1);
              this.filteredCasts.unshift(res);
            }else{
              this.filteredCasts.unshift(res);
              this.offset++;
            }
          }
        });
      }

      // reload user subsubcription
      if (this.withdrawnReloadSubscription === null) {
        this.withdrawnReloadSubscription = this.ws.reloadSubject.subscribe(eventStr => {
          if (eventStr === 'search') {
            this.reloadCasts();
          }
        });
      }
    }, (err: HttpErrorResponse) => {
      console.log(err);
    });
  }

  loadData(event: Event | null): void {
    this.page++;
    this.execSearch(event);
  }

  clearSearch(event: Event): void {
    event.preventDefault();
    event.stopPropagation();

    this.scrollToTop();
    this.cond.isCleared = true;
    const formattedFilter = this.cond.reset();
    localStorage.setItem('castFilterCond', JSON.stringify(formattedFilter));
  }

  goToFilterPage(): void {
    this.router.navigate(['/main/search/filter']);
  }

  scrollToTop(): void {
    this.ionContent?.scrollToTop();
  }

  getCastVideoPath(cast: User): string {
    if (cast.Videos && cast.Videos.length > 0){
      return cast.Videos[0].Path;
    }else{
      return '';
    }
  }

  isTodayFree(cast: User): boolean {
    if (cast.Schedules && cast.Schedules.length > 0) {
      const todayDate = moment.tz('Asia/Tokyo');
      if (todayDate.hour() < 5){
        todayDate.subtract(1, 'days');
      }
      const todayIndex = cast.Schedules.findIndex(item => item.Date === todayDate.format('YYYY-MM-DD') && item.Enabled);
      return todayIndex > -1;
    }else{
      return false;
    }
  }
}
