import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { PipesModule, ComponentsModule } from 'src/app/shared';

import { SearchRoutingModule } from './search-routing.module';
import { SearchComponent } from './search.component';
import { FilterComponent } from './filter/filter.component';

@NgModule({
  declarations: [
    SearchComponent,
    FilterComponent,
  ],
  imports: [
    CommonModule,
    IonicModule,
    PipesModule,
    FormsModule,
    ComponentsModule,
    SearchRoutingModule,
  ],
})
export class SearchModule {}
