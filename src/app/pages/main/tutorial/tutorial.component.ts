import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IonInfiniteScroll, NavController, Platform } from '@ionic/angular';
import { AlertController, IonSlides } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { AdminData, Image, User } from 'src/app/interfaces';
import { AuthService, ConditionService, WebsocketService } from 'src/app/services';
import { environment } from 'src/environments/environment';
import t from 'src/locales';

@Component({
  selector: 'app-tutorial',
  templateUrl: './tutorial.component.html',
  styleUrls: ['./tutorial.component.scss'],
})
export class TutorialComponent implements OnInit, OnDestroy {
  @ViewChild('mySlider')  slides !: IonSlides;
  @ViewChild(IonInfiniteScroll) ionInfiniteScroll !: IonInfiniteScroll;

  nickname = '';
  isProfileAdded = false;
  isLineAdded = false;
  isCastLiked = false;
  isCreditAdded = false;
  likedCasts = 0;
  isTodayWordSet = false;
  isAboutSet = false;
  lineID = '';
  favoriteCasts: User[] = [];
  casts: User[] = [];
  clientIP = environment.TELECOM_CLIENT_IP;
  phoneNumber = '';
  about = '';
  today = '';

  // role
  role = 1;

  // slide
  isEnd = false;
  slidesNum = 0;
  slideLength = 0;
  curIndex = 0;

  // infinite scroll
  allLoaded = false;
  connectSubscription: Subscription | null = null;
  page = 0;
  offset = 0;
  pageSize = 10;

  // profile image
  imageURLs: string[] = ['assets/img/avatar.svg'];
  images: File[] = [];

  first = false;
  userSubscription: Subscription | null = null;
  redirectURL = environment.SITE_URL + '/main/tutorial';
  user: User | null = null;
  admin!: AdminData;

  constructor(
    private platform: Platform,
    private route: ActivatedRoute,
    private auth: AuthService,
    private http: HttpClient,
    private alert: AlertController,
    public cond: ConditionService,
    private navi: NavController,
    private websocket: WebsocketService,
    private router: Router
  ) {
    if (this.userSubscription === null) {
      this.userSubscription = this.auth.currentUserSubject.subscribe(user => {
        if (user) {
          this.isProfileAdded = user.IsProfileAdded;
          this.isLineAdded = user.IsLineAdded;
          this.isCastLiked = user.IsCastLiked;
          this.isCreditAdded = user.IsCreditAdded;
          this.isTodayWordSet = user.Word !== null && user.Word !== '';
          this.isAboutSet = user.About !== null && user.About !== '';
          this.lineID = user.LineID ?? '';
          this.phoneNumber = user.PhoneNumber ? user.PhoneNumber.replace('+81', '') : '';
          this.user = user;

          if (user.Role === 1){
            this.getFavorites();
          }
          this.nickname = user.Nickname ?? '';
          if (user.Images.length > 0){
            this.imageURLs = user.Images.map(userImgItem => {
              return userImgItem.Path;
            });
          }
          this.role = user.Role;

          if (this.first){
            this.slides?.update().then(() => {
              this.checkCount();
            });
          }else{
            this.first = true;
          }
        }
      });
    }
  }

  ngOnInit(): void {
    this.getAdminData();
    if (this.platform.is('hybrid')) {
      this.redirectURL = this.redirectURL.replace('https://', `${environment.DEEPLINK_SCHEME}://`);
    }
    const code = this.route.snapshot.queryParamMap.get('code');
    if (code) {
      this.http.post<User>(`${environment.API_URL}/users/link/${code}?mode=tutorial`, {}).subscribe((user) => {
        if (this.lineID === ''){
          this.presentAlert(t('LINE official account has been added as a friend.'));
          this.lineID = user.LineID ?? '';
        }else{
          this.presentAlert(t('LINE Account successfully added'));
        }

        this.auth.currentUserSubject.next(user);
      }, (err) => {
        if (err.status === 409) {
          this.presentAlert(t('You already have a LINE account.'));
        } else {
          this.presentAlert(t('Operation Failed'));
        }
      });
    }
    this.route.queryParamMap.subscribe((params) => {
      if (params.get('line')) {
        const lineCode = params.get('line');
        this.http.post<User>(`${environment.API_URL}/users/link/${lineCode}?mode=tutorial&platform=mobile`, {}).subscribe((user) => {
          if (this.lineID === ''){
            this.presentAlert(t('LINE official account has been added as a friend.'));
            this.lineID = user.LineID ?? '';
          }else{
            this.presentAlert(t('LINE Account successfully added'));
          }

          this.auth.currentUserSubject.next(user);
        }, (err) => {
          if (err.status === 409) {
            this.presentAlert(t('You already have a LINE account.'));
          } else {
            this.presentAlert(t('Operation Failed'));
          }
        });
      }
    });
    const telecomcredit = this.route.snapshot.queryParamMap.get('telecomcredit') ?? '';
    if (telecomcredit === 'success') {
      this.presentAlert(t('Credit card verification is complete'));
    }
    this.route.queryParamMap.subscribe((params) => {
      if (params.get('telecom')) {
        this.presentAlert(t('Credit card verification is complete'));
      }
    });

    this.execSearch(null);
  }

  ngOnDestroy(): void {
    this.userSubscription?.unsubscribe();
  }

  reloadComponent(): void {
    const currentUrl = this.router.url;
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    this.router.navigate([currentUrl]);
  }

  ionViewDidEnter(): void{

    this.slides?.length().then(res => {
      this.slideLength = res;
      // console.log(this.slideLength);
    });

    this.slides?.getActiveIndex().then(res => {
      this.curIndex = res;
      // console.log(this.curIndex);
    });
  }

  getAdminData(): void{
    this.http.get<AdminData>(`${environment.API_URL}/users/admin`).subscribe(res => {
      this.admin = res;
    });
  }

  getFavorites(): void {
    this.http.get<User[]>(`${environment.API_URL}/users/favorite`).subscribe(res => {
      // console.log(res);
      this.favoriteCasts = res;
    });
  }

  checkCount(): void {
    this.slides?.length().then(res => {
      this.slideLength = res;
      if (this.slideLength === 1){
        this.goToMainPage();
      }
    });
    this.slides?.getActiveIndex().then(res => {
      this.curIndex = res;
      // console.log(this.curIndex);
    });

  }

  goToNext(): void{
    if (this.isEnd){
      this.goToMainPage();
    }else{
      this.slides?.slideNext();
    }
  }

  goToMainPage(): void {
    // save tutorial viewed time
    this.http.get<User>(`${environment.API_URL}/users/tutorial`).subscribe(res => {
      this.userSubscription?.unsubscribe();
      this.auth.currentUserSubject.next(res);

      // go to main page
      if (this.role === 0 || this.role === 10){
        this.navi.navigateRoot('/main/apply');
      }
      if (this.role === 1){
        this.navi.navigateRoot('/main/call');
      }
    });
  }

  saveNickname(): void {
    if (this.nickname.trim() === ''){
      this.presentAlert(t('Nickname is required'));
    }else{
      this.http.put<{}>(`${environment.API_URL}/users/profile`, {
        nickname: this.nickname,
      }).subscribe(() => {
        // this.presentAlert(t("Successfully Saved!"));
        this.goToNext();
      }, (err: HttpErrorResponse) => {
        if (err.status === 409) {
          this.presentAlert(t('Nickname already exists'));
        } else {
          this.presentAlert(t('Operation Failed'));
        }
      });
    }
  }

  saveTodayWord(): void {
    if (this.today.trim() === '') {
      this.today = '';
      this.goToNext();
    }else{
      this.http.put<{}>(`${environment.API_URL}/users/profile`, {
        word: this.today.trim(),
      }).subscribe(() => {
        this.goToNext();
      }, (_) => {
        this.presentAlert(t('Operation Failed'));
      });
    }
  }

  saveAbout(): void {
    if (this.about.trim() === '') {
      this.about = '';
      this.goToNext();
    }else{
      this.http.put<{}>(`${environment.API_URL}/users/profile`, {
        about: this.about.trim(),
      }).subscribe(() => {
        this.goToNext();
      }, (_) => {
        this.presentAlert(t('Operation Failed'));
      });
    }
  }

  async presentAlert(msg: string): Promise<void> {
    const alert = await this.alert.create({
      message: msg,
      buttons: [t('OK')],
    });
    await alert.present();
  }

  getArray(length: number): Array<unknown>{
    return new Array(length);
  }

  execSearch(event: Event | null): void {
    this.http.get<User[]>(`${environment.API_URL}/casts/all/${this.page}/${this.offset}`).subscribe((res) => {
      if (res.length < this.pageSize) {
        this.allLoaded = true;
      }
      this.casts = this.casts.concat(res);
      if (event && event.target) {
        this.ionInfiniteScroll.complete();
      }
      // console.log(this.casts);

      if (this.connectSubscription === null){
        this.connectSubscription = this.websocket.connectUserSubject.subscribe(connector => {
          const desireIndex = this.casts.findIndex(item => item.ID === connector.ID);

          if (desireIndex > -1){
            this.casts.splice(desireIndex, 1);
            this.casts.unshift(connector);
          }else{
            this.casts.unshift(connector);
            this.offset++;
          }
        });
      }
    }, (err: HttpErrorResponse) => {
      console.log(err);
    });
  }


  isFavorite(id: number): boolean{
    return this.favoriteCasts.findIndex(item => item.ID === id) > -1;
  }

  loadData(event: Event | null): void {
    this.page++;
    this.execSearch(event);
  }

  changeFavorite(cast: User): void{
    try {
      const index = this.favoriteCasts.findIndex(item => item.ID === cast.ID);
      if (index > -1) {
        this.favoriteCasts.splice(index, 1);
      } else {
        if (this.favoriteCasts.length < 5){
          this.favoriteCasts.push(cast);
        }
      }
    } catch (ex) {
      console.log(ex);
      this.favoriteCasts = [cast];
    }
  }

  viewCast(profileID: string): void {
    this.navi.navigateRoot(`main/profile/${profileID}`);
  }

  saveFavoriteCasts(): void {
    const itemArray: number[] = this.favoriteCasts.map((item: User) => {
      return item.ID;
    });
    let castStr = '';

    if (itemArray.length > 0) {
      castStr = JSON.stringify(itemArray);
      const formData = new FormData();
      formData.append('favorite_users', castStr);

      this.http.post<boolean>(`${environment.API_URL}/users/profile/register/like`, formData).subscribe(res => {
        if (res){
          // this.presentAlert(t("Successfully Saved!"));
          this.goToNext();
        }
      });
    }else{
      this.goToNext();
    }
  }

  onFileChanged(event: Event): void {
    if (event.target instanceof HTMLInputElement && event.target.files && event.target.files[0]) {
      if (event.target.files.length > 10){
        this.presentAlert(t('You can upload up to 10 images'));
        return;
      }
      const fileLength = event.target.files.length;

      for (let i = 0; i < fileLength; i++) {
        const limitSize = 5;
        const alertText = `You can not upload more than %s megabytes sized image`;
        if (event.target.files[i].size / 1024 / 1024 > limitSize) {
          this.presentAlert(t(alertText).replace('%s', limitSize.toString()));
          return;
        }
      }
      this.imageURLs = [];
      this.images = [];
      for (let i = 0; i < fileLength; i++) {
        const reader = new FileReader();
        reader.readAsDataURL(event.target.files[i]);
        this.images.push(event.target.files[i]);
        reader.onload = () => {
          this.imageURLs.push(reader.result as string);
        };
      }
    }
  }

  saveProfileImage(): void {
    if (this.images.length > 0) {
      const formData = new FormData();
      this.images.forEach(imageItem => {
        formData.append('avatar', imageItem);
      });
      this.http.post<{
        success: boolean,
        images: Image[]
      }>(`${environment.API_URL}/users/profile/register/image`, formData).subscribe(res => {
        if (res.success){
          this.imageURLs = res.images.map(item => {
            return item.Path;
          });
          // this.goToNext();
        }else{
          this.presentAlert(t('Operation Failed'));
        }
      }, () => {
        this.presentAlert(t('Operation Failed'));
      });
    }else{
      this.presentAlert(t('Please select avatar image'));
    }
  }

  associateWithLINE(): void {
    let redirectURL = `${environment.SITE_URL}/main/tutorial`;
    if (this.platform.is('hybrid')) {
      redirectURL = `${environment.SITE_URL}/line/tutorial`;
    }
    location.href = 'https://access.line.me/oauth2/v2.1/authorize?'
        + 'response_type=code&'
        + `client_id=${environment.LINE_CHANNEL_ID}&`
        + `redirect_uri=${redirectURL}&`
        + `state=${Math.random().toString(36).substring(2, 15)}&`
        + 'scope=profile%20openid%20email&'
        + 'bot_prompt=aggressive';
  }

  slideChanged(): void {
    this.slides?.getActiveIndex().then(res => {
      this.curIndex = res;
    });
    this.slides?.isEnd().then(res => {
      this.isEnd = res;
    });
  }

  getTitle(): string{
    if (this.curIndex === this.slideLength - 1){
      return t('Start Now');
    }else{
      return `${t('Next')}(${this.curIndex + 1} / ${this.slideLength})`;
    }
  }

  doneAndNext(): void{
    const curClassName = document.querySelector('ion-slide.swiper-slide-active')?.className;
    if (curClassName){
      if (curClassName.includes('nickname-slide')){
        this.saveNickname();
      }else if (curClassName.includes('cast-slide')){
        this.saveFavoriteCasts();
      }else if (curClassName.includes('profile-slide')){
        this.saveProfileImage();
      }else if (curClassName.includes('word-slide')){
        this.saveTodayWord();
      }else if (curClassName.includes('about-slide')){
        this.saveAbout();
      }else{
        this.goToNext();
      }
    }
  }

}
