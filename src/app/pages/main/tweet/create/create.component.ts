import { Component, OnInit, Input } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { ModalController, AlertController } from '@ionic/angular';

import { environment } from 'src/environments/environment';
import t from 'src/locales';


@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
})
export class CreateComponent implements OnInit {

  @Input() role = 0;
  @Input() avatarURL = '';
  @Input() nickname = '';

  content = '';
  image: File | null = null;
  imageURL: string | null = null;
  isLoading = false;
  isRecruit = false;

  constructor(
    private http: HttpClient,
    private modalController: ModalController,
    private alertController: AlertController,
  ) { }

  ngOnInit(): void { }

  onFileChanged(event: Event): void {
    if (event.target instanceof HTMLInputElement && event.target.files && event.target.files[0]) {
      this.image = event.target.files[0];
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = () => {
        this.imageURL = reader.result as string;
      };
    }
  }

  onRemoveImage(): void {
    this.image = null;
    this.imageURL = null;
  }

  async onSave(): Promise<void> {
    if (!this.content.trim()) {
      const alert = await this.alertController.create({
        message: t('Tweet is required'),
        buttons: [t('OK')],
      });
      await alert.present();
    } else {
      // check content
      let contentCheck = '';
      if (this.content.trim() !== '') {
        const params = new HttpParams().
          append('content', this.content.trim());
        try{
          contentCheck = await this.http.get<string>(`${environment.API_URL}/tweets/check`, { params }).toPromise();
        }catch (ex: unknown){
          this.showAlert(t('Operation Failed'));
          return;
        }
      }

      if (contentCheck !== ''){
        this.showAlert(t('Content can not include %s').replace('%s', contentCheck));
        return;
      }

      // if okay
      this.isLoading = true;
      const formData = new FormData();
      formData.append('content', this.content);
      if (this.image) {
        formData.append('image', this.image);
      }
      formData.append('recruit', this.isRecruit ? 'true' : 'false');

      this.http.post<{}>(`${environment.API_URL}/tweets`, formData).subscribe(() => {
        this.isLoading = false;
        this.modalController.dismiss(true);
      }, async () => {
        this.isLoading = false;
        const alert = await this.alertController.create({
          message: t('Operation Failed'),
          buttons: [t('OK')],
        });
        await alert.present();
      });
    }
  }

  async onClose(): Promise<void> {
    if (this.content.trim() || this.image) {
      const alert = await this.alertController.create({
        message: t('Tweet may be lost. Do you still want to leave?'),
        buttons: [t('Cancel'), {
          text: t('OK'),
          handler: () => {
            this.modalController.dismiss();
          },
        }],
      });
      await alert.present();
    } else {
      this.modalController.dismiss();
    }
  }

  async showAlert(msgStr: string): Promise<void> {
    const alert = await this.alertController.create({
      message: msgStr,
      buttons: ['OK']
    });
    await alert.present();
  }
}
