import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';

import { AlertController, ActionSheetController } from '@ionic/angular';

import { AuthService, TweetService } from 'src/app/services';
import { User, Tweet } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';
import t from 'src/locales';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
})
export class DetailComponent implements OnInit {

  user: User | null = null;
  tweetID: number | null = null;
  tweet: Tweet | null = null;

  constructor(
    private route: ActivatedRoute,
    private authService: AuthService,
    private tweetService: TweetService,
    private http: HttpClient,
    private actionSheetController: ActionSheetController,
    private alertController: AlertController,
  ) { }

  ngOnInit(): void {
    this.authService.currentUserSubject.subscribe(user => {
      this.user = user ? { ...user } : null;
    });
    this.route.params.subscribe(params => {
      this.tweetID = params?.id || null;
      if (this.tweetID) {
        this.getTweet(this.tweetID);
      }
    });
  }

  getTweet(id: number): void {
    this.http.get<Tweet>(`${environment.API_URL}/tweets/detail/${id}`).subscribe(tweet => {
      this.tweet = tweet;
    }, async () => {
      const alert = await this.alertController.create({
        message: t('Operation Failed'),
        buttons: [t('OK')],
      });
      await alert.present();
    });
  }

  getHeartCount(tweet: Tweet): number {
    return tweet.FavoriteUserIDs ? JSON.parse(tweet.FavoriteUserIDs).length + tweet.Bonus : tweet.Bonus;
  }

  getHeartStatus(tweet: Tweet): boolean {
    return tweet.FavoriteUserIDs ? JSON.parse(tweet.FavoriteUserIDs).includes(this.user?.ID) : 0;
  }

  onToggleHeart(tweet: Tweet): void {
    const favoriteUserIDsArr = tweet.FavoriteUserIDs ? JSON.parse(tweet.FavoriteUserIDs) : [];
    let isFavorite = false;
    if (this.getHeartStatus(tweet)) {
      const favoriteUserIDsArrIndex = favoriteUserIDsArr.findIndex((id: number) => id === this.user?.ID);
      favoriteUserIDsArr.splice(favoriteUserIDsArrIndex, 1);
      isFavorite = false;
    } else {
      favoriteUserIDsArr.push(this.user?.ID);
      isFavorite = true;
    }
    if (this.tweet) {
      this.tweet.FavoriteUserIDs = JSON.stringify(favoriteUserIDsArr);
      this.tweetService.currentTweetSubject.next(this.tweet);
    }
    this.http.put<{}>(`${environment.API_URL}/tweets/favorite`, {
      tweet_id: tweet.ID,
      favorite_user_ids: JSON.stringify(favoriteUserIDsArr),
      is_favorite: isFavorite,
    }).subscribe();
  }

  async presentActionSheet(): Promise<void> {
    const actionSheet = await this.actionSheetController.create({
      buttons: [
        {
          text: t('Hide this post'),
          handler: () => {
            if (this.user) {
              const blockedTweetIDsArr = this.user?.BlockedTweetIDs ? JSON.parse(this.user?.BlockedTweetIDs) : [];
              if (!blockedTweetIDsArr.includes(this.tweet?.ID)) {
                blockedTweetIDsArr.push(this.tweet?.ID);
                this.user.BlockedTweetIDs = JSON.stringify(blockedTweetIDsArr);
                this.http.put<{}>(`${environment.API_URL}/users/block/tweet`, {
                  blocked_tweet_ids: JSON.stringify(blockedTweetIDsArr),
                }).subscribe(() => {
                  this.authService.currentUserSubject.next(this.user);
                }, async () => {
                  const alert = await this.alertController.create({
                    message: t('Operation Failed'),
                    buttons: [t('OK')],
                  });
                  await alert.present();
                });
              }
            }
          },
        },
        {
          text: (this.tweet?.User?.Nickname ?? '') + t('Mute Tweet'),
          handler: () => {
            if (this.user) {
              const blockedTweetUserIDsArr = this.user.BlockedTweetUserIDs ? JSON.parse(this.user.BlockedTweetUserIDs) : [];
              if (!blockedTweetUserIDsArr.includes(this.tweet?.User?.ID)) {
                blockedTweetUserIDsArr.push(this.tweet?.User?.ID);
                this.user.BlockedTweetUserIDs = JSON.stringify(blockedTweetUserIDsArr);
                this.http.put<{}>(`${environment.API_URL}/users/block/user`, {
                  blocked_tweet_user_ids: JSON.stringify(blockedTweetUserIDsArr),
                }).subscribe(() => {
                  this.authService.currentUserSubject.next(this.user);
                }, async () => {
                  const alert = await this.alertController.create({
                    message: t('Operation Failed'),
                    buttons: [t('OK')],
                  });
                  await alert.present();
                });
              }
            }
          },
        },
        {
          text: t('Cancel'),
          role: 'cancel',
        },
      ],
    });
    await actionSheet.present();
  }

}
