import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TweetComponent } from './tweet.component';
import { DetailComponent } from './detail/detail.component';
import { TelecomGuard } from 'src/app/guards/telecom.guard';

const routes: Routes = [
  {
    path: '',
    component: TweetComponent,
    canActivate: [TelecomGuard]
  },
  {
    path: 'detail/:id',
    component: DetailComponent,
    canActivate: [TelecomGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TweetRoutingModule { }
