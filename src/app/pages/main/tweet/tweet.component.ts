import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ModalController, AlertController, IonRefresher, IonInfiniteScroll } from '@ionic/angular';

import { AuthService, ConditionService, TweetService, WebsocketService } from 'src/app/services';
import { Location, Tweet, User } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';
import t from 'src/locales';

import { CreateComponent } from './create/create.component';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-tweet',
  templateUrl: './tweet.component.html',
  styleUrls: ['./tweet.component.scss'],
})
export class TweetComponent implements OnInit, OnDestroy {

  @ViewChild(IonRefresher) ionRefresher!: IonRefresher;
  @ViewChild(IonInfiniteScroll) ionInfiniteScroll!: IonInfiniteScroll;

  sight = '';
  userID: number | null = null;
  userNickname: string | null = null;
  avatarURL = 'assets/img/avatar.svg';
  tweets: Tweet[] = [];
  blockedTweetIDs: string | null = null;
  blockedTweetUserIDs: string | null = null;
  pageIndex = 0;
  isAllLoaded = false;
  count = 2;
  locations: Location[] = [];
  selectedLocationID = 0;
  userSubscription: Subscription | null = null;
  tweetSubscription: Subscription | null = null;
  reloadSubscription: Subscription | null = null;
  user: User | null = null;

  constructor(
    private authService: AuthService,
    private tweetService: TweetService,
    public cond: ConditionService,
    private http: HttpClient,
    private modalController: ModalController,
    private alertController: AlertController,
    private ws: WebsocketService
  ) { }

  ngOnInit(): void {
    this.ws.unreadTweets = 0;
    if (this.userSubscription === null) {
      this.userSubscription = this.authService.currentUserSubject.subscribe(user => {
        if (user){
          this.userID = user.ID ?? null;
          this.sight = user.Sight;
          this.userNickname = user.Nickname ?? null;
          this.avatarURL = user.Images[0].Path ?? 'assets/img/avatar.svg';
          this.blockedTweetIDs = user.BlockedTweetIDs ?? null;
          this.blockedTweetUserIDs = user.BlockedTweetUserIDs ?? null;
          this.tweets = [...this.tweets];
          this.user = user;
        }
      });
    }

    if (this.tweetSubscription === null) {
      this.tweetSubscription = this.tweetService.currentTweetSubject.subscribe(tweet => {
        if (tweet) {
          const index = this.tweets.findIndex(item => item.ID === tweet.ID);
          if (index > -1) {
            this.tweets[index].FavoriteUserIDs = tweet.FavoriteUserIDs;
          }
        }
      });
    }

    this.getTweets();
    this.getTodayTweetsCount();
    this.getParentLocations();
  }

  ngOnDestroy(): void {
    this.userSubscription?.unsubscribe();
    this.tweetSubscription?.unsubscribe();
  }

  getNotifyCount(): number {
    return this.ws.unreadNotices;
  }

  doRefresh(): void {
    this.ws.unreadTweets = 0;
    this.pageIndex = 0;
    this.isAllLoaded = false;
    this.getTweetsByRefresh();
  }

  reloadTweets(): void {
    this.pageIndex = 0;
    this.isAllLoaded = false;
    this.tweets = [];
    this.getTweets();
  }

  loadData(event: Event): void {
    this.pageIndex++;
    this.getTweets(event);
  }

  getTweetsByRefresh(): void {
    this.http.get<Tweet[]>(`${environment.API_URL}/tweets?page=0&location=${this.selectedLocationID}`).subscribe(tweets => {
      this.tweets = tweets;
      this.ionRefresher.disabled = true;
      this.ionRefresher.complete();
      setTimeout(() => {
        this.ionRefresher.disabled = false;
      }, 100);
    }, async () => {
      const alert = await this.alertController.create({
        message: t('Operation Failed'),
        buttons: [t('OK')],
      });
      await alert.present();
      this.ionRefresher.disabled = true;
      this.ionRefresher.complete();
      setTimeout(() => {
        this.ionRefresher.disabled = false;
      }, 100);
    });
  }

  getTweets(event: Event | null = null): void {
    this.http.get<Tweet[]>(`${environment.API_URL}/tweets?page=${this.pageIndex}&location=${this.selectedLocationID}`).subscribe(tweets => {
      if (tweets && tweets.length === 0) {
        this.isAllLoaded = true;
      }
      this.tweets = this.tweets.concat(tweets);
      if (event) {
        this.ionInfiniteScroll.complete();
      }else{
        if (this.reloadSubscription === null) {
          this.reloadSubscription = this.authService.reloadSubject.subscribe(res => {
            if (res) {
              this.reloadTweets();
            }
          });
        }
      }
    }, async () => {
      const alert = await this.alertController.create({
        message: t('Operation Failed'),
        buttons: [t('OK')],
      });
      await alert.present();
      if (event) {
        this.ionInfiniteScroll.complete();
      }
    });
  }

  getTodayTweetsCount(): void {
    this.http.get<number>(`${environment.API_URL}/users/tweet/today`).subscribe(count => {
      this.count = count;
    });
  }

  getHeartCount(tweet: Tweet): string {
    const returnNum = tweet.FavoriteUserIDs ? JSON.parse(tweet.FavoriteUserIDs).length + tweet.Bonus : tweet.Bonus;
    if (returnNum < 1){
      return '';
    }else if (returnNum < 10){
      return '1+';
    }else if (returnNum < 20){
      return '10+';
    }else if (returnNum < 30){
      return '20+';
    }else{
      return '30+';
    }
  }

  getExactCount(tweet: Tweet): string {
    const returnNum = tweet.FavoriteUserIDs ? JSON.parse(tweet.FavoriteUserIDs).length + tweet.Bonus : tweet.Bonus;
    if (returnNum > 0) {
      return returnNum.toString();
    }else{
      return '';
    }
  }

  getHeartStatus(tweet: Tweet): boolean {
    return tweet.FavoriteUserIDs ? JSON.parse(tweet.FavoriteUserIDs).includes(this.userID) : 0;
  }

  getLocation(tweet: Tweet): string {
    let locationArr = tweet.User?.LocationIDs?.split(',') ?? [];
    locationArr = locationArr.filter(item => item !== '');
    return this.locations.find(l => l.ID.toString() === locationArr[0])?.Name ?? t('Undefined');
  }

  getParentLocations(): void {
    this.http.get<Location[]>(`${environment.API_URL}/locations?pid=0`).subscribe(locations => {
      this.locations = locations;
    });
  }

  isValidTweet(tweet: Tweet): boolean {
    const blocketTweetIDsArr = this.blockedTweetIDs ? JSON.parse(this.blockedTweetIDs) : [];
    const blockedTweetUserIDsArr = this.blockedTweetUserIDs ? JSON.parse(this.blockedTweetUserIDs) : [];
    if (blocketTweetIDsArr.includes(tweet.ID)) {
      return false;
    }
    if (blockedTweetUserIDsArr.includes(tweet.UserID)) {
      return false;
    }
    return true;
  }

  async onCreate(): Promise<void> {
    const modal = await this.modalController.create({
      component: CreateComponent,
      componentProps: {
        avatarURL: this.avatarURL,
        nickname: this.userNickname,
        role: this.user?.Role
      },
    });
    await modal.present();
    const { data } = await modal.onDidDismiss();
    if (data) {
      this.doRefresh();
      this.getTodayTweetsCount();
    }
  }

  async onDeleteTweet(tweetID: number): Promise<void> {
    const alert = await this.alertController.create({
      message: t('Are you sure to delete?'),
      buttons: [t('Cancel'), {
        text: t('OK'),
        handler: () => {
          this.http.delete<{}>(`${environment.API_URL}/users/tweet/${tweetID}`).subscribe(() => {
            this.doRefresh();
            this.getTodayTweetsCount();
          }, async () => {
            const errAlert = await this.alertController.create({
              message: t('Operation Failed'),
              buttons: [t('OK')],
            });
            await errAlert.present();
          });
        },
      }],
    });
    await alert.present();
  }

  onToggleHeart(tweet: Tweet, index: number): void {
    const favoriteUserIDsArr = tweet.FavoriteUserIDs ? JSON.parse(tweet.FavoriteUserIDs) : [];
    let isFavorite = false;
    if (this.getHeartStatus(tweet)) {
      const favoriteUserIDsArrIndex = favoriteUserIDsArr.findIndex((id: number) => id === this.userID);
      favoriteUserIDsArr.splice(favoriteUserIDsArrIndex, 1);
      isFavorite = false;
    } else {
      favoriteUserIDsArr.push(this.userID);
      isFavorite = true;
    }
    this.tweets[index].FavoriteUserIDs = JSON.stringify(favoriteUserIDsArr);
    this.http.put<{}>(`${environment.API_URL}/tweets/favorite`, {
      tweet_id: tweet.ID,
      favorite_user_ids: JSON.stringify(favoriteUserIDsArr),
      is_favorite: isFavorite,
    }).subscribe();
  }

  onLocationChanged(): void {
    this.getTweetsByRefresh();
    // console.log(this.selectedLocationID);
  }

  gotoLine(): void{
    window.location.href = 'https://lin.ee/6fAJj9Tg';
  }

}
