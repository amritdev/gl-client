import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NgxIonicImageViewerModule } from 'ngx-ionic-image-viewer';
import { PipesModule } from 'src/app/shared';
import { TweetRoutingModule } from './tweet-routing.module';
import { TweetComponent } from './tweet.component';
import { CreateComponent } from './create/create.component';
import { DetailComponent } from './detail/detail.component';

@NgModule({
  declarations: [
    TweetComponent,
    CreateComponent,
    DetailComponent,
  ],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    PipesModule,
    NgxIonicImageViewerModule,
    TweetRoutingModule,
  ],
})
export class TweetModule { }
