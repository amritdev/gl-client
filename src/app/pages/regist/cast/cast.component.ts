import { Component, OnInit } from '@angular/core';

import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-cast',
  templateUrl: './cast.component.html',
  styleUrls: ['./cast.component.scss'],
})
export class CastComponent implements OnInit {

  constructor(
    private nav: NavController
  ) { }

  ngOnInit(): void {
    setTimeout(() => {
      this.nav.navigateRoot('/main');
    }, 3500);
  }

}
