import { Component, OnInit } from '@angular/core';

import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-guest',
  templateUrl: './guest.component.html',
  styleUrls: ['./guest.component.scss'],
})
export class GuestComponent implements OnInit {

  constructor(
    private nav: NavController
  ) { }

  ngOnInit(): void {
    setTimeout(() => {
      this.nav.navigateRoot('/main');
    }, 3500);
  }

}
