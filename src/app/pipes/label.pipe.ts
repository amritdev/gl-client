import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment-timezone';
import t from 'src/locales';

interface UserLabel {
  Nickname: string | null;
  Birthday: string | null;
  BirthdayHidden: boolean;
  Withdrawn?: boolean;
}

@Pipe({
  name: 'label'
})
export class LabelPipe implements PipeTransform {

  transform(user: UserLabel): string {
    if (user.Withdrawn && user.Withdrawn === true){
      return t('Withdrawn');
    } else {
      if (user.Nickname) {
        if (user.Birthday && !user.BirthdayHidden) {
          return `${user.Nickname}  ${this.getAge(user.Birthday)}${t('Age')}`;
        } else {
          return `${user.Nickname}`;
        }
      } else {
        return '';
      }
    }
  }

  getAge(birthday: string | null): number {
    // return birthday ? new Date().getFullYear() - new Date(birthday).getFullYear() : 0;
    if (birthday){
      return moment.tz('Asia/Tokyo').diff(birthday, 'years');
    }else{
      return 0;
    }
  }
}
