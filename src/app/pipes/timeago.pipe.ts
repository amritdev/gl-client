import { Pipe, PipeTransform } from '@angular/core';

import * as moment from 'moment';

@Pipe({
  name: 'timeago'
})
export class TimeagoPipe implements PipeTransform {

  transform(value: string | null): string {
    moment.locale('ja');
    return value ? moment.duration(moment().diff(moment.utc(value))).humanize() + '前' : '';
  }

}
