import { Pipe, PipeTransform } from '@angular/core';

import t from 'src/locales';

@Pipe({
  name: 'translate'
})
export class TranslatePipe implements PipeTransform {

  transform(value: string): string {
    return t(value);
  }

}
