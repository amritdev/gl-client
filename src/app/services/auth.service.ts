import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {
  User,
  UserLoginResponse,
  GiftData,
  UserRegisterRequest,
  Log,
  CastSearchFilter,
  GuestSearchFilter,
  Class
} from 'src/app/interfaces';
import { environment } from 'src/environments/environment';
import { Subject } from 'rxjs';
import * as moment from 'moment-timezone';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public currentUserSubject = new BehaviorSubject<User | null>(null);
  public currentPhoneNumberSubject = new BehaviorSubject<{
    number: string;
    created: boolean;
  } | null>(null);
  public giftSubject = new BehaviorSubject<GiftData | null>(null);
  public registUserSubject = new BehaviorSubject<UserRegisterRequest | null>(null);
  public reloadSubject = new Subject<boolean>();
  public pointSubject = new BehaviorSubject<number>(0);
  public firebasePushToken = new BehaviorSubject<string>('');
  public enabled = true;
  private refreshTokenTimeout = 0;
  private token = '';

  constructor(
    private http: HttpClient
  ) {}

  facebookLogin(provider: string, facebookUserID: string, facebookAccessToken: string, role: string): Observable<UserLoginResponse> {
    const source = localStorage.getItem('source') ?? '';
    const introducerID = localStorage.getItem('introducer_id') ?? '';
    const campaignID = localStorage.getItem('campaign_id') ?? '';
    return this.http.post<UserLoginResponse>(`${environment.API_URL}/users/login`, {
      provider,
      facebook_user_id: facebookUserID,
      facebook_access_token: facebookAccessToken,
      role,
      source,
      introducer_id: introducerID,
      campaign_id: campaignID,
    }).pipe(map(res => {
      localStorage.setItem('token', `Bearer ${res.token}`);
      this.currentUserSubject.next(res.data);
      return res;
    }));
  }

  lineLogin(provider: string, lineCode: string, role: string): Observable<UserLoginResponse> {
    const source = localStorage.getItem('source') ?? '';
    const introducerID = localStorage.getItem('introducer_id') ?? '';
    const campaignID = localStorage.getItem('campaign_id') ?? '';
    return this.http.post<UserLoginResponse>(`${environment.API_URL}/users/login`, {
      provider,
      line_code: lineCode,
      role,
      source,
      introducer_id: introducerID,
      campaign_id: campaignID
    }).pipe(map(res => {
      localStorage.setItem('token', `Bearer ${res.token}`);
      this.currentUserSubject.next(res.data);
      return res;
    }));
  }

  lineLoginWithID(lineID: string): Observable<UserLoginResponse> {
    return this.http.post<UserLoginResponse>(`${environment.API_URL}/users/login`, {
      provider: 'line_id',
      line_id: lineID,
    }).pipe(map(res => {
      localStorage.setItem('token', `Bearer ${res.token}`);
      this.currentUserSubject.next(res.data);
      return res;
    }));
  }

  lineUpgrade(provider: string, lineID: string): Observable<UserLoginResponse> {
    return this.http.post<UserLoginResponse>(`${environment.API_URL}/users/login`, {
      provider,
      line_id: lineID,
    }).pipe(map(res => {
      localStorage.setItem('token', `Bearer ${res.token}`);
      this.currentUserSubject.next(res.data);
      return res;
    }));
  }

  lineConfirm(provider: string, lineID: string): Observable<UserLoginResponse> {
    return this.http.post<UserLoginResponse>(`${environment.API_URL}/users/login`, {
      provider,
      line_id: lineID,
    }).pipe(map(res => {
      localStorage.setItem('token', `Bearer ${res.token}`);
      this.currentUserSubject.next(res.data);
      return res;
    }));
  }

  smsLogin(provider: string, phoneNumber: string, verificationCode: string, role: string, lineID: string): Observable<UserLoginResponse> {
    const source = localStorage.getItem('source') ?? '';
    const introducerID = localStorage.getItem('introducer_id') ?? '';
    const campaignID = localStorage.getItem('campaign_id') ?? '';
    return this.http.post<UserLoginResponse>(`${environment.API_URL}/users/login`, {
      provider,
      phone_number: phoneNumber,
      verification_code: verificationCode,
      line_id: lineID,
      role,
      source,
      introducer_id: introducerID,
      campaign_id: campaignID
    }).pipe(map(res => {
      localStorage.setItem('token', `Bearer ${res.token}`);
      this.currentUserSubject.next(res.data);
      return res;
    }));
  }

  emailRegister(role: string, email: string, password: string): Observable<{}> {
    return this.http.post<{}>(`${environment.API_URL}/users/email/register`, {
      role,
      email,
      password
    }).pipe(map((res) => {
      return res;
    }));
  }

  emailLogin(provider: string, email: string, password: string, role: string): Observable<UserLoginResponse> {
    const source = localStorage.getItem('source') ?? '';
    const introducerID = localStorage.getItem('introducer_id') ?? '';
    const campaignID = localStorage.getItem('campaign_id') ?? '';
    return this.http.post<UserLoginResponse>(`${environment.API_URL}/users/login`, {
      provider,
      email,
      password,
      role,
      source,
      introducer_id: introducerID,
      campaign_id: campaignID
    }).pipe(map((res) => {
      localStorage.setItem('token', `Bearer ${res.token}`);
      this.currentUserSubject.next(res.data);
      return res;
    }));
  }

  logout(): void {
    localStorage.removeItem('token');
  }

  startRefreshTokenTimer(): void {
    // parse json object from base64 encoded jwt token
    const token = localStorage.getItem('token');
    if (token && token !== ''){
      const jwtToken = JSON.parse(atob(token.split('.')[1]));

      // set a timeout to refresh the token a minute before it expires
      const expires = new Date(jwtToken.exp * 1000);
      const timeout = expires.getTime() - Date.now() - (60 * 1000);
      // console.log(timeout);
      this.refreshTokenTimeout = window.setTimeout(() => {
        this.http.get<{
          token: string;
        }>(`${environment.API_URL}/users/refresh_token`,
          { headers: new HttpHeaders({  Authorization: token })}).subscribe(res => {
          // console.log('I got fresh token');
          // console.log(res);
          localStorage.setItem('token', `Bearer ${res.token}`);
          this.startRefreshTokenTimer();
        });
      }, timeout);
    }
  }

  stopRefreshTokenTimer(): void {
    clearTimeout(this.refreshTokenTimeout);
  }

  saveTokenStatus(token: string, sourceType: string, statusCode: number, type: string): void {
    const jwtToken = JSON.parse(atob(token.split('.')[1]));

    // set a timeout to refresh the token a minute before it expires
    const expires = new Date(jwtToken.exp * 1000);
    const userID = jwtToken.ID;
    const navigator = window.navigator;

    // console.log(expires, userID);

    this.http.post<{
      success: boolean;
      data: Log;
    }>(`${environment.API_URL}/logs`, {
      user_id: userID,
      status_code: statusCode,
      source_type: sourceType,
      expire_time: moment.tz(expires.toISOString(), 'Asia/Tokyo').format('YYYY-MM-DD HH:mm'),
      agent: navigator.userAgent,
      platform: navigator.platform,
      token,
      type
    }).subscribe(res => {
      // console.log(res);
    });
  }

  setToken(token: string): void {
    this.token = token;
  }

  getToken(): string {
    return this.token;
  }

  updatePointVal(): void{
    this.http.get<number>(`${environment.API_URL}/users/point`,
      { headers: new HttpHeaders({  Authorization: this.token })}).subscribe(res => {
      this.pointSubject.next(res);
    });
  }

  checkUser(connector: User, role: string, locationID: number, blockIDs: number[], guestLevels: Class[]): boolean {
    // console.log(connector);

    // block check
    if (blockIDs.includes(connector.ID)){
      return false;
    }

    if (role === 'guest' && connector.Role === 0){
      // image check
      if (connector.Images.some(item => item.Path.includes('/assets/img/'))){
        return false;
      }

      const filterCondStr: string | null = localStorage.getItem('castFilterCond');
      if (filterCondStr === null) {
        if (locationID === 0) {
          return true;
        }else {
          if (connector.LocationIDs !== null) {
            const locationIDs = connector.LocationIDs.split(',').filter(item => item !== '').map(item => {
              return parseInt(item, 10);
            });
            return locationIDs.includes(locationID);
          }else{
            return false;
          }
        }
      }else{
        const castFilter = JSON.parse(filterCondStr) as CastSearchFilter;

        // location check
        if (castFilter.Location > 0) {
          if (connector.LocationIDs !== null) {
            const locationIDs = connector.LocationIDs.split(',').filter(item => item !== '').map(item => {
              return parseInt(item, 10);
            });
            if (!locationIDs.includes(locationID)) {
              return false;
            }
          }else{
            return false;
          }
        }

        // class check
        if (castFilter.Class.length > 0) {
          if (connector.ClassID && connector.ClassID > 0){
            if (!castFilter.Class.includes(connector.ClassID)) {
              return false;
            }
          }else{
            return false;
          }
        }

        // point check
        if (connector.PointHalf > castFilter.PointMax || connector.PointHalf < castFilter.PointMin) {
          return false;
        }

        // tag check
        if (castFilter.Tag.length > 0) {
          if (connector.Tags !== null){
            const connTags = connector.Tags.split(',').filter(item => item !== '').map(item => {
              return parseInt(item, 10);
            });
            if (!castFilter.Tag.every(tagItem => connTags.includes(tagItem))){
              return false;
            }
          }else{
            return false;
          }
        }

        // schedule check
        if (castFilter.Schedule.length > 0) {
          if (connector.Schedules !== null) {
            const scheduleDate = connector.Schedules.map(item => item.Date);
            if (!castFilter.Schedule.some(item => scheduleDate.includes(item))){
              return false;
            }
          }else{
            return false;
          }
        }

        // birthday check
        if (castFilter.Birthday){
          const curMonth = moment.tz('Asia/Tokyo').month();
          if (connector.Birthday !== null){
            const connBirth = moment(connector.Birthday).tz('Asia/Tokyo').month();
            // console.log(connBirth);
            if (connBirth !== curMonth){
              return false;
            }
          }else{
            return false;
          }
        }

        // recently check
        if (castFilter.Recently) {
          const threshDate = moment.tz('Asia/Tokyo').subtract(30, 'days');
          if (connector.CastStart !== null){
            if (connector.CastStart <= threshDate.format('YYYY-MM-DD HH:mm:ss')){
              return false;
            }
          }else{
            return false;
          }
        }

        // check today free
        if (castFilter.TodayFree) {
          // console.log(connector);
          if (connector.Schedules && connector.Schedules.length > 0) {
            const todayDate = moment.tz('Asia/Tokyo');
            if (todayDate.hour() < 5){
              todayDate.subtract(1, 'days');
            }
            const todayIndex = connector.Schedules.findIndex(item => item.Date === todayDate.format('YYYY-MM-DD') && item.Enabled);
            if (todayIndex === -1){
              return false;
            }
          }else{
            return false;
          }
        }
      }

      return true;
    }

    if (role === 'cast' && connector.Role === 1) {
      const filterCondStr: string | null = localStorage.getItem('guestFilterCond');
      if (filterCondStr === null) {
        if (locationID === 0) {
          return true;
        }else {
          if (connector.LocationIDs !== null) {
            const locationIDs = connector.LocationIDs.split(',').filter(item => item !== '').map(item => {
              return parseInt(item, 10);
            });
            return locationIDs.includes(locationID);
          }else{
            return false;
          }
        }
      }else{
        const guestFilter = JSON.parse(filterCondStr) as GuestSearchFilter;

        // location check
        if (guestFilter.Location > 0) {
          if (connector.LocationIDs !== null) {
            const locationIDs = connector.LocationIDs.split(',').filter(item => item !== '').map(item => {
              return parseInt(item, 10);
            });
            if (!locationIDs.includes(locationID)) {
              return false;
            }
          }else{
            return false;
          }
        }

        // recently check
        if (guestFilter.Recently) {
          const threshDate = moment.tz('Asia/Tokyo').subtract(30, 'days');
          if (connector.GuestStart !== null){
            if (connector.GuestStart <= threshDate.format('YYYY-MM-DD HH:mm:ss')){
              return false;
            }
          }else{
            return false;
          }
        }

        // level check
        if (guestFilter.Level.length > 0) {
          const levelID = this.getLevelID(connector.PointUsed, guestLevels);
          if (!guestFilter.Level.includes(levelID)){
            return false;
          }
        }
      }
      return true;
    }
    return false;
  }

  getLevelID(usedPoint: number, guestLevels: Class[]): number {
    if (guestLevels.length > 0) {
      for (const [_, levelItem] of guestLevels.entries()) {
        if (usedPoint < levelItem.Point) {
          return levelItem.ID;
        } else {
          continue;
        }
      }
    }
    return 0;
  }
}
