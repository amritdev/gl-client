import { TestBed } from '@angular/core/testing';

import { CastFilterService } from './cast-filter.service';

describe('CastFilterService', () => {
  let service: CastFilterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CastFilterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
