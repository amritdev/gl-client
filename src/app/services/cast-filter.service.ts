import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs';
import { CastSearchFilter, Detail } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CastFilterService {

  public castFilterSubject = new BehaviorSubject<CastSearchFilter | null>(null);
  public isCleared = false;
  public isPrevExist = false;

  private parentTags: Detail[] = [];
  private childTags: { [key: number]: Detail[] } = {};
  public details: Detail[] = [];

  constructor(
    private http: HttpClient
  ) {
    this.getTags();
  }

  initialize(): void {
    const filterCondStr: string | null = localStorage.getItem('castFilterCond');
    let filter: CastSearchFilter = {
      Location: 0,
      Class: [],
      Tag: [],
      Schedule: [],
      PointMin: 0,
      PointMax: 20000,
      Recently: false,
      Birthday: false,
      TodayFree: false,
      SearchStr: []
    };
    if (filterCondStr) {
      this.isPrevExist = true;
      filter = JSON.parse(filterCondStr);
      if (filter.SearchStr === undefined) {
        filter.SearchStr = [];
      }
    }

    if (filter.PointMax > 20000) {
      filter.PointMax = 20000;
    }
    this.castFilterSubject.next(filter);
  }

  reset(): CastSearchFilter {
    const filter: CastSearchFilter = {
      Location: 0,
      Class: [],
      Tag: [],
      Schedule: [],
      PointMin: 0,
      PointMax: 20000,
      Recently: false,
      Birthday: false,
      TodayFree: false,
      SearchStr: []
    };
    this.castFilterSubject.next(filter);
    return filter;
  }

  getTags(): void {
    const token = localStorage.getItem('token');
    if (token) {
      this.http.get<Detail[]>(`${environment.API_URL}/admin/tags/parent`, { headers: new HttpHeaders({  Authorization: token })
      }).subscribe((pdata) => {
        this.parentTags = pdata;
        let tagItems = 0;
        for (const parentItem of this.parentTags) {
          this.http.get<Detail[]>(`${environment.API_URL}/admin/tags/children/${parentItem.ID}`,
          { headers: new HttpHeaders({  Authorization: token })
        }).subscribe((cdata) => {
            tagItems++;
            this.childTags[parentItem.ID] = cdata;
            this.details = this.details.concat(... cdata);
            if (tagItems === this.parentTags.length){
              this.initialize();
            }
          });
        }
      }, (err: HttpErrorResponse) => {
        console.log(err);
      });
    }
  }
}
