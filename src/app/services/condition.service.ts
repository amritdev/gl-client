import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AlertController } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';
import { AuthService } from './auth.service';
import { CallCondition, Location, User } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';
import t from 'src/locales';

@Injectable({
  providedIn: 'root'
})
export class ConditionService {

  callConditionSubject: BehaviorSubject<CallCondition | null>;
  locationIDs: string[] = [];
  pLocations: Location[] = [];
  userLocation = 0;
  cardReady = false;
  userPoint = 0;

  // user information
  locationIDStr = '';

  constructor(
    private auth: AuthService,
    private http: HttpClient,
    private alert: AlertController
  ) {
    this.callConditionSubject = new BehaviorSubject<CallCondition | null>(null);
    this.auth.currentUserSubject.subscribe((user) => {
      if (user && user.LocationIDs) {
        this.cardReady = user.TelecomCredit;
        this.userPoint = user.Point;

        if (this.locationIDStr !== user.LocationIDs){
          this.locationIDStr = user.LocationIDs;
          const locationIDs = user.LocationIDs ? user.LocationIDs : '';
          this.locationIDs = locationIDs.split(',');
          this.locationIDs.pop();
          this.locationIDs.shift();
          this.getParentLocations();
        }
      }
    });

  }

  initializeCondition(): void {
    const todayMoodStr = localStorage.getItem('today-situation');
    let todayMood: number[];
    if (todayMoodStr) {
      todayMood = JSON.parse(todayMoodStr);
    } else {
      todayMood = [];
    }
    this.callConditionSubject.next({
      Call: {
        ID: 0,
        Joined: [],
        Applied: [],
        User: null,
        UserID: 0,
        Status: 'condition',
        Place: null,
        Reservation: null,
        // JoinStatus: null,
        IsPrivate: false,
        BigLocationID: this.userLocation,
        BigLocation: null,
        MeetTime: t('After 60 minutes'),
        MeetTimeISO: '',
        TimeOther: false,
        LocationID: 0,
        Location: null,
        OtherLocation: '',
        Person: 2,
        Period: 2,
        ClassID: 0,
        Class: null,
        Classes: [],
        SituationIDArray: todayMood,
        GiftPoint: 0,
        DesiredArray: [],
        MixStr: '',
        MixClassIDs: '',
        SituationIDs: '',
        SuggesterID: 0,
        NotifyAdmin: '',
        IsAnonymous: false,
        CastPoint: 0,
        Desired: '',
        KeepApplying: false,
        IsDeleted: false,
      },
      Status: 'initial'
    });
  }

  getParentLocations(): void {
    const token = localStorage.getItem('token');
    if (token) {
      const headers = new HttpHeaders().set('Authorization', token);
      this.http.get<Location[]>(`${environment.API_URL}/locations?pid=0`, { headers }).subscribe(locations => {
        let selected = false;
        this.pLocations = locations;
        for (const locationItem of this.locationIDs) {
          const foundIndex = locations.findIndex(item => item.ID.toString() === locationItem);
          if (foundIndex >= 0) {
            this.userLocation = parseInt(locationItem, 10);
            selected = true;
            break;
          }
        }
        if (!selected) {
          this.userLocation = locations[0].ID;
        }
        this.initializeCondition();
      }, async () => {
        const alert = await this.alert.create({
          message: t('Operation Failed'),
          buttons: [t('OK')],
        });
        await alert.present();
      });
    }
  }

  getLocationName(user: User): string {
    if (user && user.LocationIDs) {

      const userLocationIDs = user.LocationIDs.split(',').filter(item => {
        return item !== '';
      }).map(item => {
        return parseInt(item, 10);
      });

      if (userLocationIDs.length > 0 && this.pLocations.length > 0) {
        const pLocationIndex = this.pLocations.findIndex(item => item.ID === userLocationIDs[0] );

        if (pLocationIndex > -1) {
          return this.pLocations[pLocationIndex].Name;
        }
      }
    }

    return '';
  }
}

