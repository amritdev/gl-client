import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { GeoLoc } from '../interfaces';

@Injectable({
  providedIn: 'root'
})
export class GeoLocationService {

  constructor(
    private http: HttpClient
  ) { }

  public getPosition(): Promise<GeoLoc> {
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(resp => {
        resolve({lng: resp.coords.longitude, lat: resp.coords.latitude});
      },
      err => {
        reject(err);
      });
    });
  }

  public savePosition(mode: string, callID = 0): void {
    const token = localStorage.getItem('token');
    if (token && token !== ''){
      this.getPosition().then(pos => {
        this.http.post(`${environment.API_URL}/gps`, {
          mode,
          call_id: callID,
          lng: pos.lng.toString(),
          lat: pos.lat.toString(),
        }, { headers: new HttpHeaders({  Authorization: token })}).subscribe();
      }, err => {
        console.log(err);
      });
    }
  }

}
