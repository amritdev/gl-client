import { TestBed } from '@angular/core/testing';

import { GuestFilterService } from './guest-filter.service';

describe('GuestFilterService', () => {
  let service: GuestFilterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GuestFilterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
