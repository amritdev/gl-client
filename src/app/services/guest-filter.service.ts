import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs';

import { GuestSearchFilter } from 'src/app/interfaces';

@Injectable({
  providedIn: 'root'
})
export class GuestFilterService {

  public guestFilterSubject = new BehaviorSubject<GuestSearchFilter | null>(null);
  public isCleared = false;
  public isPrevExist = false;

  constructor() {
    this.initialize();
  }

  initialize(): void {
    const filterCondStr: string | null = localStorage.getItem('guestFilterCond');
    let filter: GuestSearchFilter = {
      Location: 0,
      Level: [],
      Recently: false,
      LevelStr: '',
      SearchStr: []
    };
    if (filterCondStr) {
      this.isPrevExist = true;
      filter = JSON.parse(filterCondStr);
      if (filter.SearchStr === undefined) {
        filter.SearchStr = [];
      }
    }
    this.guestFilterSubject.next(filter);
  }

  reset(): GuestSearchFilter {
    const filter: GuestSearchFilter = {
      Location: 0,
      Level: [],
      Recently: false,
      LevelStr: '',
      SearchStr: []
    };
    this.guestFilterSubject.next(filter);
    return filter;
  }
}
