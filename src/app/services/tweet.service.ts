import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Tweet } from 'src/app/interfaces';

@Injectable({
  providedIn: 'root'
})
export class TweetService {

  public currentTweetSubject = new BehaviorSubject<Tweet | null>(null);

  constructor() { }
}
