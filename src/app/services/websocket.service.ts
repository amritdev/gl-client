import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import ReconnectingWebSocket from 'reconnecting-websocket';
import { WS, Notice, Room, ResRoom, RemoveRoom, Call, CallEvent, Join, JoinEvent, User, Notification } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';
import { AuthService } from './auth.service';
import { AlertController } from '@ionic/angular';
import t from 'src/locales';

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {

  currentWebSocketSubject = new BehaviorSubject<WS | null>(null);
  unreadNotices = 0;
  unreadMessages = 0;
  unreadTweets = 0;
  adminNotices = 0;
  notifyNotices = 0;
  noticeSubject = new Subject<Notice>();
  notificationSubject = new Subject<Notification>();
  roomSubject = new Subject<ResRoom>();
  roomRemoveSubject = new Subject<RemoveRoom>();
  messageSubject = new Subject<WS>();
  callSubject = new Subject<CallEvent>();
  joinSubject = new Subject<JoinEvent>();
  suggestSubject = new Subject<CallEvent>();
  reviewSubject = new BehaviorSubject<number>(0);
  connectUserSubject = new Subject<User>();
  reloadSubject = new Subject<string>();

  ws: ReconnectingWebSocket | null = null;

  constructor(
    private auth: AuthService,
    private alert: AlertController
  ) { }

  init(userID: number): void {
    if (this.ws) {
      this.ws.close();
    }
    this.ws = new ReconnectingWebSocket(`${environment.WEBSOCKET_URL}/${userID}`);
    this.ws.onopen = () => {
      this.currentWebSocketSubject.subscribe(data => {
        if (data) {
          this.ws?.send(JSON.stringify(data));
        }
      });
    };
    this.ws.onmessage = (event) => {
      if (event.data) {
        const delimiter = environment.DELIMITER;
        const recMessages: string[] = event.data.split(delimiter);

        recMessages.forEach(messageItem => {
          try{
            const tempData = JSON.parse(messageItem);
            switch (tempData.Type) {
              case 'NOTICE':
                if (tempData.Data) {
                  const newNotice: Notice = tempData.Data as Notice;
                  if (newNotice.Type === 'admin') {
                    this.adminNotices++;
                  }
                  this.unreadNotices++;
                  this.noticeSubject.next(newNotice);
                }
                break;
              case 'NOTIFICATION':
                if (tempData.Data) {
                  const newNotif: Notification = tempData.Data as Notification;
                  this.notifyNotices++;
                  this.unreadNotices++;
                  this.notificationSubject.next(newNotif);
                }
                break;
              case 'MESSAGE':
                if (tempData.Data) {
                  // console.log(tempData.Data);
                  this.unreadMessages++;
                  this.messageSubject.next(tempData);
                }
                break;
              case 'ROOM':
                if (tempData.Data) {
                  const newRoom: Room = tempData.Data as Room;
                  const roomEventStr: string = tempData.Event ?? '';
                  if (roomEventStr === 'create') {
                    this.roomSubject.next({
                      Room: newRoom,
                      Unread: 0
                    });
                  } else if (roomEventStr === 'update') {
                    this.roomRemoveSubject.next({
                      Room: newRoom,
                      UserID: 0
                    });
                  } else {
                    this.roomRemoveSubject.next({
                      Room: newRoom,
                      UserID: parseInt(roomEventStr, 10)
                    });
                  }
                }
                break;
              case 'CALL':
                const tempCall: Call = tempData.Data as Call;
                const callEventStr: string = tempData.Event ?? '';
                this.callSubject.next({
                  call: tempCall,
                  event: callEventStr
                });
                break;
              case 'START':
              case 'END':
                // console.log(tempData);
                const tempJoin: Join = tempData.Data as Join;
                const joinEventStr: string = tempData.Event ?? '';
                this.joinSubject.next({
                  join: tempJoin,
                  type: tempData.Type.toLowerCase(),
                  event: joinEventStr,
                });
                break;
              case 'SUGGEST':
              case 'REJECT':
              case 'CONFIRM':
                // console.log(tempData);
                const curCall: Call = tempData.Data as Call;
                this.suggestSubject.next({
                  call: curCall,
                  event: tempData.Type.toLowerCase(),
                });
                break;
              case 'REVIEW':
                const curRoom: Room = tempData.Data as Room;
                this.reviewSubject.next(curRoom.ID);
                break;
              case 'REVIEW_ARRIVED':
                const guestName: string = tempData.Data as string;
                this.presentAlert(t('Review arrived from %s').replace('%s', guestName));
                break;
              case 'USER':
                const curUser: User = tempData.Data as User;
                this.auth.currentUserSubject.next(curUser);
                break;
              case 'TWEET':
                this.unreadTweets++;
                break;
              case 'CONNECT':
                const connectedUser: User = tempData.Data as User;
                this.connectUserSubject.next(connectedUser);
                break;
              case 'RELOAD':
                const eventString: string = tempData.Data as string;
                this.reloadSubject.next(eventString);
                break;
              case 'BLOCK':
                const updatedUser: User = tempData.Data as User;
                this.auth.currentUserSubject.next(updatedUser);
                this.auth.reloadSubject.next(true);
            }
          }catch (ex){
            console.log('WEBSOCKET JSON PARSE ERROR');
            console.log(event.data);
          }
        });
      }
    };
  }

  send(data: WS): void {
    this.currentWebSocketSubject.next(data);
  }

  disconnect(): void {
    if (this.ws){
      this.ws.close();
    }
  }

  async presentAlert(msg: string): Promise<void> {
    const alert = await this.alert.create({
      message: msg,
      buttons: [t('OK')],
    });
    await alert.present();
  }
}
