import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { CastCardComponent } from 'src/app/components/cast-card/cast-card.component';
import { MiniCastCardComponent } from 'src/app/components/mini-cast-card/mini-cast-card.component';
import { GuestCardComponent } from 'src/app/components/guest-card/guest-card.component';
import { MiniGuestCardComponent } from 'src/app/components/mini-guest-card/mini-guest-card.component';
import { ReviewComponent } from 'src/app/components/review/review.component';
import { InViewportModule } from 'ng-in-viewport';
import { PipesModule } from './pipes.module';

@NgModule({
  declarations: [
    CastCardComponent,
    MiniCastCardComponent,
    GuestCardComponent,
    MiniGuestCardComponent,
    ReviewComponent
  ],
  imports: [
    CommonModule,
    PipesModule,
    FormsModule,
    IonicModule,
    InViewportModule
  ],
  exports: [
    CastCardComponent,
    MiniCastCardComponent,
    GuestCardComponent,
    MiniGuestCardComponent,
    ReviewComponent
  ]
})
export class ComponentsModule { }
