import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LimitDirective, IonClickDirective } from 'src/app/directives';

@NgModule({
  declarations: [
    LimitDirective,
    IonClickDirective
  ],
  imports: [CommonModule],
  exports: [
    LimitDirective,
    IonClickDirective
  ]
})
export class DirectivesModule { }
