import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslatePipe, TimeagoPipe, LabelPipe } from 'src/app/pipes';

@NgModule({
  declarations: [
    TranslatePipe,
    TimeagoPipe,
    LabelPipe,
  ],
  imports: [CommonModule],
  exports: [
    TranslatePipe,
    TimeagoPipe,
    LabelPipe,
  ]
})
export class PipesModule { }
