export const environment = {
  production: true,
  SITE_URL: 'https://glass.dating/s',
  API_URL: 'https://glass.dating/api',
  WEBSOCKET_URL: 'wss://glass.dating/ws',
  FACEBOOK_APP_ID: '284601446141179',
  LINE_CHANNEL_ID: '1654883158',
  LINE_CHANNEL_SECRET: '509aec4d6b5f68dacff3fc76bb0afa1f',
  TELECOM_CLIENT_IP: '54593',
  GTM_ID: 'GTM-WJVN5FL',
  EMAIL_ROUTE: 'email-r8guvfpyzvxdhefj',
  CAST_PER_PAGE: 20,
  START_AGE: 18,
  DEEPLINK_SCHEME: 'glass',
  DELIMITER: '3yz2@%PtaO'
};
