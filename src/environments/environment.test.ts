export const environment = {
  production: true,
  SITE_URL: 'https://test.glass.dating/s',
  API_URL: 'https://test.glass.dating/api',
  WEBSOCKET_URL: 'wss://test.glass.dating/ws',
  FACEBOOK_APP_ID: '284601446141179',
  LINE_CHANNEL_ID: '1655685977',
  LINE_CHANNEL_SECRET: 'b97c0423c4d17ccee741b17777e51f1f',
  TELECOM_CLIENT_IP: '00491',
  GTM_ID: 'GTM-WJVN5FL',
  EMAIL_ROUTE: 'email-r8guvfpyzvxdhefj',
  CAST_PER_PAGE: 20,
  START_AGE: 18,
  DEEPLINK_SCHEME: 'glass',
  DELIMITER: '3yz2@%PtaO'
};
