// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  SITE_URL: 'http://192.168.0.100:8100',
  API_URL: 'http://192.168.0.100:8080/api',
  WEBSOCKET_URL: 'ws://192.168.0.100:8080/ws',
  FACEBOOK_APP_ID: '284601446141179',
  LINE_CHANNEL_ID: '1655685977',
  LINE_CHANNEL_SECRET: 'b97c0423c4d17ccee741b17777e51f1f',
  TELECOM_CLIENT_IP: '',
  GTM_ID: 'GTM-WJVN5FL',
  EMAIL_ROUTE: 'email',
  CAST_PER_PAGE: 20,
  START_AGE: 18,
  DEEPLINK_SCHEME: 'glass',
  DELIMITER: '3yz2@%PtaO'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
