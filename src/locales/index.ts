import i18n from './ja';

export default (origin: string) => {
  if (origin) {
    if (i18n[origin]) {
      return i18n[origin];
    } else {
      return origin;
    }
  } else {
    return '';
  }
};
