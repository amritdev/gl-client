/**
 * Prevents Angular change detection from
 * running with certain Web Component callbacks
 */
interface Window {
  __Zone_disable_customElements: unknown;
}
(window as Window).__Zone_disable_customElements = true;
